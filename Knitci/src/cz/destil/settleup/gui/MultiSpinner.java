package cz.destil.settleup.gui;

import java.util.ArrayList;
import java.util.List;

import com.knitci.pocket.patterns.PatternSearchActivity.EFilter;

import android.app.AlertDialog;
import android.app.SearchManager.OnCancelListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import com.knitci.pocket.R;
import android.widget.Spinner;

public class MultiSpinner extends Spinner implements
        OnMultiChoiceClickListener, OnCancelListener, android.content.DialogInterface.OnCancelListener {

    private List<String> items;
    private boolean[] selected;
    private String defaultText;
    private MultiSpinnerListener listener;
    

    public MultiSpinner(Context context) {
        super(context);
    }

    public MultiSpinner(Context arg0, AttributeSet arg1) {
        super(arg0, arg1);
    }

    public MultiSpinner(Context arg0, AttributeSet arg1, int arg2) {
        super(arg0, arg1, arg2);
    }

    @Override
    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
        if (isChecked)
            selected[which] = true;
        else
            selected[which] = false;
    }

    public void onCancel(DialogInterface dialog) {
    	ArrayList<String> selectedStrings = new ArrayList<String>();
        // refresh text on spinner
        StringBuffer spinnerBuffer = new StringBuffer();
        boolean someUnselected = false;
        boolean allUnselected = true;
        for (int i = 0; i < items.size(); i++) {
            if (selected[i] == true) {
            	selectedStrings.add(items.get(i));
                spinnerBuffer.append(items.get(i));
                spinnerBuffer.append(", ");
                allUnselected = false;
            } else {
                someUnselected = true;
            }
        }
        String spinnerText;
        if(allUnselected) {
        	spinnerText = defaultText;
        }
        else if (someUnselected) {
            spinnerText = spinnerBuffer.toString();
            if (spinnerText.length() > 2)
                spinnerText = spinnerText.substring(0, spinnerText.length() - 2);
        } else {
            spinnerText = defaultText;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
        		R.layout.simple_spinner_item_white,
                new String[] { spinnerText });
        setAdapter(adapter);
        listener.onItemsSelected(selectedStrings, (EFilter) this.getTag());
        
    }

    @Override
    public boolean performClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMultiChoiceItems(
                items.toArray(new CharSequence[items.size()]), selected, this);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.setOnCancelListener(this);
        builder.show();
        return true;
    }

    public void setItems(List<String> items, String allText,
            MultiSpinnerListener listener) {
        this.items = items;
        this.defaultText = allText;
        this.listener = listener;

        // all unselected selected by default
        selected = new boolean[items.size()];
        for (int i = 0; i < selected.length; i++)
            selected[i] = false;

        // all text on the spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                R.layout.simple_spinner_item_white, new String[] { allText });
        setAdapter(adapter);
    }

    public interface MultiSpinnerListener {
        public void onItemsSelected(boolean[] selected);
        public void onItemsSelected(ArrayList<String> selected);
		public void onItemsSelected(ArrayList<String> selected, EFilter filter);
    }

	@Override
	public void onCancel() {
		ArrayList<String> selectedStrings = new ArrayList<String>();
        // refresh text on spinner
        StringBuffer spinnerBuffer = new StringBuffer();
        boolean someUnselected = false;
        boolean allUnselected = true;
        for (int i = 0; i < items.size(); i++) {
            if (selected[i] == true) {
            	selectedStrings.add(items.get(i));
                spinnerBuffer.append(items.get(i));
                spinnerBuffer.append(", ");
                allUnselected = false;
            } else {
                someUnselected = true;
            }
        }
        String spinnerText;
        if(allUnselected) {
        	spinnerText = defaultText;
        }
        else if (someUnselected) {
            spinnerText = spinnerBuffer.toString();
            if (spinnerText.length() > 2)
                spinnerText = spinnerText.substring(0, spinnerText.length() - 2);
        } else {
            spinnerText = defaultText;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                R.layout.simple_spinner_item_white,
                new String[] { spinnerText });
        setAdapter(adapter);
        listener.onItemsSelected(selectedStrings, (EFilter) this.getTag());
		
	}
	
	public void reset() {
        for (int i = 0; i < selected.length; i++)
            selected[i] = false;
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                R.layout.simple_spinner_item_white,
                new String[] { defaultText });
        setAdapter(adapter);
	}
}