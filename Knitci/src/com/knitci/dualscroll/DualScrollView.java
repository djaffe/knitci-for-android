package com.knitci.dualscroll;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.FrameLayout.LayoutParams;

import com.knitci.dualscroll.internal.DualInternalScrollView;
import com.knitci.pocket.R;

public class DualScrollView extends LinearLayout implements OnGestureListener  {

	private DualInternalScrollView sv;
	private HorizontalScrollView hsv;
	
	private ScrollView svXAxis;
	private HorizontalScrollView svYAxis;
	
	private float mx, my;
    private float curX, curY;

    private GestureDetector gestureDetector; 

	public DualScrollView(Context context) {
		this(context, null);
	}

	public DualScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mInflater.inflate(R.layout.dualscrollview, this);

		ArrayList<String> top = new ArrayList<String>();
		for(int i = 0; i < 20; i++) {
			top.add(String.valueOf(i));
		}
		populateTopHeader(top);
		
		ArrayList<String> left = new ArrayList<String>();
		for(int i = 0; i < 25; i++) {
			left.add(String.valueOf(i));
		}
		populateLeftHeader(left);
		
		ArrayList<ArrayList<String>> data = new ArrayList<ArrayList<String>>();
		for(int i = 0; i < 25; i++) {
			ArrayList<String> d = new ArrayList<String>();
			for(int j = 0; j < 25; j++) {
				d.add("0");
			}
			data.add(d);
		}
		
		gestureDetector = new GestureDetector(context, this);
		
		sv = (DualInternalScrollView) findViewById(R.id.dualscrollview_content_sv);
		hsv = (HorizontalScrollView) findViewById(R.id.dualscrollview_content_hsv);
		
		populateTable(data);
	}
	
	
	public void populateTable(ArrayList<ArrayList<String>> matrix) {
		DualTableLayout table = (DualTableLayout) findViewById(R.id.dualscrollview_table);
		table.setBackgroundColor(Color.GREEN);
		
		for(int i = 0; i < matrix.size(); i++) {
			TableRow row = new TableRow(getContext());
			TableLayout.LayoutParams params = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);
			row.setLayoutParams(params);
			
			for(int j = 0; j < matrix.get(i).size(); j++) {
				TextView tv = createTextView(matrix.get(i).get(j));
				tv.setBackgroundColor(Color.RED);
				row.addView(tv);
			}
			
			table.addView(row);
		}
	}
	
	public void populateTopHeader(ArrayList<String> values) {
		LinearLayout container = (LinearLayout) findViewById(R.id.dualscrollview_header_top_container);
		container.addView(createTextView("    ."));
		for(int i = 0; i < values.size(); i++) {
			container.addView(createTextView(values.get(i)));
		}
	}
	
	public void populateLeftHeader(ArrayList<String> values) {
		LinearLayout container = (LinearLayout) findViewById(R.id.dualscrollview_header_left_container);
		for(int i = 0; i < values.size(); i++) {
			container.addView(createTextView(values.get(i)));
		}
	}
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		// TODO Auto-generated method stub
		return gestureDetector.onTouchEvent(ev);
	}
//
//	@Override
//    public boolean onInterceptTouchEvent(MotionEvent event) {
//			
//        int action = event.getAction();
//        if (action == MotionEvent.ACTION_DOWN) {
//            lastMotionX = (int) event.getX();
//            lastMotionY = (int) event.getY();
//        }
//// 
////        final int scaledTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();
////        final int x = (int) event.getX();
////        final int y = (int) event.getY();
////        final int diffX = Math.abs(x - lastMotionX);
////        final int diffY = Math.abs(y - lastMotionY);
////        boolean isSwipingSideways = diffX > scaledTouchSlop && diffX > diffY;
//// 
////        return true;
//        return gestureDetector.onTouchEvent(event);
//    }
	
	@Override
    public boolean onTouchEvent(MotionEvent event) {
        float curX, curY;
        
        Log.d("DualScrolLView", "touch event");

        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
                mx = event.getX();
                my = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                curX = event.getX();
                curY = event.getY();
                sv.scrollBy((int) (mx - curX), (int) (my - curY));
                hsv.scrollBy((int) (mx - curX), (int) (my - curY));
                mx = curX;
                my = curY;
                break;
            case MotionEvent.ACTION_UP:
                curX = event.getX();
                curY = event.getY();
                sv.scrollBy((int) (mx - curX), (int) (my - curY));
                hsv.scrollBy((int) (mx - curX), (int) (my - curY));
                break;
        }

        return true;
    }

//	@Override
//	public boolean onDown(MotionEvent e) {
//		Log.d("DualScrolLView", "On Down!");
//		return false;
//	}
//
//	@Override
//	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
//			float velocityY) {
//		Log.d("DualScrolLView", "Fling!");
//		sv.fling((int)-velocityY);
//		hsv.fling((int)-velocityX);
//		return true;
//	}
//
////	@Override
////	public void onLongPress(MotionEvent e) {
////		Log.d("DualScrolLView", "Long press!");
////		
////	}
//
//	@Override
//	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
//			float distanceY) {
//		Log.d("DualScrollView", "Scrolling!");
//		sv.scrollBy((int)distanceX, (int)distanceY);
//		hsv.scrollBy((int)distanceX, (int)distanceY);
//		return true;
//	}
//
////	@Override
////	public void onShowPress(MotionEvent e) {
////		Log.d("DualScrolLView", "Show Press!");
////		
////	}
//
//	@Override
//	public boolean onSingleTapUp(MotionEvent e) {
//		Log.d("DualScrolLView", "Single Tap Up!");
//		return false;
//	}
//
////	@Override
////	public void onClick(View arg0) {
////		Log.d("DualScrollView", "On Click!");
////		TextView tv = (TextView) arg0;
////		Toast toast = Toast.makeText(getContext(), tv.getText(), Toast.LENGTH_SHORT);
////		toast.show();
////		
////	}
//
////	@Override
////	public boolean onScroll(MotionEvent arg0, MotionEvent event, float arg2,
////			float arg3) {
////		float curX, curY;
////
////		switch (event.getAction()) {
////
////		case MotionEvent.ACTION_DOWN:
////			mx = event.getX();
////			my = event.getY();
////			break;
////		case MotionEvent.ACTION_MOVE:
////			curX = event.getX();
////			curY = event.getY();
////			sv.scrollBy((int) (mx - curX), (int) (my - curY));
////			hsv.scrollBy((int) (mx - curX), (int) (my - curY));
////			mx = curX;
////			my = curY;
////			break;
////		case MotionEvent.ACTION_UP:
////			curX = event.getX();
////			curY = event.getY();
////			sv.scrollBy((int) (mx - curX), (int) (my - curY));
////			hsv.scrollBy((int) (mx - curX), (int) (my - curY));
////			break;
////
////		}
////
////		return true;
////	}
//
	private TextView createTextView(String disp) {
		TextView tv = new TextView(this.getContext());
		TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
		tv.setLayoutParams(params);
		tv.setText(disp);
		tv.setTextSize(26);
		return tv;
	}

	@Override
	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		Log.d("HI", "FLING");
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}


}
