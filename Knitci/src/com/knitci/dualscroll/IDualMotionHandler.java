package com.knitci.dualscroll;

import android.view.MotionEvent;

public interface IDualMotionHandler {
	boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY);
	boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY);
	boolean onSingleTapUp(MotionEvent e);
}
