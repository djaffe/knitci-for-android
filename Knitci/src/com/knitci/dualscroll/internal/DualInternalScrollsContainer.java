package com.knitci.dualscroll.internal;

import com.knitci.pocket.R;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

public class DualInternalScrollsContainer extends LinearLayout /*implements OnGestureListener*/ {
	
	private int lastMotionX;
    private int lastMotionY;
    private GestureDetector gestureDetector; 

	public DualInternalScrollsContainer(Context context) {
		this(context, null);
	}
	
	public DualInternalScrollsContainer(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		//gestureDetector = new GestureDetector(context, this);
	}
//	
//	@Override
//	public boolean onInterceptTouchEvent(MotionEvent ev) {
//		return gestureDetector.onTouchEvent(ev);
//	}
//
//	@Override
//	public boolean onDown(MotionEvent e) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//	@Override
//	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
//			float velocityY) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//	@Override
//	public void onLongPress(MotionEvent e) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
//			float distanceY) {
//		Log.d("DualInternalScrollsContainer", "OnScroll!");
//		DualInternalScrollView sv = (DualInternalScrollView) findViewById(R.id.dualscrollview_content_sv);
//		sv.scrollBy((int)distanceX, (int)distanceY);
//		
//		HorizontalScrollView hsv = (HorizontalScrollView) findViewById(R.id.dualscrollview_content_hsv);
//		hsv.scrollBy((int)distanceX, (int)distanceY);
//		return true;
//	}
//
//	@Override
//	public void onShowPress(MotionEvent e) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public boolean onSingleTapUp(MotionEvent e) {
//		// TODO Auto-generated method stub
//		return false;
//	}

}
