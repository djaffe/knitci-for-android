package com.knitci.dualscroll.internal;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

public class DualInternalHorizontalScrollView extends HorizontalScrollView {

	public DualInternalHorizontalScrollView(Context context) {
		this(context, null);
	}
	
	public DualInternalHorizontalScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }
}
