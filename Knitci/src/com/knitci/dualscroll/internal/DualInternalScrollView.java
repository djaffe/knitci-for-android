package com.knitci.dualscroll.internal;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;

import com.knitci.pocket.R;

public class DualInternalScrollView extends ScrollView  {

	public DualInternalScrollView(Context context) {
		this(context, null);
	}
	
	public DualInternalScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }

}
