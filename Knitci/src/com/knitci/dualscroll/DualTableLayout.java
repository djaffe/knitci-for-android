package com.knitci.dualscroll;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.TableLayout;

public class DualTableLayout extends TableLayout {
	
	public DualTableLayout(Context context) {
		this(context, null);
	}
	
	public DualTableLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
}
