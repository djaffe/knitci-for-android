package com.knitci.yarnpicker;

import java.util.ArrayList;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.data.object.impl.RavelryYarn;
import com.knitci.yarnpicker.internal.IYarnPicker;
import com.knitci.yarnpicker.internal.YarnPickerResultAdapter;

public class YarnPicker extends LinearLayout implements TextWatcher {

	private IYarnPicker handler;
	private final YarnPickerResultAdapter adapter;
	private RavelryYarn lastSelectedYarn;
	Context context;
	
	public YarnPicker(Context context) {
		this(context, null);
	}
	
	public YarnPicker(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		LayoutInflater  mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mInflater.inflate(R.layout.yarnpicker, this, true);
        adapter = new YarnPickerResultAdapter(context);
        ListView lv = (ListView) findViewById(R.id.yarnpicker_result_listview);
        lv.setAdapter(adapter);
        
        lv.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                case MotionEvent.ACTION_DOWN:
                    // Disallow ScrollView to intercept touch events.
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    break;

                case MotionEvent.ACTION_UP:
                    // Allow ScrollView to intercept touch events.
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
        
        final EditText et = (EditText) findViewById(R.id.yarnpicker_et);
        et.addTextChangedListener(this);
        
        final TextWatcher watcher = this;
        
        lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				RavelryYarn yarn = adapter.yarns.get(arg2);
				lastSelectedYarn = yarn;
				
				et.removeTextChangedListener(watcher);
				et.setText(yarn.getYarnCompanyName() + " " + yarn.getName());
				et.addTextChangedListener(watcher);
				hideYarnResults(true);
			}
		});
        Button btn = (Button) findViewById(R.id.yarnpicker_result_none);
        btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				hideYarnResults(true);
			}
		});
        
        
        Button link = (Button) findViewById(R.id.yarnpicker_link);
        link.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				linkButtonClicked(arg0);
			}
		});
	}
	
	public void hideYarnResults(boolean hide) {
		LinearLayout container = (LinearLayout) findViewById(R.id.yarnpicker_result_holder);
		if(hide) {
			container.setVisibility(GONE);
		}
		else {
			container.setVisibility(VISIBLE);
		}
	}
	
	public void setHandler(IYarnPicker handler) {
		this.handler = handler;
	}
	
	public void populateResults(ArrayList<RavelryYarn> yarns) {
		LinearLayout container = (LinearLayout) findViewById(R.id.yarnpicker_result_holder);
		ListView lv = (ListView) findViewById(R.id.yarnpicker_result_listview);
		Button btn = (Button) findViewById(R.id.yarnpicker_result_none);
		
		if(yarns == null || yarns.isEmpty()) {
			container.setVisibility(GONE);
		}
		else {
			container.setVisibility(VISIBLE);
			adapter.yarns = yarns;
			adapter.notifyDataSetChanged();
		}
	}
	
	protected void linkButtonClicked(View v) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getWindowToken(), 0);
				    
		String searchTerm = ((EditText) findViewById(R.id.yarnpicker_et)).getText().toString();
		handler.yarnPickerLinkClicked(searchTerm+"*");
	}
	
	public boolean isYarnFreetext() {
		if(lastSelectedYarn != null) {
			return false;
		}
		return true;
	}
	
	public String getFreetextYarn() {
		final EditText et = (EditText) findViewById(R.id.yarnpicker_et);
		return et.getText().toString();
	}
	
	public RavelryYarn getLastSelectedYarn() {
		return lastSelectedYarn;
	}

	@Override
	public void afterTextChanged(Editable arg0) {
		lastSelectedYarn = null;
		
	}
	

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}

}
