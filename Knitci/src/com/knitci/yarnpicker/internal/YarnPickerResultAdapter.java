package com.knitci.yarnpicker.internal;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.data.object.impl.RavelryYarn;

public class YarnPickerResultAdapter extends BaseAdapter {

	private Context context;
	public ArrayList<RavelryYarn> yarns;
	
	public YarnPickerResultAdapter(Context context) {
		this.context = context;
	}
	
	@Override
	public int getCount() {
		if(yarns == null) {
			return 0;
		}
		return yarns.size();
	}

	@Override
	public Object getItem(int arg0) {
		return yarns.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		View rowView = arg1;
		if(rowView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.yarnpicker_adapter_item, null);
			ViewHolder vh = new ViewHolder();
			vh.display = (TextView) rowView.findViewById(R.id.yarnpicker_item_display);
			rowView.setTag(vh);
		}
		
		ViewHolder vh = (ViewHolder) rowView.getTag();
		vh.display.setText(yarns.get(arg0).getYarnCompanyName() + " " + yarns.get(arg0).getName());
		
		return rowView;
	}

	private static class ViewHolder {
		TextView display;
	}
	
}
