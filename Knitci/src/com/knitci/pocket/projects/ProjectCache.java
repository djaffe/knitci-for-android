package com.knitci.pocket.projects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.knitci.pocket.ravelry.data.object.impl.RavelryProject;

public class ProjectCache {

	HashMap<Integer, ArrayList<Integer>> projectHolder;
	HashMap<Integer, RavelryProject> projectMap;
	HashMap<String, HashMap<String, Integer>> permaMap;
	HashMap<Integer, ArrayList<Integer>> patternMap;

	public ProjectCache() {
		projectHolder = new LinkedHashMap<Integer, ArrayList<Integer>>();
		projectMap = new LinkedHashMap<Integer, RavelryProject>();
		permaMap = new HashMap<String, HashMap<String,Integer>>();
		patternMap = new HashMap<Integer, ArrayList<Integer>>();
	}

	public void addProjects(ArrayList<RavelryProject> projects) {
		Iterator<RavelryProject> it = projects.iterator();
		while(it.hasNext()) {
			addProject(it.next());
		}
	}

	public void addProject(RavelryProject project) {
		int ownerId = project.getUserId();
		addOwner(ownerId, project.getUsername());
		projectHolder.get(ownerId).add(project.getProjectId());
		projectMap.put(project.getProjectId(), project);
		permaMap.get(project.getUsername()).put(project.getPermalink(), project.getProjectId());
		
		if(patternMap.get(project.getPatternId()) == null) {
			patternMap.put(project.getPatternId(), new ArrayList<Integer>());
		}
		patternMap.get(project.getPatternId()).add(project.getProjectId());
	}

	public boolean isUserPresent(int userId) {
		return (projectHolder.get(userId) != null);
	}

	public void addOwner(int userId, String username) {
		if(!isUserPresent(userId)) {
			projectHolder.put(userId, new ArrayList<Integer>());
			permaMap.put(username, new HashMap<String, Integer>());
		}
	}

	public ArrayList<RavelryProject> getProjects(int userId) {
		ArrayList<RavelryProject> projects = new ArrayList<RavelryProject>();
		if(!isUserPresent(userId)) {
			return projects;
		}
		Iterator<Integer> it = projectHolder.get(userId).iterator();
		while(it.hasNext()) {
			projects.add(projectMap.get(it.next()));
		}
		return projects;
	}

	public RavelryProject getProject(int projectId) {
		return projectMap.get(projectId);
	}

	public RavelryProject getProject(String username, String permalink) {
		if(permaMap.get(username) != null) {
			return projectMap.get(permaMap.get(username).get(permalink));
		}
		return null;
	}
	
	public ArrayList<RavelryProject> getProjectsByPattern(int patternId) {
		ArrayList<RavelryProject> projects = new ArrayList<RavelryProject>();
		if(patternMap.get(patternId) != null && !patternMap.get(patternId).isEmpty()) {
			Iterator<Integer> it = patternMap.get(patternId).iterator();
			while(it.hasNext()) {
				int projectId = it.next();
				projects.add(projectMap.get(projectId));
			}
		}
		return projects;
	}
}
