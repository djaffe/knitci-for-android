package com.knitci.pocket.projects;

import java.util.ArrayList;

import object.KnitciActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.patterns.PatternImageAdapter;
import com.knitci.pocket.projects.manager.ProjectManager;
import com.knitci.pocket.ravelry.RavelryManager;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPack;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPhotoSet;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject.ProjectPhotoUrlSize;
import com.knitci.pocket.social.people.manager.PersonManager;
import com.knitci.pocket.util.FullscreenImage;
import com.knitci.pocket.util.StandardConvert;

public class ProjectDetailActivity extends KnitciActivity {

	RavelryManager ravelryManager;
	PersonManager personManager;
	ProjectManager projectManager;
	RavelryProject project;
	PatternImageAdapter imageAdapter;
	int projectId;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_project_detail);
		stealAsyncListening();
		setupSlidingMenu();


		projectManager = KnitciApplication.class.cast(getApplication()).getProjectManager();
		ravelryManager = KnitciApplication.class.cast(getApplication()).getRavelryManager();
		personManager = KnitciApplication.class.cast(getApplication()).getPersonManager();

		ViewPager vp = ViewPager.class.cast(findViewById(R.id.projectdetail_imageviewpager));
		vp.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				ScrollView.class.cast(findViewById(R.id.projectdetail_scrollview)).requestDisallowInterceptTouchEvent(true);
				return false;
			}
		});

		projectId = getIntent().getExtras().getInt("PROJECTID");
		project = projectManager.getProject(projectId);
		String projOwner = getIntent().getExtras().getString("PROJECTUSER");
		String projPerm = getIntent().getExtras().getString("PROJECTPERMA");
		if(project == null) {
			if(projOwner != null && projPerm != null) {
				project = projectManager.getProject(projOwner, projPerm);
			}
		}
		if(project == null) {
			if(projOwner != null && projPerm != null) {
				ravelryManager.retrieveProject(projOwner, projPerm);
			}
			else {
				// TODO: Throw some sort of error
			}
		}
		else if(!project.isFullyFilled()) {
			if(project.getUsername() == null || project.getUsername().isEmpty()) {
				if(personManager.getUser(project.getUserId()) != null) {
					project.setUsername(personManager.getUser(project.getUserId()).getUsername());
					performFullRetrieval();
				}
				else {
					Log.w("ProjectDetailActivity", "User was not found to load project - loading user");
					ravelryManager.retrieveUserProfile(project.getUserId());
				}
			}
			else {
				performFullRetrieval();
			}
		}
		else {
			populateProject(project);
		}
	}

	private void performFullRetrieval() {
		RavelryProject project = projectManager.getProject(projectId);
		ravelryManager.retrieveProject(project.getUsername(), projectId);
	}

	private void populateProject(RavelryProject project) {
		Toast toast = Toast.makeText(getApplicationContext(), String.valueOf(project.getPacks().size()), Toast.LENGTH_SHORT);
		toast.show();

		TextView.class.cast(findViewById(R.id.projectdetail_name)).setText(project.getProjectName());
		TextView.class.cast(findViewById(R.id.projectdetail_username)).setText(project.getUsername());
		populatePhotos();
		TextView.class.cast(findViewById(R.id.projectdetail_comments_count)).setText(String.valueOf(project.getCommentsCount()));

		if(project.getCompletedDtTm() != null) {
			TextView.class.cast(findViewById(R.id.projectdetail_finished_date)).setText(StandardConvert.dateToString(project.getCompletedDtTm()));
		}
		else {
			TextView.class.cast(findViewById(R.id.projectdetail_finished_date)).setVisibility(View.GONE);
		}

		if(project.getPatternName() != null && !project.getPatternName().isEmpty()) {
			TextView.class.cast(findViewById(R.id.projectdetail_pattern)).setText(project.getPatternName());
		}
		else {
			LinearLayout.class.cast(findViewById(R.id.projectdetail_pattern_layout)).setVisibility(View.GONE);
		}

		if(project.getCraftName() != null && !project.getCraftName().isEmpty()) {
			TextView.class.cast(findViewById(R.id.projectdetail_craft)).setText(project.getCraftName());
		}
		else {
			LinearLayout.class.cast(findViewById(R.id.projectdetail_craft_layout)).setVisibility(View.GONE);
		}

		if(project.getStartedDtTm() != null) {
			String disp = StandardConvert.dateToString(project.getStartedDtTm());
			TextView.class.cast(findViewById(R.id.projectdetail_started)).setText(disp);
		}
		else {
			LinearLayout.class.cast(findViewById(R.id.projectdetail_started_layout)).setVisibility(View.GONE);
		}

		if(project.getMadeForUsername() != null && !project.getMadeForUsername().isEmpty()) {
			TextView.class.cast(findViewById(R.id.projectdetail_madefor)).setText(project.getMadeForUsername());
		}
		else {
			LinearLayout.class.cast(findViewById(R.id.projectdetail_madefor_layout)).setVisibility(View.GONE);
		}

		if(project.getNeedleSizes() != null && !project.getNeedleSizes().isEmpty()) {
			String disp = "";
			for(int i = 0; i < project.getNeedleSizes().size(); i++) {
				disp += project.getNeedleSizes().get(i).getName() + "\r";
			}
			TextView.class.cast(findViewById(R.id.projectdetail_needles)).setText(disp);
		}
		else {
			LinearLayout.class.cast(findViewById(R.id.projectdetail_needles_layout)).setVisibility(View.GONE);
		}	

		// Yarn details
		if(project.getPacks() != null && !project.getPacks().isEmpty()) {
			LinearLayout holder = (LinearLayout) findViewById(R.id.projectdetail_howmuchyarn_holder);
			for(int i = 0; i < project.getPacks().size(); i++) {

				if(i > 0) {
					LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, 1);
					params.setMargins(25, 0, 25, 0);

					View view =  new View(getApplicationContext());
					view.setLayoutParams(params);
					view.setBackgroundColor(Color.DKGRAY);

					holder.addView(view);
				}

				RavelryPack pack = project.getPacks().get(i);

				if(pack.getYarnName() != null && !pack.getYarnName().isEmpty()) {
					LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
					//params.setMargins(0, 20, 0,0);
					LinearLayout layout = new LinearLayout(getApplicationContext());
					layout.setLayoutParams(params);
					layout.setWeightSum(3f);
					layout.setOrientation(LinearLayout.HORIZONTAL);

					TextView yarnNameLabel = new TextView(getApplicationContext());
					yarnNameLabel.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f));
					yarnNameLabel.setText("Yarn");
					yarnNameLabel.setTextColor(Color.BLACK);

					TextView yarnName = new TextView(getApplicationContext());
					yarnName.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 2f));
					yarnName.setText(pack.getYarnName());
					yarnName.setTextColor(Color.BLACK);

					layout.addView(yarnNameLabel);
					layout.addView(yarnName);

					holder.addView(layout);
				}

				if(pack.getYardsPerSkein() > 0 && pack.getSkeins() > 0) {
					LinearLayout layout = new LinearLayout(getApplicationContext());
					layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
					layout.setWeightSum(3f);
					layout.setOrientation(LinearLayout.HORIZONTAL);

					TextView label = new TextView(getApplicationContext());
					label.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f));
					label.setText("Yardage");

					TextView val = new TextView(getApplicationContext());
					val.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 2f));
					val.setText(String.valueOf(pack.getYardsPerSkein() * pack.getSkeins() + " yards"));

					label.setTextColor(Color.BLACK);
					val.setTextColor(Color.BLACK);

					layout.addView(label);
					layout.addView(val);

					holder.addView(layout);
				}

				if(pack.getColorway() != null && !pack.getColorway().isEmpty()) {
					LinearLayout layout = new LinearLayout(getApplicationContext());
					layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
					layout.setWeightSum(3f);
					layout.setOrientation(LinearLayout.HORIZONTAL);

					TextView label = new TextView(getApplicationContext());
					label.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f));
					label.setText("Colorway");

					TextView val = new TextView(getApplicationContext());
					val.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 2f));
					val.setText(pack.getColorway());
					label.setTextColor(Color.BLACK);
					val.setTextColor(Color.BLACK);
					layout.addView(label);
					layout.addView(val);

					holder.addView(layout);
				}

				if(pack.getDyelot() != null && !pack.getDyelot().isEmpty()) {
					LinearLayout layout = new LinearLayout(getApplicationContext());
					layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
					layout.setWeightSum(3f);
					layout.setOrientation(LinearLayout.HORIZONTAL);

					TextView label = new TextView(getApplicationContext());
					label.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f));
					label.setText("Dye lot");

					TextView val = new TextView(getApplicationContext());
					val.setLayoutParams(new LayoutParams(9, LayoutParams.WRAP_CONTENT, 2f));
					val.setText(pack.getDyelot());
					label.setTextColor(Color.BLACK);
					val.setTextColor(Color.BLACK);
					layout.addView(label);
					layout.addView(val);

					holder.addView(layout);
				}

				if(pack.getShopName() != null && !pack.getShopName().isEmpty()) {
					LinearLayout layout = new LinearLayout(getApplicationContext());
					layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
					layout.setWeightSum(3f);
					layout.setOrientation(LinearLayout.HORIZONTAL);

					TextView label = new TextView(getApplicationContext());
					label.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f));
					label.setText("Purchased at");

					TextView val = new TextView(getApplicationContext());
					val.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 2f));
					val.setText(pack.getShopName());
					label.setTextColor(Color.BLACK);
					val.setTextColor(Color.BLACK);
					layout.addView(label);
					layout.addView(val);

					holder.addView(layout);
				}

			}

		}
		else {
			LinearLayout.class.cast(findViewById(R.id.projectdetail_howmuchyarn_holder)).setVisibility(View.GONE);
		}

		if(project.getNotesHtml() != null && !project.getNotesHtml().isEmpty()) {
			TextView.class.cast(findViewById(R.id.projectdetail_notes)).setText(Html.fromHtml(project.getNotesHtml()));
		}
		else if(project.getNotes() != null && !project.getNotes().isEmpty()) {
			TextView.class.cast(findViewById(R.id.projectdetail_notes)).setText(project.getNotes());
		}
		else {
			TextView.class.cast(findViewById(R.id.projectdetail_notes)).setVisibility(View.GONE);
		}
	}


	private void populatePhotos() {
		ArrayList<RavelryPhotoSet> photos = project.getPhotos();
		if(photos == null || photos.isEmpty()) {
			return;
		}
		ArrayList<String> urls = new ArrayList<String>();
		ViewPager vp = (ViewPager) findViewById(R.id.projectdetail_imageviewpager);
		for(int i = 0; i < photos.size(); i++) {
			RavelryPhotoSet set = photos.get(i);
			if(set.getPhoto(ProjectPhotoUrlSize.MEDIUM) != null) {
				urls.add(set.getPhoto(ProjectPhotoUrlSize.MEDIUM).getUrl());
			}
		}

		if(imageAdapter == null) {
			imageAdapter = new PatternImageAdapter(getApplicationContext());
		}
		vp.setAdapter(imageAdapter);
		imageAdapter.urls = urls;
		imageAdapter.setItemClickedListener(this);
		imageAdapter.notifyDataSetChanged();
	}

	@Override
	public void processFinish(RavelryResponse response) {
		super.processFinish(response);
		switch(response.task) {
		case USERPROFILE:
			project.setUsername(personManager.getUsername(project.getUserId()));
			performFullRetrieval();
			break;
		case PROJECT:
			RavelryProject project = RavelryProject.class.cast(response.result);
			populateProject(project);
			break;
		default:
			break;
		}
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		String url = (String) v.getTag();
		Intent intent = new Intent(this, FullscreenImage.class);
		intent.putExtra("url", url);
		startActivity(intent);
	}

}
