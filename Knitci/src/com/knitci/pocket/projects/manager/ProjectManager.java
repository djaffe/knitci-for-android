package com.knitci.pocket.projects.manager;

import java.util.ArrayList;

import android.util.Log;

import com.knitci.pocket.projects.ProjectCache;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject;

public class ProjectManager {

	ProjectCache projectCache;

	public ProjectManager() {
		projectCache = new ProjectCache();
	}

	public ArrayList<RavelryProject> getUserProjects(int userId) {
		return projectCache.getProjects(userId);
	}
	
	public ArrayList<RavelryProject> getProjectsByPattern(int patternId) {
		return projectCache.getProjectsByPattern(patternId);
	}

	public void addProjects(ArrayList<RavelryProject> projects) {
		for(int i = 0; i < projects.size(); i++) {
			addProject(projects.get(i));
		}
	}

	public void addProject(RavelryProject project) {
		if(projectCache.getProject(project.getProjectId()) != null) {
			mergeProject(project);
		}
		else {
			projectCache.addProject(project);
		}
	}

	public RavelryProject getProject(int projectId) {
		return projectCache.getProject(projectId);
	}
	
	public RavelryProject getProject(String username, String permalink) {
		return projectCache.getProject(username, permalink);
	}

	private void mergeProject(RavelryProject fullProject) {
		if(fullProject.isFullyFilled()) {
			int projectId = fullProject.getProjectId();
			RavelryProject existingProject = projectCache.getProject(projectId);
			existingProject.setNotes(fullProject.getNotes());
			existingProject.setNotesHtml(fullProject.getNotesHtml());
			existingProject.setPhotos(fullProject.getPhotos());
			existingProject.setNeedleSizes(fullProject.getNeedleSizes());
			existingProject.setComments(fullProject.getComments());
			existingProject.setPacks(fullProject.getPacks());
			existingProject.setFullyFilled();
		}
	}
}
