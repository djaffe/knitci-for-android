package com.knitci.pocket.projects;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.android.volley.toolbox.NetworkImageView;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject.ProjectPhotoUrlSize;

public class ProjectListAdapter extends BaseAdapter {

	public ArrayList<RavelryProject> projects;
	private Context context;
	
	public ProjectListAdapter(Context context) {
		this.context = context;
	}
	
	@Override
	public int getCount() {
		if(projects == null) {
			return 0;
		}
		return projects.size();
	}

	@Override
	public Object getItem(int arg0) {
		return projects.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return projects.get(arg0).getProjectId();
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		RavelryProject project = projects.get(arg0);
		View v = arg1;
		if(v == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.projectlist_projet_item, arg2, false);
			ViewHolder holder = new ViewHolder();
			//holder.img = ImageView.class.cast(v.findViewById(R.id.projectsearch_item_image));
			holder.img = NetworkImageView.class.cast(v.findViewById(R.id.projectlist_item_image));
			v.setTag(holder);
		}

		if(project != null) {
			KnitciApplication app = (KnitciApplication)context.getApplicationContext();
			ViewHolder holder = (ViewHolder) v.getTag();
			String url = null;
			if(project.getFirstPhotos() == null) {

			}
			else if(project.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.THUMBNAIL) != null
					&& project.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.THUMBNAIL).getUrl() != null) {
				url = project.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.THUMBNAIL).getUrl();
			}
			else if(project.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.SQUARE) != null
					&& project.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.SQUARE).getUrl() != null) {
				url = project.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.SQUARE).getUrl();
			}
			else if(project.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.SMALL) != null
					&& project.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.SMALL).getUrl() != null) {
				url = project.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.SMALL).getUrl();
			}
			else if(project.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.MEDIUM) != null
					&& project.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.MEDIUM).getUrl() != null) {
				url = project.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.MEDIUM).getUrl();
			}
			if(url != null) {
				holder.img.setImageUrl(url, app.getProjectLoader());
				//app.getprojectLoader().get(url, ImageLoader.getImageListener(holder.img, R.drawable.ic_launcher, R.drawable.ic_launcher));
			}
		}
		return v;
	}
	
	static class ViewHolder {
		//ImageView img;
		NetworkImageView img;
	}


}
