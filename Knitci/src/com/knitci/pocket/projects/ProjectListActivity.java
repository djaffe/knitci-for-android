package com.knitci.pocket.projects;

import java.util.ArrayList;

import object.KnitciActivity;
import android.content.Intent;
import android.os.Bundle;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuInflater;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.patterns.PatternDetailActivity;
import com.knitci.pocket.projects.manager.ProjectManager;
import com.knitci.pocket.ravelry.RavelryManager;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject;

public class ProjectListActivity extends KnitciActivity implements OnItemClickListener {

	int userId;
	int patternId;
	ProjectListAdapter adapter;
	RavelryManager ravelryManager;
	ProjectManager projectManager;
	ProjectListMode mode;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_project_list);
		setupSlidingMenu();
		stealAsyncListening();
		ravelryManager = KnitciApplication.class.cast(getApplication()).getRavelryManager();
		projectManager = KnitciApplication.class.cast(getApplication()).getProjectManager();
		int user = getIntent().getExtras().getInt("USERID");
		int pattern = getIntent().getExtras().getInt("PATTERNID");


		if(user > 0) {
			this.userId = user;
			mode = ProjectListMode.USER;
		}
		else if(pattern > 0) {
			this.patternId = pattern;
			mode = ProjectListMode.PATTERN;
		}
		else {
			userId = ravelryManager.getRavelryUser().getId();
			mode = ProjectListMode.USER;
		}

		if(mode == ProjectListMode.USER) {
			if(projectManager.getUserProjects(userId).isEmpty()) {
				ravelryManager.retrieveUserProjects(ravelryManager.getRavelryUser().getUsername(), 1);
			}
			else {
				populateProjects(projectManager.getUserProjects(userId));
			}
		}
		else if(mode == ProjectListMode.PATTERN) {
			if(projectManager.getProjectsByPattern(pattern).isEmpty()) {
				ravelryManager.retrieveProjectsByPattern(pattern, null, null, false, 1);
			}
			else {
				populateProjects(projectManager.getProjectsByPattern(pattern));
			}
		}
		else {
			// Crap... 
			finish();
		}

		GridView gv = (GridView) findViewById(R.id.projectlist_gridview);
		gv.setOnItemClickListener(this);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.project_list, menu);
		return true;
	}

	public void populateProjects(ArrayList<RavelryProject> projects) {
		if(adapter == null) {
			adapter = new ProjectListAdapter(getApplicationContext());
			GridView gv = (GridView) findViewById(R.id.projectlist_gridview);
			gv.setAdapter(adapter);
			gv.setOnItemClickListener(this);
		}
		adapter.projects = projects;
		adapter.notifyDataSetChanged();
	}

	@Override
	public void processFinish(RavelryResponse response) {
		switch(response.task) {
		case SPECIFICUSERPROJECTS:
			populateProjects(projectManager.getUserProjects(userId));
			break;
		case SPECIFICUSERPROJECTSSTILLOADING:
			Toast toast = Toast.makeText(getApplicationContext(), "More projects loading", Toast.LENGTH_SHORT);
			toast.show();
			populateProjects(projectManager.getUserProjects(userId));
			break;
		case PROJECTSBYPATTERN:
			populateProjects(projectManager.getProjectsByPattern(patternId));
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Intent intent = new Intent(this, ProjectDetailActivity.class);
		intent.putExtra("PROJECTID", adapter.projects.get(arg2).getProjectId());
		startActivity(intent);
	}

	private enum ProjectListMode {
		USER, PATTERN
	}

}
