package com.knitci.pocket.stash;

import java.util.ArrayList;

import object.KnitciActivity;
import android.os.Bundle;
import android.widget.GridView;

import com.actionbarsherlock.view.Menu;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.RavelryManager;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryStashGeneric;
import com.knitci.pocket.stash.manager.StashManager;

public class StashListActivity extends KnitciActivity {

	String username;
	RavelryManager ravelryManager;
	StashManager stashManager;
	StashListAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stash_list);
		setupSlidingMenu();
		stealAsyncListening();
		ravelryManager = KnitciApplication.class.cast(getApplication()).getRavelryManager();
		stashManager = KnitciApplication.class.cast(getApplication()).getStashManager();

		if(getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getString("USERNAME") != null) {
			username = getIntent().getExtras().getString("USERNAME");
		}
		else {
			username = ravelryManager.getRavelryUser().getUsername();
		}

		if(stashManager.getStashes(username).isEmpty()) {
			KnitciApplication.class.cast(getApplicationContext()).getRavelryManager().retrieveUnifiedStash(username, 1, 10, "");
		}
		else {
			populateStash(stashManager.getStashes(username));
		}

	}

	private void populateStash(ArrayList<RavelryStashGeneric> stashes) {
		if(adapter == null) {
			adapter = new StashListAdapter(getApplicationContext());
			GridView gv = (GridView) findViewById(R.id.stashlist_gridview);
			gv.setAdapter(adapter);
		}
		adapter.stashes = stashes;
		adapter.notifyDataSetChanged();
	}

	@Override
	public void processFinish(RavelryResponse response) {
		switch(response.task) {
		case STASH:
		case UNIFIEDSTASH:
			populateStash(stashManager.getStashes(username));
			break;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.stash_list, menu);
		return true;
	}

}
