package com.knitci.pocket.stash.manager;

import java.util.ArrayList;

import android.content.Context;

import com.knitci.pocket.ravelry.data.object.impl.RavelryStashGeneric;

public class StashManager {

	private Context context;
	private StashCache stashCache;
	
	public StashManager(Context context) {
		this.context = context;
		stashCache = new StashCache();
	}
	
	public ArrayList<RavelryStashGeneric> getStashes(String username) {
		return stashCache.getStashes(username);
	}
	
	public void addStashes(String username, ArrayList<RavelryStashGeneric> stashes, int page, int stashmaxPage) {
		stashCache.addStashes(username, stashes, page, stashmaxPage);
	}
	
}
