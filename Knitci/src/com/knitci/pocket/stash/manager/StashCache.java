package com.knitci.pocket.stash.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.knitci.pocket.ravelry.data.object.impl.RavelryFiberStash;
import com.knitci.pocket.ravelry.data.object.impl.RavelryStash;
import com.knitci.pocket.ravelry.data.object.impl.RavelryStashGeneric;
import com.knitci.pocket.ravelry.data.object.impl.RavelryStashGeneric.StashType;

public class StashCache {

	private HashMap<Integer, RavelryStashGeneric> mapping;
	private HashMap<String, ArrayList<Integer>> userStashes;
	private HashMap<String, Integer> userLastPageLoaded;
	private HashMap<String, Integer> userMaxPage;
	
	public StashCache() {
		mapping = new HashMap<Integer, RavelryStashGeneric>();
		userStashes = new HashMap<String, ArrayList<Integer>>();
		userLastPageLoaded = new HashMap<String, Integer>();
		userMaxPage = new HashMap<String, Integer>();
	}
	
	public synchronized void addStash(String username, RavelryStashGeneric stash) {
		int id;
		if(stash.getType() == StashType.STASH){
			RavelryStash stashObj = (RavelryStash) stash;
			mapping.put(stashObj.getId(), stash);
			id = stashObj.getId();
		}
		else {
			RavelryFiberStash fiber = (RavelryFiberStash) stash;
			mapping.put(fiber.getId(), stash);
			id = fiber.getId();
		}
		
		if(userStashes.get(username) == null) {
			userStashes.put(username, new ArrayList<Integer>());
		}
		userStashes.get(username).add(id);
	}
	
	public void addStashes(String username, ArrayList<RavelryStashGeneric> stashes, int page, int stashmaxPage) {
		Iterator<RavelryStashGeneric> it = stashes.iterator();
		while(it.hasNext()) {
			addStash(username, it.next());
		}
		userLastPageLoaded.put(username, page);
		userMaxPage.put(username, stashmaxPage);
	}
	
	public ArrayList<RavelryStashGeneric> getStashes(String username) {
		ArrayList<RavelryStashGeneric> stashes = new ArrayList<RavelryStashGeneric>();
		if(userStashes.get(username) != null) {
			Iterator<Integer> it = userStashes.get(username).iterator();
			while(it.hasNext()) {
				stashes.add(mapping.get(it.next()));
			}
		}
		return stashes;
	}
	
}
