package com.knitci.pocket.stash;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.android.volley.toolbox.NetworkImageView;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.data.object.impl.RavelryFiberStash;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPhotoSet;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryStash;
import com.knitci.pocket.ravelry.data.object.impl.RavelryStashGeneric;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject.ProjectPhotoUrlSize;
import com.knitci.pocket.ravelry.data.object.impl.RavelryStashGeneric.StashType;

public class StashListAdapter extends BaseAdapter {
	
	Context context;
	ArrayList<RavelryStashGeneric> stashes;
	
	public StashListAdapter(Context context) {
		this.context = context;
	}

	@Override
	public int getCount() {
		if(stashes == null) {
			return 0;
		}
		return stashes.size();
	}

	@Override
	public Object getItem(int arg0) {
		return stashes.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		if(stashes.get(arg0).getType() == StashType.STASH) {
			return RavelryStash.class.cast(stashes.get(arg0)).getId();
		}
		else {
			return RavelryFiberStash.class.cast(stashes.get(arg0)).getId();
		}
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		RavelryStashGeneric stash = stashes.get(arg0);
		View v = arg1;
		if(v == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.stashlist_stash_item, arg2, false);
			ViewHolder holder = new ViewHolder();
			//holder.img = ImageView.class.cast(v.findViewById(R.id.projectsearch_item_image));
			holder.img = NetworkImageView.class.cast(v.findViewById(R.id.stashlist_item_image));
			v.setTag(holder);
		}
		if(stash != null) {
			KnitciApplication app = (KnitciApplication)context.getApplicationContext();
			ViewHolder holder = (ViewHolder) v.getTag();
			String url = null;
			RavelryPhotoSet photos;
			if(stash.getType() == StashType.STASH) {
				photos = RavelryStash.class.cast(stash).getFirstPhoto();
			}
			else {
				photos = RavelryFiberStash.class.cast(stash).getFirstPhoto();
			}
			if(photos == null) {

			}
			else if(photos.getPhoto(ProjectPhotoUrlSize.THUMBNAIL) != null
					&& photos.getPhoto(ProjectPhotoUrlSize.THUMBNAIL).getUrl() != null) {
				url = photos.getPhoto(ProjectPhotoUrlSize.THUMBNAIL).getUrl();
			}
			else if(photos.getPhoto(ProjectPhotoUrlSize.SQUARE) != null
					&& photos.getPhoto(ProjectPhotoUrlSize.SQUARE).getUrl() != null) {
				url = photos.getPhoto(ProjectPhotoUrlSize.SQUARE).getUrl();
			}
			else if(photos.getPhoto(ProjectPhotoUrlSize.SMALL) != null
					&& photos.getPhoto(ProjectPhotoUrlSize.SMALL).getUrl() != null) {
				url = photos.getPhoto(ProjectPhotoUrlSize.SMALL).getUrl();
			}
			else if(photos.getPhoto(ProjectPhotoUrlSize.MEDIUM) != null
					&& photos.getPhoto(ProjectPhotoUrlSize.MEDIUM).getUrl() != null) {
				url = photos.getPhoto(ProjectPhotoUrlSize.MEDIUM).getUrl();
			}
			if(url != null) {
				holder.img.setImageUrl(url, app.getProjectLoader());
				//app.getprojectLoader().get(url, ImageLoader.getImageListener(holder.img, R.drawable.ic_launcher, R.drawable.ic_launcher));
			}
		}
		return v;
	}

	static class ViewHolder {
		//ImageView img;
		NetworkImageView img;
	}

	
}
