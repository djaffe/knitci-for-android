package com.knitci.pocket.tools;

import java.util.ArrayList;

public class NeedleRecordCriteria {

	private ArrayList<String> types;
	private ArrayList<Float> sizes;
	private ArrayList<Integer> lengths;
	private int minLength;
	private int maxLength;
	private float minSize;
	private float maxSize;
	
	public NeedleRecordCriteria() {
		types = new ArrayList<String>();
		sizes = new ArrayList<Float>();
		lengths = new ArrayList<Integer>();
	}
	
	/**
	 * @return the minLength
	 */
	public int getMinLength() {
		return minLength;
	}

	/**
	 * @param minLength the minLength to set
	 */
	public void setMinLength(int minLength) {
		this.minLength = minLength;
	}

	/**
	 * @return the maxLength
	 */
	public int getMaxLength() {
		return maxLength;
	}

	/**
	 * @param maxLength the maxLength to set
	 */
	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	/**
	 * @return the minSize
	 */
	public float getMinSize() {
		return minSize;
	}

	/**
	 * @param minSize the minSize to set
	 */
	public void setMinSize(float minSize) {
		this.minSize = minSize;
	}

	/**
	 * @return the maxSize
	 */
	public float getMaxSize() {
		return maxSize;
	}

	/**
	 * @param maxSize the maxSize to set
	 */
	public void setMaxSize(float maxSize) {
		this.maxSize = maxSize;
	}

	public void addType(String type) {
		if(!types.contains(type)) {
			types.add(type);
		}
	}
	
	public void addSize(float size) {
		if(!sizes.contains(size)) {
			sizes.add(size);
		}
	}
	
	public void addLength(int length) {
		if(!lengths.contains(length)) {
			lengths.add(length);
		}
	}
	
	public ArrayList<String> getTypes() {
		return types;
	}
	
	public ArrayList<Float> getSizes() {
		return sizes;
	}
	
	public ArrayList<Integer> getLengths() {
		return lengths;
	}
}
