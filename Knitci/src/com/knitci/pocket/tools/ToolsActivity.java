package com.knitci.pocket.tools;

import java.util.ArrayList;

import object.KnitciActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.edmodo.rangebar.RangeBar;
import com.edmodo.rangebar.RangeBar.OnRangeBarChangeListener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.patterns.PatternSearchActivity.EFilter;
import com.knitci.pocket.ravelry.RavelryManager;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryNeedleRecord;
import com.knitci.pocket.tools.needle.ToolsAdapter;

import cz.destil.settleup.gui.MultiSpinner;
import cz.destil.settleup.gui.MultiSpinner.MultiSpinnerListener;

public class ToolsActivity extends KnitciActivity implements MultiSpinnerListener {

	RavelryManager ravelryManager;
	ToolsManager toolsManager;
	ToolsAdapter adapter;
	NeedleRecordCriteria criteria;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		stealAsyncListening();
		setupSlidingMenu();
		setContentView(R.layout.activity_tools);
		setupSecondaryMenu();
		KnitciApplication app = (KnitciApplication) getApplicationContext();
		criteria = new NeedleRecordCriteria();
		ravelryManager = app.getRavelryManager();
		toolsManager = app.getToolsManager();
		if(!toolsManager.isDictionaryReady()) {
			initializeNeeleReference();
		}
		else {
			ArrayList<RavelryNeedleRecord> records = toolsManager.getNeedles(new NeedleRecordCriteria());
			if(records != null && !records.isEmpty()) {
				populateTools(records);
				//ravelryManager.retrieveNeedleRecords(ravelryManager.getRavelryUser().getUsername());
			}
		}
		
		MultiSpinner spin = (MultiSpinner) findViewById(R.id.slidingtool_multi_type);
		ArrayList<String> strings = new ArrayList<String>();
		strings.add("Circular");
		strings.add("Double Pointed");
		strings.add("Straight");
		strings.add("Standard Hooks");
		strings.add("Steel Hooks");
		spin.setItems(strings, "Any Needle Type", this);

		RangeBar lenRange = (RangeBar) findViewById(R.id.slidingtool_range_length);
		lenRange.setTickCount(15);
		lenRange.setOnRangeBarChangeListener(new OnRangeBarChangeListener() {
			
			@Override
			public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex, int rightThumbIndex) {
				
			}
		});

		RangeBar sizeRange = (RangeBar) findViewById(R.id.slidingtool_range_size);
		sizeRange.setTickCount(43);
		sizeRange.setOnRangeBarChangeListener(new OnRangeBarChangeListener() {
			
			@Override
			public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex, int rightThumbIndex) {
//				criteria.setMinSize(toolsManager.needleDictionary.getSortedSize().get(leftThumbIndex).getMetric());
//				criteria.setMaxSize(toolsManager.needleDictionary.getSortedSize().get(rightThumbIndex).getMetric());
//				TextView tvMin = (TextView) findViewById(R.id.slidingtool_size_min);
//				tvMin.setText(String.valueOf(toolsManager.needleDictionary.getSortedSize().get(leftThumbIndex).getMetric()));
//				TextView tvMax = (TextView) findViewById(R.id.slidingtool_size_max);
//				tvMax.setText(String.valueOf(toolsManager.needleDictionary.getSortedSize().get(rightThumbIndex).getMetric()));
			}
		});

	}

	private void populateTools(ArrayList<RavelryNeedleRecord> records) {
		if(adapter == null) {
			adapter = new ToolsAdapter(getApplicationContext(), records);
			ListView lv = (ListView) findViewById(R.id.toollist_listview);
			lv.setAdapter(adapter);
		}
		else {
			adapter.records = records;
		}
		adapter.notifyDataSetChanged();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.tools, menu);
		return true;
	}

	private void initializeNeeleReference() {
		ravelryManager.retrieveNeedleReference();
	}

	private ArrayList<RavelryNeedleRecord> performSearchForNeedles(NeedleRecordCriteria criteria) {
		return toolsManager.getNeedles(criteria);
	}

	@Override
	public void processFinish(RavelryResponse response) {
		switch (response.task) {
		case NEEDLETYPES:
			ravelryManager.retrieveNeedleRecords(ravelryManager.getRavelryUser().getUsername());
			break;
		case NEEDLERECORDS:
			populateTools(toolsManager.getNeedles(new NeedleRecordCriteria()));
			break;
		}
	}

	protected void setupSecondaryMenu() {
		SlidingMenu menu = getSlidingMenu();
		menu.setMode(SlidingMenu.LEFT_RIGHT);
		menu.setSecondaryMenu(R.layout.slidingmenu_tools);
		menu.setFadeDegree(0.35f);
		setSlidingActionBarEnabled(true);

	}

	@Override
	public void onItemsSelected(boolean[] selected) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onItemsSelected(ArrayList<String> selected) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onItemsSelected(ArrayList<String> selected, EFilter filter) {
		// TODO Auto-generated method stub
		
	}

}
