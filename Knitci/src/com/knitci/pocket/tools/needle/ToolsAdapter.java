package com.knitci.pocket.tools.needle;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.data.object.impl.RavelryNeedleRecord;

public class ToolsAdapter extends BaseAdapter {

	Context context;
	public ArrayList<RavelryNeedleRecord> records;

	public ToolsAdapter(Context context, ArrayList<RavelryNeedleRecord> records) {
		this.context = context;
		this.records = records;
	}
	@Override
	public int getCount() {
		return records.size();
	}

	@Override
	public Object getItem(int arg0) {
		return records.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int pos, View arg1, ViewGroup arg2) {
		View row = arg1;

		if(row == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.toollist_toolitem, null);
			ViewHolder vh = new ViewHolder();
			//vh.usSizeTv = (TextView) row.findViewById(R.id.toollist_us_size);
			//vh.metricSizeTv = (TextView) row.findViewById(R.id.toollist_metric_size);
			vh.lengthTv = (TextView) row.findViewById(R.id.toollist_length);
			vh.typeTv = (TextView) row.findViewById(R.id.toollist_type);
			vh.countTv = (TextView) row.findViewById(R.id.toollist_count);
			vh.sizeTv = (TextView) row.findViewById(R.id.toollist_size);
			row.setTag(vh);
		}

		ViewHolder vh = (ViewHolder) row.getTag();

		RavelryNeedleRecord rec = records.get(pos);
		
		vh.sizeTv.setText("");
		if(rec.getNeedleType().getNeedleSize().getUs() != null &&
				!rec.getNeedleType().getType().equals("hook")) {
			//vh.usSizeTv.setText("US " + rec.getNeedleType().getNeedleSize().getUs());
			vh.sizeTv.setText("US " + rec.getNeedleType().getNeedleSize().getUs());
		}
		else if(rec.getNeedleType().getNeedleSize().getHook() != null &&
				rec.getNeedleType().getType().equals("hook")) {
			//vh.usSizeTv.setText(rec.getNeedleType().getNeedleSize().getHook());
			vh.sizeTv.setText("US " + rec.getNeedleType().getNeedleSize().getHook());
		}
		else {
			//vh.usSizeTv.setText("");
			//vh.usSizeTv.setTextSize(1);
		}
		
		if(rec.getNeedleType().getNeedleSize().getMetric() > 0) {
			if(!vh.sizeTv.getText().equals("")) {
				vh.sizeTv.setText(vh.sizeTv.getText() + "\n");
			}
			//vh.metricSizeTv.setText(String.valueOf(rec.getNeedleType().getNeedleSize().getMetric()) + " mm");
			vh.sizeTv.setText(vh.sizeTv.getText() + String.valueOf(rec.getNeedleType().getNeedleSize().getMetric() + " mm"));
		}
		vh.lengthTv.setText(String.valueOf(rec.getNeedleType().getLength()) + " \"");
		vh.typeTv.setText(rec.getNeedleType().getType());
		if(rec.getComment() != null) {
			vh.countTv.setText(rec.getComment());
		}
		else {
			vh.countTv.setText("");
		}
		
		return row;
	}

	private static class ViewHolder {
		//TextView usSizeTv;
		//TextView metricSizeTv;
		TextView lengthTv;
		TextView typeTv;
		TextView countTv;
		TextView sizeTv;
	}

}
