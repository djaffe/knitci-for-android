package com.knitci.pocket.tools.needle.sizes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import android.annotation.SuppressLint;

@SuppressLint("UseSparseArrays")
public class NeedleSizes {
	
	private final HashMap<Integer, NeedleSize> idMap;
	
	public NeedleSizes() {
		idMap = new HashMap<Integer, NeedleSize>();
	}
	
	public void addNeedleSizes(ArrayList<NeedleSize> sizes) {
		Iterator<NeedleSize> it = sizes.iterator();
		while(it.hasNext()) {
			addNeedleSize(it.next());
		}
	}
	
	public synchronized void addNeedleSize(NeedleSize size) {
		idMap.put(size.getId(), size);
	}
	
	public NeedleSize getNeedleSize(int id) {
		return idMap.get(id);
	}
	
}
