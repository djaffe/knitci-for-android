package com.knitci.pocket.tools.needle.sizes;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class NeedleSize extends RavelryObject {

	private int id;
	private String hook;
	private String us;
	private float metric;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the hook
	 */
	public String getHook() {
		return hook;
	}
	/**
	 * @param hook the hook to set
	 */
	public void setHook(String hook) {
		this.hook = hook;
	}
	/**
	 * @return the us
	 */
	public String getUs() {
		return us;
	}
	/**
	 * @param us the us to set
	 */
	public void setUs(String us) {
		this.us = us;
	}
	/**
	 * @return the metric
	 */
	public float getMetric() {
		return metric;
	}
	/**
	 * @param metric the metric to set
	 */
	public void setMetric(float metric) {
		this.metric = metric;
	}
	
	
	
}
