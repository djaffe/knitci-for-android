package com.knitci.pocket.tools.needle.types;

import java.util.ArrayList;
import java.util.HashMap;

import android.util.SparseArray;

public class Needles {

	private final HashMap<Integer, Needle> needleMap;
	
	public Needles() {
		needleMap = new HashMap<Integer, Needle>();
	}
	
	public synchronized void addNeedle(Needle needle) {
		needleMap.put(needle.getId(), needle);
	}
	
	public Needle getNeedleType(int needleTypeId) {
		return needleMap.get(needleTypeId);
	}

}
