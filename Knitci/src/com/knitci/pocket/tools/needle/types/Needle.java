package com.knitci.pocket.tools.needle.types;

import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.tools.needle.sizes.NeedleSize;

public class Needle extends RavelryObject {

	private int id;
	private String type;
	private int needleSizeId;
	private NeedleSize needleSize;
	private String description;
	private String name;
	private String metricName;
	private int length;
	
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	
	/**
	 * @return the needleSize
	 */
	public NeedleSize getNeedleSize() {
		return needleSize;
	}
	/**
	 * @param needleSize the needleSize to set
	 */
	public void setNeedleSize(NeedleSize needleSize) {
		this.needleSize = needleSize;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the needleSizeId
	 */
	public int getNeedleSizeId() {
		return needleSizeId;
	}
	/**
	 * @param needleSizeId the needleSizeId to set
	 */
	public void setNeedleSizeId(int needleSizeId) {
		this.needleSizeId = needleSizeId;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the metricName
	 */
	public String getMetricName() {
		return metricName;
	}
	/**
	 * @param metricName the metricName to set
	 */
	public void setMetricName(String metricName) {
		this.metricName = metricName;
	}
	/**
	 * @return the length
	 */
	public int getLength() {
		return length;
	}
	/**
	 * @param length the length to set
	 */
	public void setLength(int length) {
		this.length = length;
	}
	
	
	
}
