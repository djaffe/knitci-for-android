package com.knitci.pocket.tools.needle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;

import android.annotation.SuppressLint;
import com.knitci.pocket.tools.needle.sizes.NeedleSize;
import com.knitci.pocket.tools.needle.sizes.NeedleSizes;
import com.knitci.pocket.tools.needle.types.Needle;
import com.knitci.pocket.tools.needle.types.Needles;

@SuppressLint("UseSparseArrays")
public class NeedleDictionary {

	//private final NeedleSizes sizes;
	private HashMap<Integer, NeedleSize> sizes;
	private ArrayList<NeedleSize> sizesOrdered;
	private HashMap<Integer, Needle> dumbNeedles;
	private HashMap<String, HashMap<Float, HashMap<Integer, Integer>>> needles;
	
	public NeedleDictionary() {
		sizes = new HashMap<Integer, NeedleSize>();
		needles = new HashMap<String, HashMap<Float,HashMap<Integer,Integer>>>();
		dumbNeedles = new HashMap<Integer, Needle>();
	}
	
	public void addNeedleSizes(ArrayList<NeedleSize> sizes) {
		Iterator<NeedleSize> it = sizes.iterator();
		while(it.hasNext()) {
			NeedleSize n = it.next();
			this.sizes.put(n.getId(), n);
		}
		
		sizesOrdered = sizes;

		class CustomComparator implements Comparator<NeedleSize> {
		    @Override
		    public int compare(NeedleSize o1, NeedleSize o2) {
		    	float change1 = o1.getMetric();
		    	float change2 = o2.getMetric();
		    	if (change1 < change2) return -1;
		    	if (change1 > change2) return 1;
		    	return 0;
		    }
		}
		
		Collections.sort(sizesOrdered, new CustomComparator());
		
	}
	
	public void addNeedles(ArrayList<Needle> needles) {
		Iterator<Needle> it = needles.iterator();
		while(it.hasNext()) {
			Needle needle = it.next();
			if(this.needles.get(needle.getType()) == null) {
				this.needles.put(needle.getType(), new HashMap<Float, HashMap<Integer,Integer>>());
			}
			
			//int sizeId = sizes.get(needle.getType()).getId();
			float f = sizes.get(needle.getNeedleSizeId()).getMetric();
			if(this.needles.get(needle.getType()).get(f) == null) {
				this.needles.get(needle.getType()).put(f, new HashMap<Integer, Integer>());
			}
			
			int l = needle.getLength();
			this.needles.get(needle.getType()).get(f).put(l, needle.getId());
			dumbNeedles.put(needle.getId(), needle);
			needle.setNeedleSize(sizes.get(needle.getNeedleSizeId()));
		}
	}
	
	public NeedleSize getNeedleSize(int needleSizeId) {
		return sizes.get(needleSizeId);
	}
	
	public Needle getNeedleType(int needleTypeId) {
		return dumbNeedles.get(needleTypeId);
	}
	
	public ArrayList<NeedleSize> getSortedSize() {
		return sizesOrdered;
	}
}
