package com.knitci.pocket.tools;

import java.util.ArrayList;
import java.util.Iterator;


import android.content.Context;

import com.knitci.pocket.ravelry.data.object.impl.RavelryNeedleRecord;
import com.knitci.pocket.tools.needle.NeedleDictionary;
import com.knitci.pocket.tools.needle.sizes.NeedleSize;
import com.knitci.pocket.tools.needle.types.Needle;

public class ToolsManager {
	
	private ToolsCache toolsCache;
	public final NeedleDictionary needleDictionary;
	private boolean dictionaryReady;

	public ToolsManager(Context context) {
		toolsCache = new ToolsCache();
		needleDictionary = new NeedleDictionary();
		dictionaryReady = false;
	}
	
	public void addSizesToDictionary(ArrayList<NeedleSize> sizes) {
		needleDictionary.addNeedleSizes(sizes);
	}
	
	public void addNeedlesToDictionary(ArrayList<Needle> needles) {
		needleDictionary.addNeedles(needles);
		dictionaryReady = true;
	}

	public void addNeedleRecords(ArrayList<RavelryNeedleRecord> needleRecords) {
		Iterator<RavelryNeedleRecord> it = needleRecords.iterator();
		while(it.hasNext()) {
			RavelryNeedleRecord record = it.next();
			record.setNeedleType(needleDictionary.getNeedleType(record.getNeedleTypeId()));
			toolsCache.addRecord(record);
		}
	}
	
	public boolean isDictionaryReady() {
		return dictionaryReady;
	}
	
	public ArrayList<RavelryNeedleRecord> getNeedles(NeedleRecordCriteria criteria) {
		return toolsCache.getNeedles(criteria.getTypes(), criteria.getSizes(), criteria.getLengths());
	}
}
