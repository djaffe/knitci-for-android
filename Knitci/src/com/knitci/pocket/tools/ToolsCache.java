package com.knitci.pocket.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import android.annotation.SuppressLint;
import com.knitci.pocket.ravelry.data.object.impl.RavelryNeedleRecord;

@SuppressLint("UseSparseArrays")
public class ToolsCache {

	private HashMap<Integer, RavelryNeedleRecord> recordHolder;
	private HashMap<String, HashMap<Float, HashMap<Integer, ArrayList<Integer>>>> needleMapping;

	public ToolsCache() {
		recordHolder = new HashMap<Integer, RavelryNeedleRecord>();
		needleMapping = new HashMap<String, HashMap<Float,HashMap<Integer,ArrayList<Integer>>>>();
	}

	public void addRecords(ArrayList<RavelryNeedleRecord> records) {
		Iterator<RavelryNeedleRecord> it = records.iterator();
		while(it.hasNext()) {
			addRecord(it.next());
		}
	}

	public synchronized void addRecord(RavelryNeedleRecord record) {
		recordHolder.put(record.getId(), record);

		String type = record.getNeedleType().getType();
		Float metric = record.getNeedleType().getNeedleSize().getMetric();
		Integer len = record.getNeedleType().getLength();

		if(needleMapping.get(type)== null) {
			needleMapping.put(record.getNeedleType().getType(), new HashMap<Float, HashMap<Integer,ArrayList<Integer>>>());
		}

		if(needleMapping.get(type).get(metric) == null) {
			needleMapping.get(type).put(metric, new HashMap<Integer, ArrayList<Integer>>());
		}

		if(needleMapping.get(type).get(metric).get(len) == null) {
			needleMapping.get(type).get(metric).put(len, new ArrayList<Integer>());
		}

		needleMapping.get(type).get(metric).get(len).add(record.getId());
	}

	// TODO: Add a timer here - possibly need to move this to a thread?
	public ArrayList<RavelryNeedleRecord> getNeedles(ArrayList<String> types, ArrayList<Float> sizes, ArrayList<Integer> lengths) {
		ArrayList<RavelryNeedleRecord> rec = new ArrayList<RavelryNeedleRecord>();

		Iterator<Entry<String, HashMap<Float, HashMap<Integer, ArrayList<Integer>>>>> it = needleMapping.entrySet().iterator();
		while(it.hasNext()) {
			Entry<String, HashMap<Float, HashMap<Integer, ArrayList<Integer>>>> typeEntry = it.next();
			String type = typeEntry.getKey();
			if(types != null && !types.isEmpty() && !types.contains(type)) {
				continue;
			}
			Iterator<Entry<Float, HashMap<Integer, ArrayList<Integer>>>> it2 = typeEntry.getValue().entrySet().iterator();
			while(it2.hasNext()) {
				Entry<Float, HashMap<Integer, ArrayList<Integer>>> sizeEntry = it2.next();
				Float size = sizeEntry.getKey();
				if(sizes != null && !sizes.isEmpty() && !sizes.contains(size)) {
					continue;
				}
				Iterator<Entry<Integer, ArrayList<Integer>>> it3 = sizeEntry.getValue().entrySet().iterator();
				while(it3.hasNext()) {
					Entry<Integer, ArrayList<Integer>> lengthEntry = it3.next();
					Integer len = lengthEntry.getKey();
					if(lengths != null && !lengths.isEmpty() && !lengths.contains(len)) {
						continue;
					}
					Iterator<Integer> it4 = lengthEntry.getValue().iterator();
					while(it4.hasNext()) {
						rec.add(recordHolder.get(it4.next()));
					}
				}
			}
		}
		return rec;
	}

}
