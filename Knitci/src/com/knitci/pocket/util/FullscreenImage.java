package com.knitci.pocket.util;

import object.KnitciActivity;
import uk.co.senab.photoview.PhotoView;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

import com.actionbarsherlock.app.SherlockActivity;
import com.android.volley.toolbox.ImageLoader;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;

public class FullscreenImage extends SherlockActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fullscreen_image);
		getSupportActionBar().hide();
		String url = getIntent().getExtras().getString("url");
		
		PhotoView pv = (PhotoView) findViewById(R.id.fullimage_photoview);
		ImageLoader il = KnitciApplication.class.cast(getApplication()).getImageLoader();
		il.get(url, ImageLoader.getImageListener(pv, R.drawable.ic_action_overflow, R.drawable.ic_action_overflow));
	}


}
