package com.knitci.pocket.util;

import java.util.List;

import org.scribe.model.Token;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;

/**
 * A class for accessing preferences. All methods should be static. 
 * @author DJ021930
 */
public class KnitciPreferences {
	
	public static final String PREFS_NAME = "KNITCIPREFS";
	public static final String TOKEN_TOKEN = "TOKEN";
	public static final String TOKEN_SECRET = "SECRET";

	/**
	 * Saves a token for later use. 
	 * @param token String The token's key. 
	 * @param secret String The token's secret key. 
	 * @param context {@link Context} Context to use for accessing the {@link SharedPreferences}. 
	 * @return boolean True or false if the save succeeded. 
	 */
	public static boolean saveToken(String token, String secret, Context context) {
		if(token.isEmpty() || secret.isEmpty() || context == null) {
			return false;
		}
		
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(TOKEN_TOKEN, token);
		editor.putString(TOKEN_SECRET, secret);
		editor.commit();
		
		return true;
	}
	
	/**
	 * Deletes the token information. 
	 * @param context {@link Context} Context to use for accessing the {@link SharedPreferences}. 
	 */
	public static void deleteToken(Context context) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.remove(TOKEN_TOKEN);
		editor.remove(TOKEN_SECRET);
		editor.commit();
	}
	
	/**
	 * Creates a {@link Token} based of previously saved token information. 
	 * @param context {@link Context} Context to use for accessing the {@link SharedPreferences}. 
	 * @return {@link Token} A token for OAuth. Can be null if a previous token was never saved. 
	 */
	public static Token getToken(Context context) {
		Token token = null;
		String access = null;
		String secret = null;
		
		if(null != context) {
			SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
			access = settings.getString(TOKEN_TOKEN, null);
			secret = settings.getString(TOKEN_SECRET, null);
		}
	    
		if((null != access && null != secret) && (!access.isEmpty() && !secret.isEmpty())) {
			token = new Token(access, secret);
		}
		
		return token;
	}
	
	/**
	 * @param context used to check the device version and DownloadManager information
	 * @return true if the download manager is available
	 */	
	public static boolean isDownloadManagerAvailable(Context context) {
	    try {
	        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD) {
	            return false;
	        }
	        Intent intent = new Intent(Intent.ACTION_MAIN);
	        intent.addCategory(Intent.CATEGORY_LAUNCHER);
	        intent.setClassName("com.android.providers.downloads.ui", "com.android.providers.downloads.ui.DownloadList");
	        List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent,
	                PackageManager.MATCH_DEFAULT_ONLY);
	        return list.size() > 0;
	    } catch (Exception e) {
	        return false;
	    }
	}
	
}
