package com.knitci.pocket.util;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/**
 * Static class for various Json utilities. 
 * @author DJ021930
 */
public class JsonUtil {

	/**
	 * Checks to see if a given key is both present and populated. 
	 * @param obj {@link JsonObject} The json object. 
	 * @param key String The key name
	 * @return True or false if the key is present <strong>and</strong> populated. 
	 */
	public static boolean isKeyPopulated(final JsonObject obj, final String key) {
		JsonElement jEle = obj.get(key);
		boolean isNull = false;
		if(jEle == null) {
			isNull = true;
		}
		else if(jEle.isJsonNull()) {
			isNull = true;
		}
		return !isNull;
	}
	
	public static JsonObject createSimpleJsonObject(HashMap<String, Object> values) {
		JsonObject obj = new JsonObject();

		for (Entry<String, Object> entry : values.entrySet()) {
			String key = entry.getKey();
			if(entry.getValue() == null) {
				continue;
			}
			
			String value = String.valueOf(entry.getValue());
			if(value.isEmpty()) {
				continue;
			}
			
			boolean isInt = true;
			try {
				Integer.parseInt(value);
			} catch (Exception e) {
				isInt = false;
			}
			
			if(isInt) {
				obj.addProperty(key, Integer.parseInt(value));
			}
			else {
				obj.addProperty(key, value);
			}
		}
		
		return obj;
	}
	
	public static JsonArray createArray(ArrayList<String> strings) {
		JsonArray arr = new JsonArray();
		for(int i = 0; i < strings.size(); i++) {
			JsonPrimitive prim = new JsonPrimitive(strings.get(i));
			arr.add(prim);
		}
		return arr;
	}
	
}
