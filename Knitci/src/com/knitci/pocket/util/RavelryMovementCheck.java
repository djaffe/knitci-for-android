package com.knitci.pocket.util;

import com.knitci.pocket.social.forum.IMovementHandler;
import com.knitci.pocket.social.forum.ThreadIntentEnum;

import android.content.Context;
import android.text.Layout;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.TextView;

public class RavelryMovementCheck extends LinkMovementMethod {

	private IMovementHandler handler;

	public RavelryMovementCheck(IMovementHandler handler) { 
		this.handler = handler;
	}
	
	@Override
	public boolean onTouchEvent(TextView widget, Spannable buffer,
			MotionEvent event) {
		int x = (int) event.getX();
		int y = (int) event.getY();
		Layout layout = widget.getLayout();
		int line = layout.getLineForVertical(y);
		int off = layout.getOffsetForHorizontal(line, x);
		URLSpan[] spans = buffer.getSpans(off, off, URLSpan.class);
		
		if(spans.length != 0) {
			Log.d("Url", spans[0].getURL());
		}
		
		try {
			return super.onTouchEvent(widget, buffer, event);
		} catch (Exception e) {
			String url = spans[0].getURL();
			if(url.substring(0, 7).equalsIgnoreCase("/people")) {
				String user = url.substring(8, url.length());
				//handler.launchIntent(ThreadIntentEnum.PEOPLE, user);
			}
			else if(url.substring(0, 17).equalsIgnoreCase("/patterns/library")) {
				String pattern = url.substring(18, url.length());
				handler.patternClickedFromTextView(pattern);
			}
			else if(url.substring(0, 5).equalsIgnoreCase("/yarn")) {
				
			}
			else if(url.substring(0, 8).equalsIgnoreCase("/project")) {
				String part = url.substring(10, url.length());
				String[] parts = part.split("/");
				String user = parts[0];
				String perm = parts[1];
				handler.projectClickedFromTextView(user, perm);
			}
			else if(url.substring(0, 6).equalsIgnoreCase("/group")) {
				
			}
			else if(url.substring(0, 5).equalsIgnoreCase("/book")) {
				
			}
			return true;
		}
	}
}
