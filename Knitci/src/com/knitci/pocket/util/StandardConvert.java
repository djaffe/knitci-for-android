package com.knitci.pocket.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.JodaTimePermission;
import org.joda.time.Minutes;
import org.joda.time.Months;
import org.joda.time.Seconds;
import org.joda.time.Weeks;
import org.joda.time.Years;

/**
 * Static class for various non-ravelry conversions. 
 * @author DJ021930
 */
public class StandardConvert {

	/**
	 * Takes a date in string form and converts it to a {@link Date} object.
	 * @param string String A date in the form of either "yyyy/MM/dd HH:mm:ss" or "yyyy/MM/dd". 
	 * @return {@link Date} The resulting Date object. Can be null if the string was not of the two forms. 
	 */
	public static Date stringToDate(String string) {
		Date date = null;
		if(null == string || string.isEmpty()) {
			return null;
		}
		
		try {
			date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss Z", Locale.US).parse(string);
			return date;
		} catch (ParseException e) { }
		try {
			date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.US).parse(string);
			return date;
		} catch (ParseException e) { }
		
		try {
			date = new SimpleDateFormat("yyyy/MM/dd", Locale.US).parse(string);
			return date;
		} catch (ParseException e) { }
		return date;
	}
	
	public static String dateToString(Date date) {
		if(date == null) {
			return "";
		}
		
		String disp = "";
		disp += date.getMonth() + "/" + date.getDate() + "/" + date.getYear();
		return disp;
	}
	
	public static String getPastActivityDisplay(Date date) {
		if(null == date) {
			return "Unknown";
		}
		
		String display = "";
		DateTime lastDate = new DateTime(date);
		
		int seconds = Seconds.secondsBetween(lastDate, new DateTime()).getSeconds();
		int minutes = Minutes.minutesBetween(lastDate, new DateTime()).getMinutes();
		int hours = Hours.hoursBetween(lastDate, new DateTime()).getHours();
		int days = Days.daysBetween(lastDate, new DateTime()).getDays();
		int weeks = Weeks.weeksBetween(lastDate, new DateTime()).getWeeks();
		int months = Months.monthsBetween(lastDate, new DateTime()).getMonths();
		int years = Years.yearsBetween(lastDate, new DateTime()).getYears();
		
		if(years > 0) {
			display = String.valueOf(years) + " years ago";
		}
		else if(months > 0) {
			display = String.valueOf(months) + " months ago";
		}
		else if(weeks > 0) {
			display = String.valueOf(weeks) + " weeks ago";
		}
		else if(days > 0) {
			display = String.valueOf(days) + " days ago";
		}
		else if(hours > 0) {
			display = String.valueOf(hours) + " hours ago";
		}
		else if(minutes > 0) {
			display = String.valueOf(minutes) + " minutes ago";
		}
		else if(seconds > 0) {
			display = String.valueOf(seconds) + " seconds ago";
		}
		else {
			return "Just now";
		}
		
		return display;
	}
}
