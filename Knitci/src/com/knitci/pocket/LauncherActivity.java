package com.knitci.pocket;

import com.knitci.pocket.home.HomeActivity;
import com.knitci.pocket.ravelry.IRavelryAsync;
import com.knitci.pocket.ravelry.RavelryManager;
import com.knitci.pocket.ravelry.RavelryOAuthActivity;
import com.knitci.pocket.ravelry.data.access.RavelryAsyncTask.ERavelryTask;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryUser;

import android.opengl.Visibility;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * The LauncherActivity class. This should be displayed on startup to determine to proceed
 * into the main app or force the user to login first. Implements IRavelryAsync in order
 * to handle async tasks ran during the login. 
 * @author DJ021930
 */
public class LauncherActivity extends Activity implements IRavelryAsync {

	private static final int REQUEST_HOME 	= 1001;
	private static final int REQUEST_LOGIN	= 1000;
	private static final int RESULT_LOGOUT	= 2001;
	
	private boolean exitingKnitci = false;

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_knitci_launcher);
		KnitciApplication.class.cast(getApplication()).loadSplashImage(ViewGroup.class.cast(findViewById(R.id.launcher_mainlayout)), R.drawable.stitch);

		TextView welcome = (TextView) findViewById(R.id.launcher_welcome_to_knitci);
		Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/Pacifico.ttf");
		welcome.setTypeface(typeFace);

		KnitciApplication app = (KnitciApplication) getApplication();
		app.getRavelryManager().setListener(this);
		app.setupVolley();

	}

	/**
	 * The onResume will handle checking if logging in is needed or if we can verify an
	 * already existing token. 
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		KnitciApplication app = (KnitciApplication) getApplication();
		app.getRavelryManager().setListener(this);
		displayLoading(true);
		
		//KnitciApplication.class.cast(getApplication()).loadSplashImage(ViewGroup.class.cast(findViewById(R.id.launcher_mainlayout)), R.drawable.stitch);

		if(!app.getRavelryManager().isTokenPresent()) {
			app.getRavelryManager().loadExistingToken(getApplicationContext());
		}

		// Check if the loadExistingToken didn't find anything - same call as before. 
		if(!app.getRavelryManager().isTokenPresent()) {
			displayLoading(false);
			displayLoginNeeded(true);
		}
		else {
			displayLoginNeeded(false);
			displayLoading(true);
			if(!exitingKnitci) {
				app.getRavelryManager().verifyToken();
			}
		}
		super.onResume();
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.knitci_home, menu);
		return true;
	}

	/* (non-Javadoc)
	 * @see com.knitci.pocket.ravelry.IRavelryAsync#processFinish(com.knitci.pocket.ravelry.data.object.RavelryResponse)
	 */
	@Override
	public void processFinish(RavelryResponse response) {
		if(response.task == ERavelryTask.USERINFO) {
			RavelryUser user = (RavelryUser) response.result;
			KnitciApplication app = (KnitciApplication) getApplication();

			// I moved this into the ravelrymanager. need to change this to see if it was null
			// and tell the user auth failed. 
			// app.getRavelryManager().setRavelryUser(user);

			if(null != user && user.isValid()) {
				displayLoading(false);
				displayLoginNeeded(false);
				displayError(false);
				launchHomeActivity();
			}
			else {
				displayLoading(false);
				displayLoginNeeded(true);
				displayError(true);
			}
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
	}
	
	/**
	 * Displays or hides the login form on the launcher (splash) activity. Includes an oh shit
	 * if for some reason the layout cannot be found to prevent crashes. 
	 * @param display boolean Whether or not to show the login form. 
	 */
	protected void displayLoginNeeded(boolean display) {
		LinearLayout layout = (LinearLayout) findViewById(R.id.launcher_login_form_layout);
		if(null == layout) {
			Toast toast = Toast.makeText(getApplicationContext(), R.string.error_has_occurred, Toast.LENGTH_LONG);
			toast.show();
		}

		if(display) {
			layout.setVisibility(View.VISIBLE);
		}
		else {
			layout.setVisibility(View.GONE);
		}
	}

	/**
	 * Starts the {@link RavelryOAuthActivity} for a user to authorize with ravelry. A token will be
	 * set via {@link RavelryManager#setAndSaveToken(org.scribe.model.Token)} if the login is successful. 
	 * @param view View Necessary when linking this method to a button onclick. Can be null. 
	 */
	public void launchRavelryLoginActivity(View view) {
		Intent intent = new Intent(this, RavelryOAuthActivity.class);
		startActivityForResult(intent, REQUEST_LOGIN);
	}


	/**
	 * Starts the HomeActivity to proceed into the application. This should only
	 * be called after it is confirmed the user is logged in (Token is saved) via
	 * {@link RavelryManager#isTokenPresent()} and the token has been authorized
	 * as valid via {@link RavelryManager#verifyToken()}
	 */
	private void launchHomeActivity() {
		Intent intent = new Intent(this, HomeActivity.class);
		startActivityForResult(intent, REQUEST_HOME);
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_LOGOUT) {
			displayError(false);
			displayLoading(false);
			displayLoginNeeded(true);
		}
		else if(requestCode == REQUEST_HOME) {
			KnitciApplication app = (KnitciApplication) getApplication();
			// app.getRavelryManager().disconnectFromAllThreads();
			exitingKnitci = true;
			finish();
		}
		else if(requestCode == REQUEST_LOGIN) {

		}

		else {
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * Displays or hides the progress bar view. 
	 * @param display boolean True or false for show or hide. 
	 */
	private void displayLoading(boolean display) {
		ProgressBar bar = (ProgressBar) findViewById(R.id.launcher_progressBar);
		if(display) {
			bar.setVisibility(View.VISIBLE);
		}
		else {
			bar.setVisibility(View.GONE);
		}
	}
	
	/**
	 * Displays or hides the error textview. 
	 * @param display boolean True or false for show or hide. 
	 */
	private void displayError(boolean display) {
		TextView tv = (TextView) findViewById(R.id.launcher_error_text);
		if(display) {
			tv.setVisibility(View.VISIBLE);
		}
		else {
			tv.setVisibility(View.GONE);
		}
	}

}
