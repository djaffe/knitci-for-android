package com.knitci.pocket.favorites;

import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.R.layout;
import com.knitci.pocket.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class FavoriteListActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_favorite_list);
		KnitciApplication.class.cast(getApplicationContext()).getRavelryManager().retrieveFavorites("helixic", null, "", true, "");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.favorite_list, menu);
		return true;
	}

}
