package com.knitci.pocket.queue;

import java.util.ArrayList;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.data.object.impl.RavelryQueuedProject;
import com.knitci.pocket.util.StandardConvert;

public class QueueListAdapter extends BaseAdapter {

	public ArrayList<RavelryQueuedProject> queues;
	Context context;
	
	public QueueListAdapter(Context context) {
		this.context = context;
	}
	
	@Override
	public int getCount() {
		if(queues != null) {
			return queues.size();
		}
		return 0;
	}

	@Override
	public Object getItem(int arg0) {
		if(queues == null || queues.size() < arg0) {
			return null;
		}
		return queues.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.queuelist_item, arg2, false);
		
		
		return rowView;
	}
	
	static class ViewHolder {
		//ImageView img;
		Spinner spin;
		NetworkImageView img;
	}

}
