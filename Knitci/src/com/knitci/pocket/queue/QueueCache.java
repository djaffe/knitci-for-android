package com.knitci.pocket.queue;

import java.util.ArrayList;
import java.util.HashMap;

import com.knitci.pocket.ravelry.data.object.impl.RavelryQueuedProject;

public class QueueCache {

	HashMap<Integer, RavelryQueuedProject> queueHolder;
	HashMap<String, ArrayList<Integer>> userQueues;
	
	public QueueCache() {
		queueHolder = new HashMap<Integer, RavelryQueuedProject>();
		userQueues = new HashMap<String, ArrayList<Integer>>();
	}
	
	public void addQueues(ArrayList<RavelryQueuedProject> queues) {
		for(int i = 0; i < queues.size(); i++) {
			addQueue(queues.get(i));
		}
	}
	
	public void addQueue(RavelryQueuedProject queue) {
		queueHolder.put(queue.getQueueId(), queue);
		
		if(userQueues.get(queue.getUsername()) == null) {
			userQueues.put(queue.getUsername(), new ArrayList<Integer>());
		}
		
		userQueues.get(queue.getUsername()).add(queue.getQueueId());
	}
	
	public ArrayList<RavelryQueuedProject> getQueues(String username) {
		ArrayList<Integer> ids = userQueues.get(username);
		ArrayList<RavelryQueuedProject> queues = new ArrayList<RavelryQueuedProject>();
		
		if(ids == null || ids.isEmpty()) {
			return queues;
		}
		
		for(int i = 0; i < ids.size(); i++) {
			queues.add(queueHolder.get(ids.get(i)));
		}
		return queues;
	}
	
	public RavelryQueuedProject getQueue(int queueId) {
		return queueHolder.get(queueId);
	}
	
}
