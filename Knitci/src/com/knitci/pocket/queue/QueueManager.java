package com.knitci.pocket.queue;

import java.util.ArrayList;

import android.content.Context;

import com.knitci.pocket.ravelry.data.object.impl.RavelryQueuedProject;

public class QueueManager {

	private Context context;
	private final QueueCache cache;
	
	public QueueManager(Context context) {
		this.context = context;
		cache = new QueueCache();
	}
	
	public void addQueue(RavelryQueuedProject queue) {
		if(cache.getQueue(queue.getQueueId()) != null) {
			mergeQueue(queue);
		}
		else {
			cache.addQueue(queue);
		}
	}
	
	public void addQueues(ArrayList<RavelryQueuedProject> queues) {
		cache.addQueues(queues);
	}
	
	private void mergeQueue(RavelryQueuedProject fullQueue) {
		RavelryQueuedProject existingQueue = cache.getQueue(fullQueue.getQueueId());
	}
	
	public ArrayList<RavelryQueuedProject> getUserQueues(String username) {
		return cache.getQueues(username);
	}
	
	public RavelryQueuedProject getQueue(int queueId) {
		return cache.getQueue(queueId);
	}
	
}
