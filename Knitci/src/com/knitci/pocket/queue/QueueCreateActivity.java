package com.knitci.pocket.queue;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.IRavelryAsync;
import com.knitci.pocket.ravelry.RavelryManager;
import com.knitci.pocket.ravelry.data.access.RavelryAsyncTask.ERavelryTask;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPattern;

import edittextloader.EditTextLoader;
import edittextloader.EditTextLoaderListener;

public class QueueCreateActivity extends Activity implements EditTextLoaderListener, IRavelryAsync, OnClickListener {

	RavelryManager ravelryManager;
	int patternId;
	String patternName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_queue_create);
		patternId = 0;
		ravelryManager = KnitciApplication.class.cast(getApplication()).getRavelryManager();
		ravelryManager.setListener(this);
		try { 
			this.patternId = (Integer) getIntent().getExtras().get("PATTERNID");
			this.patternName = (String) getIntent().getExtras().get("PATTERNNAME");
		} catch (Exception e) { }
		EditTextLoader etl = (EditTextLoader) findViewById(R.id.queuecreate_edittextloaderpattern);
		etl.setListener(this);
		
		Button submit = (Button) findViewById(R.id.queuecreate_btn_submit);
		submit.setOnClickListener(this);

		if(patternId > 0) {
			etl.prepopulateEditText(patternName);
			showFreetextPattern(false);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.queue_create, menu);
		return true;
	}

	@Override
	public void searchTriggered(String searchString) {
		ravelryManager.retrievePatternsSimple(searchString, 0, 20);
	}

	@Override
	public void processFinish(RavelryResponse response) {
		if(response.task == ERavelryTask.PATTERNSEXT) {
			RavelryCollection coll = (RavelryCollection) response.result;
			ArrayList<RavelryPattern> patterns = (ArrayList<RavelryPattern>)(ArrayList<?>)coll.getCollection();
			EditTextLoader etl = (EditTextLoader) findViewById(R.id.queuecreate_edittextloaderpattern);
			etl.searchFinished();
			if(patterns != null && patterns.size() > 0) {
				etl.populateList(patterns);
				showFreetextPattern(false);
			}
			else {
				etl.hideList();
				showFreetextPattern(true);
			}
		}
		else if(response.task == ERavelryTask.QUEUECREATE) {
			finish();
		}
	}

	private void showFreetextPattern(boolean show) {
		EditText des = (EditText) findViewById(R.id.queuecreate_designer_et);
		EditText sou = (EditText) findViewById(R.id.queuecreate_source_et);
		EditText url = (EditText) findViewById(R.id.queuecreate_url_et);
		if(!show) {
			des.setVisibility(View.GONE);
			sou.setVisibility(View.GONE);
			url.setVisibility(View.GONE);
		}
		else {
			des.setVisibility(View.VISIBLE);
			sou.setVisibility(View.VISIBLE);
			url.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onClick(View arg0) {
		if(arg0.getId() == R.id.queuecreate_btn_submit) {
			ravelryManager.submitCreateQueueFromPattern(patternId, 0, null, 2, null, "Maypah", null, null, 0);
		}
	}

}
