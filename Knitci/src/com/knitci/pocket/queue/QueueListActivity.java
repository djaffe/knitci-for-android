package com.knitci.pocket.queue;

import java.util.ArrayList;

import object.KnitciActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.RavelryManager;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryQueuedProject;

public class QueueListActivity extends KnitciActivity implements OnItemClickListener {

	String username;
	QueueListAdapter adapter;
	RavelryManager ravelryManager;
	QueueManager queueManager;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_queue_list);
		setupSlidingMenu();
		stealAsyncListening();
		ravelryManager = KnitciApplication.class.cast(getApplication()).getRavelryManager();
		queueManager = KnitciApplication.class.cast(getApplication()).getQueueManager();

		if(getIntent().getExtras() != null) {
			username = getIntent().getExtras().getString("USERNAME");
		}
		if(username == null || username.isEmpty()) {
			username = ravelryManager.getRavelryUser().getUsername();
		}
		
		if(queueManager.getUserQueues(username).isEmpty()) {
			ravelryManager.retrieveUserQueue(username, -1, null, 1);
		}
		else {
			populateQueues(queueManager.getUserQueues(username));
		}
		
		ListView lv = (ListView) findViewById(R.id.queuelist_listview);
		
	}

	public void populateQueues(ArrayList<RavelryQueuedProject> queues) {
		ListView lv = (ListView) findViewById(R.id.queuelist_listview);
		if(adapter == null) {
			adapter = new QueueListAdapter(getApplicationContext());
			lv.setAdapter(adapter);
			lv.setOnItemClickListener(this);
		}
		adapter.queues = queues;
		adapter.notifyDataSetChanged();
	}
	
	@Override
	public void processFinish(RavelryResponse response) {
		switch(response.task) {
		case QUEUELIST:
			populateQueues(queueManager.getUserQueues(username));
			break;
			
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		RavelryQueuedProject queue = adapter.queues.get(arg2);
	}

}
