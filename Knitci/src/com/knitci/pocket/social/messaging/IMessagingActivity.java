package com.knitci.pocket.social.messaging;

public interface IMessagingActivity {
	void readyToPopulateMessageThreads();
}
