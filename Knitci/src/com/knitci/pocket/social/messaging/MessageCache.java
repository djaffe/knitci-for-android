package com.knitci.pocket.social.messaging;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import com.knitci.pocket.ravelry.data.object.impl.RavelryMessage;
import com.knitci.pocket.social.messaging.util.MessagingUtil;

public class MessageCache {

	private LinkedHashMap<MessageThread, LinkedList<Integer>> threads;
	private ConcurrentHashMap<Integer, RavelryMessage> dumbMessageMap;
	private TreeMap<Long, Integer> dateSortedMap;
	public int currentUserId;

	public MessageCache() {
		threads = new LinkedHashMap<MessageThread, LinkedList<Integer>>();
		dumbMessageMap = new ConcurrentHashMap<Integer, RavelryMessage>();
		dateSortedMap = new TreeMap<Long, Integer>();
	}
	
	public void emptyCache() {
		threads.clear();
		dumbMessageMap.clear();
		dateSortedMap.clear();
	}

	public synchronized void addMessagesToDumbHolder(ArrayList<RavelryMessage> messages) {
		if(messages == null || messages.isEmpty()) {
			return;
		}

		Iterator<RavelryMessage> it = messages.iterator();
		while(it.hasNext()) {
			RavelryMessage message = it.next();
			dumbMessageMap.put(message.getMessageId(), message);
		}
	}

	public synchronized void addCompletedMessageToDumbHolder(RavelryMessage message) {
		if(message == null) {
			return;
		}
		RavelryMessage existing = dumbMessageMap.get(message.getMessageId());
		if(existing == null) {
			dumbMessageMap.put(message.getMessageId(), message);
			return;
		}
		existing.setBodyQueried(true);
		existing.setHtmlContent(message.getHtmlContent());
		
		message.setBodyQueried(true);
		
	}

	public synchronized void generateThreads() {
		for (Entry<Integer, RavelryMessage> entry : dumbMessageMap.entrySet()) {
			int messageId = entry.getKey();
			RavelryMessage message = entry.getValue();
			dateSortedMap.put(message.getMessageDtTm().getTime(), messageId);
		}
		Map<Long, Integer> newestToOldest = dateSortedMap.descendingMap();

		for(Entry<Long, Integer> entry : newestToOldest.entrySet()) {
			RavelryMessage message = dumbMessageMap.get(entry.getValue());
			putMessageInThread(message);
		}
	}

	public synchronized void putMessageInThread(RavelryMessage message) {
		for(Entry<MessageThread, LinkedList<Integer>> entry : threads.entrySet()) {
			MessageThread thread = entry.getKey();
			if(MessagingUtil.doesMessageBelongToThread(message, thread, currentUserId)) {
				entry.getValue().addFirst(message.getMessageId());
				return;
			}
		}

		MessageThread thread = new MessageThread();
		thread.setUserId(MessagingUtil.getOtherUser(message, currentUserId));
		thread.setSubject(MessagingUtil.getCleanedSubject(message.getSubject()));
		LinkedList<Integer> newList = new LinkedList<Integer>();
		newList.addFirst(message.getMessageId());
		threads.put(thread, newList);
	}

	public int getTotalThreads() {
		return threads.size();
	}

	public ArrayList<MessageThread> getDumbMessageThreads() {
		ArrayList<MessageThread> arr = new ArrayList<MessageThread>();
		Iterator<MessageThread> it = threads.keySet().iterator();
		while(it.hasNext()) {
			MessageThread thread = it.next();
			arr.add(thread);
		}
		return arr;
	}
	
	public ArrayList<RavelryMessage> getMessages(LinkedList<Integer> linkedList) {
		ArrayList<RavelryMessage> messages = new ArrayList<RavelryMessage>();
		Iterator<Integer> it = linkedList.iterator();
		while(it.hasNext()) {
			int messageId = it.next();
			messages.add(dumbMessageMap.get(messageId));
		}
		return messages;
	}
	
	public LinkedList<Integer> getThreadMessageIds(MessageThread threadObj) {
		
		for(Entry<MessageThread, LinkedList<Integer>> entry : threads.entrySet()) {
			MessageThread thread = entry.getKey();
			if(threadObj.getSubject().equals(thread.getSubject())
					&& threadObj.getUserId() == thread.getUserId()) {
				return entry.getValue();
			}
		}
		return null;
	}
}
