package com.knitci.pocket.social.messaging;

import java.util.ArrayList;

import com.knitci.pocket.R;
import com.knitci.pocket.util.StandardConvert;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MessagingAdapter extends BaseAdapter {

	Context context;
	ArrayList<MessageThread> messageThreads;
	
	public MessagingAdapter(Context context, ArrayList<MessageThread> messageThreads) {
		this.context = context;
		this.messageThreads = messageThreads;
	}
	
	@Override
	public int getCount() {
		return messageThreads.size();
	}

	@Override
	public Object getItem(int arg0) {
		return messageThreads.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.messagelist_thread_item, arg2, false);
		TextView tvUsername = (TextView) rowView.findViewById(R.id.messagelist_thread_username);
		TextView tvSubject = (TextView) rowView.findViewById(R.id.messagelist_thread_subject);
		tvUsername.setText(String.valueOf(messageThreads.get(arg0).getUserId()));
		tvSubject.setText(messageThreads.get(arg0).getSubject());
		return rowView;
	}

}
