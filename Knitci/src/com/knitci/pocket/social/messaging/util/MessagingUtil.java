package com.knitci.pocket.social.messaging.util;

import com.knitci.pocket.ravelry.data.object.impl.RavelryMessage;
import com.knitci.pocket.social.messaging.MessageThread;

public class MessagingUtil {

	public static boolean doesMessageBelongToThread(RavelryMessage message, MessageThread thread, int currentUser) {
		return (doSubjectsMatch(message, thread) && getOtherUser(message, currentUser) == thread.getUserId());
	}
	
	public static boolean doPersonMatch(RavelryMessage message, MessageThread thread) {
		return false;
	}
	
	public static boolean doSubjectsMatch(RavelryMessage message, MessageThread thread) {
		String messageSubject = message.getSubject();
		String messageClean = messageSubject.toLowerCase().replace("re:", "").trim();
		String threadClean = thread.getSubject().toLowerCase().trim();
		
		return messageClean.equals(threadClean);
	}
		
	public static String getCleanedSubject(String subject) {
		return subject.toLowerCase().replace("re:", "").trim();
	}
	
	public static int getOtherUser(RavelryMessage message, int currentUser) {
		int recId = message.getRecipient().getId();
		int sendId = message.getSender().getId();
		if(currentUser == recId) {
			return sendId;
		}
		return recId;
	}
	
	public static boolean isSentMessage(RavelryMessage message) {
		return message.getMessageType().equals("sent_message");
	}
	
	public static boolean isReceivedMessage(RavelryMessage message) {
		return message.getMessageType().equals("simple_message");
	}
	
	public static boolean isArchivedMessage(RavelryMessage message) {
		return false;
	}
}
