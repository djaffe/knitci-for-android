package com.knitci.pocket.social.messaging;

import java.util.ArrayList;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.data.object.impl.RavelryMessage;

public class MessagingThreadAdapter extends BaseAdapter {

	Context context;
	ArrayList<RavelryMessage> messages;
	
	public MessagingThreadAdapter(Context context, ArrayList<RavelryMessage> messages) {
		this.context = context;
		this.messages = messages;
	}
	
	@Override
	public int getCount() {
		return messages.size();
	}

	@Override
	public Object getItem(int arg0) {
		return messages.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.messagethread_message_item, arg2, false);
		TextView tvUsername = (TextView) rowView.findViewById(R.id.messagethread_item_username);
		TextView tvBody = (TextView) rowView.findViewById(R.id.messagethread_item_body);
		tvUsername.setText(messages.get(arg0).getUserToDisplay().getUsername());
		tvBody.setText(Html.fromHtml(messages.get(arg0).getHtmlContent()));
		return rowView;
	}

}
