package com.knitci.pocket.social.messaging;

import java.util.ArrayList;

import object.KnitciActivity;
import android.content.Intent;
import android.os.Bundle;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest.ErrorCode;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.IRavelryAsync;
import com.knitci.pocket.ravelry.RavelryManager;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryTopic;
import com.knitci.pocket.social.forum.ThreadActivity;
import com.knitci.pocket.social.messaging.manager.MessagingManager;

public class MessagingActivity extends KnitciActivity implements IRavelryAsync, AdListener, OnItemClickListener, OnScrollListener, IMessagingActivity {

	RavelryManager ravelryManager;
	MessagingManager messagingManager;
	MessagingAdapter adapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_messaging);
		stealAsyncListening();
		setupSlidingMenu();
		ravelryManager = KnitciApplication.class.cast(getApplicationContext()).getRavelryManager();
		messagingManager = KnitciApplication.class.cast(getApplicationContext()).getMessagingManager();
		messagingManager.attachMessageActivity(this);
		
		PullToRefreshListView pullToRefreshView = (PullToRefreshListView) findViewById(R.id.messagelist_listview);
		pullToRefreshView.setOnItemClickListener(this);
		pullToRefreshView.setMode(Mode.DISABLED);
		pullToRefreshView.setPullLabel("Pull to Refresh");
		pullToRefreshView.setOnRefreshListener(new OnRefreshListener<ListView>() {
		    @Override
		    public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		        // Do work to refresh the list here.
		    	messagingManager.emptyCache();
		        Toast toast = Toast.makeText(getApplicationContext(), "Refresh Triggered", Toast.LENGTH_SHORT);
		        toast.show();
		        KnitciApplication.class.cast(getApplicationContext()).getRavelryManager().retrieveMessages("inbox", 1, "");
		    }
		});
		
		if(messagingManager.getDumbMessageThreads().isEmpty()) {
			messagingManager.emptyCache();
			ravelryManager.retrieveMessages("inbox", 1, "");
		}
		else {
			populateMessageThreads(messagingManager.getDumbMessageThreads());
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		stealAsyncListening();
		messagingManager.attachMessageActivity(this);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		messagingManager.attachMessageActivity(null);
	}
	
	public void populateMessageThreads(ArrayList<MessageThread> threads) {
		PullToRefreshListView lv = PullToRefreshListView.class.cast(findViewById(R.id.messagelist_listview));
		if(adapter == null) {
			adapter = new MessagingAdapter(getApplicationContext(), threads);
			lv.setAdapter(adapter);
		}
		else {
			adapter.messageThreads = threads;
			adapter.notifyDataSetChanged();
		}
		lv.onRefreshComplete();
		lv.setMode(Mode.PULL_FROM_START);
	}
	
	@Override
	public void processFinish(RavelryResponse response) {
		super.processFinish(response);
		switch(response.task) {
		case MESSAGES:
			break;
		}
	}
	
	public void readyToPopulateMessageThreads() {
		populateMessageThreads(messagingManager.getDumbMessageThreads());
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		
		PullToRefreshListView lv = PullToRefreshListView.class.cast(findViewById(R.id.messagelist_listview));
		MessageThread thread = MessageThread.class.cast(lv.getRefreshableView().getItemAtPosition(position));
		
		Intent intent = new Intent(this, MessagingThreadActivity.class);
		intent.putExtra("MESSAGETHREAD", thread);
		startActivity(intent);
		
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.messaging, menu);
		return true;
	}

	@Override
	public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onDismissScreen(Ad arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFailedToReceiveAd(Ad arg0, ErrorCode arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLeaveApplication(Ad arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPresentScreen(Ad arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceiveAd(Ad arg0) {
		// TODO Auto-generated method stub
		
	}

}
