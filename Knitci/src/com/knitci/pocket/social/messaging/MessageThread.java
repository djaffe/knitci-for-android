package com.knitci.pocket.social.messaging;

import java.io.Serializable;

public class MessageThread implements Serializable {

	private static final long serialVersionUID = 3204384669851530417L;
	private String subject;
	private int userId;

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	
}
