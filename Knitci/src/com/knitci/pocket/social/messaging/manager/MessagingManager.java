package com.knitci.pocket.social.messaging.manager;

import java.util.ArrayList;
import java.util.LinkedList;

import android.content.Context;
import android.widget.Toast;

import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.ravelry.data.object.impl.RavelryMessage;
import com.knitci.pocket.social.messaging.IMessagingActivity;
import com.knitci.pocket.social.messaging.MessageCache;
import com.knitci.pocket.social.messaging.MessageThread;
import com.knitci.pocket.social.messaging.MessagingActivity;

public class MessagingManager {

	Context context;
	MessageCache messageCache = new MessageCache();
	IMessagingActivity activity;
	
	public MessagingManager(Context context) {
		this.context = context;
	}
	
	public void setCurrentUser() {
		messageCache.currentUserId = KnitciApplication.class.cast(context).getRavelryManager().getRavelryUser().getId();
	}
	
	public void addMessagesToDumbList(ArrayList<RavelryMessage> messages) {
		messageCache.addMessagesToDumbHolder(messages);
	}
	
	public void finishedAddingMessagesToDumbList() {
		messageCache.generateThreads();
		informUIMessageThreadsReady();
	}
	
	public void addedACompletedMessage() {
		informUIMessageThreadsReady();
	}
	
	private void informUIMessageThreadsReady() {
		if(activity != null) {
			activity.readyToPopulateMessageThreads();
		}
	}
	
	public ArrayList<MessageThread> getDumbMessageThreads() {
		return messageCache.getDumbMessageThreads();
	}
	
	public void attachMessageActivity(IMessagingActivity activity) {
		this.activity = activity;
	}
	
	public ArrayList<RavelryMessage> getMessages(LinkedList<Integer> linkedList) {
		return messageCache.getMessages(linkedList);
	}
	
	public LinkedList<Integer> getThreadMessageIds(MessageThread thread) {
		return messageCache.getThreadMessageIds(thread);
	}
	
	public void addCompletedMessage(RavelryMessage message) {
		messageCache.addCompletedMessageToDumbHolder(message);
	}
	
	public void emptyCache() {
		messageCache.emptyCache();
	}
}
