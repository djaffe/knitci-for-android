package com.knitci.pocket.social.messaging;

import java.util.ArrayList;
import java.util.Iterator;

import object.KnitciActivity;
import android.os.Bundle;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest.ErrorCode;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.IRavelryAsync;
import com.knitci.pocket.ravelry.RavelryManager;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryMessage;
import com.knitci.pocket.social.messaging.manager.MessagingManager;

public class MessagingThreadActivity extends KnitciActivity implements IRavelryAsync, AdListener, OnItemClickListener, OnScrollListener, IMessagingActivity {

	MessageThread thread;
	MessagingManager messagingManager;
	RavelryManager ravelryManager;
	MessagingThreadAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_messaging_thread);

		thread = MessageThread.class.cast(getIntent().getSerializableExtra("MESSAGETHREAD"));

		ravelryManager = KnitciApplication.class.cast(getApplicationContext()).getRavelryManager();
		messagingManager = KnitciApplication.class.cast(getApplicationContext()).getMessagingManager();
		stealAsyncListening();
		setupSlidingMenu();
		messagingManager.attachMessageActivity(this);

		ArrayList<RavelryMessage> messages = messagingManager.getMessages(messagingManager.getThreadMessageIds(thread));
		if(!messages.isEmpty()) {
			populateMessages(messages);
		}

	}

	public void populateMessages(ArrayList<RavelryMessage> messages) {
		ListView lv = ListView.class.cast(findViewById(R.id.messagethread_listview));
		if(adapter == null) {
			adapter = new MessagingThreadAdapter(getApplicationContext(), messages);
			lv.setAdapter(adapter);
		}
		else {
			adapter.messages = messages;
			adapter.notifyDataSetChanged();
		}
		// Kick off getting the message body if it doesnt exist
		Iterator<RavelryMessage> it = messages.iterator();
		while(it.hasNext()) {
			RavelryMessage message = it.next();
			if(!message.isBodyQueried()) {
				ravelryManager.retrieveMessage(message.getMessageId());
			}
		}
		
		// TODO Send off a request to mark all messages as read
	}

	@Override
	public void processFinish(RavelryResponse response) {
		super.processFinish(response);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.messaging_thread, menu);
		return true;
	}

	@Override
	public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDismissScreen(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFailedToReceiveAd(Ad arg0, ErrorCode arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLeaveApplication(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPresentScreen(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onReceiveAd(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void readyToPopulateMessageThreads() {
		adapter.notifyDataSetChanged();
	}

}
