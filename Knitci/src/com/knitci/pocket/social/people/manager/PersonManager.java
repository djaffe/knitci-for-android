package com.knitci.pocket.social.people.manager;

import android.content.Context;

import com.knitci.pocket.ravelry.data.object.impl.RavelryUser;
import com.knitci.pocket.social.people.PersonCache;

public class PersonManager {

	Context context;
	PersonCache personCache;
	
	public PersonManager(Context context) {
		this.context = context;
		personCache = new PersonCache();
	}
	
	public RavelryUser getUser(String username) {
		return personCache.getUser(username);
	}
	
	public RavelryUser getUser(int userId) {
		return personCache.getUser(userId);
	}
	
	public String getUsername(int userId) {
		return personCache.getUsername(userId);
	}
	
	public void addUser(RavelryUser user) {
		personCache.addUser(user);
	}
	
	public boolean hasUser(int userId) {
		return personCache.hasUser(userId);
	}
	
	public boolean hasUser(String username) {
		return personCache.hasUser(username);
	}
	
}
