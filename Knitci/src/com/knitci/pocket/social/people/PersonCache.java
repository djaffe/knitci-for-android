package com.knitci.pocket.social.people;

import java.util.HashMap;
import java.util.LinkedHashMap;

import com.knitci.pocket.ravelry.data.object.impl.RavelryUser;

public class PersonCache {

	private HashMap<Integer, RavelryUser> users;
	private HashMap<String, Integer> usernameToUserid;
	
	public PersonCache() {
		users = new LinkedHashMap<Integer, RavelryUser>();
		usernameToUserid = new LinkedHashMap<String, Integer>();
	}
	
	public void addUser(RavelryUser user) {
		users.put(user.getId(), user);
		usernameToUserid.put(user.getUsername(), user.getId());
	}
	
	public RavelryUser getUser(int userId) {
		return users.get(userId);
	}
	
	public RavelryUser getUser(String username) {
		return users.get(usernameToUserid.get(username));
	}
	
	public String getUsername(int userId) {
		return users.get(userId).getUsername();
	}
	
	public int getUserId(String username) {
		return usernameToUserid.get(username);
	}
	
	public boolean hasUser(int userId) {
		return (users.get(userId) != null);
	}
	
	public boolean hasUser(String username) {
		return (usernameToUserid.get(username) != null);
	}
	
}
