package com.knitci.pocket.social.forum;

import java.util.ArrayList;

import object.KnitciActivity;
import android.content.Intent;
import android.os.Bundle;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest.ErrorCode;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.IRavelryAsync;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryForum;
import com.knitci.pocket.social.forum.manager.ForumManager;

/**
 * Activity class for displaying a list of Forums.
 * @author DJ021930
 */
public class ForumListActivity extends KnitciActivity implements IRavelryAsync, AdListener, OnItemClickListener {

	private ForumManager forumManager;
	private ForumListAdapter adapter;
	
	/*
	 * (non-Javadoc)
	 * @see object.KnitciActivity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		stealAsyncListening();
		setContentView(R.layout.activity_forum_list);
		setupSlidingMenu();
		forumManager = KnitciApplication.class.cast(getApplicationContext()).getForumManager();
		ListView lv = ListView.class.cast(findViewById(R.id.forumlist_listview));
		lv.setOnItemClickListener(this);
		
		// If we already have forum data, just use that. Otherwise, call out to Ravelry for ForumSets. 
		if(forumManager.getForums() == null || forumManager.getForums().isEmpty()) {
			KnitciApplication.class.cast(getApplicationContext()).getRavelryManager().retrieveForumSets();
		}
		else {
			populateForums(forumManager.getForums());
			forumManager.refreshForumsIfNecessary();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		super.onResume();
		stealAsyncListening();
		forumManager.refreshForumsIfNecessary();
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.forum_list, menu);
		return true;
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.forum_list_menu_refresh:
			forumManager.refreshForumSets();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
		
	}
	
	/**
	 * Takes a list of {@link RavelryForum} entities and updates the adapter with them. If the adapter
	 * has not yet been initialized, set it up and point the ListView to it. 
	 * @param forums ArrayList<RavelryForum> The list of forum objects. 
	 */
	private void populateForums(ArrayList<RavelryForum> forums) {
		if(adapter == null) {
			adapter = new ForumListAdapter(getApplicationContext(), forums);
			ListView lv = ListView.class.cast(findViewById(R.id.forumlist_listview));
			lv.setAdapter(adapter);
		}
		else {
			adapter.forums = forums;
			adapter.notifyDataSetChanged();
		}
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView, android.view.View, int, long)
	 */
	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
		ListView lv = ListView.class.cast(findViewById(R.id.forumlist_listview));
		RavelryForum forum = RavelryForum.class.cast(lv.getItemAtPosition(position));
		Toast toast = Toast.makeText(getApplicationContext(), forum.getName(), Toast.LENGTH_LONG);
		toast.show();
		
		Intent intent = new Intent(this, TopicListActivity.class);
		intent.putExtra("FORUMID", forum.getId());
		startActivity(intent);
	}

	/*
	 * (non-Javadoc)
	 * @see object.KnitciActivity#processFinish(com.knitci.pocket.ravelry.data.object.RavelryResponse)
	 */
	@Override
	public void processFinish(RavelryResponse response) {
		switch(response.task) {
		case FORUMSETS:
			populateForums(forumManager.getForums());
			break;
		default:
			break;
		}
		super.processFinish(response);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.google.ads.AdListener#onDismissScreen(com.google.ads.Ad)
	 */
	@Override
	public void onDismissScreen(Ad arg0) { }

	/*
	 * (non-Javadoc)
	 * @see com.google.ads.AdListener#onFailedToReceiveAd(com.google.ads.Ad, com.google.ads.AdRequest.ErrorCode)
	 */
	@Override
	public void onFailedToReceiveAd(Ad arg0, ErrorCode arg1) { }

	/*
	 * (non-Javadoc)
	 * @see com.google.ads.AdListener#onLeaveApplication(com.google.ads.Ad)
	 */
	@Override
	public void onLeaveApplication(Ad arg0) { }

	/*
	 * (non-Javadoc)
	 * @see com.google.ads.AdListener#onPresentScreen(com.google.ads.Ad)
	 */
	@Override
	public void onPresentScreen(Ad arg0) { }

	/*
	 * (non-Javadoc)
	 * @see com.google.ads.AdListener#onReceiveAd(com.google.ads.Ad)
	 */
	@Override
	public void onReceiveAd(Ad arg0) { }



}
