package com.knitci.pocket.social.forum;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.text.Html;
import android.text.Html.ImageGetter;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.data.object.impl.RavelryForumPost;
import com.knitci.pocket.util.RavelryMovementCheck;
import com.knitci.pocket.util.StandardConvert;

public class ThreadListAdapter extends BaseAdapter implements ImageGetter {

	Context context;
	ArrayList<RavelryForumPost> posts;
	TextView tempTVHolder;
	IMovementHandler handler;
	RavelryMovementCheck movementCheck;

	public ThreadListAdapter(Context context, ArrayList<RavelryForumPost> posts, IMovementHandler handler) {
		this.context = context;
		this.posts = posts;
		this.handler = handler;
		this.movementCheck = new RavelryMovementCheck(handler);
	}

	@Override
	public int getCount() {
		return posts.size();
	}

	@Override
	public Object getItem(int arg0) {
		return posts.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(final int arg0, View arg1, ViewGroup arg2) {
		View rowView = arg1;
		if(rowView == null) {
			
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.postlist_post_item, null);
			ViewHolder vh = new ViewHolder();
			vh.avatar = (ImageView) rowView.findViewById(R.id.postlist_post_avatar);
			vh.content = (TextView) rowView.findViewById(R.id.postlist_post_body);
			vh.replyto = (TextView) rowView.findViewById(R.id.postlist_post_reply);
			vh.timeago = (TextView) rowView.findViewById(R.id.postlist_post_timeago);
			vh.username = (TextView) rowView.findViewById(R.id.postlist_post_username);
			tempTVHolder = vh.content;
			rowView.setTag(vh);
		}

		ViewHolder vh = (ViewHolder) rowView.getTag();
		vh.content.setText(Html.fromHtml(posts.get(arg0).getBodyHtml(), this, null));
		//vh.content.setMovementMethod(LinkMovementMethod.getInstance());
		vh.content.setMovementMethod(movementCheck);
		//Linkify.addLinks(vh.content, Linkify.ALL);
		vh.username.setText(posts.get(arg0).getUser().getUsername());
		vh.timeago.setText(StandardConvert.getPastActivityDisplay(posts.get(arg0).getCreatedDtTm()) + " | " + 
				String.valueOf(posts.get(arg0).getPostNumber()));

		if(posts.get(arg0).getParentPostUser() == null) {
			Log.w("Parent", String.valueOf(posts.get(arg0).getPostNumber()) + " -> PARENT USER IS NULL");
		}
		else {
			Log.w("Parent", String.valueOf(posts.get(arg0).getPostNumber()) + " -> PARENT USER IS OKAY");
		}
		
		if(posts.get(arg0).getParentPostNumber() > 0) {
			vh.replyto.setText("in reply to " + posts.get(arg0).getParentPostUser().getUsername() + "'s post #" + String.valueOf(posts.get(arg0).getParentPostNumber()));
			vh.replyto.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					handler.fireThreadChain(posts.get(arg0).getPostNumber());
					
				}
			});
		}
		else {
			vh.replyto.setText("");
		}
		
		String url = posts.get(arg0).getUser().getPhotoUrl();
		if(url != null && !url.isEmpty()) {
			KnitciApplication.class.cast(context.getApplicationContext()).getAvatarLoader().get(url, 
					ImageLoader.getImageListener(vh.avatar, R.drawable.ic_launcher, R.drawable.ic_launcher));
		}
		return rowView;
	}

	@Override
	public Drawable getDrawable(String source) {
		LevelListDrawable d = new LevelListDrawable();
		Drawable empty = context.getResources().getDrawable(R.drawable.ic_launcher);
		d.addLevel(0, 0, empty);
		d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());


		//KnitciApplication.class.cast(context).getImageLoader().get(source, ImageLoader.getImageListener(d, R.drawable.ic_launcher, R.drawable.ic_launcher))

		new LoadImage(tempTVHolder).execute(source, d);

		return d;
	}

	static class ViewHolder {
		public TextView username;
		public TextView timeago;
		public TextView replyto;
		public ImageView avatar;
		public TextView content;
	}

	class LoadImage extends AsyncTask<Object, Void, Bitmap> {

		private LevelListDrawable mDrawable;
		private TextView tv;

		public LoadImage(TextView tv) {
			this.tv = tv;
		}

		@Override
		protected Bitmap doInBackground(Object... params) {
			String source = (String) params[0];
			mDrawable = (LevelListDrawable) params[1];
			if(source == null) {
				return null;
			}
			String check = source.substring(0,14);
			if(check.equals("/forum-images/")) {
				String newSource = "http://www.ravelry.com"+source;
				source = newSource;
			}
			check = source.substring(0,12);
			if(check.equals("/images/emo/")) {
				String newSource = "http://www.ravelry.com"+source;
				source = newSource;
			}

			Bitmap cachedBitmap = ThreadImageCache.getInstance().retrieve(source);
			if(null != cachedBitmap) {
				return cachedBitmap;
			}

			try {
				Log.d("ThreadListAdapter", "No picture found in cache.");
				InputStream is = new URL(source).openStream();
				Bitmap bitmap = BitmapFactory.decodeStream(is);
				ThreadImageCache.getInstance().cache(source, bitmap);
				return bitmap;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Bitmap bitmap) {
			if (bitmap != null) {
				BitmapDrawable d = new BitmapDrawable(bitmap);
				mDrawable.addLevel(1, 1, d);
				mDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
				mDrawable.setLevel(1);
				// i don't know yet a better way to refresh TextView
				// mTv.invalidate() doesn't work as expected
				CharSequence t = this.tv.getText();
				this.tv.setText(t);
			}
		}
	}

}
