package com.knitci.pocket.social.forum;

import java.util.ArrayList;

import object.KnitciActivity;
import android.content.Intent;
import android.os.Bundle;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest.ErrorCode;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.IRavelryAsync;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryForum;
import com.knitci.pocket.ravelry.data.object.impl.RavelryTopic;
import com.knitci.pocket.social.forum.manager.ForumManager;

public class TopicListActivity extends KnitciActivity implements IRavelryAsync, AdListener, OnItemClickListener, OnScrollListener {

	protected TopicListAdapter adapter;
	private ForumManager forumManager;
	private int forumId;
	private boolean loadingMore;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_topic_list);
		loadingMore = false;
		stealAsyncListening();
		setupSlidingMenu();
		forumManager = KnitciApplication.class.cast(getApplicationContext()).getForumManager();
		ListView lv = ListView.class.cast(findViewById(R.id.topiclist_listview));
		lv.setOnItemClickListener(this);
		lv.setOnScrollListener(this);
		Bundle extras = getIntent().getExtras();
		forumId = extras.getInt("FORUMID");

		if(forumManager.getTopics(forumId) == null || forumManager.getTopics(forumId).isEmpty()) {
			KnitciApplication.class.cast(getApplicationContext()).getRavelryManager().retrieveForumTopics(0, forumId);
		}
		else {
			populateTopics(forumManager.getTopics(forumId));
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		stealAsyncListening();
		forumManager.refreshTopicsIfNecessary(forumId);
	}

	private void populateTopics(ArrayList<RavelryTopic> topics) {
		if(topics == null) {
			return;
		}
		if(adapter == null) {
			adapter = new TopicListAdapter(getApplicationContext(), topics);
			ListView lv = ListView.class.cast(findViewById(R.id.topiclist_listview));
			lv.setAdapter(adapter);
		}
		else {
			adapter.topics = topics;
			adapter.notifyDataSetChanged();
		}
		loadingMore = false;
	}

	@Override
	public void processFinish(RavelryResponse response) {
		ListView lv = ListView.class.cast(findViewById(R.id.topiclist_listview));
		super.processFinish(response);
		switch(response.task) {
		case FORUMTOPICS:
		case FORUMTOPICSCONT:
			populateTopics(forumManager.getTopics(forumId));
			break;
		default:
			break;
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.topic_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.topic_list_menu_refresh:
			forumManager.refreshTopics(forumId);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
		ListView lv = ListView.class.cast(findViewById(R.id.topiclist_listview));
		RavelryTopic topic = RavelryTopic.class.cast(lv.getItemAtPosition(position));
		Toast toast = Toast.makeText(getApplicationContext(), topic.getTitle(), Toast.LENGTH_LONG);
		toast.show();

		Intent intent = new Intent(this, ThreadActivity.class);
		intent.putExtra("TOPICID", topic.getId());
		intent.putExtra("FORUMID", topic.getForumId());
		startActivity(intent);
	}

	@Override
	public void onDismissScreen(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFailedToReceiveAd(Ad arg0, ErrorCode arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLeaveApplication(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPresentScreen(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onReceiveAd(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {

		if (adapter == null) {
			return ;
		}

		if (adapter.getCount() == 0) {
			return ;
		}

		boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
		boolean morePages = (forumManager.getTopicMaxPage(forumId) > 1 && forumManager.getTopicLastLoadedPage(forumId) != forumManager.getTopicMaxPage(forumId));

		if(loadMore && !loadingMore && morePages) {
			loadingMore = true;

			ListView lv = ListView.class.cast(findViewById(R.id.topiclist_listview));
			
			KnitciApplication.class.cast(getApplicationContext()).getRavelryManager().retrieveForumTopics(forumManager.getTopicLastLoadedPage(forumId)+1, forumId);
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub

	}

}
