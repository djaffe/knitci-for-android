package com.knitci.pocket.social.forum;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.data.object.impl.RavelryForum;
import com.knitci.pocket.util.StandardConvert;

/**
 * An implementation of {@link BaseAdapter} for populating a {@link ListView} with {@link RavelryForum} entities. 
 * @author DJ021930
 */
public class ForumListAdapter extends BaseAdapter {

	Context context;
	ArrayList<RavelryForum> forums;

	public ForumListAdapter(Context context, ArrayList<RavelryForum> forums) {
		this.forums = forums;
		this.context = context;
	}

	@Override
	public int getCount() {
		return forums.size();
	}

	@Override
	public Object getItem(int arg0) {
		return forums.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.forumlist_forum_item, arg2, false);
		TextView tvName = (TextView) rowView.findViewById(R.id.forumlist_forum_item_name);
		TextView tvTimeago = (TextView) rowView.findViewById(R.id.forumlist_forum_activity);
		tvName.setText(forums.get(arg0).getName());
		tvTimeago.setText(StandardConvert.getPastActivityDisplay(forums.get(arg0).getLastPostDtTm()));
		return rowView;
	}
}
