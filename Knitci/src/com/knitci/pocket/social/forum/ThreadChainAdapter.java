package com.knitci.pocket.social.forum;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.text.Html;
import android.text.Html.ImageGetter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.data.object.impl.RavelryForumPost;
import com.knitci.pocket.social.forum.ThreadListAdapter.LoadImage;

public class ThreadChainAdapter extends BaseAdapter implements ImageGetter {
	TextView tempTVHolder;
	private Context context;
	private LinkedList<RavelryForumPost> posts;
	
	public ThreadChainAdapter(Context context, LinkedList<RavelryForumPost> posts) {
		this.context = context;
		this.posts = posts;
	}
	
	@Override
	public int getCount() {
		return posts.size();
	}

	@Override
	public Object getItem(int arg0) {
		return posts.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.postchain_post_item, arg2, false);
		TextView tvBody = (TextView) rowView.findViewById(R.id.postchain_post_body);
		tempTVHolder = tvBody;
		tvBody.setText(Html.fromHtml(posts.get(arg0).getBodyHtml(), this, null));

		return rowView;
	}
	

	@Override
	public Drawable getDrawable(String source) {
		LevelListDrawable d = new LevelListDrawable();
		Drawable empty = context.getResources().getDrawable(R.drawable.ic_launcher);
		d.addLevel(0, 0, empty);
		d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());

		
		//KnitciApplication.class.cast(context).getImageLoader().get(source, ImageLoader.getImageListener(d, R.drawable.ic_launcher, R.drawable.ic_launcher))
		
		new LoadImage(tempTVHolder).execute(source, d);

		return d;
	}

	class LoadImage extends AsyncTask<Object, Void, Bitmap> {

		private LevelListDrawable mDrawable;
		private TextView tv;

		public LoadImage(TextView tv) {
			this.tv = tv;
		}

		@Override
		protected Bitmap doInBackground(Object... params) {
			String source = (String) params[0];
			mDrawable = (LevelListDrawable) params[1];

			String check = source.substring(0,14);
			if(check.equals("/forum-images/")) {
				String newSource = "http://www.ravelry.com"+source;
				source = newSource;
			}
			check = source.substring(0,12);
			if(check.equals("/images/emo/")) {
				String newSource = "http://www.ravelry.com"+source;
				source = newSource;
			}

			Bitmap cachedBitmap = ThreadImageCache.getInstance().retrieve(source);
			if(null != cachedBitmap) {
				return cachedBitmap;
			}
			
			try {
				Log.d("ThreadListAdapter", "No picture found in cache.");
				InputStream is = new URL(source).openStream();
				Bitmap bitmap = BitmapFactory.decodeStream(is);
				ThreadImageCache.getInstance().cache(source, bitmap);
				return bitmap;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Bitmap bitmap) {
			if (bitmap != null) {
				BitmapDrawable d = new BitmapDrawable(bitmap);
				mDrawable.addLevel(1, 1, d);
				mDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
				mDrawable.setLevel(1);
				// i don't know yet a better way to refresh TextView
				// mTv.invalidate() doesn't work as expected
				CharSequence t = this.tv.getText();
				this.tv.setText(t);
			}
		}
	}

}
