package com.knitci.pocket.social.forum;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.joda.time.DateTime;

import android.util.Log;

import com.knitci.pocket.ravelry.data.object.impl.RavelryForum;
import com.knitci.pocket.ravelry.data.object.impl.RavelryForumPost;
import com.knitci.pocket.ravelry.data.object.impl.RavelryTopic;

public class PostHolder {

	// Entity holders
	private LinkedHashMap<Integer, RavelryForum> forumsHolder;
	private ConcurrentHashMap<Integer, LinkedHashMap<Integer, RavelryTopic>> topicsHolder;
	private ConcurrentHashMap<Integer, ConcurrentHashMap<Integer, LinkedHashMap<Integer, RavelryForumPost>>> postHolder;

	// Last loaded page maps
	private ConcurrentHashMap<Integer, Integer> topicsLastLoadedPage;
	private ConcurrentHashMap<Integer, ConcurrentHashMap<Integer, Integer>> postsLastLoadedPage;

	// Bounds map
	private ConcurrentHashMap<Integer, Bounds> topicsBounds; 

	// Max page maps
	private ConcurrentHashMap<Integer, Integer> topicsMaxPage;
	private ConcurrentHashMap<Integer, ConcurrentHashMap<Integer, Integer>> postsMaxPage;

	// Refresh times
	private ConcurrentHashMap<Integer, DateTime> topicsRefreshTimes;
	private ConcurrentHashMap<Integer, ConcurrentHashMap<Integer, DateTime>> postsRefreshTimes;
	private DateTime forumRefreshTime;

	public PostHolder() {
		forumRefreshTime = DateTime.now();

		// Entity holders
		forumsHolder = new LinkedHashMap<Integer, RavelryForum>();
		topicsHolder = new ConcurrentHashMap<Integer, LinkedHashMap<Integer,RavelryTopic>>();
		postHolder = new ConcurrentHashMap<Integer, ConcurrentHashMap<Integer,LinkedHashMap<Integer,RavelryForumPost>>>();

		// Last loaded page maps
		topicsLastLoadedPage = new ConcurrentHashMap<Integer, Integer>();
		postsLastLoadedPage = new ConcurrentHashMap<Integer, ConcurrentHashMap<Integer,Integer>>();

		// Bounds
		topicsBounds = new ConcurrentHashMap<Integer, PostHolder.Bounds>();

		// Max page maps
		topicsMaxPage = new ConcurrentHashMap<Integer, Integer>();
		postsMaxPage = new ConcurrentHashMap<Integer, ConcurrentHashMap<Integer,Integer>>();

		// Refresh times
		topicsRefreshTimes = new ConcurrentHashMap<Integer, DateTime>();
		postsRefreshTimes = new ConcurrentHashMap<Integer, ConcurrentHashMap<Integer,DateTime>>();

	}

	public synchronized ArrayList<RavelryForumPost> getPosts(int forumId, int topicId) {
		ArrayList<RavelryForumPost> arr = new ArrayList<RavelryForumPost>();
		Iterator<RavelryForumPost> it = postHolder.get(forumId).get(topicId).values().iterator();
		while(it.hasNext()) {
			RavelryForumPost post = it.next();
			arr.add(post);
		}
		logPostSequence(arr);
		quickSort(arr, 0, arr.size()-1);
		logPostSequence(arr);
		return arr;
	}

	public void addPosts(ArrayList<RavelryForumPost> posts, int forumId, int topicId, int page, int maxPage) {
		//		ArrayList<Integer> oldPostNumbers = getPostNumbers(forumId, topicId);

		Iterator<RavelryForumPost> it = posts.iterator();
		while(it.hasNext()) {
			RavelryForumPost post = it.next();
			addPost(forumId, topicId, page, post);
		}

		postsLastLoadedPage.get(forumId).put(topicId, page);
		postsMaxPage.get(forumId).put(topicId, maxPage);
		postsRefreshTimes.get(forumId).put(topicId, DateTime.now());
		

		if(topicsBounds.get(topicId).upperbound == 0 && topicsBounds.get(topicId).lowerbound == 0) {
			topicsBounds.get(topicId).upperbound = page;
			topicsBounds.get(topicId).lowerbound = page;
		}
		else {
			if(page > topicsBounds.get(topicId).upperbound) {
				topicsBounds.get(topicId).upperbound = page;
			}
			if(page < topicsBounds.get(topicId).lowerbound) {
				topicsBounds.get(topicId).lowerbound = page;
			}
		}
	}

	private synchronized void addPost(int forumId, int topicId, int page, RavelryForumPost post) {
		if(null == post) { 
			Log.e("PostHolder.addPost", "Null RavelryForuMPost. Aborting. TopicId: " + topicId + ", forumId: " + forumId);
			return;
		}
		if(!doesForumExistInPosts(forumId)) {
			Log.e("PostHolder.addPost", "Forumid " + forumId + " was not found in postHolder. Aborting " +
					"the adding of post # " + post.getPostNumber());
			return;
		}
		if(!doesTopicExistInPosts(forumId, topicId)) {
			Log.e("PostHolder.addPost", "TopicId " + topicId + " was not found in postHolder. Aborting " +
					"the adding of post # " + post.getPostNumber());
		}

		postHolder.get(forumId).get(topicId).put(post.getPostNumber(), post);


	}

	public synchronized boolean doesForumExistInForums(int forumId) {
		return forumsHolder.containsKey(forumId);
	}

	public synchronized boolean doesForumExistInTopics(int forumId) {
		return topicsHolder.containsKey(forumId);
	}

	public synchronized boolean doesForumExistInPosts(int forumId) {
		return postHolder.containsKey(forumId);
	}

	public synchronized boolean doesForumExistInTopicsLastLoaded(int forumId) {
		return topicsLastLoadedPage.containsKey(forumId);
	}

	public synchronized boolean doesForumExistInPostsLastLoaded(int forumId) {
		return postsLastLoadedPage.containsKey(forumId);
	}

	public synchronized boolean doesForumExistInTopicsMaxPage(int forumId){
		return topicsMaxPage.containsKey(forumId);
	}

	public synchronized boolean doesForumExistInTopicsRefresh(int forumId) {
		return topicsRefreshTimes.containsKey(forumId);
	}

	public synchronized boolean doesForumExistInPostsMaxPage(int forumId) {
		return postsMaxPage.containsKey(forumId);
	}

	public synchronized boolean doesForumExistInPostsRefresh(int forumId) {
		return postsRefreshTimes.contains(forumId);
	}

	public synchronized boolean doesTopicExistInTopics(int forumId, int topicId) {
		return topicsHolder.get(forumId).containsKey(topicId);
	}

	public synchronized boolean doesTopicExistInPosts(int forumId, int topicId) {
		return postHolder.get(forumId).containsKey(topicId);
	}

	public synchronized boolean doesForumExistInForumLastLoadedPage(int forumId, int topicId) {
		return topicsLastLoadedPage.containsKey(forumId);
	}

	public synchronized boolean doesTopicForumExistInPostsLastLoadedPage(int forumId, int topicId) {
		return postsLastLoadedPage.get(forumId).containsKey(topicId);
	}

	public synchronized boolean doesTopicExistInPostsMaxPage(int forumId, int topicId) {
		return postsMaxPage.get(forumId).containsKey(topicId);
	}

	public synchronized boolean doesTopicExistInPostsLastLoadedPage(int forumId, int topicId) {
		return postsLastLoadedPage.get(forumId).containsKey(topicId);
	}

	public synchronized boolean doesTopicExistInPostsRefresh(int forumId, int topicId) {
		return postsRefreshTimes.get(forumId).containsKey(topicId);
	}

	public DateTime getRefreshTimeForForums() {
		if(forumRefreshTime == null) {
			return DateTime.now();
		}
		return forumRefreshTime;
	}

	public void addForums(List<RavelryForum> forums) {
		ArrayList<Integer> oldForumIds = getForumsIds();
		forumRefreshTime = DateTime.now();

		Iterator<RavelryForum> it = forums.iterator();
		while(it.hasNext()) {
			RavelryForum forum = it.next();
			addForum(forum.getId(), forum);
			oldForumIds.remove(Integer.valueOf(forum.getId()));
		}

		Iterator<Integer> it2 = oldForumIds.iterator();
		while(it2.hasNext()) {
			int id = it2.next();
			removeForum(id);
		}
	}

	private synchronized void addForum(int forumId, RavelryForum ravelryForum) {
		forumsHolder.put(forumId, ravelryForum);

		if(!doesForumExistInTopics(forumId)) {
			topicsHolder.put(forumId, new LinkedHashMap<Integer, RavelryTopic>());
		}
		if(!doesForumExistInPosts(forumId)) {
			postHolder.put(forumId, new ConcurrentHashMap<Integer, LinkedHashMap<Integer, RavelryForumPost>>());
		}
		if(!doesForumExistInTopicsLastLoaded(forumId)) {
			topicsLastLoadedPage.put(forumId, 1);
		}
		if(!doesForumExistInPostsLastLoaded(forumId)) {
			postsLastLoadedPage.put(forumId, new ConcurrentHashMap<Integer, Integer>());
		}
		if(!doesForumExistInTopicsMaxPage(forumId)) {
			topicsMaxPage.put(forumId, 1);
		}
		if(!doesForumExistInPostsMaxPage(forumId)) {
			postsMaxPage.put(forumId, new ConcurrentHashMap<Integer, Integer>());
		}
		if(!doesForumExistInTopicsRefresh(forumId)) {
			topicsRefreshTimes.put(forumId, DateTime.now());
		}
		if(!doesForumExistInPostsRefresh(forumId)) {
			postsRefreshTimes.put(forumId, new ConcurrentHashMap<Integer, DateTime>());
		}
	}

	private synchronized void removeForum(int forumId) {
		forumsHolder.remove(forumId);
		topicsHolder.remove(forumId);
		postHolder.remove(forumId);

		topicsLastLoadedPage.remove(forumId);
		postsLastLoadedPage.remove(forumId);

		topicsMaxPage.remove(forumId);
		postsMaxPage.remove(forumId);

		topicsRefreshTimes.remove(forumId);
		postsRefreshTimes.remove(forumId);
	}

	private synchronized void removeTopic(int forumId, int topicId) {
		if(doesForumExistInTopics(forumId)) {
			topicsHolder.get(forumId).remove(topicId);
		}
		if(doesForumExistInPosts(forumId) && doesTopicExistInPosts(forumId, topicId)) {
			postHolder.get(forumId).remove(topicId);
		}
		if(doesForumExistInPostsLastLoaded(forumId)) {
			postsLastLoadedPage.get(forumId).remove(topicId);
		}
		if(doesForumExistInPostsMaxPage(forumId)) {
			postsMaxPage.get(forumId).remove(topicId);
		}
		if(doesForumExistInPostsRefresh(forumId)) {
			postsRefreshTimes.get(forumId).remove(topicId);
		}
	}

	private synchronized void removePost(int forumId, int topicId, int postNumber) {
		if(doesForumExistInPosts(forumId) && doesTopicExistInPosts(forumId, topicId)) {
			postHolder.get(forumId).get(topicId).remove(postNumber);
		}
	}

	public void addTopics(ArrayList<RavelryTopic> topics, int forumId, int page, int maxPage) {
		ArrayList<Integer> oldTopicIds = getTopicIds(forumId);
		if(page <= 1) {
			clearTopics(forumId);
		}
		Iterator<RavelryTopic> it = topics.iterator();
		while(it.hasNext()) {
			RavelryTopic topic = it.next();
			addTopic(topic, forumId, topic.getId());
			if(page <= 1) {
				oldTopicIds.remove(Integer.valueOf(topic.getId()));
			}
		}

		if(page <= 1) {
			Iterator<Integer> it2 = oldTopicIds.iterator();
			while(it2.hasNext()) {
				int id = it2.next();
				removeTopic(forumId, id);
			}
		}

		topicsLastLoadedPage.put(forumId, page);
		topicsMaxPage.put(forumId, maxPage);
		topicsRefreshTimes.put(forumId, DateTime.now());
	}

	private synchronized void addTopic(RavelryTopic topic, int forumId, int topicId) {
		if(!doesForumExistInTopics(forumId)) {
			Log.e("PostHolder.addTopic", "Forumid " + forumId + " was not found in topicsHolder. Aborting " +
					"the adding of topic " + topic.getTitle());
			return;
		}	

		topicsHolder.get(forumId).put(topicId, topic);

		if(!doesTopicExistInPosts(forumId, topicId)) {
			postHolder.get(forumId).put(topicId, new LinkedHashMap<Integer, RavelryForumPost>());
		}
		if(!doesTopicExistInPostsMaxPage(forumId, topicId)) {
			postsMaxPage.get(forumId).put(topicId, 1);
		}
		if(!doesTopicExistInPostsLastLoadedPage(forumId, topicId)) {
			postsLastLoadedPage.get(forumId).put(topicId, 1);
		}
		if(!doesTopicExistInPostsRefresh(forumId, topicId)) {
			postsRefreshTimes.get(forumId).put(topicId, DateTime.now());
		}

		if(!topicsBounds.containsKey(topicId)) {
			topicsBounds.put(topicId, new Bounds());
		}
	}

	public synchronized RavelryForum getForum(int forumId) {
		return forumsHolder.get(forumId);
	}

	public synchronized ArrayList<RavelryForum> getForums() {
		ArrayList<RavelryForum> arr = new ArrayList<RavelryForum>();
		Iterator<RavelryForum> it = forumsHolder.values().iterator();
		while(it.hasNext()) {
			RavelryForum forum = it.next();
			arr.add(forum);
		}
		return arr;
	}

	public synchronized ArrayList<Integer> getForumsIds() {
		ArrayList<Integer> arr = new ArrayList<Integer>();
		arr.addAll(forumsHolder.keySet());
		return arr;
	}

	public synchronized ArrayList<Integer> getTopicIds(int forumId) {
		ArrayList<Integer> arr = new ArrayList<Integer>();
		arr.addAll(topicsHolder.get(forumId).keySet());
		return arr;
	}

	public synchronized ArrayList<Integer> getPostNumbers(int forumId, int topicId) {
		ArrayList<Integer> arr = new ArrayList<Integer>();
		arr.addAll(postHolder.get(forumId).get(topicId).keySet());
		return arr;
	}

	public synchronized RavelryForumPost getPost(int forumId, int topicId, int postNumber) {

		return postHolder.get(forumId).get(topicId).get(postNumber);
	}

	@Deprecated
	public synchronized RavelryTopic getTopic(int forumId, int topicId) {
		return topicsHolder.get(forumId).get(topicId);
	}

	public synchronized ArrayList<RavelryTopic> getTopics(int forumId) {
		ArrayList<RavelryTopic> arr = new ArrayList<RavelryTopic>();
		if(topicsHolder.get(forumId) == null || topicsHolder.get(forumId).values() == null) {
			return arr;
		}
		Iterator<RavelryTopic> it = topicsHolder.get(forumId).values().iterator();
		while(it.hasNext()) {
			RavelryTopic topic = it.next();
			arr.add(topic);
		}
		return arr;
	}

	public int getTopicMaxPage(int forumId, int topicId) {
		int maxPage = 1;
		if(doesForumExistInPostsMaxPage(forumId) && doesTopicExistInPostsMaxPage(forumId, topicId)) {
			maxPage = postsMaxPage.get(forumId).get(topicId);
		}
		return maxPage;
	}

	public int getTopicLastLoadedPage(int forumId, int topicId) {
		int lastLoaded = 1;
		if(doesForumExistInPostsLastLoaded(forumId) && doesTopicExistInPostsLastLoadedPage(forumId, topicId)) {
			lastLoaded = postsLastLoadedPage.get(forumId).get(topicId);
		}
		return lastLoaded;
	}

	public DateTime getTopicLastRefresh(int forumId, int topicId) {
		DateTime dt = null;
		if(doesForumExistInPostsRefresh(forumId) && doesTopicExistInPostsRefresh(forumId, topicId)) {
			dt = postsRefreshTimes.get(forumId).get(topicId);
		}
		return dt;
	}

	public DateTime getForumLastRefresh(int forumId) {
		DateTime dt = null;
		if(doesForumExistInTopicsRefresh(forumId)) {
			dt = topicsRefreshTimes.get(forumId);
		}
		return dt;
	}

	public int getForumMaxPage(int forumId) {
		int maxPage = 1;
		if(doesForumExistInTopicsMaxPage(forumId)) {
			maxPage = topicsMaxPage.get(forumId);
		}
		return maxPage;
	}

	public int getForumLastLoadedPage(int forumId) {
		int lastLoaded = 1;
		if(doesForumExistInTopicsLastLoaded(forumId)) {
			lastLoaded = topicsLastLoadedPage.get(forumId);
		}
		return lastLoaded;
	}

	public void clearForums() {
		forumsHolder.clear();
	}

	public void clearTopics(int forumId) {
		topicsHolder.get(forumId).clear();
	}

	public void clearPosts(int forumId, int topicId) {
		postHolder.get(forumId).get(topicId).clear();
		postsLastLoadedPage.get(forumId).put(topicId, 0);
		postsMaxPage.get(forumId).put(topicId, 0);
		topicsBounds.get(topicId).lowerbound = 0;
		topicsBounds.get(topicId).upperbound = 0;
	}

	public boolean isMoreThreadPagesPresent(int forumId, int topicId) {
		return (getTopicLastLoadedPage(forumId, topicId) < getTopicMaxPage(forumId, topicId));
	}

	public boolean isPostAReply(int forumId, int topicId, int postNumber) {
		boolean reply = false;
		RavelryForumPost post = getPost(forumId, topicId, postNumber);
		if(null != post) {
			if(post.getParentPostNumber() > 0) {
				reply = true;
			}
		}
		return reply;
	}

	public LinkedList<RavelryForumPost> getPostChain(int forumId, int topicId, int postNumber) {
		LinkedList<RavelryForumPost> arr = new LinkedList<RavelryForumPost>();
		RavelryForumPost origPost = getPost(forumId, topicId, postNumber);
		arr.addFirst(origPost);

		if(getPost(forumId, topicId, origPost.getPostNumber()) != null) {
			while(isPostAReply(forumId, topicId, origPost.getPostNumber())) {
				origPost = getPost(forumId, topicId, origPost.getParentPostNumber());
				arr.addFirst(origPost);
			}
		}

		return arr;
	}

	public int getTopicLastReadPost(int forumId, int topicId) {
		return topicsHolder.get(forumId).get(topicId).getLastRead();
	}
	
	public void setTopicLastReadPost(int forumId, int topicId, int postNumber) {
		topicsHolder.get(forumId).get(topicId).setLastRead(postNumber);
	}

	public int getUpperBoundPage(int topicId) {
		return topicsBounds.get(topicId).upperbound;
	}

	public int getLowerBoundPage(int topicId) {
		return topicsBounds.get(topicId).lowerbound;
	}



	private int partition(ArrayList<RavelryForumPost> arr, int left, int right)
	{
		int i = left, j = right;
		RavelryForumPost tmp;
		RavelryForumPost pivot = arr.get((left + right) / 2);

		while (i <= j) {
			while (arr.get(i).getPostNumber() < pivot.getPostNumber())
				i++;
			while (arr.get(j).getPostNumber() > pivot.getPostNumber())
				j--;
			if (i <= j) {
				tmp = arr.get(i);
				arr.set(i, arr.get(j));
				arr.set(j, tmp);
				i++;
				j--;
			}
		}
		return i;
	}

	private void quickSort(ArrayList<RavelryForumPost> arr, int left, int right) {
		if(arr == null || arr.isEmpty()) {
			return;
		}
		int index = partition(arr, left, right);
		if (left < index - 1) {
			quickSort(arr, left, index - 1);
		}
		if (index < right) {
			quickSort(arr, index, right);
		}
	}

	private void logPostSequence(ArrayList<RavelryForumPost> arr) {
		String disp = "";
		for(int i = 0; i < arr.size(); i++) {
			disp += String.valueOf(arr.get(i).getPostNumber()) + ",";
		}
		Log.i("Sequence", disp);
	}

	private class Bounds {
		int upperbound;
		int lowerbound;

		public Bounds() {
			upperbound = 0;
			lowerbound = 0;
		}
	}

	public void replaceForumPost(int forumId, int topicId, RavelryForumPost post) {
		RavelryForumPost existingPost = postHolder.get(forumId).get(topicId).get(post.getPostNumber());
		existingPost.setBodyHtml(post.getBodyHtml());
	}
}
