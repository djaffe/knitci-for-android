package com.knitci.pocket.social.forum;

import java.util.LinkedList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ListView;

import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.data.object.impl.RavelryForumPost;
import com.knitci.pocket.social.forum.manager.ForumManager;

public class ThreadChainActivity extends Activity {

	private ForumManager forumManager;
	private int topicId;
	private LinkedList<RavelryForumPost> posts;
	private ThreadChainAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_thread_chain);
		forumManager = KnitciApplication.class.cast(getApplication()).getForumManager();
		Bundle bundle = getIntent().getExtras();
		int topicId = bundle.getInt("TOPICID");
		this.topicId = topicId;
		int endPostNumber = bundle.getInt("ENDPOSTNUMBER");
		int forumId = bundle.getInt("FORUMID");
		posts = forumManager.getPostChain(forumId, topicId, endPostNumber);
		ListView lv = ListView.class.cast(findViewById(R.id.postchain_listview));
		adapter = new ThreadChainAdapter(getApplicationContext(), posts);
		lv.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.thread_chain, menu);
		return true;
	}

}
