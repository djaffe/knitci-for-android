package com.knitci.pocket.social.forum.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Minutes;

import android.content.Context;

import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.home.HomeForumPreviewData;
import com.knitci.pocket.ravelry.RavelryManager;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryForum;
import com.knitci.pocket.ravelry.data.object.impl.RavelryForumPost;
import com.knitci.pocket.ravelry.data.object.impl.RavelryTopic;
import com.knitci.pocket.social.forum.PostHolder;

public class ForumManager {

	KnitciApplication app;
	private PostHolder postHolder;
	DateTime forumSetRefreshTime;
	RavelryManager ravelryManager;

	public ForumManager(Context context) {
		postHolder = new PostHolder();
		app = KnitciApplication.class.cast(context);
		ravelryManager = app.getRavelryManager();
	}
	
	public ArrayList<RavelryForumPost> getPosts(int forumId, int topicId) {
		return postHolder.getPosts(forumId, topicId);
	}
	
	public ArrayList<RavelryTopic> getTopics(int forumId) {
		return postHolder.getTopics(forumId);
	}

	public int getThreadLastLoadedPage(int forumId, int topicId) {
		return postHolder.getTopicLastLoadedPage(forumId, topicId);
	}
	
	public int getThreadMaxPage(int forumId, int topicId) {
		return postHolder.getTopicMaxPage(forumId, topicId);
	}
	
	public int getTopicLastLoadedPage(int forumId) {
		return postHolder.getForumLastLoadedPage(forumId);
	}
	
	public int getTopicMaxPage(int forumId) {
		return postHolder.getForumMaxPage(forumId);
	}
	
	public int getTopicLastReadPost(int forumId, int topicId) {
		return postHolder.getTopicLastReadPost(forumId, topicId);
	}
	
	public int determineStartingPage(int forumId, int topicId) {
		int lastReadPost = postHolder.getTopicLastReadPost(forumId, topicId);
		int page = (int) Math.ceil((lastReadPost+1)/25);
		return page;
	}
	
	public void addPosts(ArrayList<RavelryForumPost> posts, int page, int forumId, int maxPage) {
		if(posts.isEmpty()) {
			return;
		}
		int topicId = posts.get(0).getTopicId();
		postHolder.addPosts(posts, forumId, topicId, page, maxPage);
	}
	
	public boolean addMorePosts(ArrayList<RavelryForumPost> posts, int page, int forumId, int maxPage) {
		if(posts == null || posts.isEmpty()) {
			return false;
		}
		int topicId = posts.get(0).getTopicId();
		
		int lastLoadedPage = getThreadLastLoadedPage(forumId, topicId);
		postHolder.addPosts(posts, forumId, topicId, page, maxPage);
		if(lastLoadedPage < maxPage) {
			return true;
		}
		return false;
	}
	
	public boolean isPostAReply(int forumId, int topicId, int postNumber) {
		return postHolder.isPostAReply(forumId, topicId, postNumber);
	}
	
	public LinkedList<RavelryForumPost> getPostChain(int forumId, int topicId, int postNumber) {
		return postHolder.getPostChain(forumId, topicId, postNumber);
	}
	
	public void checkForMorePosts(int forumId, int topicId) {
		int lastPage = getThreadLastLoadedPage(forumId, topicId);
	}
	
	public RavelryForumPost getPost(int forumId, int topicId, int postNumber) {
		return postHolder.getPost(forumId, topicId, postNumber);
	}
	
	/*@Deprecated
	public LinkedList<RavelryForumPost> generateThreadChain(int topicId, int endPostNumber) {
		RavelryForumPost post = findPostNumber(topicId, endPostNumber);
		return generateThreadChain(post);
	}*/
	
	/*@Deprecated
	public LinkedList<RavelryForumPost> generateThreadChain(RavelryForumPost endPost) {
		LinkedList<RavelryForumPost> array = new LinkedList<RavelryForumPost>();
		array.addFirst(endPost);
		int topicId = endPost.getTopicId();
		int nextPostNumber = endPost.getParentPostNumber();
		boolean finished = false;
		
		while(!finished) {
			RavelryForumPost post = findPostNumber(topicId, nextPostNumber);
			array.addFirst(post);
			if(post.getParentPostNumber() > 0) {
				nextPostNumber = post.getParentPostNumber();
			}
			else {
				finished = true;
			}
		}
		return array;
	}*/
	
	/**
	 * Takes a List of {@link RavelryForum} objects and adds them into the forum cache.
	 * @param forums A List of {@link RavelryForum} entities. 
	 */
	public void addForums(List<RavelryForum> forums) {
		forumSetRefreshTime = DateTime.now();
		postHolder.addForums(forums);
	}
	
	/**
	 * Gets all {@link RavelryForum} entities from the forum cache. 
	 * @return List of {@link RavelryForum} objects. 
	 */
	public ArrayList<RavelryForum> getForums() {
		return postHolder.getForums();
	}
	
	public void addTopics(ArrayList<RavelryTopic> topics, int page, int maxPage) {
		if(topics.isEmpty()) { return; }
		int forumId = topics.get(0).getForumId();
		postHolder.addTopics(topics, forumId, page, maxPage);
	}
	
	public void refreshForumsIfNecessary() {
		if(Minutes.minutesBetween(postHolder.getRefreshTimeForForums(), DateTime.now()).getMinutes() > 1) {
			refreshForumSets();
		}
	}
	
	@Deprecated
	public void refreshTopicsIfNecessary(int forumId) {
		/*DateTime past = postHolder.getForumTopicsRefresTime(forumId);
		if(past != null) {
			if(Minutes.minutesBetween(past, DateTime.now()).getMinutes() > 3) {
				refreshTopics(forumId);
			}
		}*/
	}
	
	public void refreshForumSets() {
		app.getRavelryManager().retrieveForumSets();
	}
	
	public void refreshTopics(int forumId) {
		app.getRavelryManager().retrieveForumTopics(0, forumId);
	}
	
	public boolean isMoreThreadPagesAvailable(int forumId, int topicId) {
		return postHolder.isMoreThreadPagesPresent(forumId, topicId);
	}
	
	public void batchLoadPosts(int forumId, int topicId, int upperBoundary) {
		ravelryManager.retrieveTopicPostsBatch(topicId, 1, true, true, forumId, upperBoundary);
	}
	
	public int getLowestLoadedPage(int topicId) {
		return postHolder.getLowerBoundPage(topicId);
	}
	
	public int getHighestLoadedPage(int topicId) {
		return postHolder.getUpperBoundPage(topicId);
	}
	
	public boolean setTopicLastReadPost(int forumId, int topicId, int postNumber) {
		if(getTopicLastReadPost(forumId, topicId) < postNumber) {
			postHolder.setTopicLastReadPost(forumId, topicId, postNumber);
			return true;
		}
		else {
			return false;
		}
	}
	
	public int getArrayPositionByPost(int forumId, int topicId, int postNumber) {
		ArrayList<RavelryForumPost> arr = postHolder.getPosts(forumId, topicId);
		for(int i = arr.size()-1; i >= 0; i--) {
			if(arr.get(i).getPostNumber() == postNumber) {
				return i;
			}
		}
		return arr.size()-1;
	}

	public void clearPosts(int forumId, int topicId) {
		postHolder.clearPosts(forumId, topicId);
	}
	
	public void replaceForumPost(int forumId, int topicId, RavelryForumPost post) {
		if(post != null) {
			postHolder.replaceForumPost(forumId, topicId, post);
		}
	}

}
