package com.knitci.pocket.social.forum;

import java.util.ArrayList;

import object.KnitciActivity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest.ErrorCode;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.patterns.PatternDetailActivity;
import com.knitci.pocket.patterns.PatternPreviewActivity;
import com.knitci.pocket.projects.ProjectDetailActivity;
import com.knitci.pocket.ravelry.IRavelryAsync;
import com.knitci.pocket.ravelry.data.access.RavelryAsyncTask.ERavelryTask;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryForumPost;
import com.knitci.pocket.social.forum.manager.ForumManager;

public class ThreadActivity extends KnitciActivity implements IRavelryAsync, AdListener, OnItemClickListener, OnScrollListener, IMovementHandler, OnClickListener {

	private int forumId;
	private int topicId;
	private ForumManager forumManager;
	private ThreadListAdapter adapter;

	private int lastReadPost;

	private boolean currentlyLoadingPosts;
	private boolean initialAutoscrollDone;
	private boolean firstPagePresent;
	private boolean updateLastReadNeeded;

	private int replyPostId;

	private View header;
	private View footer;

	private ProgressDialog progressDialog;
	private String editedString;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_thread);
		stealAsyncListening();
		setupSlidingMenu();
		updateLastReadNeeded = false;
		lastReadPost = 1;
		forumManager = KnitciApplication.class.cast(getApplicationContext()).getForumManager();
		PullToRefreshListView lv = PullToRefreshListView.class.cast(findViewById(R.id.thread_listview));
		lv.setOnItemClickListener(this);
		lv.setOnScrollListener(this);
		header = getLayoutInflater().inflate(R.layout.listview_header, null);
		footer = getLayoutInflater().inflate(R.layout.listview_header, null);
		lv.getRefreshableView().addHeaderView(header);
		lv.getRefreshableView().addFooterView(footer);
		lv.getRefreshableView().removeHeaderView(header);
		lv.getRefreshableView().removeFooterView(footer);
		Bundle extras = getIntent().getExtras();
		forumId = extras.getInt("FORUMID");
		topicId = extras.getInt("TOPICID");
		currentlyLoadingPosts = true;
		firstPagePresent = false;
		initialAutoscrollDone = false;

		PullToRefreshListView pullToRefreshView = (PullToRefreshListView) findViewById(R.id.thread_listview);
		pullToRefreshView.setMode(Mode.DISABLED);
		registerForContextMenu(pullToRefreshView.getRefreshableView());
		pullToRefreshView.setOnRefreshListener(new OnRefreshListener2<ListView>() {

			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
				int lowestPage = forumManager.getLowestLoadedPage(topicId);
				if(lowestPage > 1) {
					KnitciApplication.class.cast(getApplicationContext()).getRavelryManager().retrieveTopicPostsBackwards(topicId, lowestPage-1, true, true, forumId);
					currentlyLoadingPosts = true;
				}
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
				Toast toast = Toast.makeText(getApplicationContext(), "Refresh Triggered", Toast.LENGTH_SHORT);
				toast.show();
				KnitciApplication.class.cast(getApplicationContext()).getRavelryManager().retrieveTopicPostsCont(topicId, forumManager.getThreadLastLoadedPage(forumId, topicId), true, true, forumId);
				currentlyLoadingPosts = true;
			}

		});


		this.lastReadPost = forumManager.getTopicLastReadPost(forumId, topicId);

		int sizeCachedPosted = 0;
		if(forumManager.getPosts(forumId, topicId) != null && !forumManager.getPosts(forumId, topicId).isEmpty()) {
			sizeCachedPosted = forumManager.getPosts(forumId, topicId).size();
		}

		int lowestPage = forumManager.getLowestLoadedPage(topicId);
		int highestPage = forumManager.getHighestLoadedPage(topicId);
		int currentlyReadPage = (int) Math.ceil((double)lastReadPost/25);
		Log.d("PostQuery", "last read post is " + lastReadPost + ", which makes currently read page " + currentlyReadPage);
		if(lowestPage == 1) {
			firstPagePresent = true;
		}

		if(lowestPage == 0 && highestPage == 0) {
			// Nothing has been loaded for this topic yet
			if(currentlyReadPage == 0) {
				currentlyReadPage = 1;
			}
			Log.d("PostQuery", "Lowest page = 0, highest page = 0, so nothing has been loaded yet. YES querying for currently read page: " + currentlyReadPage);
			KnitciApplication.class.cast(getApplicationContext()).getRavelryManager().retrieveTopicPosts(topicId, currentlyReadPage, true, true, forumId);
		}
		else if(currentlyReadPage >= lowestPage && currentlyReadPage <= highestPage) {
			// The current page is in range of pages we have loaded, no need to requery.
			populatePosts(forumManager.getPosts(forumId, topicId), false);
			Log.d("PostQuery", "currently read page = " + currentlyReadPage + ", lowest page = " + 
					lowestPage + ", highest page = " + highestPage + ", NO querying.");
			scrollToPost(lastReadPost);
		}
		else {
			Log.e("PostQuery", "Hit the else statement.. fall back to just loading stuff");
			ArrayList<RavelryForumPost> posts = KnitciApplication.class.cast(getApplicationContext()).getForumManager().getPosts(forumId, topicId);
			if(posts != null && !posts.isEmpty()) {
				populatePosts(posts, false);
			}
			else {
				KnitciApplication.class.cast(getApplicationContext()).getRavelryManager().retrieveTopicPosts(topicId, currentlyReadPage, true, true, forumId);
			}
		}

		TextView replyToLabel = (TextView) findViewById(R.id.thread_reply_to);
		replyToLabel.setVisibility(View.GONE);
		replyToLabel.setOnClickListener(this);

		Button postReply = (Button) findViewById(R.id.thread_post);
		postReply.setOnClickListener(this);

	}

	@Override
	protected void onResume() {
		super.onResume();
		stealAsyncListening();
	}

	private void populatePosts(ArrayList<RavelryForumPost> posts, boolean continued) {
		if(posts == null) {
			return;
		}
		if(adapter == null) {
			adapter = new ThreadListAdapter(this, posts, this);
			PullToRefreshListView lv = PullToRefreshListView.class.cast(findViewById(R.id.thread_listview));
			lv.setAdapter(adapter);
			if(!forumManager.isMoreThreadPagesAvailable(forumId, topicId)) {
				//lv.setMode(Mode.PULL_FROM_END);
				addRefreshMode(Mode.PULL_FROM_END);
			}
		}
		else {
			if(!continued) {
				adapter.posts = posts;
				adapter.notifyDataSetChanged();
				if(!forumManager.isMoreThreadPagesAvailable(forumId, topicId)) {
					PullToRefreshListView lv = PullToRefreshListView.class.cast(findViewById(R.id.thread_listview));
					//lv.setMode(Mode.PULL_FROM_END);
					addRefreshMode(Mode.PULL_FROM_END);
				}
			}
			else {
				adapter.posts = posts;
				adapter.notifyDataSetChanged();
			}
		}
		currentlyLoadingPosts = false;
	}

	private void openThreadChain(RavelryForumPost latestPost) {
		Intent intent = new Intent(this, ThreadChainActivity.class);
		intent.putExtra("FORUMID", forumId);
		intent.putExtra("TOPICID", latestPost.getTopicId());
		intent.putExtra("ENDPOSTNUMBER", latestPost.getPostNumber());
		startActivity(intent);
	}

	@Override
	public void processFinish(RavelryResponse response) {
		super.processFinish(response);
		switch(response.task) {
		case TOPICPOSTS:
		case TOPICPOSTSCONT:
		case TOPICPOSTSBATCHCOMPLETE:
		case TOPICPOSTSBACKWARDS:
			int lowestPage = forumManager.getLowestLoadedPage(topicId);
			if(lowestPage == 1) {
				firstPagePresent = true;
				//removeRefreshMode(Mode.PULL_FROM_START);
			}
			else {
				//addRefreshMode(Mode.PULL_FROM_START);
			}

			final PullToRefreshListView lv = PullToRefreshListView.class.cast(findViewById(R.id.thread_listview));
			if(response.task == ERavelryTask.TOPICPOSTSCONT) {
				if(progressDialog != null && progressDialog.isShowing()) {
					progressDialog.dismiss();
					EditText.class.cast(findViewById(R.id.thread_content)).setText("");
					// TODO: If we're not at the last page so that the new post can be seen, clear the cache, set the last post
					// as read, and load up the last page. 
					if(1==2) {

					}
					else {
						scrollToBottom();
					}


				}
				populatePosts(forumManager.getPosts(forumId, topicId), true);
				lv.getRefreshableView().removeFooterView(footer);
				lv.onRefreshComplete();
			}
			else if(response.task == ERavelryTask.TOPICPOSTSBATCHCOMPLETE) {
				populatePosts(forumManager.getPosts(forumId, topicId), true);
				displayLengthyLoading(false);
			}
			else if(response.task == ERavelryTask.TOPICPOSTSBACKWARDS) {
				ArrayList<RavelryForumPost> tArr = forumManager.getPosts(forumId, topicId);
				populatePosts(tArr, false);
				lv.getRefreshableView().removeHeaderView(header);
				initialAutoscrollDone = false;
				scrollToPost(lowestPage*25);
			}
			else {
				populatePosts(forumManager.getPosts(forumId, topicId), false);
				lv.getRefreshableView().removeFooterView(footer);
				int lastReadPost = forumManager.getTopicLastReadPost(forumId, topicId);
				if(lastReadPost > 1 && !initialAutoscrollDone) {
					scrollToPost(lastReadPost);
				}
			}
			break;
		case FORUMSUBMITPOST:
			if(response.result != null && response.result.getHttpCode() == 200) {
				RavelryForumPost newPartialPost = (RavelryForumPost) response.result;
				int newPostNumber = newPartialPost.getPostNumber();
				forumManager.setTopicLastReadPost(forumId, topicId, newPostNumber);
				if(Math.ceil(forumManager.getTopicLastReadPost(forumId, topicId)/25) > forumManager.getHighestLoadedPage(topicId)) {
					// TODO: clear the cache for this topic, then load up the last page. 
					forumManager.clearPosts(forumId, topicId);
					KnitciApplication.class.cast(getApplicationContext()).getRavelryManager().retrieveTopicPosts(topicId, (int) Math.ceil(newPostNumber/25), true, true, forumId);
				}
				else {
					KnitciApplication.class.cast(getApplicationContext()).getRavelryManager().retrieveTopicPostsCont(topicId, forumManager.getThreadLastLoadedPage(forumId, topicId), true, true, forumId);
				}
			}
			else {
				progressDialog.dismiss();
				AlertDialog.Builder builder = new AlertDialog.Builder(this);

				builder.setTitle("Post Failed");

				builder.setMessage("Failed to submit forum post. Error code " + response.result.getHttpCode() + ".");
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
					}
				});
				AlertDialog dialog = builder.create();
				dialog.show();

			}
			break;
		case FORUMSUBMITPOSTEDIT:
			progressDialog.dismiss();
			if(response.result != null && response.result.getHttpCode() == 200) {
				adapter.notifyDataSetChanged();
				editedString = "";
			}
			else {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);

				builder.setTitle("Edit Failed");

				builder.setMessage("Failed to submit edit. Error code " + response.result.getHttpCode() + ".");
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
					}
				});
				AlertDialog dialog = builder.create();
				dialog.show();
			}
			break;
		default:
			break;
		}
	}

	private void addRefreshMode(Mode mode) {
		PullToRefreshListView lv = PullToRefreshListView.class.cast(findViewById(R.id.thread_listview));
		if(mode == Mode.PULL_FROM_END) {
			if(lv.getMode() == Mode.DISABLED) {
				lv.setMode(Mode.PULL_FROM_END);
			}
			else if(lv.getMode() == Mode.PULL_FROM_START) {
				lv.setMode(Mode.BOTH);
			}
		}
		else {
			if(lv.getMode() == Mode.DISABLED) {
				lv.setMode(Mode.PULL_FROM_START);
			}
			else if(lv.getMode() == Mode.PULL_FROM_END) {
				lv.setMode(Mode.BOTH);
			}
		}
	}

	private void removeRefreshMode(Mode mode) {
		PullToRefreshListView lv = PullToRefreshListView.class.cast(findViewById(R.id.thread_listview));
		if(mode == Mode.PULL_FROM_END) {
			if(lv.getMode() == Mode.PULL_FROM_END) {
				lv.setMode(Mode.DISABLED);
			}
			else if(lv.getMode() == Mode.BOTH) {
				lv.setMode(Mode.PULL_FROM_START);
			}
		}
		else {
			if(lv.getMode() == Mode.PULL_FROM_START) {
				lv.setMode(Mode.DISABLED);
			}
			else if(lv.getMode() == Mode.BOTH) {
				lv.setMode(Mode.PULL_FROM_END);
			}
		}
	}

	public void scrollToPost(final int post) {
		final PullToRefreshListView pullToRefreshView = (PullToRefreshListView) findViewById(R.id.thread_listview);
		final int position = forumManager.getArrayPositionByPost(forumId, topicId, post);
		pullToRefreshView.getRefreshableView().post( new Runnable() {
			@Override
			public void run() {
				pullToRefreshView.getRefreshableView().setSelection(position);
				initialAutoscrollDone = true;
			}
		});
	}

	public void scrollToBottom() {
		final PullToRefreshListView pullToRefreshView = (PullToRefreshListView) findViewById(R.id.thread_listview);
		pullToRefreshView.getRefreshableView().post( new Runnable() {
			@Override
			public void run() {
				pullToRefreshView.getRefreshableView().setSelection(adapter.getCount()-1);
				initialAutoscrollDone = true;
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.thread, menu);
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
		/*PullToRefreshListView lv = PullToRefreshListView.class.cast(findViewById(R.id.thread_listview));
		RavelryForumPost post = RavelryForumPost.class.cast(lv.getRefreshableView().getItemAtPosition(position));

		if(forumManager.isPostAReply(forumId, topicId, post.getPostNumber())) {
			openThreadChain(post);
		}*/
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		if(updateLastReadNeeded) {
			KnitciApplication.class.cast(getApplication()).getRavelryManager().submitMarkForumPostRead(topicId, forumManager.getTopicLastReadPost(forumId, topicId), true);
		}
		super.onDestroy();
	}

	private void displayLengthyLoading(boolean display) {

	}

	@Override
	public void onDismissScreen(Ad arg0) {

	}

	@Override
	public void onFailedToReceiveAd(Ad arg0, ErrorCode arg1) {

	}

	@Override
	public void onLeaveApplication(Ad arg0) {

	}

	@Override
	public void onPresentScreen(Ad arg0) {

	}

	@Override
	public void onReceiveAd(Ad arg0) {

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {

		if (adapter == null) {
			return ;
		}

		if (adapter.getCount() == 0) {
			return ;
		}

		if(currentlyLoadingPosts) {
			return;
		}

		// Scrolled down to bottom
		boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
		boolean morePagesAvailable = (forumManager.getThreadMaxPage(forumId, topicId) > 1 && forumManager.getThreadLastLoadedPage(forumId, topicId) != forumManager.getThreadMaxPage(forumId, topicId));
		PullToRefreshListView lv = PullToRefreshListView.class.cast(findViewById(R.id.thread_listview));
		if(loadMore && morePagesAvailable) {
			currentlyLoadingPosts = true;
			lv.getRefreshableView().addFooterView(footer);
			KnitciApplication.class.cast(getApplicationContext()).getRavelryManager().retrieveTopicPosts(topicId, forumManager.getThreadLastLoadedPage(forumId, topicId)+1, true, true, forumId);
		}

		// Scrolled up to top
		else if(firstVisibleItem == 0 && !firstPagePresent && initialAutoscrollDone)  {
			int lowestPage = forumManager.getLowestLoadedPage(topicId);
			if(lowestPage > 1) {
				currentlyLoadingPosts = true;
				lv.getRefreshableView().addHeaderView(header);
				KnitciApplication.class.cast(getApplicationContext()).getRavelryManager().retrieveTopicPostsBackwards(topicId, lowestPage-1, true, true, forumId);
			}
		}

		int lastVisible = (firstVisibleItem + visibleItemCount)-3;
		if(lastVisible >= 0 && lastVisible < adapter.posts.size()) {
			updateLastReadNeeded = (forumManager.setTopicLastReadPost(forumId, topicId, adapter.posts.get(lastVisible).getPostNumber()) || updateLastReadNeeded);
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		try {
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
			RavelryForumPost item = (RavelryForumPost) adapter.getItem(info.position-1);
			menu.add(0, item.getId(), 0, "Reply");
			//menu.add(0, item.getId(), 1, "Favorite");
			if(item.isEditable()) {
				menu.add(0, item.getId(), 1, "Edit");
			}
		} catch (Exception e) { }
	}

	@Override
	public boolean onContextItemSelected(android.view.MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		int pos = info.position - 1;
		RavelryForumPost post = adapter.posts.get(pos);

		if(item.getTitle().equals("Favorite")) {
			favoritePost(post);
		}
		else if(item.getTitle().equals("Reply")) {
			setPostReply(post);
		}
		else if(item.getTitle().equals("Edit")) {
			enterEditMode(post);
		}
		return true;
	}

	private void favoritePost(RavelryForumPost post) {

	}

	private void enterEditMode(final RavelryForumPost post) {
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		editedString = "";
		alert.setTitle("Title");
		alert.setMessage("Message");

		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		input.setText(Html.fromHtml(post.getBodyHtml()));
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				
				String value = input.getText().toString();
				editedString = value;
				String html = Html.toHtml(input.getText());
				postEditPost(html, post);
			}
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				editedString = "";
				// Canceled.
			}
		});

		alert.show();
	}

	private void setPostReply(RavelryForumPost post) {
		if(post != null) {
			this.replyPostId = post.getId();

			TextView replyToLabel = (TextView) findViewById(R.id.thread_reply_to);
			replyToLabel.setText("replying to " + post.getUser().getUsername() + "'s post #" + post.getPostNumber());
			replyToLabel.setVisibility(View.VISIBLE);
		}
		else {
			this.replyPostId = 0;
			TextView replyToLabel = (TextView) findViewById(R.id.thread_reply_to);
			replyToLabel.setText("");
			replyToLabel.setVisibility(View.GONE);
		}
	}

	@Override
	public void fireThreadChain(int postNumber) {
		if(forumManager.isPostAReply(forumId, topicId, postNumber)) {
			openThreadChain(forumManager.getPost(forumId, topicId, postNumber));
		}
	}

	@Override
	public void patternClickedFromTextView(String patternPermalink) {
		Intent intent = new Intent(this, PatternDetailActivity.class);
		intent.putExtra("PATTERNPERMA", patternPermalink);
		startActivity(intent);
	}

	@Override
	public void projectClickedFromTextView(String username, String projectPermalink) {
		Intent intent = new Intent(this, ProjectDetailActivity.class);
		intent.putExtra("PROJECTUSER", username);
		intent.putExtra("PROJECTPERMA", projectPermalink);
		startActivity(intent);
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.thread_reply_to) {
			setPostReply(null);
		}
		else if(v.getId() == R.id.thread_post) {
			final String newMessageContent = EditText.class.cast(findViewById(R.id.thread_content)).getText().toString();

			AlertDialog.Builder builder = new AlertDialog.Builder(this);

			builder.setTitle("Confirm new post");

			builder.setMessage(newMessageContent);
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					postNewPost(newMessageContent);
				}
			});
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					// User cancelled the dialog
				}
			});
			AlertDialog dialog = builder.create();
			dialog.show();
		}
	}

	public void postEditPost(String newBody, RavelryForumPost post) {
		progressDialog = ProgressDialog.show(ThreadActivity.this, "Please wait ...", "Posting ...", true);
		progressDialog.setCancelable(true);
		KnitciApplication.class.cast(getApplicationContext()).getRavelryManager().submitForumPostEdit(forumId, topicId, post.getId(), newBody);
	}

	public void postNewPost(String messageContent) {
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(EditText.class.cast(findViewById(R.id.thread_content)).getWindowToken(), 0);
		progressDialog = ProgressDialog.show(ThreadActivity.this, "Please wait ...", "Posting ...", true);
		progressDialog.setCancelable(true);
		KnitciApplication.class.cast(getApplicationContext()).getRavelryManager().submitForumPost(topicId, messageContent, replyPostId);
	}


}
