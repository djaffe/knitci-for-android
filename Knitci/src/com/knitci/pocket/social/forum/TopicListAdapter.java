package com.knitci.pocket.social.forum;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.data.object.impl.RavelryTopic;
import com.knitci.pocket.util.StandardConvert;

public class TopicListAdapter extends BaseAdapter{

	Context context;
	ArrayList<RavelryTopic> topics;
	
	public TopicListAdapter(Context context, ArrayList<RavelryTopic> topics) {
		this.context = context;
		this.topics = topics;
	}
	
	@Override
	public int getCount() {
		return topics.size();
	}

	@Override
	public Object getItem(int position) {
		return topics.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.topiclist_topic_item, arg2, false);
		TextView tvName = (TextView) rowView.findViewById(R.id.topiclist_topic_name);
		TextView tvTimeago = (TextView) rowView.findViewById(R.id.topiclist_topic_activity);
		TextView tvPosts = (TextView) rowView.findViewById(R.id.topiclist_topic_posts);
		tvName.setText(topics.get(arg0).getTitle());
		tvTimeago.setText(StandardConvert.getPastActivityDisplay(topics.get(arg0).getRepliedDtTm()));
		//tvPosts.setText(topics.get(arg0).getForumPostsCount());
		return rowView;
	}

}
