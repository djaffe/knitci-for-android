package com.knitci.pocket.social.forum;

import java.util.HashMap;
import java.util.LinkedList;

import android.graphics.Bitmap;

public class ThreadImageCache {

	private HashMap<String, Bitmap> bitmapCache;
	private LinkedList<String> keyKeeper;

	private static class Loader {
		static ThreadImageCache INSTANCE = new ThreadImageCache();
	}

	private ThreadImageCache() {
		bitmapCache = new HashMap<String, Bitmap>();
		keyKeeper = new LinkedList<String>();
	}

	public static ThreadImageCache getInstance() {
		return Loader.INSTANCE;
	}

	public synchronized void cache(String url, Bitmap bitmap) {
		keyKeeper.push(url);
		bitmapCache.put(url, bitmap);
		if(keyKeeper.size() > 7) {
			String goodbye = keyKeeper.removeLast();
			bitmapCache.remove(goodbye);
		}
	}

	public synchronized Bitmap retrieve(String url) {
		return bitmapCache.get(url);
	}

}
