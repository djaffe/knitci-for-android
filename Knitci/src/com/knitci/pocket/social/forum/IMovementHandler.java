package com.knitci.pocket.social.forum;

public interface IMovementHandler {
	public void fireThreadChain(int postNumber);
	void patternClickedFromTextView(String patternPermalink);
	void projectClickedFromTextView(String username, String projectPermalink);
}
