package com.knitci.pocket.social;

import java.util.HashMap;
import java.util.LinkedList;

import android.graphics.Bitmap;

public class UserAvatarCache {

	private HashMap<String, Bitmap> bitmapCache;
	private LinkedList<String> keyKeeper;

	private static class Loader {
		static UserAvatarCache INSTANCE = new UserAvatarCache();
	}

	private UserAvatarCache() {
		bitmapCache = new HashMap<String, Bitmap>();
		keyKeeper = new LinkedList<String>();
	}

	public static UserAvatarCache getInstance() {
		return Loader.INSTANCE;
	}

	public synchronized void cache(String username, Bitmap bitmap) {
		keyKeeper.push(username);
		bitmapCache.put(username, bitmap);
		if(keyKeeper.size() > 20) {
			String goodbye = keyKeeper.removeLast();
			bitmapCache.remove(goodbye);
		}
	}

	public synchronized Bitmap retrieve(String username) {
		return bitmapCache.get(username);
	}
	
}
