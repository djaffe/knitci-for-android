package com.knitci.pocket.ravelry;

import org.scribe.builder.api.DefaultApi10a;

import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.basic.DefaultOAuthProvider;

/**
 * An API object used by Scribe. Contains keys for the Knitci App to authenticate with Ravelry. 
 * @author DJ021930
 */
public class RavelryAPI extends DefaultApi10a {

	public static final String CONSUMER_KEY = "8487CDDAE0F84620D4B0";
    public static final String CONSUMER_SECRET = "7FP21JcRyZgENEIu4e48uUt5sYlyz7poVVZkdQCS";
    public static final String APPLICATION_NAME = "knitcipocket";
    
    public static final String RAVELRY_REQUEST_TOKEN_URL = "https://www.ravelry.com/oauth/request_token?scope=forum-write+message-write";
    public static final String RAVELRY_ACCESS_TOKEN_URL = "https://www.ravelry.com/oauth/access_token";
    public static final String RAVELRY_AUTHORIZE_URL = "https://www.ravelry.com/oauth/authorize";
    
	private static final String AUTHORIZE_URL = "https://www.ravelry.com/oauth/authorize?oauth_token=%s";

	/* (non-Javadoc)
	 * @see org.scribe.builder.api.DefaultApi10a#getAccessTokenEndpoint()
	 */
	@Override
	public String getAccessTokenEndpoint()
	{
		return "https://www.ravelry.com/oauth/access_token";
	}

	/* (non-Javadoc)
	 * @see org.scribe.builder.api.DefaultApi10a#getRequestTokenEndpoint()
	 */
	@Override
	public String getRequestTokenEndpoint()
	{
		//return "https://www.ravelry.com/oauth/request_token";
		return RAVELRY_REQUEST_TOKEN_URL;
	}

	/* (non-Javadoc)
	 * @see org.scribe.builder.api.DefaultApi10a#getAuthorizationUrl(org.scribe.model.Token)
	 */
	@Override
	public String getAuthorizationUrl(org.scribe.model.Token requestToken)
	{
		return String.format(AUTHORIZE_URL, requestToken.getToken());
	}


}
