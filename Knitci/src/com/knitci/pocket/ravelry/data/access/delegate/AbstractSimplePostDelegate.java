package com.knitci.pocket.ravelry.data.access.delegate;

import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import android.util.Log;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public abstract class AbstractSimplePostDelegate extends AbstractDelegate {

	protected final String URL;
	protected final OAuthService service;
	protected final Token token;
	
	protected AbstractSimplePostDelegate(OAuthService service, Token token, String URL) {
		this.service = service;
		this.token = token;
		this.URL = URL;
	}

	/*
	 * (non-Javadoc)
	 * @see com.knitci.pocket.ravelry.data.access.delegate.AbstractDelegate#execute()
	 */
	@Override
	protected RavelryObject execute() {
		String paramString = prepareParamString();
		OAuthRequest request = new OAuthRequest(Verb.POST, URL+paramString);
		Log.d("URL", URL+paramString);
		if(service == null) {
			Log.e("AbstractSimpleDelegate.execute", "service object was null.. execute aborting.");
			return null;
		}
		if(token == null) {
			Log.e("AbstractSimpleDelegate.execute", "token object was null.. execute aborting.");
			return null;
		}
		
		service.signRequest(token, request);
		Response response = request.send();
		Log.d("Body", response.getBody());
		return translate(response);
	}


	
}
