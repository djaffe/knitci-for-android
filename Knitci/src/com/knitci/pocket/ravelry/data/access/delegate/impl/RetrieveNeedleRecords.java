package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.ravelry.data.object.impl.RavelryNeedleRecord;
import com.knitci.pocket.tools.needle.sizes.NeedleSize;
import com.knitci.pocket.util.JsonUtil;

public class RetrieveNeedleRecords extends AbstractSimpleGetDelegate {

	protected RetrieveNeedleRecords(OAuthService service, Token token, String username) {
		super(service, token, "https://api.ravelry.com/people/"+username+"/needles/list.json");
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, String username) {
		return new RetrieveNeedleRecords(service, token, username).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			return null;
		}
		RavelryCollection needles = new RavelryCollection();
		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}

		if(!JsonUtil.isKeyPopulated(json.getAsJsonObject(), "needle_records")) {
			return null;
		}
		
		JsonArray needleRecs = json.getAsJsonObject().getAsJsonArray("needle_records");
		for(int i = 0; i < needleRecs.size(); i++) {
			JsonObject obj = needleRecs.get(i).getAsJsonObject();
			RavelryNeedleRecord rec = new RavelryNeedleRecord();
			
			rec.setId(getSafeJsonInt(obj, "id"));
			rec.setNeedleTypeId(getSafeJsonInt(obj, "needle_type_id"));
			rec.setComment(getSafeJsonString(obj, "comment"));
			
			needles.add(rec);
		}

		needles.setHttpCode(httpResponseCode);
		return needles;
	}

}
