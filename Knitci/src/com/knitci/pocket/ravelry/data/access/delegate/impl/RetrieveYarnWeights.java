package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import android.util.Log;

import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;

/**
 * Delegate for calling Ravlery API for available Yarn Weights. 
 * @author DJ021930
 */
public class RetrieveYarnWeights extends AbstractSimpleGetDelegate {

	/**
	 * Constructor, not to be called externally. 
	 * @param service {@link OAuthService} Service to use. 
	 * @param token {@link Token} Token to use. 
	 * @param URL String URL to API. 
	 */
	protected RetrieveYarnWeights(OAuthService service, Token token, String URL) {
		super(service, token, URL);
	}

	/**
	 * Public method for consuming this delegate. 
	 * @param service {@link OAuthService} An OAuthService object for OAuth queries. Cannot be null. 
	 * @param token {@link Token} Token to be used. Cannot be null. 
	 */
	public static RavelryObject retrieve(OAuthService service, Token token) {
		return new RetrieveYarnWeights(service, token, "http://api.ravelry.com/yarn_weights.json").execute();
	}

	/*
	 * (non-Javadoc)
	 * @see com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate#translate(org.scribe.model.Response)
	 */
	@Override
	protected RavelryObject translate(Response response) {
		// TODO Auto-generated method stub
		Log.d("YARNWEIGHTS", response.getBody());
		return null;
	}

}
