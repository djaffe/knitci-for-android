package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RetrieveYarn extends AbstractSimpleGetDelegate {

	protected RetrieveYarn(OAuthService service, Token token, int yarnId) {
		super(service, token, "https://api.ravelry.com/yarns/"+String.valueOf(yarnId)+".json");
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, int yarnId) {
		return new RetrieveYarn(service, token, yarnId).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		// TODO Auto-generated method stub
		return null;
	}

}
