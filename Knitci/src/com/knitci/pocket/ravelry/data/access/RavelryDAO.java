package com.knitci.pocket.ravelry.data.access;

import java.util.ArrayList;

import org.scribe.builder.ServiceBuilder;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import android.content.Context;

import com.knitci.pocket.ravelry.RavelryAPI;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RemoveFavorite;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveColorFamilies;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveCurrentUser;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveFavorites;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveForumPost;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveForumSets;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveForumTopics;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveFriends;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveLibrary;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveMessage;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveMessages;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveNeedleRecords;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveNeedleSizes;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveNeedleTypes;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrievePack;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrievePattern;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrievePatternByPermalink;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrievePatterns;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrievePhotoCreationStatus;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrievePhotoSizes;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveProjectCrafts;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveProjectStatuses;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveProjectsByPattern;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveQueueOrdering;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveShop;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveShops;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveSingleStash;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveSpecificProject;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveSpecificProjectByPermalink;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveSpecificQueue;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveStash;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveTopic;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveTopicPosts;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveUnifiedStash;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveUploadImageStatus;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveUserProfile;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveUserProjects;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveUserQueue;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveVolume;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveYarn;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveYarnWeights;
import com.knitci.pocket.ravelry.data.access.delegate.impl.RetrieveYarns;
import com.knitci.pocket.ravelry.data.access.delegate.impl.SubmitCreateFavorite;
import com.knitci.pocket.ravelry.data.access.delegate.impl.SubmitCreateQueue;
import com.knitci.pocket.ravelry.data.access.delegate.impl.SubmitForumPost;
import com.knitci.pocket.ravelry.data.access.delegate.impl.SubmitForumPostEdit;
import com.knitci.pocket.ravelry.data.access.delegate.impl.SubmitMarkForumPostRead;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryParam;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.ravelry.data.object.impl.RavelryColorFamily;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryUser;
import com.knitci.pocket.ravelry.data.object.impl.RavelryYarnWeight;

/**
 * DAO for accessing delegates to make a Ravelry API call. 
 * @author DJ021930
 */
public class RavelryDAO {

	/**
	 * Uses the {@link RetrieveCurrentUser} delegate for retrieving current user information from Ravelry. 
	 * @param token {@link Token} Token to be used in the OAuth. 
	 * @return {@link RavelryUser} The user information, could be null if the call failed. 
	 */
	public RavelryObject retrieveCurrentUser(Token token, Context context) {
		return RetrieveCurrentUser.retrieve(getService(), token, context);
	}
	
	/**
	 * Uses the {@link RetrieveUserProjects} delegate for retrieving a specific user's projects from Ravelry.
	 * @param token {@link Token} Token to be used in the OAuth.
	 * @param username String The username to get projects for. 
	 * @return {@link RavelryCollection} A collection of {@link RavelryProject} objects. Can be empty or null. 
	 */
	public RavelryObject retrieveUserProjects(Token token, String username, ArrayList<RavelryParam> params) {
		return RetrieveUserProjects.retrieve(getService(), token, username, params);
	}
	
	/**
	 * Uses the {@link RetrieveColorFamilies} delegate for retrieving the ravelry color families
	 * @param token {@link Token} Token to be used in the OAuth
	 * @return {@link RavelryCollection} A collection of {@link RavelryColorFamily} objects. Can be null or empty. 
	 */
	public RavelryObject retrieveColorFamilies(Token token) {
		return RetrieveColorFamilies.retrieve(getService(), token);
	}
	
	/**
	 * Uses the {@link RetrieveYarnWeights} delegate for retrieving ravelry yarn weights.
	 * @param token {@link Token} Token to be used in the OAuth.
	 * @return {@link RavelryCollection} A collection of {@link RavelryYarnWeight} objects. Can be null or empty. 
	 */
	public RavelryObject retrieveYarnWeights(Token token) {
		return RetrieveYarnWeights.retrieve(getService(), token);
	}
	
	/**
	 * Uses the {@link RetrieveFavorites} delegate for retrieving a user's favorites. 
	 * @param token Token to be used in the OAuth.
	 * @param username The user to get favorites from.
	 * @param params A list of RavelryParams needed for the API call. 
	 * @return {@link RavelryCollection} A collection of RavelryBookmarks. 
	 */
	public RavelryObject retrieveFavorites(Token token, String username, ArrayList<RavelryParam> params) {
		return RetrieveFavorites.retrieve(getService(), token, username, params);
	}
	
	public RavelryObject removeFavorite(Token token, String username, int favoriteId) {
		return RemoveFavorite.retrieve(getService(), token, username, favoriteId);
	}
	
	public RavelryObject retrieveForumPost(Token token, int forumPostId) {
		return RetrieveForumPost.retrieve(getService(), token, forumPostId);
	}
	
	public RavelryObject retrieveForumSets(Token token) {
		return RetrieveForumSets.retrieve(getService(), token);
	}
	
	public RavelryObject retrieveForumTopics(Token token, int forumId, ArrayList<RavelryParam> params) {
		return RetrieveForumTopics.retrieve(getService(), token, forumId, params);
	}
	
	public RavelryObject retrieveFriends(Token token, String username) {
		return RetrieveFriends.retrieve(getService(), token, username);
	}
	
	public RavelryObject retrieveLibrary(Token token, String username, ArrayList<RavelryParam> params) {
		return RetrieveLibrary.retrieve(getService(), token, username, params);
	}
	
	public RavelryObject retrieveMessage(Token token, int messageId) {
		return RetrieveMessage.retrieve(getService(), token, messageId);
	}
	
	public RavelryObject retrieveMessages(Token token, ArrayList<RavelryParam> params, Context context) {
		return RetrieveMessages.retrieve(getService(), token, params, context);
	}
	
	public RavelryObject retrieveNeedleTypes(Token token) {
		return RetrieveNeedleTypes.retrieve(getService(), token);
	}
	
	public RavelryObject retrieveNeedleSizes(Token token, ArrayList<RavelryParam> params) {
		return RetrieveNeedleSizes.retrieve(getService(), token, params);
	}
	
	public RavelryObject retrieveNeedleRecords(Token token, String username) {
		return RetrieveNeedleRecords.retrieve(getService(), token, username);
	}
	
	public RavelryObject retrievePack(Token token, int packId) {
		return RetrievePack.retrieve(getService(), token, packId);
	}
	
	public RavelryObject retrievePattern(Token token, int patternId) {
		return RetrievePattern.retrieve(getService(), token, patternId);
	}
	
	public RavelryObject retrievePattern(Token token, String permalink) {
		return RetrievePatternByPermalink.retrieve(getService(), token, permalink);
	}
	
	public RavelryObject retrievePatterns(Token token, ArrayList<RavelryParam> params) {
		return RetrievePatterns.retrieve(getService(), token, params);
	}
	
	public RavelryObject retrieveUserProfile(Token token, int userId, Context context) {
		return RetrieveUserProfile.retrieve(getService(), token, userId, context);
	}
	
	public RavelryObject retrievePhotoSizes(Token token, int photoId) {
		return RetrievePhotoSizes.retrieve(getService(), token, photoId);
	}
	
	public RavelryObject retrievePhotoCreationStatus(Token token, ArrayList<RavelryParam> params) {
		return RetrievePhotoCreationStatus.retrieve(getService(), token, params);
	}
	
	public RavelryObject retrieveUploadImageStatus(Token token, ArrayList<RavelryParam> params) {
		return RetrieveUploadImageStatus.retrieve(getService(), token, params);
	}
	
	public RavelryObject retrieveProjectCrafts(Token token) {
		return RetrieveProjectCrafts.retrieve(getService(), token);
	}
	
	public RavelryObject retrieveProjectStatuses(Token token) {
		return RetrieveProjectStatuses.retrieve(getService(), token);
	}
	
	public RavelryObject retrieveQueueOrdering(Token token, String username) {
		return RetrieveQueueOrdering.retrieve(getService(), token, username);
	}
	
	public RavelryObject retrieveUserQueue(Token token, String username, ArrayList<RavelryParam> params) {
		return RetrieveUserQueue.retrieve(getService(), token, username, params);
	}
	
	public RavelryObject retrieveSpecificQueue(Token token, String username, int queueId) {
		return RetrieveSpecificQueue.retrieve(getService(), token, username, queueId);
	}
	
	public RavelryObject retrieveShops(Token token, ArrayList<RavelryParam> params) {
		return RetrieveShops.retrieve(getService(), token, params);
	}
	
	public RavelryObject retrieveShop(Token token, int shopId, ArrayList<RavelryParam> params) {
		return RetrieveShop.retrieve(getService(), token, shopId, params);
	}
	
	public RavelryObject retrieveStash(Token token, String username, ArrayList<RavelryParam> params) {
		return RetrieveStash.retrieve(getService(), token, username, params);
	}
	
	public RavelryObject retrieveUnifiedStash(Token token, String username, ArrayList<RavelryParam> params) {
		return RetrieveUnifiedStash.retrieve(getService(), token, username, params);
	}
	
	public RavelryObject retrieveSingleStash(Token token, String username, int stashId) {
		return RetrieveSingleStash.retrieve(getService(), token, username, stashId);
	}
	
	public RavelryObject retrieveTopic(Token token, int topicId) {
		return RetrieveTopic.retrieve(getService(), token, topicId);
	}
	
	public RavelryObject retrieveTopicPosts(Token token, int topicId, ArrayList<RavelryParam> params, Context context) {
		return RetrieveTopicPosts.retrieve(getService(), token, topicId, params, context);
	}
	
	public RavelryObject retrieveVolume(Token token, int volId) {
		return RetrieveVolume.retrieve(getService(), token, volId);
	}
	
	public RavelryObject retrieveYarn(Token token, int yarnId) {
		return RetrieveYarn.retrieve(getService(), token, yarnId);
	}
	
	public RavelryObject retrieveYarns(Token token, ArrayList<RavelryParam> params) {
		return RetrieveYarns.retrieve(getService(), token, params);
	}
	
	public RavelryObject retrieveProject(Token token, String username, int projectId) {
		return RetrieveSpecificProject.retrieve(getService(), token, username, projectId);
	}
	
	public RavelryObject retrieveProject(Token token, String username, String permalink) {
		return RetrieveSpecificProjectByPermalink.retrieve(getService(), token, username, permalink);
	}
	
	public RavelryObject retrieveProjectsByPattern(Token token, int patternId, ArrayList<RavelryParam> params) {
		return RetrieveProjectsByPattern.retrieve(getService(), token, patternId, params);
	}
	
	public RavelryObject submitForumPost(Token token, int topicId, ArrayList<RavelryParam> params) {
		return SubmitForumPost.submit(getService(), token, topicId, params);
	}
	
	public RavelryObject submitForumPostEdit(Token token, int forumPostId, ArrayList<RavelryParam> params) {
		return SubmitForumPostEdit.submit(getService(), token, forumPostId, params);
	}
	
	public RavelryObject submitCreateFavorite(Token token, String username, String data) {
		return SubmitCreateFavorite.submit(getService(), token, data, username);
	}
	
	public RavelryObject submitCreateQueue(Token token, String username, String data) {
		return SubmitCreateQueue.submit(getService(), token, username, data);
	}
	
	public RavelryObject submitMarkForumPostRead(Token token, int topicId, ArrayList<RavelryParam> params) {
		return SubmitMarkForumPostRead.submit(getService(), token, topicId, params);
	}
	
	/**
	 * Gets a new {@link OAuthService} object for making the Scribe OAuth call. 
	 * @return {@link OAuthService} A service object. 
	 */
	private OAuthService getService() {
		final OAuthService s = new ServiceBuilder()
	    .provider(RavelryAPI.class)
	    .apiKey(RavelryAPI.CONSUMER_KEY)
	    .apiSecret(RavelryAPI.CONSUMER_SECRET)
	    .callback("oauthapp://ravelry.com")
	    .debug()
	    .build();
		
		return s;
	}
}
