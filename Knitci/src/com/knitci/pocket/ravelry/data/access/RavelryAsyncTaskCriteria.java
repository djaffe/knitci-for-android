package com.knitci.pocket.ravelry.data.access;

import java.util.ArrayList;

import org.scribe.model.Token;

import android.content.Context;

import com.knitci.pocket.ravelry.data.access.RavelryAsyncTask.ERavelryTask;
import com.knitci.pocket.ravelry.data.object.RavelryParam;

/**
 * A criteria object used for sending multiple objects to a {@link RavelryAsyncTask}. 
 * @author DJ021930
 */
public class RavelryAsyncTaskCriteria {
	public Token token;
	public ERavelryTask taskType;
	public String username;
	public ArrayList<RavelryParam> params;
	public int miscId;
	public int forumId;
	public int topicId;
	public String messageBox;
	public String data;
	public String miscString;
	public int upperbound;
	public int lowerbound;
	public Context context;
	public String permalink;
}
