package com.knitci.pocket.ravelry.data.access;

import org.scribe.model.Token;

import android.os.AsyncTask;

import com.knitci.pocket.ravelry.IRavelryAsync;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;

/**
 * Subclassed AsyncTask for performing Ravelry OAuth API calls. 
 * @author DJ021930
 */
public class RavelryAsyncTask extends AsyncTask<RavelryAsyncTaskCriteria, Void, RavelryResponse> {

	public IRavelryAsync listener = null;
	public ERavelryTask ravTaskType;
	private boolean finished;

	/**
	 * Task constructor.
	 * @param listener {@link IRavelryAsync} Class to receive the onPostExecute message. This should
	 * generally be the RavelryManager which will pass it off to the current activity if necessary. 
	 * @param taskType {@link ERavelryTask} Task representing the Ravelry API call. 
	 */
	public RavelryAsyncTask(IRavelryAsync listener) {
		finished = false;
		this.listener = listener;
	}

	/* (non-Javadoc)
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 */
	@Override
	protected RavelryResponse doInBackground(RavelryAsyncTaskCriteria... params) {
		RavelryAsyncTaskCriteria criteria = params[0];
		Token token = criteria.token;
		this.ravTaskType = criteria.taskType;
		RavelryDAO dao = new RavelryDAO();

		RavelryResponse response = new RavelryResponse();

		switch(ravTaskType) {
		case PROJECT:
			if(criteria.permalink != null) {
				response.result = dao.retrieveProject(token, criteria.username, criteria.permalink);
				response.task = ERavelryTask.PROJECT;
			}
			else {
				response.result = dao.retrieveProject(token, criteria.username, criteria.miscId);
				response.task = ERavelryTask.PROJECT;
			}
			break;
		case USERINFO:
			response.result = dao.retrieveCurrentUser(token, criteria.context);
			response.task = ERavelryTask.USERINFO;
			break;
		case SPECIFICUSERPROJECTS:
			response.result = dao.retrieveUserProjects(token, criteria.username, criteria.params);
			response.task = ERavelryTask.SPECIFICUSERPROJECTS;
			response.username = criteria.username;
			break;
		case COLORFAMILIES:
			response.result = dao.retrieveColorFamilies(token);
			response.task = ERavelryTask.COLORFAMILIES;
			break;
		case YARNWEIGHTS:
			response.result = dao.retrieveYarnWeights(token);
			response.task = ERavelryTask.YARNWEIGHTS;
			break;
		case LISTFAVORITES:
			response.result = dao.retrieveFavorites(token, criteria.username, criteria.params);
			response.task = ERavelryTask.LISTFAVORITES;
			break;
		case REMOVEFAVORITE:
			response.result = dao.removeFavorite(token, criteria.username, criteria.miscId);
			response.task = ERavelryTask.REMOVEFAVORITE;
			break;
		case FORUMPOST:
			response.result = dao.retrieveForumPost(token, criteria.miscId);
			response.task = ERavelryTask.FORUMPOST;
			break;
		case FORUMSETS:
			response.result = dao.retrieveForumSets(token);
			response.task = ERavelryTask.FORUMSETS;
			break;
		case FORUMTOPICS:
			response.result = dao.retrieveForumTopics(token, criteria.miscId, criteria.params);
			response.task = ERavelryTask.FORUMTOPICS;
			break;
		case FRIENDS:
			response.result = dao.retrieveFriends(token, criteria.username);
			response.task = ERavelryTask.FRIENDS;
			break;
		case LIBRARY:
			response.result = dao.retrieveLibrary(token, criteria.username, criteria.params);
			response.task = ERavelryTask.LIBRARY;
			break;
		case MESSAGE:
			response.result = dao.retrieveMessage(token, criteria.miscId);
			response.task = ERavelryTask.MESSAGE;
			break;
		case MESSAGES:
			response.result = dao.retrieveMessages(token, criteria.params, criteria.context);
			response.task = ERavelryTask.MESSAGES;
			response.messageBox = criteria.messageBox;
			break;
		case NEEDLETYPES:
			response.result = dao.retrieveNeedleTypes(token);
			response.task = ERavelryTask.NEEDLETYPES;
			break;
		case NEEDLESIZES:
			response.result = dao.retrieveNeedleSizes(token, criteria.params);
			response.task = ERavelryTask.NEEDLESIZES;
			break;
		case NEEDLERECORDS:
			response.result = dao.retrieveNeedleRecords(token, criteria.username);
			response.task = ERavelryTask.NEEDLERECORDS;
			break;
		case PACK:
			response.result = dao.retrievePack(token, criteria.miscId);
			response.task = ERavelryTask.PACK;
			break;
		case PATTERN:
			if(criteria.permalink != null) {
				response.result = dao.retrievePattern(token, criteria.permalink);
				response.task = ERavelryTask.PATTERN;
			}
			else {
				response.result = dao.retrievePattern(token, criteria.miscId);
				response.task = ERavelryTask.PATTERN;
			}
			break;
		case PATTERNS:
			response.result = dao.retrievePatterns(token, criteria.params);
			response.task = ERavelryTask.PATTERNS;
			break;
		case PATTERNSEXT:
			response.result = dao.retrievePatterns(token, criteria.params);
			response.task = ERavelryTask.PATTERNSEXT;
			break;
		case USERPROFILE:
			response.result = dao.retrieveUserProfile(token, criteria.miscId, criteria.context);
			response.task = ERavelryTask.USERPROFILE;
			break;
		case PHOTOSIZES:
			response.result = dao.retrievePhotoSizes(token, criteria.miscId);
			response.task = ERavelryTask.PHOTOSIZES;
			break;
		case PHOTOSTATUS:
			response.result = dao.retrievePhotoCreationStatus(token, criteria.params);
			response.task = ERavelryTask.PHOTOSTATUS;
			break;
		case CRAFTS:
			response.result = dao.retrieveProjectCrafts(token);
			response.task = ERavelryTask.CRAFTS;
			break;
		case PROJECTSTATUSES:
			response.result = dao.retrieveProjectStatuses(token);
			response.task = ERavelryTask.PROJECTSTATUSES;
			break;
		case QUEUEORDERING:
			response.result = dao.retrieveQueueOrdering(token, criteria.username);
			response.task = ERavelryTask.QUEUEORDERING;
			break;
		case QUEUELIST:
			response.result = dao.retrieveUserQueue(token, criteria.username, criteria.params);
			response.task = ERavelryTask.QUEUELIST;
			response.username = criteria.username;

			break;
		case QUEUE:
			response.result = dao.retrieveSpecificQueue(token, criteria.username, criteria.miscId);
			response.task = ERavelryTask.QUEUE;
			break;
		case FINDSHOPS:
			response.result = dao.retrieveShops(token, criteria.params);
			response.task = ERavelryTask.FINDSHOPS;
			break;
		case SHOP:
			response.result = dao.retrieveShop(token, criteria.miscId, criteria.params);
			response.task = ERavelryTask.SHOP;
			break;
		case STASH:
			response.result = dao.retrieveStash(token, criteria.username, criteria.params);
			response.task = ERavelryTask.STASH;
			response.username = criteria.username;
			break;
		case UNIFIEDSTASH:
			response.result = dao.retrieveUnifiedStash(token, criteria.username, criteria.params);
			response.task = ERavelryTask.UNIFIEDSTASH;
			response.username = criteria.username;
			break;
		case STASHSINGLE:
			response.result = dao.retrieveSingleStash(token, criteria.username, criteria.miscId);
			response.task = ERavelryTask.STASHSINGLE;
			break;
		case TOPIC:
			response.result = dao.retrieveTopic(token, criteria.miscId);
			response.task = ERavelryTask.TOPIC;
			break;
		case TOPICPOSTS:
		case TOPICPOSTSCONT:
		case TOPICPOSTSBATCH:
		case TOPICPOSTSBACKWARDS:
			response.result = dao.retrieveTopicPosts(token, criteria.topicId, criteria.params, criteria.context);
			response.task = ravTaskType;
			response.miscId = criteria.miscId;
			response.forumId = criteria.forumId;
			response.topicId = criteria.topicId;
			response.upperbound = criteria.upperbound;
			break;
		case UPLOADSTATUS:
			response.result = dao.retrieveUploadImageStatus(token, criteria.params);
			response.task = ERavelryTask.UPLOADSTATUS;
			break;
		case VOLUME:
			response.result = dao.retrieveVolume(token, criteria.miscId);
			response.task = ERavelryTask.VOLUME;
			break;
		case YARN:
			response.result = dao.retrieveYarn(token, criteria.miscId);
			response.task = ERavelryTask.YARN;
			break;
		case YARNS:
			response.result = dao.retrieveYarns(token, criteria.params);
			response.task = ERavelryTask.YARNS;
			break;
		case FORUMSUBMITPOST:
			response.result = dao.submitForumPost(token, criteria.topicId, criteria.params);
			response.task = ERavelryTask.FORUMSUBMITPOST;
			break;
		case FORUMSUBMITPOSTEDIT:
			response.result = dao.submitForumPostEdit(token, criteria.miscId, criteria.params);
			response.task = ERavelryTask.FORUMSUBMITPOSTEDIT;
			response.forumId = criteria.forumId;
			response.topicId = criteria.topicId;
			break;
		case FAVORITECREATE:
			response.result = dao.submitCreateFavorite(token, criteria.username, criteria.data);
			response.task = ERavelryTask.FAVORITECREATE;
			break;
		case QUEUECREATE:
			response.result = dao.submitCreateQueue(token, criteria.username, criteria.data);
			response.task = ERavelryTask.QUEUECREATE;
			break;
		case MARKTOPICREAD:
			response.result = dao.submitMarkForumPostRead(token, criteria.topicId, criteria.params);
			response.task = ERavelryTask.MARKTOPICREAD;
			break;
		case PROJECTSBYPATTERN:
			response.result = dao.retrieveProjectsByPattern(token, criteria.miscId, criteria.params);
			response.task = ERavelryTask.PROJECTSBYPATTERN;
			break;
		default:
			break;
		}

		return response;
	}

	/* (non-Javadoc)
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 */
	protected void onPostExecute(RavelryResponse response) {
		finished = true;
		if(null != listener) {
			listener.processFinish(response);
		}
	}

	/**
	 * Enum class of Ravelry Task Types representing all the API calls. 
	 * @author DJ021930
	 */
	public static enum ERavelryTask {
		USERINFO, SPECIFICUSERPROJECTS, COLORFAMILIES, YARNWEIGHTS, LISTFAVORITES, REMOVEFAVORITE, 
		FORUMPOST, FORUMSETS, FORUMTOPICS, FORUMTOPICSCONT, FRIENDS, LIBRARY, MESSAGE, MESSAGES, NEEDLETYPES,
		NEEDLESIZES, NEEDLERECORDS, PACK, PATTERN, PATTERNS, USERPROFILE, PHOTOSIZES, 
		PHOTOSTATUS, CRAFTS, PROJECTSTATUSES, QUEUEORDERING, QUEUELIST, QUEUE, FINDSHOPS, 
		SHOP, STASH, UNIFIEDSTASH, STASHSINGLE, TOPIC, TOPICPOSTS, TOPICPOSTSCONT, UPLOADSTATUS, 
		FORUMSUBMITPOST, VOLUME, YARNS, YARN, HTTPERROR, FAVORITECREATE, QUEUECREATE, PATTERNSEXT, PROJECT,
		SPECIFICUSERPROJECTSSTILLOADING, QUEUELISTSTILLLOADING, TOPICPOSTSBATCH, TOPICPOSTSBATCHCOMPLETE,
		TOPICPOSTSBACKWARDS, MARKTOPICREAD, FORUMSUBMITPOSTEDIT, PROJECTSBYPATTERN
	}

	/**
	 * Check if the task is finished. 
	 * @return boolean true or false if the task is finished. 
	 */
	public boolean isFinished() {
		return finished;
	}

}