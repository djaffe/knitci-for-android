package com.knitci.pocket.ravelry.data.access.delegate.impl;

import java.util.ArrayList;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryParam;

public class RetrieveShops extends AbstractSimpleGetDelegate {

	protected RetrieveShops(OAuthService service, Token token, ArrayList<RavelryParam> params) {
		super(service, token, "https://api.ravelry.com/shops/search.json");
		setParams(params);
	}

	public static RavelryObject retrieve(OAuthService service, Token token, ArrayList<RavelryParam> params) {
		return new RetrieveShops(service, token, params).execute();
	}
	
	@Override
	protected RavelryObject translate(Response response) {
		// TODO Auto-generated method stub
		return null;
	}

}
