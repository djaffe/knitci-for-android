package com.knitci.pocket.ravelry.data.access.delegate.impl;

import java.util.ArrayList;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryPaginator;
import com.knitci.pocket.ravelry.data.object.RavelryParam;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.ravelry.data.object.impl.RavelryForumPost;
import com.knitci.pocket.util.JsonUtil;
import com.knitci.pocket.util.StandardConvert;

public class RetrieveTopicPosts extends AbstractSimpleGetDelegate {

	private int topicId;
	Context context;
	
	protected RetrieveTopicPosts(OAuthService service, Token token, int topicId, ArrayList<RavelryParam> params, Context context) {
		super(service, token, "https://api.ravelry.com/topics/"+String.valueOf(topicId)+"/posts.json");
		this.topicId = topicId;
		this.context = context;
		setParams(params);
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, int topicId, ArrayList<RavelryParam> params, Context context) {
		return new RetrieveTopicPosts(service, token, topicId, params, context).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		
		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			return null;
		}

		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}
		JsonObject jObj = json.getAsJsonObject();
		
		if(!JsonUtil.isKeyPopulated(jObj, "posts")) {
			return null;
		}
		
		RavelryPaginator paginator = null;
		if(JsonUtil.isKeyPopulated(jObj, "paginator")) {
			paginator = translatePaginator(jObj.getAsJsonObject("paginator"));
		}
				
		RavelryCollection collection = new RavelryCollection();
		collection.setPaginator(paginator);
		collection.miscId = this.topicId;
		
		JsonArray postsArr = jObj.getAsJsonArray("posts");
		for(int i = 0; i < postsArr.size(); i++) {
			JsonObject postObj = postsArr.get(i).getAsJsonObject();
			RavelryForumPost post = new RavelryForumPost();
			
			post.setParentPostNumber(JsonUtil.isKeyPopulated(postObj, "parent_post_number") ? postObj.get("parent_post_number").getAsInt() : 0);
			post.setCreatedDtTm(JsonUtil.isKeyPopulated(postObj, "created_at") ? StandardConvert.stringToDate(postObj.get("created_at").getAsString()) : null);
			post.setUser(JsonUtil.isKeyPopulated(postObj, "user") ? translateRavelryUser(postObj.get("user").getAsJsonObject(), context) : null);
			post.setBody(JsonUtil.isKeyPopulated(postObj, "body") ? postObj.get("body").getAsString() : null);
			post.setUpdatedDtTm(JsonUtil.isKeyPopulated(postObj, "updated_at") ? StandardConvert.stringToDate(postObj.get("updated_at").getAsString()) : null);
			post.setEditable(JsonUtil.isKeyPopulated(postObj, "editable") ? postObj.get("editable").getAsBoolean() : false);
			post.setId(JsonUtil.isKeyPopulated(postObj, "id") ? postObj.get("id").getAsInt() : -1);
			post.setDeleted(JsonUtil.isKeyPopulated(postObj, "deleted") ? postObj.get("deleted").getAsBoolean() : false);
			post.setReplyCount(JsonUtil.isKeyPopulated(postObj, "reply_count") ? postObj.get("reply_count").getAsInt() : -1);
			post.setBodyHtml(JsonUtil.isKeyPopulated(postObj, "body_html") ? postObj.get("body_html").getAsString() : null);
			post.setParentPostId(JsonUtil.isKeyPopulated(postObj, "parent_post_id") ? postObj.get("parent_post_id").getAsInt() : -1);
			post.setTopicId(topicId);
			post.setPostNumber(JsonUtil.isKeyPopulated(postObj, "post_number") ? postObj.get("post_number").getAsInt() : -1);
			
			post.setParentPostUser(translateRavelryUser(getSafeJsonObject(postObj, "parent_post_user"), context));
			
			collection.add(post);
			
		}
		
		return collection;
	}

}
