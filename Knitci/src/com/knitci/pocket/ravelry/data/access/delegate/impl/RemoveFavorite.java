package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleDeleteDelegate;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryBookmark;
import com.knitci.pocket.util.JsonUtil;

public class RemoveFavorite extends AbstractSimpleDeleteDelegate {

	private RemoveFavorite(OAuthService service, Token token, String username, int favoriteId) {
		super(service, token, "https://api.ravelry.com/people/"+username+"/favorites/"+String.valueOf(favoriteId)+".json");
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, String username, int favoriteId) {
		return new RemoveFavorite(service, token, username, favoriteId).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		if(response == null || response.getBody().isEmpty()) {
			return null;
		}
		
		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}

		if(!JsonUtil.isKeyPopulated(json.getAsJsonObject(), "bookmark")) {
			return null;
		}
		
		JsonObject obj = json.getAsJsonObject().getAsJsonObject("bookmark");
		
		RavelryBookmark bookmark = new RavelryBookmark();
		bookmark.setType(getSafeJsonString(obj, "type"));
		bookmark.setId(getSafeJsonInt(obj, "id"));
		bookmark.setComment(getSafeJsonString(obj, "comment"));
		
		JsonObject fObj = obj.getAsJsonObject("favorited");
		bookmark.setItemId(getSafeJsonInt(fObj, "id"));
		
		return bookmark;
	}

}
