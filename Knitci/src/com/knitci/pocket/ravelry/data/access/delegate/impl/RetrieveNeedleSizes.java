package com.knitci.pocket.ravelry.data.access.delegate.impl;

import java.util.ArrayList;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryParam;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.tools.needle.sizes.NeedleSize;
import com.knitci.pocket.util.JsonUtil;

public class RetrieveNeedleSizes extends AbstractSimpleGetDelegate {

	protected RetrieveNeedleSizes(OAuthService service, Token token, ArrayList<RavelryParam> params) {
		super(service, token, "https://api.ravelry.com/needles/sizes.json");
		setParams(params);
		// TODO Auto-generated constructor stub
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, ArrayList<RavelryParam> params) {
		return new RetrieveNeedleSizes(service, token, params).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			return null;
		}
		RavelryCollection needles = new RavelryCollection();
		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}

		if(!JsonUtil.isKeyPopulated(json.getAsJsonObject(), "needle_sizes")) {
			return null;
		}
		
		JsonArray needleSizes = json.getAsJsonObject().getAsJsonArray("needle_sizes");
		for(int i = 0; i < needleSizes.size(); i++) {
			JsonObject obj = needleSizes.get(i).getAsJsonObject();
			NeedleSize size = new NeedleSize();
			size.setHook(getSafeJsonString(obj, "hook"));
			size.setUs(getSafeJsonString(obj, "us"));
			size.setMetric(getSafeJsonFloat(obj, "metric"));
			size.setId(getSafeJsonInt(obj, "id"));
			needles.add(size);
		}

		needles.setHttpCode(httpResponseCode);
		return needles;
	}

}
