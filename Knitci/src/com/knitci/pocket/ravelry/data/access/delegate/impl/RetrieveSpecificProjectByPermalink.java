package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject;
import com.knitci.pocket.util.JsonUtil;

public class RetrieveSpecificProjectByPermalink extends AbstractSimpleGetDelegate {

	protected RetrieveSpecificProjectByPermalink(OAuthService service, Token token, String username, String permalink) {
		super(service, token, "https://api.ravelry.com/projects/"+username+"/"+permalink+".json");
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, String username, String permalink) {
		return new RetrieveSpecificProjectByPermalink(service, token, username, permalink).execute();
	}


	@Override
	protected RavelryObject translate(Response response) {
		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			return null;
		}

		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}

		if(!JsonUtil.isKeyPopulated(json.getAsJsonObject(), "project")) {
			return null;
		}
		
		RavelryProject proj = translateProject(json.getAsJsonObject().getAsJsonObject("project"));
		proj.setFullyFilled(true);
		return proj;

	}

}
