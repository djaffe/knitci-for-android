package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryMessage;
import com.knitci.pocket.util.JsonUtil;

public class RetrieveMessage extends AbstractSimpleGetDelegate {

	protected RetrieveMessage(OAuthService service, Token token, int messageId) {
		super(service, token, "https://api.ravelry.com/messages/"+String.valueOf(messageId)+".json");
	}

	public static RavelryObject retrieve(OAuthService service, Token token, int messageId) {
		return new RetrieveMessage(service, token, messageId).execute();
	}
	
	@Override
	protected RavelryObject translate(Response response) {
		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			return null;
		}

		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}
		
		JsonObject jObj = json.getAsJsonObject();
		if(!JsonUtil.isKeyPopulated(jObj, "message")) {
			return null;
		}
		
		RavelryMessage message = new RavelryMessage();
		JsonObject obj = jObj.getAsJsonObject("message");
		message.setMessageId(JsonUtil.isKeyPopulated(obj, "id") ? obj.get("id").getAsInt() : 0);
		message.setHtmlContent(JsonUtil.isKeyPopulated(obj, "content_html") ? obj.get("content_html").getAsString() : null);
		return message;
		
	}

}
