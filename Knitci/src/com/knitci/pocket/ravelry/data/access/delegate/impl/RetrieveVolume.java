package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RetrieveVolume extends AbstractSimpleGetDelegate {

	protected RetrieveVolume(OAuthService service, Token token, int volId) {
		super(service, token, "https://api.ravelry.com/volumes/"+String.valueOf(volId)+".json");
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, int volId) {
		return new RetrieveVolume(service, token, volId).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		// TODO Auto-generated method stub
		return null;
	}

}
