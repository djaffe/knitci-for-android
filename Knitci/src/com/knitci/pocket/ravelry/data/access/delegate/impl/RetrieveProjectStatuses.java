package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RetrieveProjectStatuses extends AbstractSimpleGetDelegate {

	protected RetrieveProjectStatuses(OAuthService service, Token token) {
		super(service, token, "https://api.ravelry.com/projects/project_statuses.json");
	}

	public static RavelryObject retrieve(OAuthService service, Token token) {
		return new RetrieveProjectStatuses(service, token).execute();
	}
	
	@Override
	protected RavelryObject translate(Response response) {
		// TODO Auto-generated method stub
		return null;
	}

}
