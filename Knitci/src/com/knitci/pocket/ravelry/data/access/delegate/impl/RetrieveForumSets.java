package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.ravelry.data.object.impl.RavelryForum;
import com.knitci.pocket.util.JsonUtil;
import com.knitci.pocket.util.StandardConvert;

public class RetrieveForumSets extends AbstractSimpleGetDelegate {

	protected RetrieveForumSets(OAuthService service, Token token) {
		super(service, token, "https://api.ravelry.com/forums/sets.json");
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token) {
		return new RetrieveForumSets(service, token).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			return null;
		}

		RavelryCollection collection = new RavelryCollection();

		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}
		
		JsonObject jObj = json.getAsJsonObject();
		JsonArray jArr = jObj.getAsJsonArray("forum_sets");
		
		for(int i = 0; i < jArr.size(); i++) {
			JsonObject jSet = (JsonObject) jArr.get(i);
			
			if(JsonUtil.isKeyPopulated(jSet, "permalink") && jSet.get("permalink").getAsString().equals("your-boards")) {
				JsonArray jArr2 = jSet.get("selected_forums").getAsJsonArray();
				for(int j = 0; j < jArr2.size(); j++) {
					RavelryForum forum = new RavelryForum();
					
					if(!JsonUtil.isKeyPopulated(jArr2.get(j).getAsJsonObject(), "forum")) {
						continue;
					}
					
					JsonObject jForum = jArr2.get(j).getAsJsonObject().get("forum").getAsJsonObject();
					forum.setPermalink(JsonUtil.isKeyPopulated(jForum, "permalink") ? jForum.get("permalink").getAsString() : null);
					forum.setName(JsonUtil.isKeyPopulated(jForum, "name") ? jForum.get("name").getAsString() : null);
					forum.setId(JsonUtil.isKeyPopulated(jForum, "id") ? jForum.get("id").getAsInt() : -1);
					forum.setTopicsCount(JsonUtil.isKeyPopulated(jForum, "topics_count") ? jForum.get("topics_count").getAsInt() : -1);
					forum.setLastPostDtTm(JsonUtil.isKeyPopulated(jForum, "last_post") ? StandardConvert.stringToDate(jForum.get("last_post").getAsString()) : null);					
					if(forum.getPermalink() != null && forum.getName() != null && forum.getId() > 0) {
						collection.add(forum);
					}
				}
			}
		}
		return collection;
	}

}
