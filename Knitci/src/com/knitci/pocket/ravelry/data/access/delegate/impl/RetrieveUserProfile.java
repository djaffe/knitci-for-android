package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import android.content.Context;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryUser;

public class RetrieveUserProfile extends AbstractSimpleGetDelegate {

	Context context; 
	
	protected RetrieveUserProfile(OAuthService service, Token token, int userId, Context context) {
		super(service, token, "https://api.ravelry.com/people/" + userId + ".json");
		this.context = context;
	}
	
	protected RetrieveUserProfile(OAuthService service, Token token, String username, Context context) {
		super(service, token, "https://api.ravelry.com/people/" + username + ".json");
		this.context = context;
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, int userId, Context context) {
		return new RetrieveUserProfile(service, token, userId, context).execute();
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, String username, Context context) {
		return new RetrieveUserProfile(service, token, username, context).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			return null;
		}
		
		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}
		
		JsonObject obj = json.getAsJsonObject().getAsJsonObject("user");
		
		RavelryUser user = new RavelryUser();
		
		translateRavelryUser(obj, context);
		
		return user;
	}

}
