package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RetrieveFriends extends AbstractSimpleGetDelegate {

	protected RetrieveFriends(OAuthService service, Token token, String username) {
		super(service, token, "https://api.ravelry.com/people/"+username+"/frields/list.json");
		// TODO Auto-generated constructor stub
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, String username) {
		return new RetrieveFriends(service, token, username).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		// TODO Auto-generated method stub
		return null;
	}

}
