package com.knitci.pocket.ravelry.data.access.delegate.impl;

import java.util.ArrayList;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryPaginator;
import com.knitci.pocket.ravelry.data.object.RavelryParam;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.ravelry.data.object.impl.RavelryMessage;
import com.knitci.pocket.util.JsonUtil;
import com.knitci.pocket.util.StandardConvert;

public class RetrieveMessages extends AbstractSimpleGetDelegate {

	Context context; 
	
	protected RetrieveMessages(OAuthService service, Token token, ArrayList<RavelryParam> params, Context context) {
		super(service, token, "https://api.ravelry.com/messages/list.json");
		setParams(params);
		this.context = context;
	}

	public static RavelryObject retrieve(OAuthService service, Token token, ArrayList<RavelryParam> params, Context context) {
		return new RetrieveMessages(service, token, params, context).execute();
	}
	
	@Override
	protected RavelryObject translate(Response response) {
		
		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			return null;
		}

		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}
		JsonObject jObj = json.getAsJsonObject();
		
		if(!JsonUtil.isKeyPopulated(jObj, "messages")) {
			return null;
		}
		
		RavelryPaginator paginator = null;
		if(JsonUtil.isKeyPopulated(jObj, "paginator")) {
			paginator = translatePaginator(jObj.getAsJsonObject("paginator"));
		}
				
		RavelryCollection collection = new RavelryCollection();
		collection.setPaginator(paginator);
		
		JsonArray messageArr = jObj.getAsJsonArray("messages");
		for(int i = 0; i < messageArr.size(); i++) {
			JsonObject messageObj = messageArr.get(i).getAsJsonObject();
			RavelryMessage message = new RavelryMessage();
			
			message.setMessageType(JsonUtil.isKeyPopulated(messageObj, "message_type_name") ? messageObj.get("message_type_name").getAsString() : null);
			message.setRecipient(JsonUtil.isKeyPopulated(messageObj, "recipient") ? translateRavelryUser(messageObj.get("recipient").getAsJsonObject(), context) : null);
			message.setSender(JsonUtil.isKeyPopulated(messageObj, "sender") ? translateRavelryUser(messageObj.get("sender").getAsJsonObject(), context) : null);
			message.setSubject(JsonUtil.isKeyPopulated(messageObj, "subject") ? messageObj.get("subject").getAsString() : null);
			message.setMessageId(JsonUtil.isKeyPopulated(messageObj, "id") ? messageObj.get("id").getAsInt() : 0);
			message.setMessageRead(JsonUtil.isKeyPopulated(messageObj, "read_message") ? messageObj.get("read_message").getAsBoolean() : true);
			message.setMessageDtTm(JsonUtil.isKeyPopulated(messageObj, "sent_at") ? StandardConvert.stringToDate(messageObj.get("sent_at").getAsString()) : null);
			
			collection.add(message);
		}
		
		return collection;
	}

}
