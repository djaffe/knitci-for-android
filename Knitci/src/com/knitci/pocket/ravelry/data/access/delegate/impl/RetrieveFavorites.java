package com.knitci.pocket.ravelry.data.access.delegate.impl;

import java.util.ArrayList;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractComplexGetDelegate;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryPaginator;
import com.knitci.pocket.ravelry.data.object.RavelryParam;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.util.JsonUtil;

public class RetrieveFavorites extends AbstractSimpleGetDelegate {

	private RetrieveFavorites(OAuthService service, Token token, String username, ArrayList<RavelryParam> params) {
		super(service, token, "https://api.ravelry.com/people/"+username+"/favorites/list.json");
		setParams(params);
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, String username, ArrayList<RavelryParam> params) {
		return new RetrieveFavorites(service, token, username, params).execute();
	}
	
	@Override
	protected RavelryObject translate(Response response) {


		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			return null;
		}

		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}
		JsonObject jObj = json.getAsJsonObject();
		
		if(!JsonUtil.isKeyPopulated(jObj, "favorites")) {
			return null;
		}
		
		RavelryPaginator paginator = null;
		if(JsonUtil.isKeyPopulated(jObj, "paginator")) {
			paginator = translatePaginator(jObj.getAsJsonObject("paginator"));
		}
		 
		JsonArray jArr = jObj.getAsJsonArray("favorites");
		RavelryCollection collection = new RavelryCollection();
		collection.setPaginator(paginator);
		
		for(int i = 0; i < jArr.size(); i++) {
			JsonObject obj = jArr.get(i).getAsJsonObject();
			
		}
		return null;
	}
	
}
