package com.knitci.pocket.ravelry.data.access.delegate.impl;

import java.util.ArrayList;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimplePostDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryParam;

public class SubmitMarkForumPostRead extends AbstractSimplePostDelegate {

	protected SubmitMarkForumPostRead(OAuthService service, Token token, int topicId, ArrayList<RavelryParam> params) {
		super(service, token, "https://api.ravelry.com/topics/"+topicId+"/read.json");
		setParams(params);
	}
	
	public static RavelryObject submit(OAuthService service, Token token, int topicId, ArrayList<RavelryParam> params) {
		return new SubmitMarkForumPostRead(service, token, topicId, params).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		// TODO Auto-generated method stub
		return null;
	}

}
