package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryUser;
import com.knitci.pocket.util.JsonUtil;

/**
 * 
 * @author DJ021930
 *
 */
public class RetrieveSpecificProject extends AbstractSimpleGetDelegate {
	
	private final String username;
	private final int projectId;
	
	/**
	 * Constructor. This should never be directly called. 
	 * @param service {@link OAuthService} An OAuthService for OAuth queries. Cannot be null. 
	 * @param token {@link Token} The token containing token and secret key. 
	 * @param username String The username of the owner of the project.
	 * @param projectId int The project Id. 
	 */
	private RetrieveSpecificProject(OAuthService service, Token token, String username, int projectId) {
		super(service, token, "https://api.ravelry.com/projects/"+username+"/"+projectId+".json");
		this.username = username;
		this.projectId = projectId;
	}
	
	/**
	 * Public method for consuming this delegate. 
	 * @param service {@link OAuthService} An OAuthService object for OAuth queries. Cannot be null. 
	 * @param token {@link Token} Token to be used. Cannot be null. 
	 * @param username String The username of the owner of the project.
	 * @param projectId int The project id. 
	 * @return RavelryProject A {@link RavelryProject} object. Can be null if the call failed or parsing failed. 
	 */
	public static RavelryObject retrieve(OAuthService service, Token token, String username, int projectId) {
		return new RetrieveSpecificProject(service, token, username, projectId).execute();
	}

	/*
	 * (non-Javadoc)
	 * @see com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate#translate(org.scribe.model.Response)
	 */
	@Override
	protected RavelryObject translate(Response response) {
		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			return null;
		}

		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}

		if(!JsonUtil.isKeyPopulated(json.getAsJsonObject(), "project")) {
			return null;
		}
		
		RavelryProject proj = translateProject(json.getAsJsonObject().getAsJsonObject("project"));
		proj.setFullyFilled(true);
		return proj;

	}

}
