package com.knitci.pocket.ravelry.data.access.delegate.impl;

import java.util.ArrayList;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryPaginator;
import com.knitci.pocket.ravelry.data.object.RavelryParam;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.ravelry.data.object.impl.RavelryQueuedProject;
import com.knitci.pocket.util.JsonUtil;
import com.knitci.pocket.util.StandardConvert;

public class RetrieveUserQueue extends AbstractSimpleGetDelegate {

	private String username;
	
	protected RetrieveUserQueue(OAuthService service, Token token, String username, ArrayList<RavelryParam> params) {
		super(service, token, "https://api.ravelry.com/people/"+username+"/queue/list.json");
		this.username = username;
		setParams(params);
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, String username, ArrayList<RavelryParam> params) {
		return new RetrieveUserQueue(service, token, username, params).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			return null;
		}
		
		RavelryCollection coll = new RavelryCollection();

		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}
		
		JsonObject jObj = json.getAsJsonObject();
		
		if(!JsonUtil.isKeyPopulated(jObj, "queued_projects")) {
			return null;
		}
		
		if(JsonUtil.isKeyPopulated(jObj, "paginator")) {
			coll.setPaginator(translatePaginator(getSafeJsonObject(jObj, "paginator")));
		}
		
		JsonArray arr = getSafeJsonArray(jObj, "queued_projects");
		for(int i = 0; i < arr.size(); i++) {
			JsonObject qObj = arr.get(i).getAsJsonObject();
			coll.add(translateQueue(qObj));
		}
		return coll;
	}
	
	private RavelryQueuedProject translateQueue(JsonObject obj) {
		RavelryQueuedProject queue = new RavelryQueuedProject();
		
		queue.setYarnId(getSafeJsonInt(obj, "yarn_id"));
		queue.setPatternName(getSafeJsonString(obj, "pattern_name"));
		queue.setPatternId(getSafeJsonInt(obj, "pattern_id"));
		queue.setCreatedDtTm(StandardConvert.stringToDate(getSafeJsonString(obj, "created_at")));
		queue.setMakeFor(getSafeJsonString(obj, "make_for"));
		queue.setPosition(getSafeJsonInt(obj, "position_in_queue"));
		queue.setNotes(getSafeJsonString(obj, "notes"));
		queue.setSortOrder(getSafeJsonInt(obj, "sort_order"));
		queue.setBestPhoto(translatePhoto(getSafeJsonObject(obj, "best_photo")));
		queue.setPatternAuthorId(getSafeJsonInt(obj, "pattern_author_id"));
		queue.setSkeins(getSafeJsonInt(obj, "skeins"));
		queue.setQueueId(getSafeJsonInt(obj, "id"));
		queue.setUserId(getSafeJsonInt(obj, "user_id"));
		queue.setStartDtTm(StandardConvert.stringToDate(getSafeJsonString(obj, "start_on")));
		queue.setName(getSafeJsonString(obj, "name"));
		queue.setPatternAuthorName(getSafeJsonString(obj, "pattern_author_name"));
		queue.setShortPatternName(getSafeJsonString(obj, "short_pattern_name"));
		queue.setYarnName(getSafeJsonString(obj, "yarn_name"));
		queue.setUsername(this.username);
		
		return queue;
	}

}
