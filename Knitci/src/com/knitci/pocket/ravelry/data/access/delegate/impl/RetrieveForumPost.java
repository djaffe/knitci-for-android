package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RetrieveForumPost extends AbstractSimpleGetDelegate {

	protected RetrieveForumPost(OAuthService service, Token token, int forumPostId) {
		super(service, token, "https://api.ravelry.com/forum_posts/"+String.valueOf(forumPostId)+".json");
	}

	public static RavelryObject retrieve(OAuthService service, Token token, int forumPostId) {
		return new RetrieveForumPost(service, token, forumPostId).execute();
	}
	
	@Override
	protected RavelryObject translate(Response response) {
		// TODO Auto-generated method stub
		return null;
	}

}
