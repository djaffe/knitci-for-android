package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.tools.needle.types.Needle;
import com.knitci.pocket.util.JsonUtil;

public class RetrieveNeedleTypes extends AbstractSimpleGetDelegate {

	protected RetrieveNeedleTypes(OAuthService service, Token token) {
		super(service, token, "https://api.ravelry.com/needles/types.json");
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token) {
		return new RetrieveNeedleTypes(service, token).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {

		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			return null;
		}
		RavelryCollection needles = new RavelryCollection();
		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}

		if(!JsonUtil.isKeyPopulated(json.getAsJsonObject(), "needle_types")) {
			return null;
		}
		
		JsonArray needleTypes = json.getAsJsonObject().getAsJsonArray("needle_types");
		for(int i = 0; i < needleTypes.size(); i++) {
			JsonObject obj = needleTypes.get(i).getAsJsonObject();
			Needle needle = new Needle();

			needle.setNeedleSizeId(getSafeJsonInt(obj, "needle_size_id"));
			needle.setType(getSafeJsonString(obj, "type_name"));
			needle.setDescription(getSafeJsonString(obj, "description"));
			needle.setId(getSafeJsonInt(obj, "id"));
			needle.setName(getSafeJsonString(obj, "name"));
			needle.setMetricName(getSafeJsonString(obj, "metric_name"));
			needle.setLength(getSafeJsonInt(obj, "length"));
			
			needles.add(needle);
		}

		needles.setHttpCode(httpResponseCode);
		return needles;
		
	}

}
