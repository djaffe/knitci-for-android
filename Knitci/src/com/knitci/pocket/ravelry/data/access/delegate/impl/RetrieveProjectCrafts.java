package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RetrieveProjectCrafts extends AbstractSimpleGetDelegate {

	protected RetrieveProjectCrafts(OAuthService service, Token token) {
		super(service, token, "https://api.ravelry.com/projects/crafts.json");
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token) {
		return new RetrieveProjectCrafts(service, token).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		// TODO Auto-generated method stub
		return null;
	}

}
