package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RetrieveSpecificQueue extends AbstractSimpleGetDelegate {

	protected RetrieveSpecificQueue(OAuthService service, Token token, String username, int queueId) {
		super(service, token, "https://api.ravelry.com/people/"+username+"/queue/"+String.valueOf(queueId)+".json");
	}

	public static RavelryObject retrieve(OAuthService service, Token token, String username, int queueId) {
		return new RetrieveSpecificQueue(service, token, username, queueId).execute();
	}
	
	@Override
	protected RavelryObject translate(Response response) {
		// TODO Auto-generated method stub
		return null;
	}

}
