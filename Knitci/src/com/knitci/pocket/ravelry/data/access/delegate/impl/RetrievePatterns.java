package com.knitci.pocket.ravelry.data.access.delegate.impl;

import java.util.ArrayList;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryParam;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPattern;
import com.knitci.pocket.util.JsonUtil;

public class RetrievePatterns extends AbstractSimpleGetDelegate {

	protected RetrievePatterns(OAuthService service, Token token, ArrayList<RavelryParam> params) {
		super(service, token, "https://api.ravelry.com/patterns/search.json");
		setParams(params);
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, ArrayList<RavelryParam> params) {
		return new RetrievePatterns(service, token, params).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			return null;
		}

		RavelryCollection collection = new RavelryCollection();

		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}
		
		JsonObject jObj = json.getAsJsonObject();
		if(JsonUtil.isKeyPopulated(jObj, "paginator")) {
			collection.setPaginator(translatePaginator(jObj.getAsJsonObject("paginator")));
		}
		
		JsonArray jArr = jObj.getAsJsonArray("patterns");
		for(int i = 0; i < jArr.size(); i++) {
			JsonObject jProj = jArr.get(i).getAsJsonObject();
			
			RavelryPattern pattern = new RavelryPattern();
			
			pattern.setPermalink(JsonUtil.isKeyPopulated(jProj, "permalink") ? jProj.get("permalink").getAsString() : null);
			pattern.setPatternName(JsonUtil.isKeyPopulated(jProj, "name") ? jProj.get("name").getAsString() : null);
			pattern.setPatternId(JsonUtil.isKeyPopulated(jProj, "id") ? jProj.get("id").getAsInt() : -1);
			pattern.setFirstPhotos(JsonUtil.isKeyPopulated(jProj, "first_photo") ? translateFirstPhotos(jProj.getAsJsonObject("first_photo")) : null);
			
			pattern.applyPersonalAttributes(translatePersonalAttributes(getSafeJsonObject(jProj, "personal_attributes")));
			
			collection.add(pattern);
		}
		
		return collection;
	}

}
