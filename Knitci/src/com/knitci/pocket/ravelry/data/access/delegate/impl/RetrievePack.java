package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RetrievePack extends AbstractSimpleGetDelegate {

	protected RetrievePack(OAuthService service, Token token, int packId) {
		super(service, token, "https://api.ravelry.com/packs/"+String.valueOf(packId)+".json");
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, int packId) {
		return new RetrievePack(service, token, packId).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		// TODO Auto-generated method stub
		return null;
	}

}
