package com.knitci.pocket.ravelry.data.access.delegate.impl;

import java.util.ArrayList;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimplePostDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryParam;
import com.knitci.pocket.ravelry.data.object.impl.RavelryForumPost;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPostResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryQueuedProject;
import com.knitci.pocket.util.JsonUtil;
import com.knitci.pocket.util.StandardConvert;

public class SubmitForumPost extends AbstractSimplePostDelegate {

	protected SubmitForumPost(OAuthService service, Token token, int topicId, ArrayList<RavelryParam> params) {
		super(service, token, "https://api.ravelry.com/topics/"+topicId+"/reply.json");
		setParams(params);
	}

	public static RavelryObject submit(OAuthService service, Token token, int topicId, ArrayList<RavelryParam> params) {
		return new SubmitForumPost(service, token, topicId, params).execute();
	}
	
	@Override
	protected RavelryObject translate(Response response) {
		Log.e("SubmitForumPost", "ResponseCode was " + response.getCode());
		Log.e("SubmitForumPost", "Body was " + response.getBody());
		RavelryForumPost post = new RavelryForumPost();
		
		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			post.setHttpCode(httpResponseCode);
			return post;
		}

		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}

		if(!JsonUtil.isKeyPopulated(json.getAsJsonObject(), "forum_post")) {
			return null;
		}
		
		JsonObject postObj = json.getAsJsonObject().getAsJsonObject("forum_post");
		
		
		
		post.setParentPostNumber(JsonUtil.isKeyPopulated(postObj, "parent_post_number") ? postObj.get("parent_post_number").getAsInt() : 0);
		post.setCreatedDtTm(JsonUtil.isKeyPopulated(postObj, "created_at") ? StandardConvert.stringToDate(postObj.get("created_at").getAsString()) : null);
		post.setBody(JsonUtil.isKeyPopulated(postObj, "body") ? postObj.get("body").getAsString() : null);
		post.setUpdatedDtTm(JsonUtil.isKeyPopulated(postObj, "updated_at") ? StandardConvert.stringToDate(postObj.get("updated_at").getAsString()) : null);
		post.setEditable(JsonUtil.isKeyPopulated(postObj, "editable") ? postObj.get("editable").getAsBoolean() : false);
		post.setId(JsonUtil.isKeyPopulated(postObj, "id") ? postObj.get("id").getAsInt() : -1);
		post.setDeleted(JsonUtil.isKeyPopulated(postObj, "deleted") ? postObj.get("deleted").getAsBoolean() : false);
		post.setReplyCount(JsonUtil.isKeyPopulated(postObj, "reply_count") ? postObj.get("reply_count").getAsInt() : -1);
		post.setBodyHtml(JsonUtil.isKeyPopulated(postObj, "body_html") ? postObj.get("body_html").getAsString() : null);
		post.setParentPostId(JsonUtil.isKeyPopulated(postObj, "parent_post_id") ? postObj.get("parent_post_id").getAsInt() : -1);
		post.setPostNumber(JsonUtil.isKeyPopulated(postObj, "post_number") ? postObj.get("post_number").getAsInt() : -1);
		
		return post;
	}

}
