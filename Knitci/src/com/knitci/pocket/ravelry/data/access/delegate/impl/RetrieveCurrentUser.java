package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import android.content.Context;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryUser;

/**
 * Delegate for calling ravelry API to get the current user information. 
 * @author DJ021930
 */
public class RetrieveCurrentUser extends AbstractSimpleGetDelegate {

	Context context;
	
	/**
	 * Constructor. This should never be directly called. 
	 * @param service {@link OAuthService} An OAuthService for OAuth queries. Cannot be null. 
	 * @param token {@link Token} The token containing token and secret key. 
	 */
	private RetrieveCurrentUser(OAuthService service, Token token, Context context) {
		super(service, token, "https://api.ravelry.com/current_user.json");
		this.context = context;
	}
	
	/**
	 * Public method for consuming this delegate. 
	 * @param service {@link OAuthService} An OAuthService object for OAuth queries. Cannot be null. 
	 * @param token {@link Token} Token to be used. Cannot be null. 
	 * @return RavelryUser A {@link RavelryUser} object. Can be null if the call failed or parsing failed. 
	 */
	public static RavelryObject retrieve(OAuthService service, Token token, Context context) {
		return new RetrieveCurrentUser(service, token, context).execute();
	}
	
	/* (non-Javadoc)
	 * @see com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate#translate(org.scribe.model.Response)
	 */
	protected RavelryObject translate(Response response) {
		int httpResponseCd = response.getCode();
		
		if(httpResponseCd != 200) { // OK
			RavelryUser user = new RavelryUser();
			user.setHttpCode(httpResponseCd);
			return user;
		}
		
		RavelryUser user = null;
		
		JsonObject jsonObjUser;
		try {
			JsonElement json = new JsonParser().parse(response.getBody());
			JsonObject jsonObj = json.getAsJsonObject();
			JsonElement jsonUserEle = jsonObj.get("user");
			
			jsonObjUser = jsonUserEle.getAsJsonObject();
			
			user = translateRavelryUser(jsonObjUser, context);
			//JsonElement jsonUsername = jsonObjUser.get("username");
			//user.setUsername(jsonUsername.getAsString());
		} 
		catch (JsonParseException e) {
			return null;
		}
		
		// Not concerned with not able to get the user photo URLs, so ignore the
		// exception and let the user object return. 
		/*try {
			JsonElement jsonPhotoL = jsonObjUser.get("photo_url");
			user.setPhotoUrl(jsonPhotoL.getAsString());

			JsonElement jsonPhotoS = jsonObjUser.get("small_photo_url");
			user.setPhotoUrlSmall(jsonPhotoS.getAsString());

			JsonElement jsonPhotoT = jsonObjUser.get("tiny_photo_url");
			user.setPhotoUrlTiny(jsonPhotoT.getAsString());
		}
		catch (JsonParseException e) { }
		*/
		return user;
	}

}		