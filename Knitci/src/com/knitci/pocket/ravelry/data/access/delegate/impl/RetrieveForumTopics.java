package com.knitci.pocket.ravelry.data.access.delegate.impl;

import java.util.ArrayList;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryPaginator;
import com.knitci.pocket.ravelry.data.object.RavelryParam;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.ravelry.data.object.impl.RavelryTopic;
import com.knitci.pocket.util.JsonUtil;
import com.knitci.pocket.util.StandardConvert;

public class RetrieveForumTopics extends AbstractSimpleGetDelegate {

	protected RetrieveForumTopics(OAuthService service, Token token, int forumId, ArrayList<RavelryParam> params) {
		super(service, token, "https://api.ravelry.com/forums/"+String.valueOf(forumId)+"/topics.json");
		setParams(params);
		// TODO Auto-generated constructor stub
	}

	public static RavelryObject retrieve(OAuthService service, Token token, int forumId, ArrayList<RavelryParam> params) {
		return new RetrieveForumTopics(service, token, forumId, params).execute();
	}
	@Override
	protected RavelryObject translate(Response response) {
		
		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			return null;
		}

		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}
		JsonObject jObj = json.getAsJsonObject();
		
		if(!JsonUtil.isKeyPopulated(jObj, "forum")) {
			return null;
		}
		
		JsonObject forumObj = jObj.getAsJsonObject("forum");
		int forumId = (JsonUtil.isKeyPopulated(forumObj, "id") ? forumObj.get("id").getAsInt() : -1);
		if(forumId <= 0) {
			return null;
		}
		
		RavelryPaginator paginator = null;
		if(JsonUtil.isKeyPopulated(jObj, "paginator")) {
			paginator = translatePaginator(jObj.getAsJsonObject("paginator"));
		}
		
		if(!JsonUtil.isKeyPopulated(jObj, "topics")) {
			return null;
		}
		
		RavelryCollection collection = new RavelryCollection();
		collection.miscId = forumId;
		collection.setPaginator(paginator);
		
		JsonArray topicsArr = jObj.getAsJsonArray("topics");
		
		for(int i = 0; i < topicsArr.size(); i++) {
			JsonObject tObj = topicsArr.get(i).getAsJsonObject();
			
			RavelryTopic topic = new RavelryTopic();
			topic.setCreatedDtTm(JsonUtil.isKeyPopulated(tObj, "created_at") ? StandardConvert.stringToDate(tObj.get("created_at").getAsString()) : null);
			topic.setTitle(JsonUtil.isKeyPopulated(tObj, "title") ? tObj.get("title").getAsString() : null);
			topic.setRepliedDtTm(JsonUtil.isKeyPopulated(tObj, "replied_at") ? StandardConvert.stringToDate(tObj.get("replied_at").getAsString()) : null);
			topic.setForumImagesCount(JsonUtil.isKeyPopulated(tObj, "forum_images_count") ? tObj.get("forum_images_count").getAsInt() : 0);
			topic.setArchived(JsonUtil.isKeyPopulated(tObj, "archived") ? tObj.get("archived").getAsBoolean() : false);
			topic.setId(JsonUtil.isKeyPopulated(tObj, "id") ? tObj.get("id").getAsInt() : -1);
			topic.setForumId(JsonUtil.isKeyPopulated(tObj, "forum_id") ? tObj.get("forum_id").getAsInt() : -1);
			topic.setIgnored(JsonUtil.isKeyPopulated(tObj, "ignored") ? tObj.get("ignored").getAsBoolean() : false);
			topic.setSticky(JsonUtil.isKeyPopulated(tObj, "sticky") ? tObj.get("sticky").getAsBoolean() : false);
			topic.setLatestReply(JsonUtil.isKeyPopulated(tObj, "latest_reply") ? tObj.get("latest_reply").getAsInt() : 0);
			topic.setLastRead(JsonUtil.isKeyPopulated(tObj, "last_read") ? tObj.get("last_read").getAsInt() : 0);
			topic.setForumPostsCount(JsonUtil.isKeyPopulated(tObj, "forum_posts_count") ? tObj.get("forum_posts_count").getAsInt() : -1);
			topic.setWatched(JsonUtil.isKeyPopulated(tObj, "locked") ? tObj.get("locked").getAsBoolean() : false);
			
			collection.add(topic);
		}
		
		return collection;
	}

}
