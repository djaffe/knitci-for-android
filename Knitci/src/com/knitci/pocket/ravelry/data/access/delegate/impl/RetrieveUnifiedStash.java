package com.knitci.pocket.ravelry.data.access.delegate.impl;

import java.util.ArrayList;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryPaginator;
import com.knitci.pocket.ravelry.data.object.RavelryParam;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.ravelry.data.object.impl.RavelryFiberStash;
import com.knitci.pocket.ravelry.data.object.impl.RavelryStash;
import com.knitci.pocket.ravelry.data.object.impl.RavelryStashGeneric.StashType;
import com.knitci.pocket.util.JsonUtil;

public class RetrieveUnifiedStash extends AbstractSimpleGetDelegate {

	protected RetrieveUnifiedStash(OAuthService service, Token token, String username, ArrayList<RavelryParam> params) {
		super(service, token, "https://api.ravelry.com/people/"+username+"/stash/unified/list.json");
		setParams(params);
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, String username, ArrayList<RavelryParam> params) {
		return new RetrieveUnifiedStash(service, token, username, params).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		
		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			return null;
		}

		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}
		JsonObject jObj = json.getAsJsonObject();
		
		if(!JsonUtil.isKeyPopulated(jObj, "unified_stash")) {
			return null;
		}
		
		RavelryPaginator paginator = null;
		if(JsonUtil.isKeyPopulated(jObj, "paginator")) {
			paginator = translatePaginator(jObj.getAsJsonObject("paginator"));
		}
		 
		JsonArray jArr = jObj.getAsJsonArray("unified_stash");
		RavelryCollection collection = new RavelryCollection();
		collection.setPaginator(paginator);
		
		for(int i = 0; i < jArr.size(); i++) {
			JsonObject unifObj = jArr.get(i).getAsJsonObject();
			if(JsonUtil.isKeyPopulated(unifObj, "stash")) {
				RavelryStash st = translateStash(getSafeJsonObject(unifObj, "stash"));
				st.setType(StashType.STASH);
				collection.add(st);
			}
			else if(JsonUtil.isKeyPopulated(unifObj, "fiber_stash")) {
				RavelryFiberStash st = translateFiberStash(getSafeJsonObject(unifObj, "fiber_stash"));
				st.setType(StashType.FIBER);
				collection.add(st);
			}
		}
		
		return collection;
	}
}
