package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RetrievePhotoSizes extends AbstractSimpleGetDelegate {

	protected RetrievePhotoSizes(OAuthService service, Token token, int photoId) {
		super(service, token, "https://api.ravelry.com/photos/"+String.valueOf(photoId)+"/sizes.json");
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, int photoId) {
		return new RetrievePhotoSizes(service, token, photoId).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		// TODO Auto-generated method stub
		return null;
	}

}
