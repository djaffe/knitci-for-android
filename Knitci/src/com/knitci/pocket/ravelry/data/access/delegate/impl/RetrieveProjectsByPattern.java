package com.knitci.pocket.ravelry.data.access.delegate.impl;

import java.util.ArrayList;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryParam;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject;


public class RetrieveProjectsByPattern extends AbstractSimpleGetDelegate {

	protected RetrieveProjectsByPattern(OAuthService service, Token token, int patternId, ArrayList<RavelryParam> params) {
		super(service, token, "https://api.ravelry.com/patterns/" + patternId + "/projects.json");
		setParams(params);
	}

	public static RavelryObject retrieve(OAuthService service, Token token, int patternId, ArrayList<RavelryParam> params) {
		return new RetrieveProjectsByPattern(service, token, patternId, params).execute();
	}
	
	@Override
	protected RavelryObject translate(Response response) {


		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			return null;
		}

		RavelryCollection collection = new RavelryCollection();

		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}
		
		JsonObject jObj = json.getAsJsonObject();
		JsonArray jArr = jObj.getAsJsonArray("projects");
		for(int i = 0; i < jArr.size(); i++) {
			RavelryProject ravelryProject = new RavelryProject();
			
			JsonObject jProj = jArr.get(i).getAsJsonObject();
			ravelryProject = translateProject(jProj);	
			
			collection.add(ravelryProject);
		}
		
		collection.setPaginator(translatePaginator(getSafeJsonObject(jObj, "paginator")));
		
		return collection;
		
	}

}
