package com.knitci.pocket.ravelry.data.access.delegate;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.knitci.pocket.ravelry.data.object.RavelryObject;


public abstract class AbstractComplexGetDelegate extends AbstractDelegate {

	protected final String URL;
	protected final OAuthService service;
	protected final Token token;
	
	protected AbstractComplexGetDelegate(OAuthService service, Token token, String URL) {
		this.service = service;
		this.token = token;
		this.URL = URL;
	}

	
	/*
	 * (non-Javadoc)
	 * @see com.knitci.pocket.ravelry.data.access.delegate.AbstractDelegate#execute()
	 */
	protected RavelryObject execute() {
		return null;
		
	}
	
	
}
