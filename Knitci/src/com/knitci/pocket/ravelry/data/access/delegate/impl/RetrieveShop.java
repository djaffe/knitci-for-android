package com.knitci.pocket.ravelry.data.access.delegate.impl;

import java.util.ArrayList;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryParam;

public class RetrieveShop extends AbstractSimpleGetDelegate {

	protected RetrieveShop(OAuthService service, Token token, int shopId, ArrayList<RavelryParam> params) {
		super(service, token, "http://api.ravelry.com/shops/"+String.valueOf(shopId)+".json");
		setParams(params);
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, int shopId, ArrayList<RavelryParam> params) {
		return new RetrieveShop(service, token, shopId, params).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		// TODO Auto-generated method stub
		return null;
	}

}
