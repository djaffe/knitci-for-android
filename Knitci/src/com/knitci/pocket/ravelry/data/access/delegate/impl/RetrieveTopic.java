package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RetrieveTopic extends AbstractSimpleGetDelegate {

	protected RetrieveTopic(OAuthService service, Token token, int topicId) {
		super(service, token, "https://api.ravelry.com/topics/"+String.valueOf(topicId)+".json");
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, int topicId) {
		return new RetrieveTopic(service, token, topicId).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		// TODO Auto-generated method stub
		return null;
	}

}
