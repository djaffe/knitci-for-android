package com.knitci.pocket.ravelry.data.access.delegate;

import java.util.ArrayList;
import java.util.Iterator;

import org.scribe.model.Response;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryPaginator;
import com.knitci.pocket.ravelry.data.object.RavelryParam;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCraft;
import com.knitci.pocket.ravelry.data.object.impl.RavelryDownloadLocation;
import com.knitci.pocket.ravelry.data.object.impl.RavelryFiberStash;
import com.knitci.pocket.ravelry.data.object.impl.RavelryNeedleSize;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPack;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPersonalAttribute;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPhoto;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPhotoSet;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryQueuedProject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryStash;
import com.knitci.pocket.ravelry.data.object.impl.RavelryStashStatus;
import com.knitci.pocket.ravelry.data.object.impl.RavelryYarnCompany;
import com.knitci.pocket.ravelry.data.object.impl.RavelryYarnWeight;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject.ProjectPhotoUrlSize;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject.ProjectStatus;
import com.knitci.pocket.ravelry.data.object.impl.RavelryUser;
import com.knitci.pocket.ravelry.data.object.impl.RavelryYarn;
import com.knitci.pocket.social.people.manager.PersonManager;
import com.knitci.pocket.util.JsonUtil;
import com.knitci.pocket.util.StandardConvert;

abstract class AbstractDelegate {

	protected ArrayList<RavelryParam> params;

	protected void setParams(ArrayList<RavelryParam> params) {
		this.params = params;
	}

	/**
	 * Executes the delegate. This should be called from the static retrieve method
	 * after constructing the delegate. It must be overridden to return the appropriate
	 * data object type. 
	 * @return Object A data object. 
	 */
	protected abstract RavelryObject execute();

	/**
	 * Translates an OAuth response object into a Ravelry data object. This function must be overridden
	 * in order to take a Ravelry OAuth JSON rsponse and parse it into a data object. 
	 * @param response {@link Response} A Scribe OAuth response. 
	 * @return {@link RavelryObject} A data object.
	 */
	protected abstract RavelryObject translate(Response response);

	protected RavelryPaginator translatePaginator(JsonObject paginatorJson) {
		RavelryPaginator paginator = null;

		if(null != paginatorJson && !paginatorJson.isJsonNull()) {
			paginator = new RavelryPaginator();
			if(JsonUtil.isKeyPopulated(paginatorJson, "last_page")) {
				paginator.setLastPage(paginatorJson.get("last_page").getAsInt());
			}
			if(JsonUtil.isKeyPopulated(paginatorJson, "page_count")) {
				paginator.setPageCount(paginatorJson.get("page_count").getAsInt());
			}
			if(JsonUtil.isKeyPopulated(paginatorJson, "page")) {
				paginator.setPage(paginatorJson.get("page").getAsInt());
			}
			if(JsonUtil.isKeyPopulated(paginatorJson, "results")) {
				paginator.setResults(paginatorJson.get("results").getAsInt());
			}
			if(JsonUtil.isKeyPopulated(paginatorJson, "page_size")) {
				paginator.setPageSize(paginatorJson.get("page_size").getAsInt());
			}
		}

		return paginator;
	}

	/**
	 * Takes the RavelryParams and turns them all into a single param string to be appended to
	 * the end of the URL. 
	 * @return String The prepared string to be added to the URL. 
	 */
	protected String prepareParamString() {
		String urlParams = "";
		boolean startDone = false;
		boolean andNeeded = false;

		if(null == params) {
			return urlParams;
		}

		Iterator<RavelryParam> itRav = params.iterator();
		while(itRav.hasNext()) {
			if(!startDone) {
				urlParams += "?";
				startDone = true;
			}

			if(andNeeded) {
				urlParams += "&";
			}

			RavelryParam param = itRav.next();
			urlParams += param.getKey()+"=";

			String delimiter = param.getDelimiter();
			boolean needsDelimiter = false;

			Iterator<String> itVals = param.iterator();
			while(itVals.hasNext()) {
				String value = itVals.next();
				if(needsDelimiter) {
					urlParams += delimiter;
				}
				urlParams += value;

				needsDelimiter = true;
			}
			andNeeded = true;
		}

		Log.d("QueryParam", urlParams);
		return urlParams;
	}

	/**
	 * Takes the first_photo json and translates it into a {@link RavelryPhotoSet}
	 * @param jOb A {@link JsonObject} from the first_photo element. 
	 * @return {@link RavelryPhotoSet} Will not e null but could not have any photos set. 
	 */
	protected RavelryPhotoSet translateFirstPhotos(JsonObject jOb) {
		RavelryPhotoSet photos = new RavelryPhotoSet();
		if(jOb == null || jOb.isJsonNull()) {
			return photos;
		}

		if(!jOb.get("medium_url").isJsonNull() && !jOb.get("medium_url").getAsString().equals("h")) {
			RavelryPhoto medium = new RavelryPhoto();
			medium.setSize(ProjectPhotoUrlSize.MEDIUM);
			medium.setUrl(jOb.get("medium_url").getAsString());
			photos.addPhoto(ProjectPhotoUrlSize.MEDIUM, medium);
		}

		if(!jOb.get("small_url").isJsonNull() && !jOb.get("small_url").getAsString().equals("h")) {
			RavelryPhoto small = new RavelryPhoto();
			small.setSize(ProjectPhotoUrlSize.SMALL);
			small.setUrl(jOb.get("small_url").getAsString());
			photos.addPhoto(ProjectPhotoUrlSize.SMALL, small);
		}

		if(!jOb.get("square_url").isJsonNull() && !jOb.get("square_url").getAsString().equals("h")) {
			RavelryPhoto square = new RavelryPhoto();
			square.setSize(ProjectPhotoUrlSize.SQUARE);
			square.setUrl(jOb.get("square_url").getAsString());
			photos.addPhoto(ProjectPhotoUrlSize.SQUARE, square);
		}

		if(!jOb.get("thumbnail_url").isJsonNull() && !jOb.get("thumbnail_url").getAsString().equals("h")) {
			RavelryPhoto thumb = new RavelryPhoto();
			thumb.setSize(ProjectPhotoUrlSize.THUMBNAIL);
			thumb.setUrl(jOb.get("thumbnail_url").getAsString());
			photos.addPhoto(ProjectPhotoUrlSize.THUMBNAIL, thumb);
		}
		return photos;
	}

	protected RavelryUser translateRavelryUser(JsonObject userObj, Context context) {
		if(userObj == null || userObj.isJsonNull()) {
			return null;
		}

		PersonManager pm = KnitciApplication.class.cast(context).getPersonManager();
		RavelryUser tUser = pm.getUser(getSafeJsonInt(userObj, "id"));
		if(tUser != null) {
			return tUser;
		}

		RavelryUser user = new RavelryUser();

		user.setId(JsonUtil.isKeyPopulated(userObj, "id") ? userObj.get("id").getAsInt() : -1);
		user.setPhotoUrlSmall(JsonUtil.isKeyPopulated(userObj, "small_photo_url") ? userObj.get("small_photo_url").getAsString() : null);
		user.setPhotoUrl(JsonUtil.isKeyPopulated(userObj, "photo_url") ? userObj.get("photo_url").getAsString() : null);
		user.setPhotoUrlTiny(JsonUtil.isKeyPopulated(userObj, "tiny_photo_url") ? userObj.get("tiny_photo_url").getAsString() : null);
		user.setUsername(JsonUtil.isKeyPopulated(userObj, "username") ? userObj.get("username").getAsString() : null);
		user.setAboutMeHtml(getSafeJsonString(userObj, "about_me_html"));
		user.setAboutMe(getSafeJsonString(userObj, "about_me"));
		user.setLocation(getSafeJsonString(userObj, "location"));
		user.setFavoriteCurse(getSafeJsonString(userObj, "fave_curse"));
		user.setFirstName(getSafeJsonString(userObj, "first_name"));

		KnitciApplication.class.cast(context).getPersonManager().addUser(user);

		return user;
	}

	protected ArrayList<RavelryPhotoSet> translatePhotos(JsonArray photosArr) {
		ArrayList<RavelryPhotoSet> photoSets = new ArrayList<RavelryPhotoSet>();

		if(photosArr == null || photosArr.isJsonNull()) {
			return photoSets;
		}

		for(int i = 0; i < photosArr.size(); i++) {
			JsonObject photoObj = photosArr.get(i).getAsJsonObject();
			photoSets.add(translatePhoto(photoObj));
		}

		return photoSets;
	}

	protected RavelryPhotoSet translatePhoto(JsonObject photo) {
		RavelryPhotoSet photoSet = new RavelryPhotoSet();
		if(photo == null) {
			return photoSet;
		}
		if(photo.get("square_url") != null && !photo.get("square_url").isJsonNull() && !photo.get("square_url").getAsString().equals("h")) {
			RavelryPhoto square = new RavelryPhoto();
			square.setSize(ProjectPhotoUrlSize.SQUARE);
			square.setUrl(photo.get("square_url").getAsString());
			photoSet.addPhoto(ProjectPhotoUrlSize.SQUARE, square);
		}

		if(photo.get("flickr_url") != null && !photo.get("flickr_url").isJsonNull() && !photo.get("flickr_url").getAsString().equals("h")) {
			RavelryPhoto flickr = new RavelryPhoto();
			flickr.setSize(ProjectPhotoUrlSize.FLICKR);
			flickr.setUrl(photo.get("flickr_url").getAsString());
			photoSet.addPhoto(ProjectPhotoUrlSize.FLICKR, flickr);
		}

		if(photo.get("medium_url") != null && !photo.get("medium_url").isJsonNull() && !photo.get("medium_url").getAsString().equals("h")) {
			RavelryPhoto medium = new RavelryPhoto();
			medium.setSize(ProjectPhotoUrlSize.MEDIUM);
			medium.setUrl(photo.get("medium_url").getAsString());
			photoSet.addPhoto(ProjectPhotoUrlSize.MEDIUM, medium);
		}

		if(photo.get("thumbnail_url") != null && !photo.get("thumbnail_url").isJsonNull() && !photo.get("thumbnail_url").getAsString().equals("h")) {
			RavelryPhoto thumb = new RavelryPhoto();
			thumb.setSize(ProjectPhotoUrlSize.THUMBNAIL);
			thumb.setUrl(photo.get("thumbnail_url").getAsString());
			photoSet.addPhoto(ProjectPhotoUrlSize.THUMBNAIL, thumb);
		}

		if(photo.get("small_url") != null && !photo.get("small_url").isJsonNull() && !photo.get("small_url").getAsString().equals("h")) {
			RavelryPhoto small = new RavelryPhoto();
			small.setSize(ProjectPhotoUrlSize.SMALL);
			small.setUrl(photo.get("small_url").getAsString());
			photoSet.addPhoto(ProjectPhotoUrlSize.SMALL, small);
		}

		if(photo.get("shelved_url") != null && !photo.get("shelved_url").isJsonNull() && !photo.get("shelved_url").getAsString().equals("h")) {
			RavelryPhoto shelv = new RavelryPhoto();
			shelv.setSize(ProjectPhotoUrlSize.SHELVED);
			shelv.setUrl(photo.get("square_url").getAsString());
			photoSet.addPhoto(ProjectPhotoUrlSize.SHELVED, shelv);
		}

		photoSet.setSortOrder(getSafeJsonInt(photo, "sort_order"));
		photoSet.setId(getSafeJsonInt(photo, "id"));
		photoSet.setxOffset(getSafeJsonInt(photo, "x_offset"));
		photoSet.setyOffset(getSafeJsonInt(photo, "y_offset"));

		return photoSet;
	}

	protected RavelryCraft translateCraft(JsonObject craftObj) {
		RavelryCraft craft = new RavelryCraft();
		craft.setId(getSafeJsonInt(craftObj, "id"));
		craft.setCraft(getSafeJsonString(craftObj, "name"));
		craft.setPermalink(getSafeJsonString(craftObj, "permalink"));
		return craft;
	}

	protected RavelryDownloadLocation translateDownloadLocation(JsonObject locObj) {
		RavelryDownloadLocation loc = new RavelryDownloadLocation();
		loc.setUrl(getSafeJsonString(locObj, "url"));
		loc.setType(getSafeJsonString(locObj, "type"));
		loc.setFree(getSafeJsonBool(locObj, "free"));
		return loc;
	}

	protected ArrayList<RavelryNeedleSize> translateNeedleSizes(JsonArray JArr) {
		ArrayList<RavelryNeedleSize> arr = new ArrayList<RavelryNeedleSize>();

		if(JArr == null || JArr.isJsonNull()) {
			return arr;
		}

		for(int i = 0; i < JArr.size(); i++) {
			arr.add(translateNeedleSize(JArr.get(i).getAsJsonObject()));
		}

		return arr;
	}

	protected RavelryNeedleSize translateNeedleSize(JsonObject needleObj) {
		RavelryNeedleSize needle = new RavelryNeedleSize();
		needle.setHook(getSafeJsonString(needleObj, "hook"));
		needle.setCrochet(getSafeJsonBool(needleObj, "crochet"));
		needle.setUsSteel(getSafeJsonString(needleObj, "us_steel"));
		needle.setKnitting(getSafeJsonBool(needleObj, "knitting"));
		needle.setName(getSafeJsonString(needleObj, "name"));
		needle.setMetric(getSafeJsonFloat(needleObj, "metric"));
		needle.setUs(getSafeJsonString(needleObj, "us"));
		needle.setId(getSafeJsonInt(needleObj, "id"));
		return needle;
	}

	protected RavelryPack translatePack(JsonObject packObj) {
		RavelryPack pack = new RavelryPack();
		pack.setQuantityDescription(getSafeJsonString(packObj, "quantity_description"));
		pack.setYardsPerSkein(getSafeJsonFloat(packObj, "yards_per_skein"));
		pack.setColorFamilyId(getSafeJsonString(packObj, "color_family_id"));
		if(JsonUtil.isKeyPopulated(packObj, "yarn")) {
			pack.setRavYarn(translatePackYarn(packObj.getAsJsonObject("yarn")));
		}
		pack.setStashId(getSafeJsonInt(packObj, "stash_id"));
		pack.setPreferMetricWeight(getSafeJsonBool(packObj, "prefer_metric_weight"));
		pack.setTotalGrams(getSafeJsonFloat(packObj, "total_grams"));
		pack.setProjectId(getSafeJsonInt(packObj, "project_id"));
		pack.setPersonalName(getSafeJsonString(packObj, "personal_name"));
		pack.setYarnId(getSafeJsonInt(packObj, "yarn_id"));
		pack.setPackId(getSafeJsonFloat(packObj, "id"));
		pack.setDyelot(getSafeJsonString(packObj, "dye_lot"));
		pack.setYarnName(getSafeJsonString(packObj, "yarn_name"));
		pack.setTotalOunces(getSafeJsonFloat(packObj, "total_ounces"));
		pack.setTotalMeters(getSafeJsonFloat(packObj, "total_meters"));
		pack.setMetersPerSkein(getSafeJsonFloat(packObj, "meters_per_skein"));
		pack.setShopName(getSafeJsonString(packObj, "shop_name"));
		pack.setSkeins(getSafeJsonInt(packObj, "skeins"));
		pack.setShopId(getSafeJsonInt(packObj, "shop_id"));
		pack.setPreferMetricLength(getSafeJsonBool(packObj, "prefer_metric_length"));
		pack.setOuncesPerSkein(getSafeJsonFloat(packObj, "ounces_per_skein"));
		if(JsonUtil.isKeyPopulated(packObj, "yarn_weight")) {
			pack.setYarnWeight(translatePatternYarnWeight(packObj.getAsJsonObject("yarn_weight")));
		}
		pack.setColorway(getSafeJsonString(packObj, "colorway"));
		pack.setTotalYarns(getSafeJsonFloat(packObj, "total_yards"));
		pack.setPrimaryPackId(getSafeJsonInt(packObj, "primary_pack_id"));
		pack.setGramsPerSkein(getSafeJsonFloat(packObj, "grams_per_skein"));

		return pack;
	}

	protected RavelryYarn translatePackYarn(JsonObject yarnObj) {
		RavelryYarn yarn = new RavelryYarn();
		yarn.setPermalink(getSafeJsonString(yarnObj, "permalink"));
		yarn.setYarnCompanyId(getSafeJsonInt(yarnObj, "yarn_company_id"));
		yarn.setYarnCompanyName(getSafeJsonString(yarnObj, "yarn_company_name"));
		yarn.setName(getSafeJsonString(yarnObj, "name"));
		yarn.setId(getSafeJsonInt(yarnObj, "id"));
		return yarn;
	}
	
	protected RavelryYarn translateYarn(JsonObject yarnObj) {
		RavelryYarn yarn = new RavelryYarn();
		yarn.setYardage(getSafeJsonFloat(yarnObj, "yardage"));
		yarn.setRatingCount(getSafeJsonInt(yarnObj, "rating_count"));
		yarn.setRatingAverage(getSafeJsonFloat(yarnObj, "rating_average"));
		yarn.setId(getSafeJsonInt(yarnObj, "id"));
		yarn.setWpi(getSafeJsonInt(yarnObj, "wpi"));
		yarn.setGaugeDivisor(getSafeJsonInt(yarnObj, "gauge_divisor"));
		yarn.setRatingTotal(getSafeJsonInt(yarnObj, "rating_total"));
		yarn.setCompany(translateYarnCompany(getSafeJsonObject(yarnObj, "yarn_company")));
		yarn.setWeight(translateYarnWeight(getSafeJsonObject(yarnObj, "yarn_weight")));
		yarn.setNotesHtml(getSafeJsonString(yarnObj, "notes_html"));
		yarn.setPermalink(getSafeJsonString(yarnObj, "permalink"));
		yarn.setOrganic(getSafeJsonBool(yarnObj, "organic"));
		yarn.setMinGauge(getSafeJsonFloat(yarnObj, "min_gauge"));
		yarn.setName(getSafeJsonString(yarnObj, "name"));
		yarn.setGrams(getSafeJsonFloat(yarnObj, "grams"));
		
		if(getSafeJsonObject(yarnObj, "first_photo") != null) {
			yarn.setFirstPhoto(translateFirstPhotos(getSafeJsonObject(yarnObj, "first_photo")));
		}
		
		yarn.setMaxGauge(getSafeJsonFloat(yarnObj, "max_gauge"));
		yarn.setTexture(getSafeJsonString(yarnObj, "texture"));
		yarn.setDiscontinued(getSafeJsonBool(yarnObj, "discontinued"));
		yarn.setYarnCompanyName(getSafeJsonString(yarnObj, "yarn_company_name"));
		yarn.setMachineWashable(getSafeJsonBool(yarnObj, "machine_washable"));
		
		return yarn;
	}
	
	protected RavelryYarnWeight translateYarnWeight(JsonObject obj) {
		if(obj == null || obj.isJsonNull()) {
			return null;
		}
		
		RavelryYarnWeight weight = new RavelryYarnWeight();
		weight.setKnitGauge(getSafeJsonString(obj, "knit_gauge"));
		weight.setId(getSafeJsonInt(obj, "id"));
		weight.setWpi(getSafeJsonString(obj, "wpi"));
		weight.setCrochetGauge(getSafeJsonString(obj, "crochet_gauge"));
		weight.setPly(getSafeJsonInt(obj, "ply"));
		weight.setMinGauge(getSafeJsonInt(obj, "min_gauge"));
		weight.setName(getSafeJsonString(obj, "name"));
		weight.setMaxGauge(getSafeJsonString(obj, "max_gauge"));
		return weight;
	}
	
	protected RavelryYarnCompany translateYarnCompany(JsonObject obj) {
		if(obj == null || obj.isJsonNull()) {
			return null;
		}
		RavelryYarnCompany comp = new RavelryYarnCompany();
		comp.setId(getSafeJsonInt(obj, "id"));
		comp.setPermalink(getSafeJsonString(obj, "permalink"));
		comp.setName(getSafeJsonString(obj, "name"));
		return comp;
	}

	protected RavelryYarnWeight translatePatternYarnWeight(JsonObject weightObj) {
		RavelryYarnWeight weight = new RavelryYarnWeight();
		weight.setMaxGauge(getSafeJsonString(weightObj, "max_gauge"));
		weight.setWpi(getSafeJsonString(weightObj, "wpi"));
		weight.setMinGauge(getSafeJsonInt(weightObj, "min_gauge"));
		weight.setId(getSafeJsonInt(weightObj, "id"));
		weight.setName(getSafeJsonString(weightObj, "name"));
		weight.setKnitGauge(getSafeJsonString(weightObj, "knit_gauge"));
		weight.setPly(getSafeJsonInt(weightObj, "ply"));
		weight.setCrochetGauge(getSafeJsonString(weightObj, "crochet_gauge"));
		return weight;
	}

	protected String getSafeJsonString(JsonObject obj, String key) {
		return (JsonUtil.isKeyPopulated(obj, key) ? obj.get(key).getAsString() : null);
	}

	protected int getSafeJsonInt(JsonObject obj, String key) {
		return (JsonUtil.isKeyPopulated(obj, key) ? obj.get(key).getAsInt() : 0);
	}

	protected boolean getSafeJsonBool(JsonObject obj, String key) {
		return (JsonUtil.isKeyPopulated(obj, key) ? obj.get(key).getAsBoolean() : false);
	}

	protected float getSafeJsonFloat(JsonObject obj, String key) {
		return (JsonUtil.isKeyPopulated(obj, key) ? obj.get(key).getAsFloat() : 0);
	}

	protected JsonObject getSafeJsonObject(JsonObject obj, String key) {
		return (JsonUtil.isKeyPopulated(obj, key) ? obj.get(key).getAsJsonObject() : null);
	}

	protected JsonArray getSafeJsonArray(JsonObject obj, String key) {
		return (JsonUtil.isKeyPopulated(obj, key) ? obj.get(key).getAsJsonArray() : null);
	}

	protected RavelryProject translateProject(JsonObject obj) {
		RavelryProject project = new RavelryProject();
		project.setStatusName(getSafeJsonString(obj, "status_name"));
		project.setPermalink(getSafeJsonString(obj, "permalink"));
		project.setMadeForUserId(getSafeJsonInt(obj, "made_for_user_id"));
		project.setUpdatedDtTm(StandardConvert.stringToDate(getSafeJsonString(obj, "updated_at")));
		project.setCommentsCount(getSafeJsonInt(obj, "comments_count"));
		project.setRating(getSafeJsonFloat(obj, "rating"));
		project.setProjectId(getSafeJsonInt(obj, "id"));
		project.setTags(translateStringArray(getSafeJsonArray(obj, "tag_names")));
		project.setPatternId(getSafeJsonInt(obj, "pattern_id"));
		project.setPhotoCount(getSafeJsonInt(obj, "photos_count"));
		project.setProjectStatus(ProjectStatus.getStatus(getSafeJsonInt(obj, "project_status_id")));
		project.setCompletedDtTm(StandardConvert.stringToDate(getSafeJsonString(obj, "completed")));
		project.setPatternName(getSafeJsonString(obj, "pattern_name"));
		project.setSize(getSafeJsonString(obj, "size"));
		project.setStartedDtTm(StandardConvert.stringToDate(getSafeJsonString(obj, "started")));
		project.setUserId(getSafeJsonInt(obj, "user_id"));
		project.setProjectStatusChangedDtTm(StandardConvert.stringToDate(getSafeJsonString(obj, "started")));
		project.setProjectName(getSafeJsonString(obj, "name"));
		project.setFavoritesCount(getSafeJsonInt(obj, "favorites_count"));
		project.setFirstPhotos(translateFirstPhotos(getSafeJsonObject(obj, "first_photo")));
		project.setCraftName(getSafeJsonString(obj, "craft_name"));
		project.setProgress(getSafeJsonInt(obj, "progress"));
		project.setMadeForUsername(getSafeJsonString(obj, "made_for"));
		project.setCreatedDtTm(StandardConvert.stringToDate(getSafeJsonString(obj, "created_at")));
		project.setCraftId(getSafeJsonInt(obj, "craft_id"));
		project.setPhotos(translatePhotos(getSafeJsonArray(obj, "photos")));

		//TODO: user
		//Project Status Changed
		project.setNeedleSizes(translateNeedleSizes(getSafeJsonArray(obj, "needle_sizes")));
		project.setNotesHtml(getSafeJsonString(obj, "notes_html"));
		project.setNotes(getSafeJsonString(obj, "notes"));
		project.setPacks(translatePacks(getSafeJsonArray(obj, "packs")));
		project.setComments(translateStringArray(getSafeJsonArray(obj, "comments")));

		return project;
	}

	protected RavelryQueuedProject translateQueuedProject(JsonObject obj) {
		RavelryQueuedProject queue = new RavelryQueuedProject();

		return queue;
	}

	protected ArrayList<String> translateStringArray(JsonArray jArr) {
		ArrayList<String> arr = new ArrayList<String>();
		if(jArr == null || jArr.isJsonNull()) {
			return arr;
		}

		for(int i = 0; i < jArr.size(); i++) {
			arr.add(jArr.get(i).getAsString());
		}

		return arr;
	}

	protected ArrayList<RavelryPack> translatePacks(JsonArray jArr) {
		ArrayList<RavelryPack> arr = new ArrayList<RavelryPack>();
		if(jArr == null || jArr.isJsonNull()) {
			return arr;
		}

		for(int i = 0; i < jArr.size(); i++) {
			arr.add(translatePack(jArr.get(i).getAsJsonObject()));
		}

		return arr;
	}

	/**
	 * Takes a jsonarray of tags and parses it into an array of strings. 
	 * @param jsonArray {@link JsonArray} A JsonArray of tags. 
	 * @return ArrayList A list of strings. Will never be null but can be empty. 
	 */
	protected ArrayList<String> translateTags(JsonArray jsonArray) {
		ArrayList<String> tagArray = new ArrayList<String>();

		for(int i = 0; i < jsonArray.size(); i++) {
			tagArray.add(jsonArray.get(i).getAsString());
		}

		return tagArray;
	}

	protected RavelryStash translateStash(JsonObject jObj) {
		RavelryStash stash = new  RavelryStash();

		if(JsonUtil.isKeyPopulated(jObj, "yarn")) {
			stash.setYarn(translateYarn(jObj.getAsJsonObject("yarn")));
		}
		
		if(JsonUtil.isKeyPopulated(jObj, "first_photo")) {
			stash.setFirstPhoto(translateFirstPhotos(getSafeJsonObject(jObj, "first_photo")));
		}
		
		stash.setColorFamilyName(getSafeJsonString(jObj, "color_family_name"));
		stash.setPermalink(getSafeJsonString(jObj, "permalink"));
		stash.setLocation(getSafeJsonString(jObj, "location"));
		stash.setCreatedDtTm(StandardConvert.stringToDate(getSafeJsonString(jObj, "created_at")));
		stash.setUpdatedDtTm(StandardConvert.stringToDate(getSafeJsonString(jObj, "updated_at")));
		stash.setStatus(translateStashStatus(getSafeJsonObject(jObj, "stash_status")));
		stash.setDyelot(getSafeJsonString(jObj, "dye_lot"));
		stash.setColorwayName(getSafeJsonString(jObj, "colorway_name"));
		stash.setId(getSafeJsonInt(jObj, "id"));
		stash.setHasPhoto(getSafeJsonBool(jObj, "has_photo"));
		stash.setHandspun(getSafeJsonBool(jObj, "handspun"));
		stash.setFavoritesCount(getSafeJsonInt(jObj, "favorites_count"));
		stash.setName(getSafeJsonString(jObj, "name"));
		stash.setCommentsCount(getSafeJsonInt(jObj, "comments_count"));
		
		return stash;
	}

	protected RavelryStashStatus translateStashStatus(JsonObject obj) {
		if(obj == null || obj.isJsonNull()) {
			return null;
		}
		RavelryStashStatus stat = new RavelryStashStatus();
		stat.setName(getSafeJsonString(obj, "name"));
		stat.setId(getSafeJsonInt(obj, "id"));
		return stat;
				
	}
	
	protected RavelryFiberStash translateFiberStash(JsonObject jObj) {
		RavelryFiberStash fiber = new RavelryFiberStash();
		
		if(JsonUtil.isKeyPopulated(jObj, "first_photo")) {
			fiber.setFirstPhoto(translateFirstPhotos(getSafeJsonObject(jObj, "first_photo")));
		}
		
		fiber.setPermalink(getSafeJsonString(jObj, "permalink"));
		fiber.setName(getSafeJsonString(jObj, "name"));
		fiber.setLocation(getSafeJsonString(jObj, "location"));
		fiber.setFirstPhotoId(getSafeJsonInt(jObj, "first_photo_id"));
		fiber.setCreatedDtTm(StandardConvert.stringToDate(getSafeJsonString(jObj, "created_at")));
		fiber.setUpdatedDtTm(StandardConvert.stringToDate(getSafeJsonString(jObj, "updated_at")));
		fiber.setNotes(getSafeJsonString(jObj, "notes"));
		fiber.setFiberCompanySubmissionId(getSafeJsonInt(jObj, "fiber_company_submission_id" +
				""));
		fiber.setId(getSafeJsonInt(jObj, "id"));
		fiber.setFiberCompanyId(getSafeJsonInt(jObj, "fiber_company_id"));
		fiber.setUserId(getSafeJsonInt(jObj, "user_id"));
		fiber.setHasPhoto(getSafeJsonBool(jObj, "has_photo"));
		fiber.setRemainingFiberPackId(getSafeJsonInt(jObj, "remaining_fiber_pack_id"));
		fiber.setFavoritesCount(getSafeJsonInt(jObj, "favorites_count"));
		fiber.setStashStatusId(getSafeJsonInt(jObj, "stash_status_id"));
		fiber.setImportedFromYarnId(getSafeJsonInt(jObj, "imported_from_yarn_id"));
		fiber.setCommentsCount(getSafeJsonInt(jObj, "comments_count"));
		fiber.setPrimaryFiberPackId(getSafeJsonInt(jObj, "primary_fiber_pack_id"));
		
		return fiber;
	}

	protected RavelryPersonalAttribute translatePersonalAttributes(JsonObject obj) {
		RavelryPersonalAttribute atts = new RavelryPersonalAttribute();
		
		if(obj == null || obj.isJsonNull()) {
			return atts;
		}
		
		atts.setFavorited(getSafeJsonBool(obj, "favorited"));
		atts.setQueued(getSafeJsonBool(obj, "queued"));
		atts.setBookmarkId(getSafeJsonInt(obj, "bookmark_id"));
		
		return atts;
	}
	
	
	
	
	public static void longInfo(String str) {
	    if(str.length() > 4000) {
	        Log.i("Body", str.substring(0, 4000));
	        longInfo(str.substring(4000));
	    } else
	        Log.i("Body", str);
	}
	
	
}
