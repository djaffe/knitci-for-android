package com.knitci.pocket.ravelry.data.access.delegate;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.http.HttpResponse;


import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.util.EntityUtils;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import android.util.Log;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public abstract class AbstractComplexPostDelegate extends AbstractDelegate {

	protected final String URL;
	protected final OAuthService service;
	protected final Token token;
	protected String payload;
	
	public AbstractComplexPostDelegate(OAuthService service, Token token, String URL) {
		this.service = service;
		this.token = token;
		this.URL = URL;
	}
	
	protected void setPayload(String payload) {
		this.payload = payload;
	}
	
	@Override
	protected RavelryObject execute() {
		String paramString = prepareParamString();
		
		OAuthConsumer consumer = new CommonsHttpOAuthConsumer("8487CDDAE0F84620D4B0", "7FP21JcRyZgENEIu4e48uUt5sYlyz7poVVZkdQCS");
	    consumer.setTokenWithSecret(token.getToken(), token.getSecret());
	    DefaultHttpClient httpclient = new DefaultHttpClient();
	    HttpPost request = new HttpPost(URL+paramString);
	    
	    StringEntity entity = null;
		try {
			entity = new StringEntity("data=" + URLEncoder.encode(payload, "UTF-8"));
		} catch (UnsupportedEncodingException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} 
	    entity.setContentType("application/x-www-form-urlencoded"); 
	    request.setEntity(entity);
	    
	    org.apache.http.HttpResponse resp = null;
	    
	    try {
			consumer.sign(request);
		} catch (OAuthMessageSignerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (OAuthExpectationFailedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (OAuthCommunicationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    try {
			resp = httpclient.execute(request);
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		try {
			return translate(EntityUtils.toString(resp.getEntity()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new RavelryObject() {
		};

	}
	
	protected abstract RavelryObject translate(String response);


}
