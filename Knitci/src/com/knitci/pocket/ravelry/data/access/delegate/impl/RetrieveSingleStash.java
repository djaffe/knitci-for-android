package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RetrieveSingleStash extends AbstractSimpleGetDelegate {

	protected RetrieveSingleStash(OAuthService service, Token token, String username, int stashId) {
		super(service, token, "https://api.ravelry.com/people/"+username+"/stash/"+String.valueOf(stashId));
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, String username, int stashId) {
		return new RetrieveSingleStash(service, token, username, stashId).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		// TODO Auto-generated method stub
		return null;
	}

}
