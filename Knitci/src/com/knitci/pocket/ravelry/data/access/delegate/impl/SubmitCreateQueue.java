package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.knitci.pocket.ravelry.data.access.delegate.AbstractComplexPostDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryQueuedProject;

public class SubmitCreateQueue extends AbstractComplexPostDelegate {

	public SubmitCreateQueue(OAuthService service, Token token, String username, String payload) {
		super(service, token, "https://api.ravelry.com/people/"+username+"/queue/create.json");
		setPayload(payload);
	}
	
	public static RavelryObject submit(OAuthService service, Token token, String username, String payload) {
		return new SubmitCreateQueue(service, token, username, payload).execute();
	}

	@Override
	protected RavelryObject translate(String response) {
		RavelryQueuedProject pro = new RavelryQueuedProject();
		pro.setHttpCode(200);
		return pro;
	}

	@Override
	protected RavelryObject translate(Response response) {
		return new RavelryObject() {
		};
	}

}
