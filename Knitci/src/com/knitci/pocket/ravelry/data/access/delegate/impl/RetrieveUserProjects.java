package com.knitci.pocket.ravelry.data.access.delegate.impl;

import java.util.ArrayList;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryParam;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryUser;


/**
 * Delegate for calling ravelry API to get a specific user's projects.  
 * @author DJ021930
 */
public class RetrieveUserProjects extends AbstractSimpleGetDelegate {

	private final String username; 

	/**
	 * Constructor. This should never be directly called. 
	 * @param service {@link OAuthService} An OAuthService for OAuth queries. Cannot be null. 
	 * @param token {@link Token} The token containing token and secret key. 
	 */
	private RetrieveUserProjects(OAuthService service, Token token, String username, ArrayList<RavelryParam> params) {
		super(service, token, "https://api.ravelry.com/projects/"+username+"/list.json");
		setParams(params);
		this.username = username;
		
	}

	/**
	 * Public method for consuming this delegate. 
	 * @param service {@link OAuthService} An OAuthService object for OAuth queries. Cannot be null. 
	 * @param token {@link Token} Token to be used. Cannot be null. 
	 * @return RavelryUser A {@link RavelryUser} object. Can be null if the call failed or parsing failed. 
	 */
	public static RavelryObject retrieve(OAuthService service, Token token, String username, ArrayList<RavelryParam> params) {
		return new RetrieveUserProjects(service, token, username, params).execute();
	}

	/* (non-Javadoc)
	 * @see com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate#translate(org.scribe.model.Response)
	 */
	@Override
	protected RavelryCollection translate(Response response) {

		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			return null;
		}

		RavelryCollection collection = new RavelryCollection();

		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}
		
		JsonObject jObj = json.getAsJsonObject();
		JsonArray jArr = jObj.getAsJsonArray("projects");
		for(int i = 0; i < jArr.size(); i++) {
			RavelryProject ravelryProject = new RavelryProject();
			
			JsonObject jProj = jArr.get(i).getAsJsonObject();
			ravelryProject = translateProject(jProj);	
			
			collection.add(ravelryProject);
		}
		
		collection.setPaginator(translatePaginator(getSafeJsonObject(jObj, "paginator")));
		
		return collection;
	}



}
