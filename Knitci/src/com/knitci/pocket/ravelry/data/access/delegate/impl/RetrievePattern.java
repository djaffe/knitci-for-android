package com.knitci.pocket.ravelry.data.access.delegate.impl;

import java.util.ArrayList;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryNeedleSize;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPack;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPattern;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPatternAuthor;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPatternCategory;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPatternSource;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPatternType;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPrintings;
import com.knitci.pocket.ravelry.data.object.impl.RavelryYarnWeight;
import com.knitci.pocket.util.JsonUtil;
import com.knitci.pocket.util.StandardConvert;

public class RetrievePattern extends AbstractSimpleGetDelegate {

	protected RetrievePattern(OAuthService service, Token token, int patternId) {
		super(service, token, "https://api.ravelry.com/patterns/"+String.valueOf(patternId)+".json");
	}

	public static RavelryObject retrieve(OAuthService service, Token token, int patternId) {
		return new RetrievePattern(service, token, patternId).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		int httpResponseCode = response.getCode();
		if(httpResponseCode != 200) {
			return null;
		}

		JsonElement json = null;
		try {
			json = new JsonParser().parse(response.getBody());
		}
		catch (JsonSyntaxException e) { 
			return null;
		}

		if(!JsonUtil.isKeyPopulated(json.getAsJsonObject(), "pattern")) {
			return null;
		}

		JsonObject patternObj = json.getAsJsonObject().getAsJsonObject("pattern");
		RavelryPattern pattern = new RavelryPattern();
		pattern.setHttpCode(httpResponseCode);
		
		pattern.applyPersonalAttributes(translatePersonalAttributes(getSafeJsonObject(patternObj, "personal_attributes")));

		// Printings
		if(JsonUtil.isKeyPopulated(patternObj, "printings")) {
			JsonArray printingsObj = patternObj.getAsJsonArray("printings");
			pattern.setPrintings(translatePrintings(printingsObj));
		}

		// Pattern Author
		if(JsonUtil.isKeyPopulated(patternObj, "pattern_author")) {
			JsonObject authorObj = patternObj.getAsJsonObject("pattern_author");
			pattern.setAuthor(translateAuthor(authorObj));
		}

		// Volumes In Library...

		// Pdf Url
		if(JsonUtil.isKeyPopulated(patternObj, "pdf_url")) {
			pattern.setPdfUrl(getSafeJsonString(patternObj, "pdf_url"));
		}

		// Currency
		if(JsonUtil.isKeyPopulated(patternObj, "currency")) {
			pattern.setCurrency(getSafeJsonString(patternObj, "currency"));
		}

		// Photos
		if(JsonUtil.isKeyPopulated(patternObj, "photos")) {
			pattern.setPhotos(translatePhotos(patternObj.getAsJsonArray("photos")));
		}

		// Sizes Available
		if(JsonUtil.isKeyPopulated(patternObj, "sizes_available")) {
			pattern.setSizesAvailable(getSafeJsonString(patternObj, "sizes_available"));
		}

		// Yardage
		if(JsonUtil.isKeyPopulated(patternObj, "yardage")) {
			pattern.setYardageDescription(getSafeJsonString(patternObj, "yardage"));
		}

		// Queued projects count
		if(JsonUtil.isKeyPopulated(patternObj, "queued_projects_count")) {
			pattern.setQueuedProjectsCount(getSafeJsonInt(patternObj, "queued_projects_count"));
		}

		// Favorites count
		if(JsonUtil.isKeyPopulated(patternObj, "favorites_count")) {
			pattern.setFavoritesCount(getSafeJsonInt(patternObj, "favorites_count"));
		}

		// Craft
		if(JsonUtil.isKeyPopulated(patternObj, "craft")) {
			pattern.setCraft(translateCraft(patternObj.getAsJsonObject("craft")));
		}

		//Gauge Description
		if(JsonUtil.isKeyPopulated(patternObj, "gauge_description")) {
			pattern.setGaugeDescription(getSafeJsonString(patternObj, "gauge_description"));
		}

		//Yardage max
		if(JsonUtil.isKeyPopulated(patternObj, "yardage_max")) {
			pattern.setYardageMax(getSafeJsonInt(patternObj, "yardage_max"));
		}

		// Projects count
		if(JsonUtil.isKeyPopulated(patternObj, "projects_count")) {
			pattern.setProjectsCount(getSafeJsonInt(patternObj, "projects_count"));
		}

		// Gauge
		if(JsonUtil.isKeyPopulated(patternObj, "gauge")) {
			pattern.setGauge(getSafeJsonFloat(patternObj, "gauge"));
		}

		// Free?
		if(JsonUtil.isKeyPopulated(patternObj, "free")) {
			pattern.setFree(getSafeJsonBool(patternObj, "free"));
		}

		pattern.setDownloadable(getSafeJsonBool(patternObj, "downloadable"));
		pattern.setProductId(getSafeJsonInt(patternObj, "product_id"));
		pattern.setRatingCount(getSafeJsonInt(patternObj, "rating_count"));
		pattern.setPrice(getSafeJsonFloat(patternObj, "price"));
		pattern.setYarnWeightDescription(getSafeJsonString(patternObj, "yarn_weight_description"));
		pattern.setPublishedDtTm(StandardConvert.stringToDate(getSafeJsonString(patternObj, "published")));
		pattern.setPatternId(getSafeJsonInt(patternObj, "id"));

		if(JsonUtil.isKeyPopulated(patternObj, "pattern_categories")) {
			pattern.setPatternCategories(translatePatternCategories(patternObj.getAsJsonArray("pattern_categories")));
		}

		pattern.setNotes(getSafeJsonString(patternObj, "notes"));
		pattern.setCurrencySymbol(getSafeJsonString(patternObj, "currency_symbol"));
		pattern.setPatternName(getSafeJsonString(patternObj, "name"));
		pattern.setDifficultyAverage(getSafeJsonFloat(patternObj, "difficulty_average"));

		if(JsonUtil.isKeyPopulated(patternObj, "pattern_type")) {
			pattern.setPatternType(translatePatternType(patternObj.getAsJsonObject("pattern_type")));
		}

		pattern.setYardageDescription(getSafeJsonString(patternObj, "yardage_description"));
		pattern.setUrl(getSafeJsonString(patternObj, "url"));
		pattern.setGaugePattern(getSafeJsonString(patternObj, "gauge_pattern"));

		if(JsonUtil.isKeyPopulated(patternObj, "download_location")) {
			pattern.setDownloadLocation(translateDownloadLocation(patternObj.getAsJsonObject("download_location")));
		}

		pattern.setRowGauge(getSafeJsonFloat(patternObj, "row_gauge"));

		if(JsonUtil.isKeyPopulated(patternObj, "yarn_weight")) {
			pattern.setYarnWeight(translatePatternYarnWeight(patternObj.getAsJsonObject("yarn_weight")));
		}

		if(JsonUtil.isKeyPopulated(patternObj, "pattern_needle_sizes")) {
			pattern.setNeedles(translateNeedleSizes(patternObj.getAsJsonArray("pattern_needle_sizes")));
		}

		pattern.setPermalink(getSafeJsonString(patternObj, "permalink"));
		pattern.setCommentsCount(getSafeJsonInt(patternObj, "comments_count"));

		if(JsonUtil.isKeyPopulated(patternObj, "packs")) {
			pattern.setPacks(translatePacks(patternObj.getAsJsonArray("packs")));
		}
		
		pattern.setNotesHtml(getSafeJsonString(patternObj, "notes_html"));
		pattern.setPdfInLibary(getSafeJsonBool(patternObj, "pdf_in_library"));
		pattern.setRavelryDownload(getSafeJsonBool(patternObj, "ravelry_download"));
		pattern.setAverageRating(getSafeJsonFloat(patternObj, "rating_average"));
		pattern.setDifficultyCount(getSafeJsonInt(patternObj, "difficulty_count"));
		pattern.setGaugeDivisor(getSafeJsonInt(patternObj, "gauge_divisor"));

		return pattern;
	}

	protected RavelryPrintings translatePrintings(JsonArray printingArray) {
		RavelryPrintings printings = new RavelryPrintings();

		for(int i = 0; i < printingArray.size(); i++) {
			JsonObject printingObj = printingArray.get(i).getAsJsonObject();
			boolean primarySource = false;
			if(JsonUtil.isKeyPopulated(printingObj, "primary_source")) {
				primarySource = printingObj.get("primary_source").getAsBoolean();
			}

			if(!JsonUtil.isKeyPopulated(printingObj, "pattern_source")) {
				continue;
			}
			JsonObject sourceObj = printingObj.getAsJsonObject("pattern_source");

			RavelryPatternSource source = new RavelryPatternSource();
			source.setAuthor(getSafeJsonString(sourceObj, "author"));
			source.setPrice(getSafeJsonString(sourceObj, "price"));
			source.setPatternsCount(getSafeJsonInt(sourceObj, "patterns_count"));
			source.setName(getSafeJsonString(sourceObj, "name"));
			source.setListPrice(getSafeJsonString(sourceObj, "list_price"));
			source.setUrl(getSafeJsonString(sourceObj, "url"));
			source.setShelfImageUrl(getSafeJsonString(sourceObj, "shelf_image_path"));
			source.setAmazonUrl(getSafeJsonString(sourceObj, "amazon_url"));
			source.setPermalink(getSafeJsonString(sourceObj, "permalink"));
			source.setOutOfPrint(getSafeJsonBool(sourceObj, "out_of_print"));
			source.setAmazonRating(getSafeJsonString(sourceObj, "amazon_rating"));

			if(primarySource) {
				printings.setPrimarySource(source);
			}
			else {
				printings.addSource(source);
			}

		}
		return printings;
	}

	protected RavelryPatternAuthor translateAuthor(JsonObject authorObj) {
		RavelryPatternAuthor author = new RavelryPatternAuthor();
		author.setFavoritesCount(getSafeJsonInt(authorObj, "favorites_count"));
		author.setPatternsCount(getSafeJsonInt(authorObj, "patterns_count"));
		author.setId(getSafeJsonInt(authorObj, "id"));
		author.setName(getSafeJsonString(authorObj, "name"));
		author.setPermalink(getSafeJsonString(authorObj, "permalink"));
		return author;
	}

	protected ArrayList<RavelryPatternCategory> translatePatternCategories(JsonArray categoriesArr) {
		ArrayList<RavelryPatternCategory> categories = new ArrayList<RavelryPatternCategory>();
		for(int i = 0; i < categoriesArr.size(); i++) {
			categories.add(translatePatternCategory(categoriesArr.get(i).getAsJsonObject()));
		}
		return categories;
	}
	
	protected RavelryPatternCategory translatePatternCategory(JsonObject categoryObj) {
		RavelryPatternCategory category = new RavelryPatternCategory();
		category.setId(getSafeJsonInt(categoryObj, "id"));
		category.setName(getSafeJsonString(categoryObj, "name"));
		category.setPermalink(getSafeJsonString(categoryObj, "permalink"));
		if(JsonUtil.isKeyPopulated(categoryObj, "parent")) {
			category.setParentCategory(translatePatternCategory(categoryObj.getAsJsonObject("parent")));
		}
		return category;
	}

	protected RavelryPatternType translatePatternType(JsonObject typeObj) {
		RavelryPatternType type = new RavelryPatternType();
		type.setId(getSafeJsonString(typeObj, "id"));
		type.setName(getSafeJsonString(typeObj, "name"));
		type.setClothing(getSafeJsonBool(typeObj, "clothing"));
		type.setPermalink(getSafeJsonString(typeObj, "permalink"));
		return type;
	}

	@Override
	protected ArrayList<RavelryPack> translatePacks(JsonArray packsArr) {
		ArrayList<RavelryPack> packs = new ArrayList<RavelryPack>();
		for(int i = 0; i < packsArr.size(); i++) {
			packs.add(translatePack(packsArr.get(i).getAsJsonObject()));
		}
		return packs;
	}
	
	@Override
	protected ArrayList<RavelryNeedleSize> translateNeedleSizes(JsonArray needleArr) {
		ArrayList<RavelryNeedleSize> needles = new ArrayList<RavelryNeedleSize>();
		for(int i = 0; i < needleArr.size(); i++) {
			needles.add(translateNeedleSize(needleArr.get(i).getAsJsonObject()));
		}
		return needles;
	}
}
