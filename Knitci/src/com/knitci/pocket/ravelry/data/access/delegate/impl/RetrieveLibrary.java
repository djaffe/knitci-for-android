package com.knitci.pocket.ravelry.data.access.delegate.impl;

import java.util.ArrayList;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryParam;

public class RetrieveLibrary extends AbstractSimpleGetDelegate {

	protected RetrieveLibrary(OAuthService service, Token token, String username, ArrayList<RavelryParam> params) {
		super(service, token, "http://api.ravelry.com/people/"+username+"/library/search.json");
		// TODO Auto-generated constructor stub
	}
	
	public static RavelryObject retrieve(OAuthService service, Token token, String username, ArrayList<RavelryParam> params) {
		return new RetrieveLibrary(service, token, username, params).execute();
	}

	@Override
	protected RavelryObject translate(Response response) {
		// TODO Auto-generated method stub
		return null;
	}

}
