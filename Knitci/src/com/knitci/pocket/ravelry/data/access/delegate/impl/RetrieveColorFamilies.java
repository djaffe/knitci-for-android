package com.knitci.pocket.ravelry.data.access.delegate.impl;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import android.util.Log;

import com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.ravelry.data.object.impl.RavelryColorFamily;
import com.knitci.pocket.ravelry.data.object.impl.RavelryUser;

/**
 * Delegate for calling Ravelry API to retrieve available color families. 
 * @author DJ021930
 */
public class RetrieveColorFamilies extends AbstractSimpleGetDelegate {

	/**
	 * Constructor. Should never be consumed externally. 
	 * @param service {@link OAuthService}  An OAuthService for OAuth queries. Cannot be null. 
	 * @param token {@link Token} Token to use. 
	 */
	protected RetrieveColorFamilies(OAuthService service, Token token) {
		super(service, token, "https://api.ravelry.com/color_families.json");
	}

	/**
	 * Public method for consuming this delegate. 
	 * @param service {@link OAuthService} An OAuthService object for OAuth queries. Cannot be null. 
	 * @param token {@link Token} Token to be used. Cannot be null. 
	 * @return {@link RavelryCollection} A collection of {@link RavelryColorFamily} objects. Can be empty or null.  
	 */
	public static RavelryObject retrieve(OAuthService service, Token token) {
		return new RetrieveColorFamilies(service, token).execute();
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.knitci.pocket.ravelry.data.access.delegate.AbstractSimpleGetDelegate#translate(org.scribe.model.Response)
	 */
	@Override
	protected RavelryObject translate(Response response) {
		Log.d("COLORFAMILIES", response.getBody());
		// TODO Auto-generated method stub
		return null;
	}

}
