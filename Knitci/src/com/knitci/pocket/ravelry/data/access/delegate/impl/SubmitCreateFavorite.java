package com.knitci.pocket.ravelry.data.access.delegate.impl;

import java.net.URLEncoder;

import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.knitci.pocket.ravelry.data.access.delegate.AbstractComplexPostDelegate;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryBookmark;
import com.knitci.pocket.util.JsonUtil;

public class SubmitCreateFavorite extends AbstractComplexPostDelegate {

	public SubmitCreateFavorite(OAuthService service, Token token, String username, String payload) {
		super(service, token, "https://api.ravelry.com/people/" + username + "/favorites/create.json");
		setPayload(payload);
	}

	public static RavelryObject submit(OAuthService service, Token token, String payload, String username) {
		return new SubmitCreateFavorite(service, token, username, payload).execute();
	}
	
	@Override
	protected RavelryObject translate(Response response) {
		//Log.i("HTTP Response", String.valueOf(response.getCode()));
		//Log.i("Respones", response.getBody());
		return null;
	}

	@Override
	protected RavelryObject translate(String response) {
		longInfo(response);
		
		if(response == null || response.isEmpty()) {
			return null;
		}
		
		JsonElement json = null;
		try {
			json = new JsonParser().parse(response);
		}
		catch (JsonSyntaxException e) { 
			return null;
		}

		if(!JsonUtil.isKeyPopulated(json.getAsJsonObject(), "bookmark")) {
			return null;
		}
		
		JsonObject obj = json.getAsJsonObject().getAsJsonObject("bookmark");
		
		RavelryBookmark bookmark = new RavelryBookmark();
		bookmark.setType(getSafeJsonString(obj, "type"));
		bookmark.setId(getSafeJsonInt(obj, "id"));
		bookmark.setComment(getSafeJsonString(obj, "comment"));
		
		JsonObject fObj = obj.getAsJsonObject("favorited");
		bookmark.setItemId(getSafeJsonInt(fObj, "id"));
		
		return bookmark;
		
	}

}
