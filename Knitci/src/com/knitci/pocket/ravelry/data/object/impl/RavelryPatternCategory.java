package com.knitci.pocket.ravelry.data.object.impl;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryPatternCategory extends RavelryObject {

	private int id;
	private String name;
	private String permalink;
	private RavelryPatternCategory parentCategory;
	
	public boolean hasParent() {
		return (null != parentCategory);
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the permalink
	 */
	public String getPermalink() {
		return permalink;
	}
	
	/**
	 * @param permalink the permalink to set
	 */
	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}
	
	/**
	 * @return the parentCategory
	 */
	public RavelryPatternCategory getParentCategory() {
		return parentCategory;
	}
	
	/**
	 * @param parentCategory the parentCategory to set
	 */
	public void setParentCategory(RavelryPatternCategory parentCategory) {
		this.parentCategory = parentCategory;
	}
	
	public String getCategoryDisplay() {
		String disp = "";
		disp += this.name;
		if(this.parentCategory != null) {
			disp += "-->" + this.parentCategory.getCategoryDisplay();
		}
		return disp;
	}
	
}
