package com.knitci.pocket.ravelry.data.object.impl;

import java.util.ArrayList;
import java.util.Date;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryQueuedProject extends RavelryObject {

	private RavelryPhotoSet bestPhoto;
	private Date createdDtTm;
	private Date finishByDtTm;
	private int queueId;
	private String makeFor;
	private String name;
	private String notes;
	private String notesHtml;
	private RavelryPattern pattern;
	private int patternId;
	private String patternName;
	private int position;
	private ArrayList<RavelryStash> stashes;
	private int skeins;
	private int sortOrder;
	private Date startDtTm;
	private int userId;
	private RavelryYarn yarn;
	private int yarnId;
	private String yarnName;
	private String username;
	private int patternAuthorId;
	private String patternAuthorName;
	private String shortPatternName;
	
	
	/**
	 * @return the shortPatternName
	 */
	public String getShortPatternName() {
		return shortPatternName;
	}
	/**
	 * @param shortPatternName the shortPatternName to set
	 */
	public void setShortPatternName(String shortPatternName) {
		this.shortPatternName = shortPatternName;
	}
	/**
	 * @return the patternAuthorName
	 */
	public String getPatternAuthorName() {
		return patternAuthorName;
	}
	/**
	 * @param patternAuthorName the patternAuthorName to set
	 */
	public void setPatternAuthorName(String patternAuthorName) {
		this.patternAuthorName = patternAuthorName;
	}
	/**
	 * @return the patternAuthorId
	 */
	public int getPatternAuthorId() {
		return patternAuthorId;
	}
	/**
	 * @param patternAuthorId the patternAuthorId to set
	 */
	public void setPatternAuthorId(int patternAuthorId) {
		this.patternAuthorId = patternAuthorId;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the bestPhoto
	 */
	public RavelryPhotoSet getBestPhoto() {
		return bestPhoto;
	}
	/**
	 * @param bestPhoto the bestPhoto to set
	 */
	public void setBestPhoto(RavelryPhotoSet bestPhoto) {
		this.bestPhoto = bestPhoto;
	}
	/**
	 * @return the createdDtTm
	 */
	public Date getCreatedDtTm() {
		return createdDtTm;
	}
	/**
	 * @param createdDtTm the createdDtTm to set
	 */
	public void setCreatedDtTm(Date createdDtTm) {
		this.createdDtTm = createdDtTm;
	}
	/**
	 * @return the finishByDtTm
	 */
	public Date getFinishByDtTm() {
		return finishByDtTm;
	}
	/**
	 * @param finishByDtTm the finishByDtTm to set
	 */
	public void setFinishByDtTm(Date finishByDtTm) {
		this.finishByDtTm = finishByDtTm;
	}
	/**
	 * @return the queueId
	 */
	public int getQueueId() {
		return queueId;
	}
	/**
	 * @param queueId the queueId to set
	 */
	public void setQueueId(int queueId) {
		this.queueId = queueId;
	}
	/**
	 * @return the makeFor
	 */
	public String getMakeFor() {
		return makeFor;
	}
	/**
	 * @param makeFor the makeFor to set
	 */
	public void setMakeFor(String makeFor) {
		this.makeFor = makeFor;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}
	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}
	/**
	 * @return the notesHtml
	 */
	public String getNotesHtml() {
		return notesHtml;
	}
	/**
	 * @param notesHtml the notesHtml to set
	 */
	public void setNotesHtml(String notesHtml) {
		this.notesHtml = notesHtml;
	}
	/**
	 * @return the pattern
	 */
	public RavelryPattern getPattern() {
		return pattern;
	}
	/**
	 * @param pattern the pattern to set
	 */
	public void setPattern(RavelryPattern pattern) {
		this.pattern = pattern;
	}
	/**
	 * @return the patternId
	 */
	public int getPatternId() {
		return patternId;
	}
	/**
	 * @param patternId the patternId to set
	 */
	public void setPatternId(int patternId) {
		this.patternId = patternId;
	}
	/**
	 * @return the patternName
	 */
	public String getPatternName() {
		return patternName;
	}
	/**
	 * @param patternName the patternName to set
	 */
	public void setPatternName(String patternName) {
		this.patternName = patternName;
	}
	/**
	 * @return the position
	 */
	public int getPosition() {
		return position;
	}
	/**
	 * @param position the position to set
	 */
	public void setPosition(int position) {
		this.position = position;
	}
	/**
	 * @return the stashes
	 */
	public ArrayList<RavelryStash> getStashes() {
		return stashes;
	}
	/**
	 * @param stashes the stashes to set
	 */
	public void setStashes(ArrayList<RavelryStash> stashes) {
		this.stashes = stashes;
	}
	/**
	 * @return the skeins
	 */
	public int getSkeins() {
		return skeins;
	}
	/**
	 * @param skeins the skeins to set
	 */
	public void setSkeins(int skeins) {
		this.skeins = skeins;
	}
	/**
	 * @return the sortOrder
	 */
	public int getSortOrder() {
		return sortOrder;
	}
	/**
	 * @param sortOrder the sortOrder to set
	 */
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}
	/**
	 * @return the startDtTm
	 */
	public Date getStartDtTm() {
		return startDtTm;
	}
	/**
	 * @param startDtTm the startDtTm to set
	 */
	public void setStartDtTm(Date startDtTm) {
		this.startDtTm = startDtTm;
	}
	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}
	/**
	 * @return the yarn
	 */
	public RavelryYarn getYarn() {
		return yarn;
	}
	/**
	 * @param yarn the yarn to set
	 */
	public void setYarn(RavelryYarn yarn) {
		this.yarn = yarn;
	}
	/**
	 * @return the yarnId
	 */
	public int getYarnId() {
		return yarnId;
	}
	/**
	 * @param yarnId the yarnId to set
	 */
	public void setYarnId(int yarnId) {
		this.yarnId = yarnId;
	}
	/**
	 * @return the yarnName
	 */
	public String getYarnName() {
		return yarnName;
	}
	/**
	 * @param yarnName the yarnName to set
	 */
	public void setYarnName(String yarnName) {
		this.yarnName = yarnName;
	}
	
	
	
	
	
}
