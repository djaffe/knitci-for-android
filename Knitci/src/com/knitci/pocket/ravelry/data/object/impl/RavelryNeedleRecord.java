package com.knitci.pocket.ravelry.data.object.impl;

import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.tools.needle.sizes.NeedleSize;
import com.knitci.pocket.tools.needle.types.Needle;

public class RavelryNeedleRecord extends RavelryObject {

	private int id;
	private int needleTypeId;
	private String comment;
	private Needle needleType;
	
	
	/**
	 * @return the needleType
	 */
	public Needle getNeedleType() {
		return needleType;
	}
	/**
	 * @param needleType the needleType to set
	 */
	public void setNeedleType(Needle needleType) {
		this.needleType = needleType;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the needleTypeId
	 */
	public int getNeedleTypeId() {
		return needleTypeId;
	}
	/**
	 * @param needleTypeId the needleTypeId to set
	 */
	public void setNeedleTypeId(int needleTypeId) {
		this.needleTypeId = needleTypeId;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
	
}
