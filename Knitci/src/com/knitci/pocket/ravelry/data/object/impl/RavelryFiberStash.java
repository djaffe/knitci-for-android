package com.knitci.pocket.ravelry.data.object.impl;

import java.util.Date;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryFiberStash extends RavelryStashGeneric {

	private String permalink;
	private String name;
	private String location;
	private int firstPhotoId;
	private Date createdDtTm;
	private Date updatedDtTm;
	private String notes;
	private int fiberCompanySubmissionId;
	private int id;
	private int fiberCompanyId;
	private int userId;
	private boolean hasPhoto;
	private int remainingFiberPackId;
	private int favoritesCount;
	private int stashStatusId;
	private int importedFromYarnId;
	private int commentsCount;
	private int primaryFiberPackId;
	private RavelryPhotoSet firstPhoto;
	/**
	 * @return the permalink
	 */
	public String getPermalink() {
		return permalink;
	}
	/**
	 * @param permalink the permalink to set
	 */
	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	/**
	 * @return the firstPhotoId
	 */
	public int getFirstPhotoId() {
		return firstPhotoId;
	}
	/**
	 * @param firstPhotoId the firstPhotoId to set
	 */
	public void setFirstPhotoId(int firstPhotoId) {
		this.firstPhotoId = firstPhotoId;
	}
	/**
	 * @return the createdDtTm
	 */
	public Date getCreatedDtTm() {
		return createdDtTm;
	}
	/**
	 * @param createdDtTm the createdDtTm to set
	 */
	public void setCreatedDtTm(Date createdDtTm) {
		this.createdDtTm = createdDtTm;
	}
	/**
	 * @return the updatedDtTm
	 */
	public Date getUpdatedDtTm() {
		return updatedDtTm;
	}
	/**
	 * @param updatedDtTm the updatedDtTm to set
	 */
	public void setUpdatedDtTm(Date updatedDtTm) {
		this.updatedDtTm = updatedDtTm;
	}
	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}
	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}
	/**
	 * @return the fiberCompanySubmissionId
	 */
	public int getFiberCompanySubmissionId() {
		return fiberCompanySubmissionId;
	}
	/**
	 * @param fiberCompanySubmissionId the fiberCompanySubmissionId to set
	 */
	public void setFiberCompanySubmissionId(int fiberCompanySubmissionId) {
		this.fiberCompanySubmissionId = fiberCompanySubmissionId;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the fiberCompanyId
	 */
	public int getFiberCompanyId() {
		return fiberCompanyId;
	}
	/**
	 * @param fiberCompanyId the fiberCompanyId to set
	 */
	public void setFiberCompanyId(int fiberCompanyId) {
		this.fiberCompanyId = fiberCompanyId;
	}
	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}
	/**
	 * @return the hasPhoto
	 */
	public boolean isHasPhoto() {
		return hasPhoto;
	}
	/**
	 * @param hasPhoto the hasPhoto to set
	 */
	public void setHasPhoto(boolean hasPhoto) {
		this.hasPhoto = hasPhoto;
	}
	/**
	 * @return the remainingFiberPackId
	 */
	public int getRemainingFiberPackId() {
		return remainingFiberPackId;
	}
	/**
	 * @param remainingFiberPackId the remainingFiberPackId to set
	 */
	public void setRemainingFiberPackId(int remainingFiberPackId) {
		this.remainingFiberPackId = remainingFiberPackId;
	}
	/**
	 * @return the favoritesCount
	 */
	public int getFavoritesCount() {
		return favoritesCount;
	}
	/**
	 * @param favoritesCount the favoritesCount to set
	 */
	public void setFavoritesCount(int favoritesCount) {
		this.favoritesCount = favoritesCount;
	}
	/**
	 * @return the stashStatusId
	 */
	public int getStashStatusId() {
		return stashStatusId;
	}
	/**
	 * @param stashStatusId the stashStatusId to set
	 */
	public void setStashStatusId(int stashStatusId) {
		this.stashStatusId = stashStatusId;
	}
	/**
	 * @return the importedFromYarnId
	 */
	public int getImportedFromYarnId() {
		return importedFromYarnId;
	}
	/**
	 * @param importedFromYarnId the importedFromYarnId to set
	 */
	public void setImportedFromYarnId(int importedFromYarnId) {
		this.importedFromYarnId = importedFromYarnId;
	}
	/**
	 * @return the commentsCount
	 */
	public int getCommentsCount() {
		return commentsCount;
	}
	/**
	 * @param commentsCount the commentsCount to set
	 */
	public void setCommentsCount(int commentsCount) {
		this.commentsCount = commentsCount;
	}
	/**
	 * @return the primaryFiberPackId
	 */
	public int getPrimaryFiberPackId() {
		return primaryFiberPackId;
	}
	/**
	 * @param primaryFiberPackId the primaryFiberPackId to set
	 */
	public void setPrimaryFiberPackId(int primaryFiberPackId) {
		this.primaryFiberPackId = primaryFiberPackId;
	}
	/**
	 * @return the firstPhoto
	 */
	public RavelryPhotoSet getFirstPhoto() {
		return firstPhoto;
	}
	/**
	 * @param firstPhoto the firstPhoto to set
	 */
	public void setFirstPhoto(RavelryPhotoSet firstPhoto) {
		this.firstPhoto = firstPhoto;
	}
	
	
	
}
