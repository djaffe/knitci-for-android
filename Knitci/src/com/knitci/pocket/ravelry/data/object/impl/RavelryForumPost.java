package com.knitci.pocket.ravelry.data.object.impl;

import java.util.Date;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryForumPost extends RavelryObject {

	private int id;
	private int parentPostNumber;
	private int postNumber;
	private Date createdDtTm;
	private RavelryUser user;
	private Date updatedDtTm;
	private String body;
	private boolean editable;
	private boolean deleted;
	private int replyCount;
	private String bodyHtml;
	private RavelryUser parentPostUser;
	private int parentPostId;
	private int topicId;

	/**
	 * @return the parentPostUser
	 */
	public RavelryUser getParentPostUser() {
		return parentPostUser;
	}

	/**
	 * @param parentPostUser the parentPostUser to set
	 */
	public void setParentPostUser(RavelryUser parentPostUser) {
		this.parentPostUser = parentPostUser;
	}
	
	
	/**
	 * @return the parentPostNumber
	 */
	public int getParentPostNumber() {
		return parentPostNumber;
	}

	/**
	 * @param parentPostNumber the parentPostNumber to set
	 */
	public void setParentPostNumber(int parentPostNumber) {
		this.parentPostNumber = parentPostNumber;
	}

	/**
	 * @return the postNumber
	 */
	public int getPostNumber() {
		return postNumber;
	}

	/**
	 * @param postNumber the postNumber to set
	 */
	public void setPostNumber(int postNumber) {
		this.postNumber = postNumber;
	}

	/**
	 * @return the createdDtTm
	 */
	public Date getCreatedDtTm() {
		return createdDtTm;
	}

	/**
	 * @param createdDtTm the createdDtTm to set
	 */
	public void setCreatedDtTm(Date createdDtTm) {
		this.createdDtTm = createdDtTm;
	}

	/**
	 * @return the user
	 */
	public RavelryUser getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(RavelryUser user) {
		this.user = user;
	}

	/**
	 * @return the updatedDtTm
	 */
	public Date getUpdatedDtTm() {
		return updatedDtTm;
	}

	/**
	 * @param updatedDtTm the updatedDtTm to set
	 */
	public void setUpdatedDtTm(Date updatedDtTm) {
		this.updatedDtTm = updatedDtTm;
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * @return the editable
	 */
	public boolean isEditable() {
		return (editable && !deleted);
	}

	/**
	 * @param editable the editable to set
	 */
	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	/**
	 * @return the deleted
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * @return the replyCount
	 */
	public int getReplyCount() {
		return replyCount;
	}

	/**
	 * @param replyCount the replyCount to set
	 */
	public void setReplyCount(int replyCount) {
		this.replyCount = replyCount;
	}

	/**
	 * @return the bodyHtml
	 */
	public String getBodyHtml() {
		return bodyHtml;
	}

	/**
	 * @param bodyHtml the bodyHtml to set
	 */
	public void setBodyHtml(String bodyHtml) {
		this.bodyHtml = bodyHtml;
	}

	/**
	 * @return the parentPostId
	 */
	public int getParentPostId() {
		return parentPostId;
	}

	/**
	 * @param parentPostId the parentPostId to set
	 */
	public void setParentPostId(int parentPostId) {
		this.parentPostId = parentPostId;
	}

	/**
	 * @return the topicId
	 */
	public int getTopicId() {
		return topicId;
	}

	/**
	 * @param topicId the topicId to set
	 */
	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	
	
}
