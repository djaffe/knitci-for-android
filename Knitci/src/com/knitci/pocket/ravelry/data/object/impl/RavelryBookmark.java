package com.knitci.pocket.ravelry.data.object.impl;

import java.util.Date;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryBookmark extends RavelryObject {

	private String type;
	private Date createdDtTm;
	private String comment;
	private int id;
	private int itemId;
	private RavelryPhotoSet firstPhoto;
	
	public RavelryBookmark() {
		
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the createdDtTm
	 */
	public Date getCreatedDtTm() {
		return createdDtTm;
	}

	/**
	 * @param createdDtTm the createdDtTm to set
	 */
	public void setCreatedDtTm(Date createdDtTm) {
		this.createdDtTm = createdDtTm;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the itemId
	 */
	public int getItemId() {
		return itemId;
	}

	/**
	 * @param itemId the itemId to set
	 */
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	/**
	 * @return the firstPhoto
	 */
	public RavelryPhotoSet getFirstPhoto() {
		return firstPhoto;
	}

	/**
	 * @param firstPhoto the firstPhoto to set
	 */
	public void setFirstPhoto(RavelryPhotoSet firstPhoto) {
		this.firstPhoto = firstPhoto;
	}
	
	
	
}
