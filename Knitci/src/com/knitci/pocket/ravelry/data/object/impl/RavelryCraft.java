package com.knitci.pocket.ravelry.data.object.impl;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryCraft extends RavelryObject {

	private int id;
	private String craft;
	private String permalink;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the craft
	 */
	public String getCraft() {
		return craft;
	}
	/**
	 * @param craft the craft to set
	 */
	public void setCraft(String craft) {
		this.craft = craft;
	}
	/**
	 * @return the permalink
	 */
	public String getPermalink() {
		return permalink;
	}
	/**
	 * @param permalink the permalink to set
	 */
	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}
	
	
	
}
