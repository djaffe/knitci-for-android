package com.knitci.pocket.ravelry.data.object.impl;

import java.util.ArrayList;
import java.util.Date;

import com.knitci.pocket.home.HomeForumPreviewData;
import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryForum extends RavelryObject {

	private String permalink;
	private java.util.Date lastPostDtTm;
	private int topicsCount;
	private String name;
	private int id;
	private ArrayList<RavelryCollection> topicCollections;
	private ArrayList<RavelryTopic> topics;
	
	public RavelryForum() {
		topics = new ArrayList<RavelryTopic>();
	}	
	
	/**
	 * @return the permalink
	 */
	public String getPermalink() {
		return permalink;
	}
	/**
	 * @param permalink the permalink to set
	 */
	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}
	/**
	 * @return the lastPostDtTm
	 */
	public Date getLastPostDtTm() {
		return lastPostDtTm;
	}
	/**
	 * @param date the lastPostDtTm to set
	 */
	public void setLastPostDtTm(java.util.Date date) {
		this.lastPostDtTm = date;
	}
	/**
	 * @return the topicsCount
	 */
	public int getTopicsCount() {
		return topicsCount;
	}
	/**
	 * @param topicsCount the topicsCount to set
	 */
	public void setTopicsCount(int topicsCount) {
		this.topicsCount = topicsCount;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	public void addTopic(RavelryTopic topic) {
		topics.add(topic);
	}
	
	public ArrayList<RavelryTopic> getTopics() {
		return topics;
	}
	
}
