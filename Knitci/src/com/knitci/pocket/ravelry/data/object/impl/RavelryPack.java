package com.knitci.pocket.ravelry.data.object.impl;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryPack extends RavelryObject {

	private String colorFamilyId;
	private String colorway;
	private String dyelot;
	private float gramsPerSkein;
	private float packId;
	private float metersPerSkein;
	private float ouncesPerSkein;
	private String personalName;
	private boolean preferMetricLength;
	private boolean preferMetricWeight;
	private int primaryPackId;
	private int projectId;
	private String quantityDescription;
	private int shopId;
	private String shopName;
	private int skeins;
	private int stashId;
	private float totalGrams;
	private float totalMeters;
	private float totalOunces;
	private float totalYarns;
	private float yardsPerSkein;
	private String yarn;
	private int yarnId;
	private String yarnName;
	private RavelryYarnWeight yarnWeight;
	private RavelryYarn ravYarn;
	
	/**
	 * @return the ravYarn
	 */
	public RavelryYarn getRavYarn() {
		return ravYarn;
	}
	/**
	 * @param ravYarn the ravYarn to set
	 */
	public void setRavYarn(RavelryYarn ravYarn) {
		this.ravYarn = ravYarn;
	}
	/**
	 * @return the colorFamilyId
	 */
	public String getColorFamilyId() {
		return colorFamilyId;
	}
	/**
	 * @param colorFamilyId the colorFamilyId to set
	 */
	public void setColorFamilyId(String colorFamilyId) {
		this.colorFamilyId = colorFamilyId;
	}
	/**
	 * @return the colorway
	 */
	public String getColorway() {
		return colorway;
	}
	/**
	 * @param colorway the colorway to set
	 */
	public void setColorway(String colorway) {
		this.colorway = colorway;
	}
	/**
	 * @return the dyelot
	 */
	public String getDyelot() {
		return dyelot;
	}
	/**
	 * @param dyelot the dyelot to set
	 */
	public void setDyelot(String dyelot) {
		this.dyelot = dyelot;
	}
	/**
	 * @return the gramsPerSkein
	 */
	public float getGramsPerSkein() {
		return gramsPerSkein;
	}
	/**
	 * @param gramsPerSkein the gramsPerSkein to set
	 */
	public void setGramsPerSkein(float gramsPerSkein) {
		this.gramsPerSkein = gramsPerSkein;
	}
	/**
	 * @return the packId
	 */
	public float getPackId() {
		return packId;
	}
	/**
	 * @param packId the packId to set
	 */
	public void setPackId(float packId) {
		this.packId = packId;
	}
	/**
	 * @return the metersPerSkein
	 */
	public float getMetersPerSkein() {
		return metersPerSkein;
	}
	/**
	 * @param metersPerSkein the metersPerSkein to set
	 */
	public void setMetersPerSkein(float metersPerSkein) {
		this.metersPerSkein = metersPerSkein;
	}
	/**
	 * @return the ouncesPerSkein
	 */
	public float getOuncesPerSkein() {
		return ouncesPerSkein;
	}
	/**
	 * @param ouncesPerSkein the ouncesPerSkein to set
	 */
	public void setOuncesPerSkein(float ouncesPerSkein) {
		this.ouncesPerSkein = ouncesPerSkein;
	}
	/**
	 * @return the personalName
	 */
	public String getPersonalName() {
		return personalName;
	}
	/**
	 * @param personalName the personalName to set
	 */
	public void setPersonalName(String personalName) {
		this.personalName = personalName;
	}
	/**
	 * @return the preferMetricLength
	 */
	public boolean getPreferMetricLength() {
		return preferMetricLength;
	}
	/**
	 * @param preferMetricLength the preferMetricLength to set
	 */
	public void setPreferMetricLength(boolean preferMetricLength) {
		this.preferMetricLength = preferMetricLength;
	}
	/**
	 * @return the preferMetricWeight
	 */
	public boolean getPreferMetricWeight() {
		return preferMetricWeight;
	}
	/**
	 * @param preferMetricWeight the preferMetricWeight to set
	 */
	public void setPreferMetricWeight(boolean preferMetricWeight) {
		this.preferMetricWeight = preferMetricWeight;
	}
	/**
	 * @return the primaryPackId
	 */
	public int getPrimaryPackId() {
		return primaryPackId;
	}
	/**
	 * @param primaryPackId the primaryPackId to set
	 */
	public void setPrimaryPackId(int primaryPackId) {
		this.primaryPackId = primaryPackId;
	}
	/**
	 * @return the projectId
	 */
	public int getProjectId() {
		return projectId;
	}
	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	/**
	 * @return the quantityDescription
	 */
	public String getQuantityDescription() {
		return quantityDescription;
	}
	/**
	 * @param quantityDescription the quantityDescription to set
	 */
	public void setQuantityDescription(String quantityDescription) {
		this.quantityDescription = quantityDescription;
	}
	/**
	 * @return the shopId
	 */
	public int getShopId() {
		return shopId;
	}
	/**
	 * @param shopId the shopId to set
	 */
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	/**
	 * @return the shopName
	 */
	public String getShopName() {
		return shopName;
	}
	/**
	 * @param string the shopName to set
	 */
	public void setShopName(String string) {
		this.shopName = string;
	}
	/**
	 * @return the skeins
	 */
	public int getSkeins() {
		return skeins;
	}
	/**
	 * @param skeins the skeins to set
	 */
	public void setSkeins(int skeins) {
		this.skeins = skeins;
	}
	/**
	 * @return the stashId
	 */
	public int getStashId() {
		return stashId;
	}
	/**
	 * @param stashId the stashId to set
	 */
	public void setStashId(int stashId) {
		this.stashId = stashId;
	}
	/**
	 * @return the totalGrams
	 */
	public float getTotalGrams() {
		return totalGrams;
	}
	/**
	 * @param totalGrams the totalGrams to set
	 */
	public void setTotalGrams(float totalGrams) {
		this.totalGrams = totalGrams;
	}
	/**
	 * @return the totalMeters
	 */
	public float getTotalMeters() {
		return totalMeters;
	}
	/**
	 * @param totalMeters the totalMeters to set
	 */
	public void setTotalMeters(float totalMeters) {
		this.totalMeters = totalMeters;
	}
	/**
	 * @return the totalOunces
	 */
	public float getTotalOunces() {
		return totalOunces;
	}
	/**
	 * @param totalOunces the totalOunces to set
	 */
	public void setTotalOunces(float totalOunces) {
		this.totalOunces = totalOunces;
	}
	/**
	 * @return the totalYarns
	 */
	public float getTotalYarns() {
		return totalYarns;
	}
	/**
	 * @param totalYarns the totalYarns to set
	 */
	public void setTotalYarns(float totalYarns) {
		this.totalYarns = totalYarns;
	}
	/**
	 * @return the yardsPerSkein
	 */
	public float getYardsPerSkein() {
		return yardsPerSkein;
	}
	/**
	 * @param yardsPerSkein the yardsPerSkein to set
	 */
	public void setYardsPerSkein(float yardsPerSkein) {
		this.yardsPerSkein = yardsPerSkein;
	}
	/**
	 * @return the yarn
	 */
	public String getYarn() {
		return yarn;
	}
	/**
	 * @param yarn the yarn to set
	 */
	public void setYarn(String yarn) {
		this.yarn = yarn;
	}
	/**
	 * @return the yarnId
	 */
	public int getYarnId() {
		return yarnId;
	}
	/**
	 * @param yarnId the yarnId to set
	 */
	public void setYarnId(int yarnId) {
		this.yarnId = yarnId;
	}
	/**
	 * @return the yarnName
	 */
	public String getYarnName() {
		return yarnName;
	}
	/**
	 * @param yarnName the yarnName to set
	 */
	public void setYarnName(String yarnName) {
		this.yarnName = yarnName;
	}
	/**
	 * @return the yarnWeight
	 */
	public RavelryYarnWeight getYarnWeight() {
		return yarnWeight;
	}
	/**
	 * @param yarnWeight the yarnWeight to set
	 */
	public void setYarnWeight(RavelryYarnWeight yarnWeight) {
		this.yarnWeight = yarnWeight;
	}
	
	
	
}
