package com.knitci.pocket.ravelry.data.object.impl;

import com.knitci.pocket.ravelry.data.object.impl.RavelryProject.ProjectPhotoUrlSize;

public class RavelryPhoto {

	private String url;
	private ProjectPhotoUrlSize size;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public ProjectPhotoUrlSize getSize() {
		return size;
	}
	public void setSize(ProjectPhotoUrlSize size) {
		this.size = size;
	}
}
