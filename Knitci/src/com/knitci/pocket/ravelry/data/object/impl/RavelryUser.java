package com.knitci.pocket.ravelry.data.object.impl;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

/**
 * Data object representing a Ravelry user. 
 * @author DJ021930
 */
public class RavelryUser extends RavelryObject {

	private String username;
	private String photoUrl;
	private String photoUrlSmall;
	private String photoUrlTiny;
	private int id;
	private String aboutMeHtml;
	private String aboutMe;
	private String location;
	private String favoriteCurse;
	private String firstName;
	
	/**
	 * @return the aboutMeHtml
	 */
	public String getAboutMeHtml() {
		return aboutMeHtml;
	}

	/**
	 * @param aboutMeHtml the aboutMeHtml to set
	 */
	public void setAboutMeHtml(String aboutMeHtml) {
		this.aboutMeHtml = aboutMeHtml;
	}

	/**
	 * @return the aboutMe
	 */
	public String getAboutMe() {
		return aboutMe;
	}

	/**
	 * @param aboutMe the aboutMe to set
	 */
	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the favoriteCurse
	 */
	public String getFavoriteCurse() {
		return favoriteCurse;
	}

	/**
	 * @param favoriteCurse the favoriteCurse to set
	 */
	public void setFavoriteCurse(String favoriteCurse) {
		this.favoriteCurse = favoriteCurse;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public RavelryUser() {
		super();
		username = null;
		photoUrl = null;
		photoUrlSmall = null;
		photoUrlTiny = null;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * A photo URL. 
	 * @return String A URL. Can be null. 
	 */
	public String getPhotoUrl() {
		return photoUrl;
	}

	/**
	 * Sets the photo URL. 
	 * @param String A URL. Can be null. 
	 */
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	/**
	 * A small photo URL. Can be null. 
	 * @return String A URL. Can be null. 
	 */
	public String getPhotoUrlSmall() {
		return photoUrlSmall;
	}

	/**
	 * Sets the small photo URL.
	 * @param String photoUrlSmall A URL. 
	 */
	public void setPhotoUrlSmall(String photoUrlSmall) {
		this.photoUrlSmall = photoUrlSmall;
	}

	/**
	 * Gets the tiny photo URL. 
	 * @return String A URL. Can be null. 
	 */
	public String getPhotoUrlTiny() {
		return photoUrlTiny;
	}

	/**
	 * Sets the tiny photo URL. 
	 * @param String photoUrlTiny A URL. 
	 */
	public void setPhotoUrlTiny(String photoUrlTiny) {
		this.photoUrlTiny = photoUrlTiny;
	}

	/**
	 * Gets the Ravelry Username. This should never be null. 
	 * @return String The username on Ravelry. Should never be null. 
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the Ravelry Username. Should not be null. 
	 * @param String username Should not be null. 
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
	/**
	 * Checks if the RavelryUser object is valid. Considered valid if a username is set. 
	 * @return boolean True or false if valid. 
	 */
	public boolean isValid() {
		return !username.isEmpty();
	}
	
}
