package com.knitci.pocket.ravelry.data.object.impl;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryPatternType extends RavelryObject {

	private String id;
	private String name;
	private boolean clothing;
	private String permalink;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the clothing
	 */
	public boolean isClothing() {
		return clothing;
	}
	/**
	 * @param clothing the clothing to set
	 */
	public void setClothing(boolean clothing) {
		this.clothing = clothing;
	}
	/**
	 * @return the permalink
	 */
	public String getPermalink() {
		return permalink;
	}
	/**
	 * @param permalink the permalink to set
	 */
	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}
	
	
	
}
