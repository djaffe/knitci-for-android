package com.knitci.pocket.ravelry.data.object.impl;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryYarnWeight extends RavelryObject {

	private String name;
	private int minGauge;
	private String wpi;
	private int yarnWeightId;
	private int minCrochetGauge;
	private int maxCrochetGauge;
	private int ply;
	private int minKnitGauge;
	private int maxKnitGauge;
	
	// Pattern specific values
	private String maxGauge;
	private int id;
	private String knitGauge;
	private String crochetGauge;
	
	public boolean isKnitGaugeRange() {
		return (minKnitGauge != maxKnitGauge);
	}
	
	public boolean isCrochetGaugeRange() {
		return (minCrochetGauge != maxCrochetGauge);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the minGauge
	 */
	public int getMinGauge() {
		return minGauge;
	}

	/**
	 * @param minGauge the minGauge to set
	 */
	public void setMinGauge(int minGauge) {
		this.minGauge = minGauge;
	}

	/**
	 * @return the wpi
	 */
	public String getWpi() {
		return wpi;
	}

	/**
	 * @param wpi the wpi to set
	 */
	public void setWpi(String wpi) {
		this.wpi = wpi;
	}

	/**
	 * @return the yarnWeightId
	 */
	public int getYarnWeightId() {
		return yarnWeightId;
	}

	/**
	 * @param yarnWeightId the yarnWeightId to set
	 */
	public void setYarnWeightId(int yarnWeightId) {
		this.yarnWeightId = yarnWeightId;
	}

	/**
	 * @return the minCrochetGauge
	 */
	public int getMinCrochetGauge() {
		return minCrochetGauge;
	}

	/**
	 * @param minCrochetGauge the minCrochetGauge to set
	 */
	public void setMinCrochetGauge(int minCrochetGauge) {
		this.minCrochetGauge = minCrochetGauge;
	}

	/**
	 * @return the maxCrochetGauge
	 */
	public int getMaxCrochetGauge() {
		return maxCrochetGauge;
	}

	/**
	 * @param maxCrochetGauge the maxCrochetGauge to set
	 */
	public void setMaxCrochetGauge(int maxCrochetGauge) {
		this.maxCrochetGauge = maxCrochetGauge;
	}

	/**
	 * @return the ply
	 */
	public int getPly() {
		return ply;
	}

	/**
	 * @param ply the ply to set
	 */
	public void setPly(int ply) {
		this.ply = ply;
	}

	/**
	 * @return the minKnitGauge
	 */
	public int getMinKnitGauge() {
		return minKnitGauge;
	}

	/**
	 * @param minKnitGauge the minKnitGauge to set
	 */
	public void setMinKnitGauge(int minKnitGauge) {
		this.minKnitGauge = minKnitGauge;
	}

	/**
	 * @return the maxKnitGauge
	 */
	public int getMaxKnitGauge() {
		return maxKnitGauge;
	}

	/**
	 * @param maxKnitGauge the maxKnitGauge to set
	 */
	public void setMaxKnitGauge(int maxKnitGauge) {
		this.maxKnitGauge = maxKnitGauge;
	}

	/**
	 * @return the maxGauge
	 */
	public String getMaxGauge() {
		return maxGauge;
	}

	/**
	 * @param maxGauge the maxGauge to set
	 */
	public void setMaxGauge(String maxGauge) {
		this.maxGauge = maxGauge;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the knitGauge
	 */
	public String getKnitGauge() {
		return knitGauge;
	}

	/**
	 * @param knitGauge the knitGauge to set
	 */
	public void setKnitGauge(String knitGauge) {
		this.knitGauge = knitGauge;
	}

	/**
	 * @return the crochetGauge
	 */
	public String getCrochetGauge() {
		return crochetGauge;
	}

	/**
	 * @param crochetGauge the crochetGauge to set
	 */
	public void setCrochetGauge(String crochetGauge) {
		this.crochetGauge = crochetGauge;
	}
	
	
	
}
