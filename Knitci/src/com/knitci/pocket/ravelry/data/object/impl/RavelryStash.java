package com.knitci.pocket.ravelry.data.object.impl;

import java.util.Date;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryStash extends RavelryStashGeneric {
	
	private int id;
	private boolean hasPhoto;
	private String colorFamilyName;
	private RavelryStashStatus status;
	private Date updatedDtTm;
	private String dyelot;
	private Date createdDtTm;
	private String colorwayName;
	private String permalink;
	private int favoritesCount;
	private String location;
	private boolean handspun;
	private String name;
	private int commentsCount;
	private RavelryYarn yarn;
	private RavelryPhotoSet firstPhoto;
	
	
	
	/**
	 * @return the firstPhoto
	 */
	public RavelryPhotoSet getFirstPhoto() {
		return firstPhoto;
	}
	/**
	 * @param firstPhoto the firstPhoto to set
	 */
	public void setFirstPhoto(RavelryPhotoSet firstPhoto) {
		this.firstPhoto = firstPhoto;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the hasPhoto
	 */
	public boolean isHasPhoto() {
		return hasPhoto;
	}
	/**
	 * @param hasPhoto the hasPhoto to set
	 */
	public void setHasPhoto(boolean hasPhoto) {
		this.hasPhoto = hasPhoto;
	}
	/**
	 * @return the colorFamilyName
	 */
	public String getColorFamilyName() {
		return colorFamilyName;
	}
	/**
	 * @param colorFamilyName the colorFamilyName to set
	 */
	public void setColorFamilyName(String colorFamilyName) {
		this.colorFamilyName = colorFamilyName;
	}
	/**
	 * @return the status
	 */
	public RavelryStashStatus getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(RavelryStashStatus status) {
		this.status = status;
	}
	/**
	 * @return the updatedDtTm
	 */
	public Date getUpdatedDtTm() {
		return updatedDtTm;
	}
	/**
	 * @param updatedDtTm the updatedDtTm to set
	 */
	public void setUpdatedDtTm(Date updatedDtTm) {
		this.updatedDtTm = updatedDtTm;
	}
	/**
	 * @return the dyelot
	 */
	public String getDyelot() {
		return dyelot;
	}
	/**
	 * @param dyelot the dyelot to set
	 */
	public void setDyelot(String dyelot) {
		this.dyelot = dyelot;
	}
	/**
	 * @return the createdDtTm
	 */
	public Date getCreatedDtTm() {
		return createdDtTm;
	}
	/**
	 * @param createdDtTm the createdDtTm to set
	 */
	public void setCreatedDtTm(Date createdDtTm) {
		this.createdDtTm = createdDtTm;
	}
	/**
	 * @return the colorwayName
	 */
	public String getColorwayName() {
		return colorwayName;
	}
	/**
	 * @param colorwayName the colorwayName to set
	 */
	public void setColorwayName(String colorwayName) {
		this.colorwayName = colorwayName;
	}
	/**
	 * @return the permalink
	 */
	public String getPermalink() {
		return permalink;
	}
	/**
	 * @param permalink the permalink to set
	 */
	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}
	/**
	 * @return the favoritesCount
	 */
	public int getFavoritesCount() {
		return favoritesCount;
	}
	/**
	 * @param favoritesCount the favoritesCount to set
	 */
	public void setFavoritesCount(int favoritesCount) {
		this.favoritesCount = favoritesCount;
	}
	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	/**
	 * @return the handspun
	 */
	public boolean isHandspun() {
		return handspun;
	}
	/**
	 * @param handspun the handspun to set
	 */
	public void setHandspun(boolean handspun) {
		this.handspun = handspun;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the commentsCount
	 */
	public int getCommentsCount() {
		return commentsCount;
	}
	/**
	 * @param commentsCount the commentsCount to set
	 */
	public void setCommentsCount(int commentsCount) {
		this.commentsCount = commentsCount;
	}
	/**
	 * @return the yarn
	 */
	public RavelryYarn getYarn() {
		return yarn;
	}
	/**
	 * @param yarn the yarn to set
	 */
	public void setYarn(RavelryYarn yarn) {
		this.yarn = yarn;
	}
	
	
	

}
