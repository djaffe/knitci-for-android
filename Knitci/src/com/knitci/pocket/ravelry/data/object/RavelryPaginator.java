package com.knitci.pocket.ravelry.data.object;


public class RavelryPaginator {
	
	private int lastPage;
	private int pageCount;
	private int page;
	private int results;
	private int pageSize;
	
	public boolean isMorePagesAvail() {
		return (page < pageCount);
	}
	
	/**
	 * @return the lastPage
	 */
	public int getLastPage() {
		return lastPage;
	}
	/**
	 * @param lastPage the lastPage to set
	 */
	public void setLastPage(int lastPage) {
		this.lastPage = lastPage;
	}
	/**
	 * @return the pageCount
	 */
	public int getPageCount() {
		return pageCount;
	}
	/**
	 * @param pageCount the pageCount to set
	 */
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}
	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}
	/**
	 * @param page the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}
	/**
	 * @return the results
	 */
	public int getResults() {
		return results;
	}
	/**
	 * @param results the results to set
	 */
	public void setResults(int results) {
		this.results = results;
	}
	/**
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}
	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	
	
}
