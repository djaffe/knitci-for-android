package com.knitci.pocket.ravelry.data.object.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryPattern extends RavelryObject {

	private String permalink;
	private int patternId;
	private String patternName;
	private RavelryCollection patternSources;
	private RavelryPatternAuthor patternDesigner;
	private RavelryPatternAuthor patternAuthor;
	private RavelryPhotoSet firstPhotos;
	
	//Extra info (when retriving a single pattern)
	private RavelryPrintings printings;
	private RavelryPatternAuthor author;
	private String pdfUrl;
	private String currency;
	private ArrayList<RavelryPhotoSet> photos;
	private String sizesAvailable;
	private int queuedProjectsCount;
	private int favoritesCount;
	private RavelryCraft craft;
	private String gaugeDescription;
	private int yardageMax;
	private int projectsCount;
	private float gauge;
	private boolean free;
	private boolean downloadable;
	private int productId;
	private int ratingCount;
	private float price;
	private String yarnWeightDescription;
	private Date publishedDtTm;
	private ArrayList<RavelryPatternCategory> patternCategories;
	private ArrayList<RavelryNeedleSize> patternNeedles;
	private String notes;
	private String currencySymbol;
	private float difficultyAverage;
	private RavelryPatternType patternType;
	private String yardageDescription;
	private String url;
	private String gaugePattern;
	private RavelryDownloadLocation downloadLocation;
	private float rowGauge;
	private RavelryYarnWeight yarnWeight;
	private int commentsCount;
	private ArrayList<RavelryPack> packs;
	private String notesHtml;
	private boolean pdfInLibary;
	private boolean ravelryDownload;
	private float averageRating;
	private int difficultyCount;
	private int gaugeDivisor;
	
	public RavelryPattern() {
		this.patternCategories = new ArrayList<RavelryPatternCategory>();
		packs = new ArrayList<RavelryPack>();
	}
	
	/**
	 * @return the pdfInLibary
	 */
	public boolean isPdfInLibary() {
		return pdfInLibary;
	}

	/**
	 * @param pdfInLibary the pdfInLibary to set
	 */
	public void setPdfInLibary(boolean pdfInLibary) {
		this.pdfInLibary = pdfInLibary;
	}

	/**
	 * @return the ravelryDownload
	 */
	public boolean isRavelryDownload() {
		return ravelryDownload;
	}

	/**
	 * @param ravelryDownload the ravelryDownload to set
	 */
	public void setRavelryDownload(boolean ravelryDownload) {
		this.ravelryDownload = ravelryDownload;
	}

	/**
	 * @return the averageRating
	 */
	public float getAverageRating() {
		return averageRating;
	}

	/**
	 * @param averageRating the averageRating to set
	 */
	public void setAverageRating(float averageRating) {
		this.averageRating = averageRating;
	}

	/**
	 * @return the difficultyCount
	 */
	public int getDifficultyCount() {
		return difficultyCount;
	}

	/**
	 * @param difficultyCount the difficultyCount to set
	 */
	public void setDifficultyCount(int difficultyCount) {
		this.difficultyCount = difficultyCount;
	}

	/**
	 * @return the gaugeDivisor
	 */
	public int getGaugeDivisor() {
		return gaugeDivisor;
	}

	/**
	 * @param gaugeDivisor the gaugeDivisor to set
	 */
	public void setGaugeDivisor(int gaugeDivisor) {
		this.gaugeDivisor = gaugeDivisor;
	}

	/**
	 * @return the notesHtml
	 */
	public String getNotesHtml() {
		return notesHtml;
	}

	/**
	 * @param notesHtml the notesHtml to set
	 */
	public void setNotesHtml(String notesHtml) {
		this.notesHtml = notesHtml;
	}

	
	/**
	 * @return the packs
	 */
	public ArrayList<RavelryPack> getPacks() {
		return packs;
	}

	public void addPack(RavelryPack pack) {
		this.packs.add(pack);
	}
	
	/**
	 * @param packs the packs to set
	 */
	public void setPacks(ArrayList<RavelryPack> packs) {
		this.packs = packs;
	}

	/**
	 * @return the commentsCount
	 */
	public int getCommentsCount() {
		return commentsCount;
	}
	/**
	 * @param commentsCount the commentsCount to set
	 */
	public void setCommentsCount(int commentsCount) {
		this.commentsCount = commentsCount;
	}
	/**
	 * @return the yarnWeight
	 */
	public RavelryYarnWeight getYarnWeight() {
		return yarnWeight;
	}
	/**
	 * @param yarnWeight the yarnWeight to set
	 */
	public void setYarnWeight(RavelryYarnWeight yarnWeight) {
		this.yarnWeight = yarnWeight;
	}
	/**
	 * @return the rowGauge
	 */
	public float getRowGauge() {
		return rowGauge;
	}
	/**
	 * @param rowGauge the rowGauge to set
	 */
	public void setRowGauge(float rowGauge) {
		this.rowGauge = rowGauge;
	}
	/**
	 * @return the downloadLocation
	 */
	public RavelryDownloadLocation getDownloadLocation() {
		return downloadLocation;
	}
	/**
	 * @param downloadLocation the downloadLocation to set
	 */
	public void setDownloadLocation(RavelryDownloadLocation downloadLocation) {
		this.downloadLocation = downloadLocation;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the gaugePattern
	 */
	public String getGaugePattern() {
		return gaugePattern;
	}
	/**
	 * @param gaugePattern the gaugePattern to set
	 */
	public void setGaugePattern(String gaugePattern) {
		this.gaugePattern = gaugePattern;
	}
	/**
	 * @return the yardageDescription
	 */
	public String getYardageDescription() {
		return yardageDescription;
	}
	/**
	 * @param yardageDescription the yardageDescription to set
	 */
	public void setYardageDescription(String yardageDescription) {
		this.yardageDescription = yardageDescription;
	}
	/**
	 * @return the patternType
	 */
	public RavelryPatternType getPatternType() {
		return patternType;
	}
	/**
	 * @param patternType the patternType to set
	 */
	public void setPatternType(RavelryPatternType patternType) {
		this.patternType = patternType;
	}
	/**
	 * @return the difficultyAverage
	 */
	public float getDifficultyAverage() {
		return difficultyAverage;
	}
	/**
	 * @param difficultyAverage the difficultyAverage to set
	 */
	public void setDifficultyAverage(float difficultyAverage) {
		this.difficultyAverage = difficultyAverage;
	}
	/**
	 * @return the currencySymbol
	 */
	public String getCurrencySymbol() {
		return currencySymbol;
	}
	/**
	 * @param currencySymbol the currencySymbol to set
	 */
	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}
	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}
	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}
	/**
	 * @return the patternCategory
	 */
	public ArrayList<RavelryPatternCategory> setPatternCategories() {
		return patternCategories;
	}
	/**
	 * @param patternCategory the patternCategory to set
	 */
	public void setPatternCategories(ArrayList<RavelryPatternCategory> patternCategory) {
		this.patternCategories = patternCategory;
	}
	
	public void addPatternCategory(RavelryPatternCategory cat) {
		this.patternCategories.add(cat);
	}
	
	public ArrayList<RavelryPatternCategory> getPatternCategories() {
		return this.patternCategories;
	}
	
	/**
	 * @return the productId
	 */
	public int getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(int productId) {
		this.productId = productId;
	}
	/**
	 * @return the ratingCount
	 */
	public int getRatingCount() {
		return ratingCount;
	}
	/**
	 * @param ratingCount the ratingCount to set
	 */
	public void setRatingCount(int ratingCount) {
		this.ratingCount = ratingCount;
	}
	/**
	 * @return the price
	 */
	public float getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(float price) {
		this.price = price;
	}
	/**
	 * @return the yarnWeightDescription
	 */
	public String getYarnWeightDescription() {
		return yarnWeightDescription;
	}
	/**
	 * @param yarnWeightDescription the yarnWeightDescription to set
	 */
	public void setYarnWeightDescription(String yarnWeightDescription) {
		this.yarnWeightDescription = yarnWeightDescription;
	}
	/**
	 * @return the publishedDtTm
	 */
	public Date getPublishedDtTm() {
		return publishedDtTm;
	}
	/**
	 * @param publishedDtTm the publishedDtTm to set
	 */
	public void setPublishedDtTm(Date publishedDtTm) {
		this.publishedDtTm = publishedDtTm;
	}
	/**
	 * @return the downloadable
	 */
	public boolean isDownloadable() {
		return downloadable;
	}
	/**
	 * @param downloadable the downloadable to set
	 */
	public void setDownloadable(boolean downloadable) {
		this.downloadable = downloadable;
	}
	/**
	 * @return the free
	 */
	public boolean isFree() {
		return free;
	}
	/**
	 * @param free the free to set
	 */
	public void setFree(boolean free) {
		this.free = free;
	}
	/**
	 * @return the gauge
	 */
	public float getGauge() {
		return gauge;
	}
	/**
	 * @param gauge the gauge to set
	 */
	public void setGauge(float gauge) {
		this.gauge = gauge;
	}
	/**
	 * @return the projectsCount
	 */
	public int getProjectsCount() {
		return projectsCount;
	}
	/**
	 * @param projectsCount the projectsCount to set
	 */
	public void setProjectsCount(int projectsCount) {
		this.projectsCount = projectsCount;
	}
	/**
	 * @return the yardageMax
	 */
	public int getYardageMax() {
		return yardageMax;
	}
	/**
	 * @param yardageMax the yardageMax to set
	 */
	public void setYardageMax(int yardageMax) {
		this.yardageMax = yardageMax;
	}
	/**
	 * @return the gaugeDescription
	 */
	public String getGaugeDescription() {
		return gaugeDescription;
	}
	/**
	 * @param gaugeDescription the gaugeDescription to set
	 */
	public void setGaugeDescription(String gaugeDescription) {
		this.gaugeDescription = gaugeDescription;
	}
	/**
	 * @return the craft
	 */
	public RavelryCraft getCraft() {
		return craft;
	}
	/**
	 * @param craft the craft to set
	 */
	public void setCraft(RavelryCraft craft) {
		this.craft = craft;
	}
	/**
	 * @return the favoritesCount
	 */
	public int getFavoritesCount() {
		return favoritesCount;
	}
	/**
	 * @param favoritesCount the favoritesCount to set
	 */
	public void setFavoritesCount(int favoritesCount) {
		this.favoritesCount = favoritesCount;
	}
	/**
	 * @return the queuedProjectsCount
	 */
	public int getQueuedProjectsCount() {
		return queuedProjectsCount;
	}
	/**
	 * @param queuedProjectsCount the queuedProjectsCount to set
	 */
	public void setQueuedProjectsCount(int queuedProjectsCount) {
		this.queuedProjectsCount = queuedProjectsCount;
	}
	/**
	 * @return the sizesAvailable
	 */
	public String getSizesAvailable() {
		return sizesAvailable;
	}
	/**
	 * @param sizesAvailable the sizesAvailable to set
	 */
	public void setSizesAvailable(String sizesAvailable) {
		this.sizesAvailable = sizesAvailable;
	}
	/**
	 * @return the photos
	 */
	public ArrayList<RavelryPhotoSet> getPhotos() {
		return photos;
	}
	/**
	 * @param photos the photos to set
	 */
	public void setPhotos(ArrayList<RavelryPhotoSet> photos) {
		this.photos = photos;
	}
	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}
	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	/**
	 * @return the pdfUrl
	 */
	public String getPdfUrl() {
		return pdfUrl;
	}
	/**
	 * @param pdfUrl the pdfUrl to set
	 */
	public void setPdfUrl(String pdfUrl) {
		this.pdfUrl = pdfUrl;
	}
	/**
	 * @return the author
	 */
	public RavelryPatternAuthor getAuthor() {
		return author;
	}
	/**
	 * @param author the author to set
	 */
	public void setAuthor(RavelryPatternAuthor author) {
		this.author = author;
	}
	/**
	 * @return the patternId
	 */
	public int getPatternId() {
		return patternId;
	}
	/**
	 * @param patternId the patternId to set
	 */
	public void setPatternId(int patternId) {
		this.patternId = patternId;
	}
	/**
	 * @return the patternName
	 */
	public String getPatternName() {
		return patternName;
	}
	/**
	 * @param patternName the patternName to set
	 */
	public void setPatternName(String patternName) {
		this.patternName = patternName;
	}
	/**
	 * @return the permalink
	 */
	public String getPermalink() {
		return permalink;
	}
	/**
	 * @param permalink the permalink to set
	 */
	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}
	/**
	 * @return the patternSources
	 */
	public RavelryCollection getPatternSources() {
		return patternSources;
	}
	/**
	 * @param patternSources the patternSources to set
	 */
	public void setPatternSources(RavelryCollection patternSources) {
		this.patternSources = patternSources;
	}
	/**
	 * @return the patternDesigner
	 */
	public RavelryPatternAuthor getPatternDesigner() {
		return patternDesigner;
	}
	/**
	 * @param patternDesigner the patternDesigner to set
	 */
	public void setPatternDesigner(RavelryPatternAuthor patternDesigner) {
		this.patternDesigner = patternDesigner;
	}
	/**
	 * @return the patternAuthor
	 */
	public RavelryPatternAuthor getPatternAuthor() {
		return patternAuthor;
	}
	/**
	 * @param patternAuthor the patternAuthor to set
	 */
	public void setPatternAuthor(RavelryPatternAuthor patternAuthor) {
		this.patternAuthor = patternAuthor;
	}
	/**
	 * @return the firstPhotos
	 */
	public RavelryPhotoSet getFirstPhotos() {
		return firstPhotos;
	}
	/**
	 * @param firstPhotos the firstPhotos to set
	 */
	public void setFirstPhotos(RavelryPhotoSet firstPhotos) {
		this.firstPhotos = firstPhotos;
	}
	/**
	 * @return the printings
	 */
	public RavelryPrintings getPrintings() {
		return printings;
	}
	/**
	 * @param printings the printings to set
	 */
	public void setPrintings(RavelryPrintings printings) {
		this.printings = printings;
	}
	
	public void setNeedles(ArrayList<RavelryNeedleSize> needles) {
		this.patternNeedles = needles;
	}
	
	public void addNeedle(RavelryNeedleSize needle) {
		if(patternNeedles == null) {
			patternNeedles = new ArrayList<RavelryNeedleSize>();
		}
		patternNeedles.add(needle);
	}
	
	public ArrayList<RavelryNeedleSize> getNeedles() {
		return patternNeedles;
	}
	
}
