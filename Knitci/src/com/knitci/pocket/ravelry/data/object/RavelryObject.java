package com.knitci.pocket.ravelry.data.object;

import com.knitci.pocket.ravelry.data.object.impl.RavelryPersonalAttribute;



public abstract class RavelryObject extends Object {

	protected int httpcode;
	protected String etag;

	protected boolean isFavorited;
	protected boolean isQueued;
	protected int bookmarkId;

	public void applyPersonalAttributes(RavelryPersonalAttribute atts) {
		if(atts != null) {
			this.isFavorited = atts.isFavorited();
			this.isQueued = atts.isQueued();
			this.bookmarkId = atts.getBookmarkId();
		}
	}

	/**
	 * @return the isFavorited
	 */
	public boolean isFavorited() {
		return isFavorited;
	}

	/**
	 * @param isFavorited the isFavorited to set
	 */
	public void setFavorited(boolean isFavorited) {
		this.isFavorited = isFavorited;
	}

	/**
	 * @return the isQueued
	 */
	public boolean isQueued() {
		return isQueued;
	}

	/**
	 * @param isQueued the isQueued to set
	 */
	public void setQueued(boolean isQueued) {
		this.isQueued = isQueued;
	}

	/**
	 * @return the bookmarkId
	 */
	public int getBookmarkId() {
		return bookmarkId;
	}

	/**
	 * @param bookmarkId the bookmarkId to set
	 */
	public void setBookmarkId(int bookmarkId) {
		this.bookmarkId = bookmarkId;
	}

	public RavelryObject() {
		httpcode = 200;
	}

	public boolean isSuccessful() {
		if(httpcode != 200) {
			return false;
		}
		return true;
	}

	public void setHttpCode(int code) {
		httpcode = code;
	}

	public int getHttpCode() {
		return httpcode;
	}

	public void setETag(String etag) {
		this.etag = etag;
	}

	public String getETag() {
		return this.etag;
	}

}
