package com.knitci.pocket.ravelry.data.object.impl;

import java.util.ArrayList;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryPrintings extends RavelryObject {

	RavelryPatternSource primarySource;
	ArrayList<RavelryPatternSource> otherSources;
	
	public RavelryPrintings() {
		otherSources = new ArrayList<RavelryPatternSource>();
	}

	/**
	 * @return the primarySource
	 */
	public RavelryPatternSource getPrimarySource() {
		return primarySource;
	}

	/**
	 * @param primarySource the primarySource to set
	 */
	public void setPrimarySource(RavelryPatternSource primarySource) {
		this.primarySource = primarySource;
	}
	
	public void addSource(RavelryPatternSource source) {
		otherSources.add(source);
	}
	
	public ArrayList<RavelryPatternSource> getSources() {
		return otherSources;
	}
	
}
