package com.knitci.pocket.ravelry.data.object.impl;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryPatternSource extends RavelryObject {

	private String amazonRating;
	private String amazonUrl;
	private String author;
	private String listPrice;
	private String name;
	private boolean outOfPrint;
	private int patternsCount;
	private String permalink;
	private String price;
	private String shelfImageUrl;
	private String url;
	/**
	 * @return the amazonRating
	 */
	public String getAmazonRating() {
		return amazonRating;
	}
	/**
	 * @param amazonRating the amazonRating to set
	 */
	public void setAmazonRating(String amazonRating) {
		this.amazonRating = amazonRating;
	}
	/**
	 * @return the amazonUrl
	 */
	public String getAmazonUrl() {
		return amazonUrl;
	}
	/**
	 * @param amazonUrl the amazonUrl to set
	 */
	public void setAmazonUrl(String amazonUrl) {
		this.amazonUrl = amazonUrl;
	}
	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	/**
	 * @return the listPrice
	 */
	public String getListPrice() {
		return listPrice;
	}
	/**
	 * @param listPrice the listPrice to set
	 */
	public void setListPrice(String listPrice) {
		this.listPrice = listPrice;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the outOfPrint
	 */
	public boolean isOutOfPrint() {
		return outOfPrint;
	}
	/**
	 * @param outOfPrint the outOfPrint to set
	 */
	public void setOutOfPrint(boolean outOfPrint) {
		this.outOfPrint = outOfPrint;
	}
	/**
	 * @return the patternsCount
	 */
	public int getPatternsCount() {
		return patternsCount;
	}
	/**
	 * @param patternsCount the patternsCount to set
	 */
	public void setPatternsCount(int patternsCount) {
		this.patternsCount = patternsCount;
	}
	/**
	 * @return the permalink
	 */
	public String getPermalink() {
		return permalink;
	}
	/**
	 * @param permalink the permalink to set
	 */
	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}
	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}
	/**
	 * @return the shelfImageUrl
	 */
	public String getShelfImageUrl() {
		return shelfImageUrl;
	}
	/**
	 * @param shelfImageUrl the shelfImageUrl to set
	 */
	public void setShelfImageUrl(String shelfImageUrl) {
		this.shelfImageUrl = shelfImageUrl;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	
	
}
