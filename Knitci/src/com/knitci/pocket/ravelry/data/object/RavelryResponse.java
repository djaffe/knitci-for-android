package com.knitci.pocket.ravelry.data.object;

import com.knitci.pocket.ravelry.data.access.RavelryAsyncTask;
import com.knitci.pocket.ravelry.data.access.RavelryAsyncTask.ERavelryTask;

/**
 * A response object for returning multiple objects from a finished {@link RavelryAsyncTask}.
 * @author DJ021930
 */
public class RavelryResponse {
	public ERavelryTask task;
	public RavelryObject result;
	public int httpResponseCode;
	public int miscId;
	public int forumId;
	public int topicId;
	public String messageBox;
	public String username;
	public String miscString;
	public int upperbound;
	public int lowerbound;
}
