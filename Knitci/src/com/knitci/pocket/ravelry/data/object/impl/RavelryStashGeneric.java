package com.knitci.pocket.ravelry.data.object.impl;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryStashGeneric extends RavelryObject {

	private StashType type;
	
	/**
	 * @return the type
	 */
	public StashType getType() {
		return type;
	}



	/**
	 * @param type the type to set
	 */
	public void setType(StashType type) {
		this.type = type;
	}



	public enum StashType {
		STASH, FIBER
	}
	
}
