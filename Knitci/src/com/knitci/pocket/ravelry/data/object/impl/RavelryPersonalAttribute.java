package com.knitci.pocket.ravelry.data.object.impl;

public class RavelryPersonalAttribute {

	private boolean isFavorited;
	private boolean isQueued;
	private int bookmarkId;
	
	public RavelryPersonalAttribute() {
		isFavorited = false;
		isQueued = false;
		bookmarkId = 0;
	}

	/**
	 * @return the isFavorited
	 */
	public boolean isFavorited() {
		return isFavorited;
	}

	/**
	 * @param isFavorited the isFavorited to set
	 */
	public void setFavorited(boolean isFavorited) {
		this.isFavorited = isFavorited;
	}

	/**
	 * @return the isQueued
	 */
	public boolean isQueued() {
		return isQueued;
	}

	/**
	 * @param isQueued the isQueued to set
	 */
	public void setQueued(boolean isQueued) {
		this.isQueued = isQueued;
	}

	/**
	 * @return the bookmarkId
	 */
	public int getBookmarkId() {
		return bookmarkId;
	}

	/**
	 * @param bookmarkId the bookmarkId to set
	 */
	public void setBookmarkId(int bookmarkId) {
		this.bookmarkId = bookmarkId;
	}
	
	
	
}

