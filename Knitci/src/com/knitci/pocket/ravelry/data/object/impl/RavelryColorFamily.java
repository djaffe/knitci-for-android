package com.knitci.pocket.ravelry.data.object.impl;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryColorFamily extends RavelryObject {

	private String color;
	private int colorFamilyId;
	private String name;
	private String permalink;
	private int spectrumOrder;
	
}
