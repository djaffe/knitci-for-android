package com.knitci.pocket.ravelry.data.object.impl;

import java.util.HashMap;

import com.knitci.pocket.ravelry.data.object.impl.RavelryProject.ProjectPhotoUrlSize;

public class RavelryPhotoSet {
	
	private HashMap<ProjectPhotoUrlSize, RavelryPhoto> map;
	private int id;
	private int sortOrder;
	private int xOffset;
	private int yOffset;
	
	/**
	 * @return the map
	 */
	public HashMap<ProjectPhotoUrlSize, RavelryPhoto> getMap() {
		return map;
	}

	/**
	 * @param map the map to set
	 */
	public void setMap(HashMap<ProjectPhotoUrlSize, RavelryPhoto> map) {
		this.map = map;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the sortOrder
	 */
	public int getSortOrder() {
		return sortOrder;
	}

	/**
	 * @param sortOrder the sortOrder to set
	 */
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	/**
	 * @return the xOffset
	 */
	public int getxOffset() {
		return xOffset;
	}

	/**
	 * @param xOffset the xOffset to set
	 */
	public void setxOffset(int xOffset) {
		this.xOffset = xOffset;
	}

	/**
	 * @return the yOffset
	 */
	public int getyOffset() {
		return yOffset;
	}

	/**
	 * @param yOffset the yOffset to set
	 */
	public void setyOffset(int yOffset) {
		this.yOffset = yOffset;
	}

	public RavelryPhotoSet() {
		map = new HashMap<ProjectPhotoUrlSize, RavelryPhoto>();
	}
	
	public void addPhoto(ProjectPhotoUrlSize size, RavelryPhoto photo) {
		if(null != photo) {
			map.put(size, photo);
		}
	}
	
	public RavelryPhoto getPhoto(ProjectPhotoUrlSize size) {
		return map.get(size);
	}
	
}