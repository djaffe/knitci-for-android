package com.knitci.pocket.ravelry.data.object.impl;

import java.util.ArrayList;

import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryPaginator;

public class RavelryCollection extends RavelryObject {

	ArrayList<RavelryObject> collection;
	public int miscId;
	private RavelryPaginator paginator;
	
	public RavelryCollection() {
		collection = new ArrayList<RavelryObject>();
	}
	
	public void add(RavelryObject obj) {
		collection.add(obj);
	}
	
	public ArrayList<RavelryObject> getCollection() {
		return collection;
	}
	
	public boolean isAdditionalPagesNeeded() {
		if(null != paginator) {
			return paginator.isMorePagesAvail();
		}
		return false;
	}
	
	public void setPaginator(RavelryPaginator paginator) {
		this.paginator = paginator;
	}
	
	public RavelryPaginator getPaginator() {
		return this.paginator;
	}
}
