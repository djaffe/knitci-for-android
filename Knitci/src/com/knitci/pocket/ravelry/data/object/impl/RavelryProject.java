package com.knitci.pocket.ravelry.data.object.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryProject extends RavelryObject {

	private String projectName;
	private int photoCount;
	private int favoritesCount;
	private String patternName;
	private int userId;
	private Date updatedDtTm;
	private ProjectStatus projectStatus;
	private int projectId;
	private int progress;
	private ArrayList<String> tags;
	private String size;
	private int madeForUserId;
	private int craftId;
	private String craftName;
	private int patternId;
	private String statusName;
	private Date startedDtTm;
	private Date createdDtTm;
	private Date completedDtTm;
	private int commentsCount;
	private String madeForUsername;
	private Date projectStatusChangedDtTm;
	private float rating;
	private RavelryPhotoSet firstPhotos;
	private boolean completedDaySet;
	private String permalink;
	private boolean startedDaySet;
	private String username;
	
	
	private String notes;
	
	private boolean isFullyFilled;
	
	
	private ArrayList<RavelryPhotoSet> photos;
	private ArrayList<RavelryNeedleSize> needleSizes;
	private String notesHtml;
	private ArrayList<RavelryPack> packs;
	private ArrayList<String> comments;
	
	
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the packs
	 */
	public ArrayList<RavelryPack> getPacks() {
		return packs;
	}

	/**
	 * @param packs the packs to set
	 */
	public void setPacks(ArrayList<RavelryPack> packs) {
		this.packs = packs;
	}

	public void addPack(RavelryPack pack) {
		this.packs.add(pack);
	}
	
	public RavelryProject() {
		packs = new ArrayList<RavelryPack>();
		photos = new ArrayList<RavelryPhotoSet>();
		needleSizes = new ArrayList<RavelryNeedleSize>();
		comments = new ArrayList<String>();
		isFullyFilled = false;
	}
	
	public void addPhoto(RavelryPhotoSet set) {
		photos.add(set);
	}
	
	public void addNeedleSizes(RavelryNeedleSize needle) {
		needleSizes.add(needle);
	}
	
	public void addComment(String comment) {
		comments.add(comment);
	}
	
	/**
	 * @return the photos
	 */
	public ArrayList<RavelryPhotoSet> getPhotos() {
		return photos;
	}

	/**
	 * @param photos the photos to set
	 */
	public void setPhotos(ArrayList<RavelryPhotoSet> photos) {
		this.photos = photos;
	}

	/**
	 * @return the needleSizes
	 */
	public ArrayList<RavelryNeedleSize> getNeedleSizes() {
		return needleSizes;
	}

	/**
	 * @param needleSizes the needleSizes to set
	 */
	public void setNeedleSizes(ArrayList<RavelryNeedleSize> needleSizes) {
		this.needleSizes = needleSizes;
	}

	/**
	 * @return the notesHtml
	 */
	public String getNotesHtml() {
		return notesHtml;
	}

	/**
	 * @param notesHtml the notesHtml to set
	 */
	public void setNotesHtml(String notesHtml) {
		this.notesHtml = notesHtml;
	}

	/**
	 * @return the comments
	 */
	public ArrayList<String> getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(ArrayList<String> comments) {
		this.comments = comments;
	}

	/**
	 * @param isFullyFilled the isFullyFilled to set
	 */
	public void setFullyFilled(boolean isFullyFilled) {
		this.isFullyFilled = isFullyFilled;
	}

	public void setFullyFilled() {
		isFullyFilled = true;
	}
	
	public boolean isFullyFilled() {
		return isFullyFilled;
	}
	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public int getPhotoCount() {
		return photoCount;
	}

	public void setPhotoCount(int photoCount) {
		this.photoCount = photoCount;
	}

	public int getFavoritesCount() {
		return favoritesCount;
	}

	public void setFavoritesCount(int favoritesCount) {
		this.favoritesCount = favoritesCount;
	}

	public String getPatternName() {
		return patternName;
	}

	public void setPatternName(String patternName) {
		this.patternName = patternName;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Date getUpdatedDtTm() {
		return updatedDtTm;
	}

	public void setUpdatedDtTm(Date updatedDtTm) {
		this.updatedDtTm = updatedDtTm;
	}

	public ProjectStatus getProjectStatus() {
		return projectStatus;
	}

	public void setProjectStatus(ProjectStatus projectStatus) {
		this.projectStatus = projectStatus;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public ArrayList<String> getTags() {
		return tags;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public int getMadeForUserId() {
		return madeForUserId;
	}

	public void setMadeForUserId(int madeForUserId) {
		this.madeForUserId = madeForUserId;
	}

	public int getCraftId() {
		return craftId;
	}

	public void setCraftId(int craftId) {
		this.craftId = craftId;
	}

	public String getCraftName() {
		return craftName;
	}

	public void setCraftName(String craftName) {
		this.craftName = craftName;
	}

	public int getPatternId() {
		return patternId;
	}

	public void setPatternId(int patternId) {
		this.patternId = patternId;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public Date getStartedDtTm() {
		return startedDtTm;
	}

	public void setStartedDtTm(Date startedDtTm) {
		this.startedDtTm = startedDtTm;
	}

	public Date getCreatedDtTm() {
		return createdDtTm;
	}

	public void setCreatedDtTm(Date createdDtTm) {
		this.createdDtTm = createdDtTm;
	}

	public Date getCompletedDtTm() {
		return completedDtTm;
	}

	public void setCompletedDtTm(Date completedDtTm) {
		this.completedDtTm = completedDtTm;
	}

	public int getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(int commentsCount) {
		this.commentsCount = commentsCount;
	}

	public String getMadeForUsername() {
		return madeForUsername;
	}

	public void setMadeForUsername(String madeForUsername) {
		this.madeForUsername = madeForUsername;
	}

	public Date getProjectStatusChangedDtTm() {
		return projectStatusChangedDtTm;
	}

	public void setProjectStatusChangedDtTm(Date projectStatusChangedDtTm) {
		this.projectStatusChangedDtTm = projectStatusChangedDtTm;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public RavelryPhotoSet getFirstPhotos() {
		return firstPhotos;
	}

	public void setFirstPhotos(RavelryPhotoSet firstPhotos) {
		this.firstPhotos = firstPhotos;
	}

	public boolean isCompletedDaySet() {
		return completedDaySet;
	}

	public void setCompletedDaySet(boolean completedDaySet) {
		this.completedDaySet = completedDaySet;
	}

	public String getPermalink() {
		return permalink;
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}

	public boolean isStartedDaySet() {
		return startedDaySet;
	}

	public void setStartedDaySet(boolean startedDaySet) {
		this.startedDaySet = startedDaySet;
	}

	public static enum ProjectStatus {
		
		INPROGRESS(1, "In progress");
		
		private final int id;
		private final String ravName;
	    ProjectStatus(int id, String ravName) { this.id = id; this.ravName = ravName; }
	    public int getValue() { return id; }
	    public String getRavName() { return ravName; }
	    
	    public static ProjectStatus getStatus(String status) {
	    	return Cache.CACHE.get(status);
	    }
	    
	    public static ProjectStatus getStatus(int ravId) {
	    	return Cache.CACHE2.get(ravId);
	    }
	    
	    private static class Cache {
	    	private static Map<String, ProjectStatus> CACHE = new HashMap<String, ProjectStatus>();
	    	private static Map<Integer, ProjectStatus> CACHE2 = new HashMap<Integer, ProjectStatus>();
	    	static {
	    		for(final ProjectStatus p : ProjectStatus.values()) {
	    			CACHE.put(p.ravName, p);
	    			CACHE2.put(p.id, p);
	    		}
	    	}
	    }
	}
	
	public static enum ProjectPhotoUrlSize {
		SQUARE, MEDIUM, THUMBNAIL, SMALL, FLICKR, SHELVED
	}
}
