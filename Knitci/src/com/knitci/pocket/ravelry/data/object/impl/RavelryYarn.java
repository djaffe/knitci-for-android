package com.knitci.pocket.ravelry.data.object.impl;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryYarn extends RavelryObject {

	private String permalink;
	private int yarnCompanyId;
	private String name;
	private int id;
	private String yarnCompanyName;
	
	private float yardage;
	private int ratingCount;
	private float ratingAverage;
	private int wpi;
	private int gaugeDivisor;
	private int ratingTotal;
	private RavelryYarnCompany company;
	private RavelryYarnWeight weight;
	private String notesHtml;
	private boolean organic;
	private float minGauge;
	private float grams;
	private float maxGauge;
	private String texture;
	private boolean discontinued;
	private boolean machineWashable;
	private RavelryPhotoSet photos;
	private RavelryPhotoSet firstPhoto;
	
	/**
	 * @return the firstPhoto
	 */
	public RavelryPhotoSet getFirstPhoto() {
		return firstPhoto;
	}
	/**
	 * @param firstPhoto the firstPhoto to set
	 */
	public void setFirstPhoto(RavelryPhotoSet firstPhoto) {
		this.firstPhoto = firstPhoto;
	}
	/**
	 * @return the permalink
	 */
	public String getPermalink() {
		return permalink;
	}
	/**
	 * @param permalink the permalink to set
	 */
	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}
	/**
	 * @return the yarnCompanyId
	 */
	public int getYarnCompanyId() {
		return yarnCompanyId;
	}
	/**
	 * @param yarnCompanyId the yarnCompanyId to set
	 */
	public void setYarnCompanyId(int yarnCompanyId) {
		this.yarnCompanyId = yarnCompanyId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the yarnCompanyName
	 */
	public String getYarnCompanyName() {
		return yarnCompanyName;
	}
	/**
	 * @param yarnCompanyName the yarnCompanyName to set
	 */
	public void setYarnCompanyName(String yarnCompanyName) {
		this.yarnCompanyName = yarnCompanyName;
	}
	/**
	 * @return the yardage
	 */
	public float getYardage() {
		return yardage;
	}
	/**
	 * @param yardage the yardage to set
	 */
	public void setYardage(float yardage) {
		this.yardage = yardage;
	}
	/**
	 * @return the ratingCount
	 */
	public int getRatingCount() {
		return ratingCount;
	}
	/**
	 * @param ratingCount the ratingCount to set
	 */
	public void setRatingCount(int ratingCount) {
		this.ratingCount = ratingCount;
	}
	/**
	 * @return the ratingAverage
	 */
	public float getRatingAverage() {
		return ratingAverage;
	}
	/**
	 * @param ratingAverage the ratingAverage to set
	 */
	public void setRatingAverage(float ratingAverage) {
		this.ratingAverage = ratingAverage;
	}
	/**
	 * @return the wpi
	 */
	public int getWpi() {
		return wpi;
	}
	/**
	 * @param wpi the wpi to set
	 */
	public void setWpi(int wpi) {
		this.wpi = wpi;
	}
	/**
	 * @return the gaugeDivisor
	 */
	public int getGaugeDivisor() {
		return gaugeDivisor;
	}
	/**
	 * @param gaugeDivisor the gaugeDivisor to set
	 */
	public void setGaugeDivisor(int gaugeDivisor) {
		this.gaugeDivisor = gaugeDivisor;
	}
	/**
	 * @return the ratingTotal
	 */
	public int getRatingTotal() {
		return ratingTotal;
	}
	/**
	 * @param ratingTotal the ratingTotal to set
	 */
	public void setRatingTotal(int ratingTotal) {
		this.ratingTotal = ratingTotal;
	}
	/**
	 * @return the company
	 */
	public RavelryYarnCompany getCompany() {
		return company;
	}
	/**
	 * @param company the company to set
	 */
	public void setCompany(RavelryYarnCompany company) {
		this.company = company;
	}
	/**
	 * @return the weight
	 */
	public RavelryYarnWeight getWeight() {
		return weight;
	}
	/**
	 * @param weight the weight to set
	 */
	public void setWeight(RavelryYarnWeight weight) {
		this.weight = weight;
	}
	/**
	 * @return the notesHtml
	 */
	public String getNotesHtml() {
		return notesHtml;
	}
	/**
	 * @param notesHtml the notesHtml to set
	 */
	public void setNotesHtml(String notesHtml) {
		this.notesHtml = notesHtml;
	}
	/**
	 * @return the organic
	 */
	public boolean isOrganic() {
		return organic;
	}
	/**
	 * @param organic the organic to set
	 */
	public void setOrganic(boolean organic) {
		this.organic = organic;
	}
	/**
	 * @return the minGauge
	 */
	public float getMinGauge() {
		return minGauge;
	}
	/**
	 * @param minGauge the minGauge to set
	 */
	public void setMinGauge(float minGauge) {
		this.minGauge = minGauge;
	}
	/**
	 * @return the grams
	 */
	public float getGrams() {
		return grams;
	}
	/**
	 * @param grams the grams to set
	 */
	public void setGrams(float grams) {
		this.grams = grams;
	}
	/**
	 * @return the maxGauge
	 */
	public float getMaxGauge() {
		return maxGauge;
	}
	/**
	 * @param maxGauge the maxGauge to set
	 */
	public void setMaxGauge(float maxGauge) {
		this.maxGauge = maxGauge;
	}
	/**
	 * @return the texture
	 */
	public String getTexture() {
		return texture;
	}
	/**
	 * @param texture the texture to set
	 */
	public void setTexture(String texture) {
		this.texture = texture;
	}
	/**
	 * @return the discontinued
	 */
	public boolean isDiscontinued() {
		return discontinued;
	}
	/**
	 * @param discontinued the discontinued to set
	 */
	public void setDiscontinued(boolean discontinued) {
		this.discontinued = discontinued;
	}
	/**
	 * @return the machineWashable
	 */
	public boolean isMachineWashable() {
		return machineWashable;
	}
	/**
	 * @param machineWashable the machineWashable to set
	 */
	public void setMachineWashable(boolean machineWashable) {
		this.machineWashable = machineWashable;
	}
	/**
	 * @return the photos
	 */
	public RavelryPhotoSet getPhotos() {
		return photos;
	}
	/**
	 * @param photos the photos to set
	 */
	public void setPhotos(RavelryPhotoSet photos) {
		this.photos = photos;
	}
	
	
	
}
