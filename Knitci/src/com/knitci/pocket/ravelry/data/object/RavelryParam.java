package com.knitci.pocket.ravelry.data.object;

import java.util.ArrayList;
import java.util.Iterator;

public class RavelryParam {

	final private String key;
	private ArrayList<String> values;
	private String delimiter;
	
	public RavelryParam(String key) {
		values = new ArrayList<String>();
		this.key = key;
	}
	
	public String getKey() {
		return key;
	}
	
	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}
	
	public String getDelimiter() {
		return delimiter;
	}
	
	public void addValue(String value) {
		values.add(value);
	}
	
	public synchronized void addValues(ArrayList<String> values) {
		Iterator<String> it = values.iterator();
		while(it.hasNext()) {
			this.values.add(it.next());
		}
	}
	
	public Iterator<String> iterator() {
		return values.iterator();
	}
}
