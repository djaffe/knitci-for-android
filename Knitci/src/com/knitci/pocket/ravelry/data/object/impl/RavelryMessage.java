package com.knitci.pocket.ravelry.data.object.impl;

import java.util.Date;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryMessage extends RavelryObject {

	private String messageType;
	private Date messageDtTm;
	private int messageId;
	private RavelryUser recipient;
	private RavelryUser sender;
	private String subject;
	private boolean messageRead;
	private boolean isBodyQueried;
	private String htmlContent;
	
	public RavelryMessage() {
		htmlContent = "Not yet loaded";
		isBodyQueried = false;
	}
	
	public RavelryUser getUserToDisplay() {
		if(messageType.equals("simple-message")) {
			return recipient;
		}
		return sender;
	}
	
	
	
	/**
	 * @return the htmlContent
	 */
	public String getHtmlContent() {
		return htmlContent;
	}

	/**
	 * @param htmlContent the htmlContent to set
	 */
	public void setHtmlContent(String htmlContent) {
		this.htmlContent = htmlContent;
	}

	/**
	 * @return the isBodyQueried
	 */
	public boolean isBodyQueried() {
		return isBodyQueried;
	}
	/**
	 * @param isBodyQueried the isBodyQueried to set
	 */
	public void setBodyQueried(boolean isBodyQueried) {
		this.isBodyQueried = isBodyQueried;
	}
	/**
	 * @return the messageType
	 */
	public String getMessageType() {
		return messageType;
	}
	/**
	 * @param messageType the messageType to set
	 */
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	/**
	 * @return the messageDtTm
	 */
	public Date getMessageDtTm() {
		return messageDtTm;
	}
	/**
	 * @param messageDtTm the messageDtTm to set
	 */
	public void setMessageDtTm(Date messageDtTm) {
		this.messageDtTm = messageDtTm;
	}
	/**
	 * @return the messageId
	 */
	public int getMessageId() {
		return messageId;
	}
	/**
	 * @param messageId the messageId to set
	 */
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	/**
	 * @return the recipient
	 */
	public RavelryUser getRecipient() {
		return recipient;
	}
	/**
	 * @param recipient the recipient to set
	 */
	public void setRecipient(RavelryUser recipient) {
		this.recipient = recipient;
	}
	/**
	 * @return the sender
	 */
	public RavelryUser getSender() {
		return sender;
	}
	/**
	 * @param sender the sender to set
	 */
	public void setSender(RavelryUser sender) {
		this.sender = sender;
	}
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @return the messageRead
	 */
	public boolean isMessageRead() {
		return messageRead;
	}
	/**
	 * @param messageRead the messageRead to set
	 */
	public void setMessageRead(boolean messageRead) {
		this.messageRead = messageRead;
	}
	
	
	
}
