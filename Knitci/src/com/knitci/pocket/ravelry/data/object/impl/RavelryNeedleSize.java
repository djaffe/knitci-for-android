package com.knitci.pocket.ravelry.data.object.impl;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryNeedleSize extends RavelryObject {

	private String hook;
	private boolean crochet;
	private String usSteel;
	private boolean knitting;
	private String name;
	private float metric;
	private String us;
	private int id;
	/**
	 * @return the hook
	 */
	public String getHook() {
		return hook;
	}
	/**
	 * @param hook the hook to set
	 */
	public void setHook(String hook) {
		this.hook = hook;
	}
	/**
	 * @return the crochet
	 */
	public boolean isCrochet() {
		return crochet;
	}
	/**
	 * @param crochet the crochet to set
	 */
	public void setCrochet(boolean crochet) {
		this.crochet = crochet;
	}
	/**
	 * @return the usSteel
	 */
	public String isUsSteel() {
		return usSteel;
	}
	/**
	 * @param string the usSteel to set
	 */
	public void setUsSteel(String string) {
		this.usSteel = string;
	}
	/**
	 * @return the knitting
	 */
	public boolean isKnitting() {
		return knitting;
	}
	/**
	 * @param knitting the knitting to set
	 */
	public void setKnitting(boolean knitting) {
		this.knitting = knitting;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the metric
	 */
	public float getMetric() {
		return metric;
	}
	/**
	 * @param metric the metric to set
	 */
	public void setMetric(float metric) {
		this.metric = metric;
	}
	/**
	 * @return the us
	 */
	public String getUs() {
		return us;
	}
	/**
	 * @param us the us to set
	 */
	public void setUs(String us) {
		this.us = us;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	
	
}
