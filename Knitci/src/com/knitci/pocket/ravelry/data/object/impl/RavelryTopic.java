package com.knitci.pocket.ravelry.data.object.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryTopic extends RavelryObject {

	private ArrayList<RavelryForumPost> posts;
	private HashMap<Integer, RavelryForumPost> postMap;
	
	private String title;
	private Date createdDtTm;
	private Date repliedDtTm;
	private int forumImagesCount;
	private boolean archived;
	private int id;
	private int forumId;
	private boolean ignored;
	private boolean sticky;
	private int latestReply;
	private int lastRead;
	private int forumPostsCount;
	private boolean watched;
	private boolean locked;
	
	
	
	/**
	 * @return the name
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param name the name to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the createdDtTm
	 */
	public Date getCreatedDtTm() {
		return createdDtTm;
	}

	/**
	 * @param createdDtTm the createdDtTm to set
	 */
	public void setCreatedDtTm(Date createdDtTm) {
		this.createdDtTm = createdDtTm;
	}

	/**
	 * @return the repliedDtTm
	 */
	public Date getRepliedDtTm() {
		return repliedDtTm;
	}

	/**
	 * @param repliedDtTm the repliedDtTm to set
	 */
	public void setRepliedDtTm(Date repliedDtTm) {
		this.repliedDtTm = repliedDtTm;
	}

	/**
	 * @return the forumImagesCount
	 */
	public int getForumImagesCount() {
		return forumImagesCount;
	}

	/**
	 * @param forumImagesCount the forumImagesCount to set
	 */
	public void setForumImagesCount(int forumImagesCount) {
		this.forumImagesCount = forumImagesCount;
	}

	/**
	 * @return the archived
	 */
	public boolean isArchived() {
		return archived;
	}

	/**
	 * @param archived the archived to set
	 */
	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the forumId
	 */
	public int getForumId() {
		return forumId;
	}

	/**
	 * @param forumId the forumId to set
	 */
	public void setForumId(int forumId) {
		this.forumId = forumId;
	}

	/**
	 * @return the ignored
	 */
	public boolean isIgnored() {
		return ignored;
	}

	/**
	 * @param ignored the ignored to set
	 */
	public void setIgnored(boolean ignored) {
		this.ignored = ignored;
	}

	/**
	 * @return the sticky
	 */
	public boolean isSticky() {
		return sticky;
	}

	/**
	 * @param sticky the sticky to set
	 */
	public void setSticky(boolean sticky) {
		this.sticky = sticky;
	}

	/**
	 * @return the latestReply
	 */
	public int getLatestReply() {
		return latestReply;
	}

	/**
	 * @param latestReply the latestReply to set
	 */
	public void setLatestReply(int latestReply) {
		this.latestReply = latestReply;
	}

	/**
	 * @return the lastRead
	 */
	public int getLastRead() {
		return lastRead;
	}

	/**
	 * @param lastRead the lastRead to set
	 */
	public void setLastRead(int lastRead) {
		this.lastRead = lastRead;
	}

	/**
	 * @return the forumPostsCount
	 */
	public int getForumPostsCount() {
		return forumPostsCount;
	}

	/**
	 * @param forumPostsCount the forumPostsCount to set
	 */
	public void setForumPostsCount(int forumPostsCount) {
		this.forumPostsCount = forumPostsCount;
	}

	/**
	 * @return the watched
	 */
	public boolean isWatched() {
		return watched;
	}

	/**
	 * @param watched the watched to set
	 */
	public void setWatched(boolean watched) {
		this.watched = watched;
	}

	/**
	 * @return the locked
	 */
	public boolean isLocked() {
		return locked;
	}

	/**
	 * @param locked the locked to set
	 */
	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public RavelryTopic() {
		posts = new ArrayList<RavelryForumPost>();
		postMap = new HashMap<Integer, RavelryForumPost>();
	}
	
	public void addPost(RavelryForumPost post) {
		posts.add(post);
		postMap.put(post.getId(), post);
	}
}
