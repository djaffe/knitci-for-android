package com.knitci.pocket.ravelry.data.object.impl;

import java.io.Serializable;

import com.knitci.pocket.ravelry.data.object.RavelryObject;

public class RavelryPatternAuthor extends RavelryObject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 925480698020666698L;
	private int favoritesCount;
	private int patternsCount;
	private int id;
	private String name;
	private String permalink;
	/**
	 * @return the favoritesCount
	 */
	public int getFavoritesCount() {
		return favoritesCount;
	}
	/**
	 * @param favoritesCount the favoritesCount to set
	 */
	public void setFavoritesCount(int favoritesCount) {
		this.favoritesCount = favoritesCount;
	}
	/**
	 * @return the patternsCount
	 */
	public int getPatternsCount() {
		return patternsCount;
	}
	/**
	 * @param patternsCount the patternsCount to set
	 */
	public void setPatternsCount(int patternsCount) {
		this.patternsCount = patternsCount;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the permalink
	 */
	public String getPermalink() {
		return permalink;
	}
	/**
	 * @param permalink the permalink to set
	 */
	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}
	
	
	
}
