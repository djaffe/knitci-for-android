package com.knitci.pocket.ravelry;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.scribe.model.Token;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.patterns.PatternSearchCriteria;
import com.knitci.pocket.ravelry.data.access.RavelryAsyncTask;
import com.knitci.pocket.ravelry.data.access.RavelryAsyncTask.ERavelryTask;
import com.knitci.pocket.ravelry.data.access.RavelryAsyncTaskCriteria;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.RavelryPaginator;
import com.knitci.pocket.ravelry.data.object.RavelryParam;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryBookmark;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.ravelry.data.object.impl.RavelryForum;
import com.knitci.pocket.ravelry.data.object.impl.RavelryForumPost;
import com.knitci.pocket.ravelry.data.object.impl.RavelryMessage;
import com.knitci.pocket.ravelry.data.object.impl.RavelryNeedleRecord;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPattern;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryQueuedProject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryStashGeneric;
import com.knitci.pocket.ravelry.data.object.impl.RavelryTopic;
import com.knitci.pocket.ravelry.data.object.impl.RavelryUser;
import com.knitci.pocket.tools.needle.sizes.NeedleSize;
import com.knitci.pocket.tools.needle.types.Needle;
import com.knitci.pocket.util.JsonUtil;
import com.knitci.pocket.util.KnitciPreferences;

/**
 * Manager for all-things Ravelry related. Only one manager should be used for the
 * entire span of the application. 
 * @author DJ021930
 */
public class RavelryManager implements IRavelryAsync {

	KnitciApplication app;
	Token token;
	private IRavelryAsync listener;
	public ArrayList<RavelryAsyncTask> threads;
	private RavelryUser ravelryUser;

	/**
	 * Constructor. 
	 */
	public RavelryManager(Context context) {
		threads = new ArrayList<RavelryAsyncTask>();
		token = null;
		app = KnitciApplication.class.cast(context);
	}

	/**
	 * Sets the current listener. When an activity is started, it should set itself as the new
	 * listener so that it can receive any tasks that are finished. 
	 * @param listener {@link IRavelryAsync} A listener to receive {@link RavelryAsyncTask} calls. 
	 */
	public void setListener(IRavelryAsync listener) {
		this.listener = listener;
	}

	/**
	 * Gets the {@link Token} being used for Scribe OAuth. 
	 * @return {@link Token} The token. Could be null. 
	 */
	public Token getToken() {
		return token;
	}

	/**
	 * Takes a token and saves it to the preferences and sets it into context. 
	 * @param token {@link Token} A filled out token. 
	 * @param context {@link Context} Context to be used for accessing the preferences. 
	 */
	public void setAndSaveToken(Token token, Context context) {
		KnitciPreferences.saveToken(token.getToken(), token.getSecret(), context);
		this.token = token;
	}

	/**
	 * Attempts to retrieve an existing token into context. 
	 * @param context {@link Context} Context to be used for accessing the preferences. 
	 */
	public void loadExistingToken(Context context) {
		this.token = KnitciPreferences.getToken(context);
	}

	/**
	 * Checks to see if a {@link Token} is set and non-null. 
	 * @return boolean True or false if a token is available. 
	 */
	public boolean isTokenPresent() {
		if(null == token) {
			return false;
		}
		return true;
	}

	/**
	 * Call Ravelry to check if the current Token is valid. The USERINFO (currentUser) API call
	 * is used to check if anything returns. If something returns, we assume this means the token 
	 * is valid. A task is started and posted back with the response. That response should be used
	 * to determine if the user is logged in.  
	 */
	public void verifyToken() {
		retrieveCurrentUser();
	}

	/**
	 * Starts a {@link RavelryAsyncTask} to retrieve the current user from Ravelry.
	 */
	public void retrieveCurrentUser() {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.USERINFO;
		criteria.context = app;
		kickOffRavelryTask(criteria);
	}

	/**
	 * Retrieve projects for the current user. Does not kick off a task if username
	 * is null or empty as that means the user is not logged in. 
	 * @see {@link RavelryManager#retrieveUserProjects(String)}
	 */
	public void retrieveUserProjects(int page) {
		if(getRavelryUser().getUsername() != null && !getRavelryUser().getUsername().isEmpty()) {
			retrieveUserProjects(getRavelryUser().getUsername(), page);
		}
	}

	/**
	 * Starts a {@link RavelryAsyncTask} to retrieve a given user's projects from Ravelry. 
	 * @param username
	 */
	public void retrieveUserProjects(String username, int page) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.SPECIFICUSERPROJECTS;
		criteria.username = username;

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();
		RavelryParam pageParam = new RavelryParam("page");
		pageParam.addValue(String.valueOf(page));
		params.add(pageParam);

		RavelryParam pageSizeParam = new RavelryParam("page_size");
		pageSizeParam.addValue(String.valueOf(100));
		params.add(pageSizeParam);

		criteria.params = params;		
		kickOffRavelryTask(criteria);
	}

	/**
	 * Starts a {@link RavelryAsyncTask} to retrieve Ravelry supported color families. 
	 */
	public void retrieveColorFamilies() {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.COLORFAMILIES;
		kickOffRavelryTask(criteria);
	}

	public void retrieveYarnWeights() {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.YARNWEIGHTS;
		kickOffRavelryTask(criteria);
	}

	public void retrieveYarn(int yarnId) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.YARN;
		criteria.miscId = yarnId;
		kickOffRavelryTask(criteria);
	}

	public void retrieveYarns(String query, int page, int pageSize, String sort) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.taskType = ERavelryTask.YARNS;
		criteria.token = this.token;

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();

		RavelryParam queryParam = new RavelryParam("query");
		queryParam.addValue(URLEncoder.encode(query));
		params.add(queryParam);

		RavelryParam sortParam = new RavelryParam("sort");
		sortParam.addValue(sort);
		params.add(sortParam);

		RavelryParam pageParam = new RavelryParam("page");
		pageParam.addValue(String.valueOf(page));
		params.add(pageParam);

		RavelryParam pageSizeParam = new RavelryParam("page_size");
		pageSizeParam.addValue(String.valueOf(pageSize));
		params.add(pageSizeParam);

		criteria.params = params;

		kickOffRavelryTask(criteria);
	}

	public void retrieveForumTopicsCont(int page, int forumId) {
		retrieveForumTopics(page, forumId, true);
	}

	public void retrieveForumTopics(int page, int forumId) {
		retrieveForumTopics(page, forumId, false);
	}

	private void retrieveForumTopics(int page, int forumId, boolean continued) {
		ERavelryTask task;

		if(continued) {
			task = ERavelryTask.FORUMTOPICSCONT;
		}
		else {
			task = ERavelryTask.FORUMTOPICS;
		}

		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = task;
		criteria.miscId = forumId;

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();

		RavelryParam pageParam = new RavelryParam("page");
		pageParam.addValue(String.valueOf(page));
		params.add(pageParam);

		criteria.params = params;
		kickOffRavelryTask(criteria);
	}

	public void retrieveForumTopics(RavelryCollection forums) {
		ArrayList<RavelryObject> collection = forums.getCollection();
		synchronized (collection) {
			Iterator it = collection.iterator();
			while(it.hasNext()) {
				RavelryForum forum = (RavelryForum) it.next();
				if(forum != null) {
					retrieveForumTopics(1, forum.getId());
				}
			}
		}
	}

	public void retrieveFriends(String username) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.username = username;
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.FRIENDS;
		kickOffRavelryTask(criteria);
	}

	public void retrieveLibrary(String username, String query, String type, String sort, int page, int pageSize) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.username = username;
		criteria.taskType = ERavelryTask.LIBRARY;
		criteria.token = this.token;

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();

		RavelryParam queryParam = new RavelryParam("query");
		queryParam.addValue(query);
		params.add(queryParam);

		RavelryParam typeParam = new RavelryParam("type");
		typeParam.addValue(type);
		params.add(typeParam);

		RavelryParam sortParam = new RavelryParam("sort");
		sortParam.addValue(sort);
		params.add(sortParam);

		RavelryParam pageParam = new RavelryParam("page");
		pageParam.addValue(String.valueOf(page));
		params.add(pageParam);

		RavelryParam pageSizeParam = new RavelryParam("page_size");
		pageSizeParam.addValue(String.valueOf(pageSize));
		params.add(pageSizeParam);

		criteria.params = params;

		kickOffRavelryTask(criteria);
	}

	public void retrieveCompleteMessages(ArrayList<Integer> messageIds) {
		Iterator<Integer> it = messageIds.iterator();
		while(it.hasNext()) {
			int messageId = it.next();
			retrieveMessage(messageId);
		}
	}

	public void retrieveMessage(int messageId) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.miscId = messageId;
		criteria.taskType = ERavelryTask.MESSAGE;
		kickOffRavelryTask(criteria);
	}

	public void retrieveMessages(String folder, int page, String search) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();

		RavelryParam folderParam = new RavelryParam("folder");
		folderParam.addValue(folder);
		params.add(folderParam);
		criteria.messageBox = folder;

		RavelryParam pageParam = new RavelryParam("page");
		pageParam.addValue(String.valueOf(page));
		params.add(pageParam);

		if(search != null && !search.isEmpty()) {
			RavelryParam searchParam = new RavelryParam("search");
			searchParam.addValue(search);
			params.add(searchParam);
		}

		criteria.params = params;
		criteria.taskType = ERavelryTask.MESSAGES;
		criteria.context = app;

		kickOffRavelryTask(criteria);
	}

	/**
	 * This will kick off retrieving needle sizes and types. Directly kicks off retrieving the
	 * sizes. When sizes comes back and processes, {@link RavelryManager} will kick off
	 * retrieving the needle types. 
	 * @see RavelryManager#retrieveNeedleSizes
	 */
	public void retrieveNeedleReference() {
		retrieveNeedleSizes(null);
	}

	public void retrieveNeedleTypes() {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.NEEDLETYPES;
		kickOffRavelryTask(criteria);
	}

	public void retrieveNeedleSizes(String craft) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.NEEDLESIZES;

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();
		RavelryParam craftParam = new RavelryParam("craft");

		if(null == craft || craft.isEmpty()) {
			// Do nothing to default to blank
		}
		else {
			craftParam.addValue(craft);
			params.add(craftParam);
		}

		criteria.params = params;
		kickOffRavelryTask(criteria);
	}

	public void retrieveNeedleRecords(String username) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.NEEDLERECORDS;
		criteria.username = username;
		kickOffRavelryTask(criteria);
	}

	public void retrievePack(int packId) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.PACK;
		criteria.miscId = packId;
		kickOffRavelryTask(criteria);
	}

	public void retrieveProject(String username, int projectId) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.PROJECT;
		criteria.username = username;
		criteria.miscId = projectId;
		kickOffRavelryTask(criteria);
	}

	public void retrieveProject(String username, String permalink) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.PROJECT;
		criteria.username = username;
		criteria.permalink = permalink;
		kickOffRavelryTask(criteria);
	}

	public void retrieveProjectsByPattern(int patternId, String query, String sort, boolean photoless, int page) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.PROJECTSBYPATTERN;
		criteria.miscId = patternId;

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();

		if(query != null && !query.isEmpty()) {
			RavelryParam qParam = new RavelryParam("query");
			qParam.addValue(query);
			params.add(qParam);
		}
		if(sort != null && !sort.isEmpty()) {
			RavelryParam sParam = new RavelryParam("sort");
			sParam.addValue(sort);
			params.add(sParam);
		}
		if(photoless) {
			RavelryParam pParam = new RavelryParam("photoless");
			pParam.addValue("1");
			params.add(pParam);
		}
		if(page < 1) {
			page = 1;
		}
		RavelryParam pageParam = new RavelryParam("page");
		pageParam.addValue(String.valueOf(page));
		params.add(pageParam);

		criteria.params = params;
		kickOffRavelryTask(criteria);
	}

	public void retrievePattern(int patternid) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.PATTERN;
		criteria.miscId = patternid;
		kickOffRavelryTask(criteria);
	}

	public void retrievePattern(String permalink) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.PATTERN;
		criteria.permalink = permalink;
		kickOffRavelryTask(criteria);
	}

	public void retrievePatterns(PatternSearchCriteria searchCriteria, int page, int pageSize) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.PATTERNS;
		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();

		RavelryParam add = new RavelryParam("personal_attributes");
		add.addValue("1");
		params.add(add);

		// Turn criteria into params
		// Search query
		if(searchCriteria.getQuery() != null && !searchCriteria.getQuery().isEmpty()) {
			RavelryParam queryParam = new RavelryParam("query");
			queryParam.addValue(URLEncoder.encode(searchCriteria.getQuery()));
			params.add(queryParam);
		}

		// Sorting
		RavelryParam sortParam = new RavelryParam("sort");
		sortParam.addValue(searchCriteria.getSorting());
		params.add(sortParam);

		// Only photos
		if(searchCriteria.getOnlyPhotos() != null && searchCriteria.getOnlyPhotos().equals("yes")) {
			RavelryParam onlyPhotoParam = new RavelryParam("photo");
			onlyPhotoParam.addValue("yes");
			params.add(onlyPhotoParam);
		}

		// Crafting
		if(!searchCriteria.getCrafts().isEmpty()) {
			RavelryParam craftParam = new RavelryParam("craft");
			craftParam.addValues(searchCriteria.getCrafts());
			craftParam.setDelimiter("%7C");
			params.add(craftParam);
		}

		// Categories
		if(!searchCriteria.getCategories().isEmpty()) {
			RavelryParam catParam = new RavelryParam("pc");
			catParam.addValues(searchCriteria.getCategories());
			catParam.setDelimiter("%7C");
			params.add(catParam);
		}

		// Attributes
		if(!searchCriteria.getAttributes().isEmpty()) {
			RavelryParam attParam = new RavelryParam("pa");
			attParam.addValues(searchCriteria.getAttributes());
			attParam.setDelimiter("%7C");
			params.add(attParam);
		}

		// Availability
		if(!searchCriteria.getAvailability().isEmpty()) {
			RavelryParam avaParam = new RavelryParam("availability");
			avaParam.addValues(searchCriteria.getAvailability());
			avaParam.setDelimiter("%7C");
			params.add(avaParam);
		}

		// Fits
		if(!searchCriteria.getFits().isEmpty()) {
			RavelryParam fitParam = new RavelryParam("fit");
			fitParam.addValues(searchCriteria.getFits());
			fitParam.setDelimiter("%7C");
			params.add(fitParam);
		}

		// Yarn Weights
		if(!searchCriteria.getWeight().isEmpty()) {
			RavelryParam weightParam = new RavelryParam("weight");
			weightParam.addValues(searchCriteria.getWeight());
			weightParam.setDelimiter("%7C");
			params.add(weightParam);
		}

		// Yardage / Meterage
		if(searchCriteria.getYardageInt() != null && !searchCriteria.getYardageInt().isEmpty()) {
			RavelryParam yardageParam = new RavelryParam("yardage");
			yardageParam.addValue(searchCriteria.getYardageInt());
			params.add(yardageParam);
		}
		else if(searchCriteria.getMeterageIn() != null && !searchCriteria.getMeterageIn().isEmpty()) {
			RavelryParam meterageParam = new RavelryParam("meterage");
			meterageParam.addValue(searchCriteria.getMeterageIn());
			params.add(meterageParam);
		}

		// Notebook
		if(!searchCriteria.getmyNotebook().isEmpty()) {
			RavelryParam noteParam = new RavelryParam("notebook-p");
			noteParam.addValues(searchCriteria.getmyNotebook());
			noteParam.setDelimiter("%7C");
			params.add(noteParam);
		}

		// Sources
		if(!searchCriteria.getSources().isEmpty()) {
			RavelryParam sourceParam = new RavelryParam("source-type");
			sourceParam.addValues(searchCriteria.getSources());
			sourceParam.setDelimiter("%7C");
			params.add(sourceParam);
		}

		// Fibers
		if(!searchCriteria.getFibers().isEmpty()) {
			RavelryParam fiberParam = new RavelryParam("fibertype");
			fiberParam.addValues(searchCriteria.getFibers());
			fiberParam.setDelimiter("%7C");
			params.add(fiberParam);
		}

		// Hooks
		if(!searchCriteria.getHookSizes().isEmpty()) {
			RavelryParam hookParam = new RavelryParam("hooks");
			hookParam.addValues(searchCriteria.getHookSizes());
			hookParam.setDelimiter("%7C");
			params.add(hookParam);
		}

		// Needles
		if(!searchCriteria.getNeedleSizes().isEmpty()) {
			RavelryParam needleParam = new RavelryParam("needles");
			needleParam.addValues(searchCriteria.getNeedleSizes());
			needleParam.setDelimiter("%7C");
			params.add(needleParam);
		}

		// Rating
		if(searchCriteria.getRating() != null && !searchCriteria.getRating().isEmpty() && !searchCriteria.getRating().equals("0")) {
			RavelryParam ratingParam = new RavelryParam("ratings");
			ratingParam.addValue(searchCriteria.getRating());
			params.add(ratingParam);
		}

		// Difficulty
		if(searchCriteria.getDifficulty() != null && !searchCriteria.getDifficulty().isEmpty()) {
			RavelryParam diffParam = new RavelryParam("difficulties");
			diffParam.addValue(searchCriteria.getDifficulty());
			params.add(diffParam);
		}

		// Language
		if(!searchCriteria.getLanguages().isEmpty()) {
			RavelryParam langParam = new RavelryParam("language");
			langParam.setDelimiter("%7C");
			langParam.addValues(searchCriteria.getLanguages());
			params.add(langParam);
		}
		// Designer Name
		if(searchCriteria.getDesignerName() != null && !searchCriteria.getDesignerName().isEmpty()) {
			RavelryParam designerParam = new RavelryParam("designer");
			designerParam.addValue(URLEncoder.encode(searchCriteria.getDesignerName()));
			params.add(designerParam);
		}

		// Keywords
		if(searchCriteria.getKeywords() != null && !searchCriteria.getKeywords().isEmpty()) {
			RavelryParam keyParam = new RavelryParam("keywords");
			keyParam.addValue(URLEncoder.encode(searchCriteria.getKeywords()));
			params.add(keyParam);
		}

		RavelryParam pageParam = new RavelryParam("page");
		pageParam.addValue(String.valueOf(page));
		params.add(pageParam);

		RavelryParam pageSizeParam = new RavelryParam("page_size");
		pageSizeParam.addValue(String.valueOf(pageSize));
		params.add(pageSizeParam);

		criteria.params = params;

		kickOffRavelryTask(criteria);
	}

	public void retrievePatternsSimple(String query, int page, int pageSize) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.PATTERNSEXT;

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();
		if(null != query && !query.isEmpty()) {
			RavelryParam queryParam = new RavelryParam("query");
			queryParam.addValue(URLEncoder.encode(query));
			params.add(queryParam);
		}

		RavelryParam add = new RavelryParam("personal_attributes");
		add.addValue("1");
		params.add(add);

		criteria.params = params;

		kickOffRavelryTask(criteria);
	}

	public void retrievePatterns(String query, int page, int pageSize, String category, String sort) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.PATTERNS;

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();
		if(null != query && !query.isEmpty()) {
			RavelryParam queryParam = new RavelryParam("query");
			queryParam.addValue(URLEncoder.encode(query));
			params.add(queryParam);
		}

		RavelryParam add = new RavelryParam("personal_attributes");
		add.addValue("1");
		params.add(add);

		if(null != sort && !sort.isEmpty()) {
			RavelryParam sortParam = new RavelryParam("sort");
			sortParam.addValue(sort);
			params.add(sortParam);
		}


		//		if(null != category && !category.isEmpty()) {
		//			RavelryParam categoryParam = new RavelryParam("pc");
		//			categoryParam.addValue(category);
		//			params.add(categoryParam);
		//		}

		RavelryParam pageParam = new RavelryParam("page");
		pageParam.addValue(String.valueOf(page));
		params.add(pageParam);

		RavelryParam print = new RavelryParam("source-type");
		//		print.addValue("eBook");
		//		print.addValue("webzine");
		//		print.addValue("website");
		//		print.addValue("book");
		//		print.setDelimiter("%2B");
		print.addValue("website");
		params.add(print);

		RavelryParam ratings = new RavelryParam("ratings");
		ratings.addValue("5");
		params.add(ratings);

		RavelryParam pageSizeParam = new RavelryParam("page_size");
		pageSizeParam.addValue(String.valueOf(pageSize));
		params.add(pageSizeParam);

		criteria.params = params;

		kickOffRavelryTask(criteria);
	}

	public void retrieveUserProfile(int userId) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.USERPROFILE;
		criteria.miscId = userId;
		criteria.context = app; 
		kickOffRavelryTask(criteria);
	}

	public void retrievePhotoSizes(int photoId) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.PHOTOSIZES;
		criteria.miscId = photoId;
		kickOffRavelryTask(criteria);
	}

	public void retrieveQueueOrdering(String username) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.QUEUEORDERING;
		criteria.username = username;
		kickOffRavelryTask(criteria);
	}

	public void retrieveSpecificQueue(String username, int queueId) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.QUEUE;
		criteria.username = username;
		criteria.miscId = queueId;
		kickOffRavelryTask(criteria);
	}

	public void retrieveShops(String query, boolean localOnly, float latitude, float longatude, int radius, String units) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.FINDSHOPS;

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();

		if(null != query && !query.isEmpty()) {
			RavelryParam queryParam = new RavelryParam("query");
			queryParam.addValue(query);
			params.add(queryParam);
		}

		if(localOnly) {
			RavelryParam localParam = new RavelryParam("shop_type_id");
			localParam.addValue("1");
			params.add(localParam);
		}

		if(latitude != 999 && longatude != 999 && radius != -1 && !units.isEmpty() && (null != units))
		{
			RavelryParam latParam = new RavelryParam("lat");
			latParam.addValue(String.valueOf(latitude));
			params.add(latParam);


			RavelryParam longParam = new RavelryParam("lng");
			longParam.addValue(String.valueOf(longatude));
			params.add(longParam);



			RavelryParam radiusParam = new RavelryParam("radius");
			radiusParam.addValue(String.valueOf(radius));
			params.add(radiusParam);


			RavelryParam unitParam = new RavelryParam("units");
			unitParam.addValue(units);
			params.add(unitParam);
		}

		criteria.params = params;
		kickOffRavelryTask(criteria);
	}

	public void retrieveStash(String username, int page, int pageSize, String sort) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.username = username;
		criteria.taskType = ERavelryTask.STASH;

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();

		RavelryParam pageParam = new RavelryParam("page");
		pageParam.addValue("1");
		params.add(pageParam);

		RavelryParam pageSizeParam = new RavelryParam("page_size");
		pageSizeParam.addValue("100");
		params.add(pageSizeParam);

		RavelryParam sortParam = new RavelryParam("sort");
		sortParam.addValue(sort);
		params.add(sortParam);

		kickOffRavelryTask(criteria);
	}

	public void retrieveUnifiedStash(String username, int page, int pageSize, String sort) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.username = username;
		criteria.taskType = ERavelryTask.UNIFIEDSTASH;

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();

		RavelryParam pageParam = new RavelryParam("page");
		pageParam.addValue("1");
		params.add(pageParam);

		RavelryParam pageSizeParam = new RavelryParam("page_size");
		pageSizeParam.addValue("100");
		params.add(pageSizeParam);

		RavelryParam sortParam = new RavelryParam("sort");
		sortParam.addValue(sort);
		params.add(sortParam);

		kickOffRavelryTask(criteria);
	}

	public void retrieveTopic(int topicId) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.TOPIC;
		criteria.miscId = topicId;
		kickOffRavelryTask(criteria);
	}

	public void retrieveTopicPostsBackwards(int topicId, int page, boolean voteTotals, boolean userVotes, int forumId) {
		retrieveTopicPosts(topicId, page, voteTotals, userVotes, false, forumId, 0, true);
	}

	public void retrieveTopicPosts(int topicId, int page, boolean voteTotals, boolean userVotes, int forumId) {
		retrieveTopicPosts(topicId, page, voteTotals, userVotes, false, forumId, 0, false);
	}

	public void retrieveTopicPostsCont(int topicId, int page, boolean voteTotals, boolean userVotes, int forumId) {
		retrieveTopicPosts(topicId, page, voteTotals, userVotes, true, forumId, 0, false);
	}

	public void retrieveTopicPostsBatch(int topicId, int page, boolean voteTotals, boolean userVotes, int forumId, int upperbound) {
		retrieveTopicPosts(topicId, page, voteTotals, userVotes, false, forumId, upperbound, false);
	}

	private void retrieveTopicPosts(int topicId, int page, boolean voteTotals, boolean userVotes, boolean continued, 
			int forumId, int upperbound, boolean backwards) {
		Log.d("PostQuery", "Querying out for page " + page);
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.context = app;
		ERavelryTask task;
		if(continued) {
			task = ERavelryTask.TOPICPOSTSCONT;
		}
		else if(upperbound > 0) {
			task = ERavelryTask.TOPICPOSTSBATCH;
			criteria.upperbound = upperbound;
		}
		else if(backwards) {
			task = ERavelryTask.TOPICPOSTSBACKWARDS;
		}
		else {
			task = ERavelryTask.TOPICPOSTS;
		}
		criteria.miscId = topicId;
		criteria.taskType = task;
		criteria.forumId = forumId;
		criteria.topicId = topicId;


		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();

		RavelryParam pageParam = new RavelryParam("page");
		pageParam.addValue(String.valueOf(page));
		params.add(pageParam);

		if(voteTotals || userVotes) {
			RavelryParam incParam = new RavelryParam("include");
			if(voteTotals) {
				incParam.addValue("vote_totals");
			}
			if(userVotes) {
				incParam.addValue("user_votes");
			}
			params.add(incParam);
		}

		criteria.params = params;
		kickOffRavelryTask(criteria);
	}

	public void retrieveSingleStash(String username, int stashId) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.username = username;
		criteria.miscId = stashId;
		criteria.taskType = ERavelryTask.STASHSINGLE;
		kickOffRavelryTask(criteria);

	}

	public void retrieveShop(int shopId, ArrayList<String> includes) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.SHOP;
		criteria.miscId = shopId;
		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();

		if(includes.size() > 0) {
			RavelryParam incParam = new RavelryParam("include");
			incParam.setDelimiter("+");
			for(int i = 0; i < includes.size(); i++) {
				incParam.addValue(includes.get(i));
			}
			params.add(incParam);
		}

		criteria.params = params;
		kickOffRavelryTask(criteria);		
	}

	public void retrieveUserQueue(String username, int patternId, String query, int page) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.QUEUELIST;
		criteria.username = username;
		criteria.miscId = patternId;
		criteria.miscString = query;

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();

		if(patternId > 0) {
			RavelryParam patternParam = new RavelryParam("pattern_id");
			patternParam.addValue(String.valueOf(patternId));
			params.add(patternParam);
		}

		if(null != query && !query.isEmpty()) {
			RavelryParam queryParam = new RavelryParam("query");
			queryParam.addValue(String.valueOf(patternId));
			params.add(queryParam);
		}

		RavelryParam pageParam = new RavelryParam("page");
		pageParam.addValue(String.valueOf(page));
		params.add(pageParam);

		RavelryParam pageSizeParam = new RavelryParam("page_size");
		pageSizeParam.addValue("100");
		params.add(pageSizeParam);


		kickOffRavelryTask(criteria);
	}

	public void retrieveProjectCrafts() {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.CRAFTS;
		kickOffRavelryTask(criteria);
	}

	public void retrieveProjectStatuses() {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.PROJECTSTATUSES;
		kickOffRavelryTask(criteria);
	}


	public void retrievePhotoCreationStatus(String statusToken) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.PHOTOSTATUS;

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();
		RavelryParam statusParam = new RavelryParam("status_token");
		statusParam.addValue(statusToken);
		params.add(statusParam);

		criteria.params = params;		

		kickOffRavelryTask(criteria);
	}

	public void retrieveVolume(int volId) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.VOLUME;
		criteria.miscId = volId;
		kickOffRavelryTask(criteria);
	}

	public void retrieveUploadImageStatus(String statusToken) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.UPLOADSTATUS;

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();
		RavelryParam statusParam = new RavelryParam("upload_token");
		statusParam.addValue(statusToken);
		params.add(statusParam);

		criteria.params = params;		

		kickOffRavelryTask(criteria);
	}

	public void retrieveFavorites(String username, ArrayList<String> types, String query, boolean deepSearch, String tag) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.taskType = ERavelryTask.LISTFAVORITES;
		criteria.username = username;

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();

		RavelryParam typeParam = new RavelryParam("types");
		typeParam.setDelimiter("%20");
		if(types != null) {
			for(int i = 0; i < types.size(); i++) {
				typeParam.addValue(types.get(i));
			}
			params.add(typeParam);
		}

		RavelryParam deepParam = new RavelryParam("deep_search");
		deepParam.addValue(String.valueOf(deepSearch));
		params.add(deepParam);

		if(null != query && !query.isEmpty()) {
			RavelryParam queryParam = new RavelryParam("query");
			queryParam.addValue(query);
			params.add(queryParam);
		}
		else if(null != tag && !tag.isEmpty()) {
			RavelryParam tagParam = new RavelryParam("tag");
			tagParam.addValue(tag);
			params.add(tagParam);
		}

		RavelryParam pageParam = new RavelryParam("page");
		pageParam.addValue("1");
		params.add(pageParam);

		RavelryParam pageSizeParam = new RavelryParam("page_size");
		pageSizeParam.addValue("100");
		params.add(pageSizeParam);

		criteria.params = params;
		kickOffRavelryTask(criteria);
	}

	public void removeFavorite(int favoriteId) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.token = this.token;
		criteria.username = getRavelryUser().getUsername();
		criteria.taskType = ERavelryTask.REMOVEFAVORITE;
		criteria.miscId = favoriteId;
		kickOffRavelryTask(criteria);
	}

	public void retrieveForumPost(int forumPostId) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.miscId = forumPostId;
		criteria.taskType = ERavelryTask.FORUMPOST;
		criteria.token = this.token;
		kickOffRavelryTask(criteria);
	}

	public void retrieveForumSets() {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.taskType = ERavelryTask.FORUMSETS;
		criteria.token = this.token;
		kickOffRavelryTask(criteria);
	}

	public void submitForumPost(int topicId, String body) {
		submitForumPost(topicId, body, 0);
	}

	public void submitForumPost(int topicId, String body, int parentPostId) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.taskType = ERavelryTask.FORUMSUBMITPOST;
		criteria.token = this.token;
		criteria.topicId = topicId;

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();

		RavelryParam bodyParam = new RavelryParam("body");
		bodyParam.addValue(URLEncoder.encode(body));
		params.add(bodyParam);

		if(parentPostId > 0) {
			RavelryParam parentParam = new RavelryParam("parent_post_id");
			parentParam.addValue(String.valueOf(parentPostId));
			params.add(parentParam);
		}

		criteria.params = params;
		kickOffRavelryTask(criteria);
	}

	public void submitForumPostEdit(int forumId, int topicId, int forumPostId, String body) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.taskType = ERavelryTask.FORUMSUBMITPOSTEDIT;
		criteria.token = this.token;
		criteria.miscId = forumPostId;
		criteria.forumId = forumId;
		criteria.topicId = topicId;

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();
		RavelryParam bodyParam = new RavelryParam("body");
		bodyParam.addValue(URLEncoder.encode(body));
		params.add(bodyParam);

		criteria.params = params;
		kickOffRavelryTask(criteria);

	}

	public void submitMarkForumPostRead(int topicId, int postNumber, boolean force) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();

		ArrayList<RavelryParam> params = new ArrayList<RavelryParam>();

		RavelryParam postParam = new RavelryParam("last_read");
		postParam.addValue(String.valueOf(postNumber));
		params.add(postParam);

		RavelryParam forceParam = new RavelryParam("force");
		forceParam.addValue(force ? "1" : "0");
		params.add(forceParam);

		criteria.params = params;
		criteria.taskType = ERavelryTask.MARKTOPICREAD;
		criteria.topicId = topicId;
		criteria.token = this.token;

		kickOffRavelryTask(criteria);

	}

	public void submitCreateFavorite(String type, int id, String comment) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.taskType = ERavelryTask.FAVORITECREATE;
		criteria.token = this.token;
		criteria.username = getRavelryUser().getUsername();
		criteria.data = jsonEncodeFavorite(type, id, comment, null).toString();
		kickOffRavelryTask(criteria);
	}

	public void submitCreateQueue(String finishByDt, String makeFor, String notes, int patternId,
			String patternAuthor, String patternName, String patternUrl, String personalYarnName,
			int skeins, int sortPosition, String startOnDt, int yarnId, ArrayList<String> tags) {
		RavelryAsyncTaskCriteria criteria = new RavelryAsyncTaskCriteria();
		criteria.taskType = ERavelryTask.QUEUECREATE;
		criteria.token = this.token;
		criteria.username = getRavelryUser().getUsername();
		criteria.data = jsonEncodeQueue(finishByDt, makeFor, notes, patternId, patternAuthor, patternName,
				patternUrl, personalYarnName, skeins, sortPosition, startOnDt, yarnId, tags).toString();
		kickOffRavelryTask(criteria);
	}

	public void submitCreateQueueFromPattern(int patternId, int yarnId, String personalYarnName, 
			int skeins, ArrayList<String> tags, String makeFor, String finishByDt, String notes, int position) {
		submitCreateQueue(finishByDt, makeFor, notes, patternId, null, null, null, 
				personalYarnName, skeins, position, null, yarnId, tags);
	}

	/**
	 * Kicks off a {@link RavelryAsyncTask}. The criteria provided determines what kind of
	 * {@link RavelryAsyncTask} is created and ran. 
	 * @param criteria {@link RavelryAsyncTaskCriteria} Criteria to be used for creating the AsyncTask. 
	 */
	private void kickOffRavelryTask(RavelryAsyncTaskCriteria criteria) {
		RavelryAsyncTask task = new RavelryAsyncTask(this);
		addAsyncTask(task);
		task.execute(criteria);
	}

	/**
	 * Removes the listener from all active threads. This will prevent a rogue thread
	 * from posting a finish and possible re-spawning an activity. 
	 */
	public void disconnectFromAllThreads() {
		synchronized (threads) {
			for(RavelryAsyncTask task : threads) {
				task.listener = null;
			}
		}
	}

	/**
	 * Connects a listener to all currently active threads. 
	 * @param listener {@link IRavelryAsync} Listener to receive all posts from threads. 
	 */
	public void connectToAllThreads(IRavelryAsync listener) {
		synchronized (threads) {
			for(RavelryAsyncTask task : threads) {
				task.listener = listener;
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.knitci.pocket.ravelry.IRavelryAsync#processFinish(com.knitci.pocket.ravelry.data.object.RavelryResponse)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void processFinish(RavelryResponse response) {
		synchronized (threads) {
			for (Iterator<RavelryAsyncTask> it = threads.iterator(); it.hasNext();) {
				RavelryAsyncTask task = (RavelryAsyncTask)it.next();
				it.remove();
			}
		}

		if(response.result == null) {
			// TODO Some sort of parsing or translate failed. 
		}

		else {
			switch(response.task) {
			case USERINFO:
				setRavelryUser((RavelryUser) response.result);
				app.getMessagingManager().setCurrentUser();
				app.getPersonManager().addUser((RavelryUser) response.result);
				break;
			case PROJECT:
				RavelryProject fullProj = (RavelryProject) response.result;
				app.getProjectManager().addProject(fullProj);
				break;
			case QUEUELIST:
				RavelryCollection qColl = (RavelryCollection) response.result;
				ArrayList<RavelryQueuedProject> queues = (ArrayList<RavelryQueuedProject>)(ArrayList<?>) qColl.getCollection();
				app.getQueueManager().addQueues(queues);
				RavelryPaginator qPage = qColl.getPaginator();
				if(qPage != null) {
					int page = qPage.getPage();
					int maxPage = qPage.getPageCount();
					if(page < maxPage) {
						retrieveUserQueue(response.username, response.miscId, response.miscString, page+1);
						response.task = ERavelryTask.QUEUELISTSTILLLOADING;
					}
				}
				break;
			case SPECIFICUSERPROJECTS:
				RavelryCollection coll = (RavelryCollection) response.result;
				ArrayList<RavelryProject> projects = (ArrayList<RavelryProject>)(ArrayList<?>) coll.getCollection();
				app.getProjectManager().addProjects(projects);
				RavelryPaginator proPage = coll.getPaginator();
				if(proPage != null) {
					int page = proPage.getPage();
					int maxPage = proPage.getPageCount();
					if(page < maxPage) {
						retrieveUserProjects(response.username, page+1);
						response.task = ERavelryTask.SPECIFICUSERPROJECTSSTILLOADING;
					}
				}
				break;
			case PROJECTSBYPATTERN:
				RavelryCollection collect = (RavelryCollection) response.result;
				ArrayList<RavelryProject> projs = (ArrayList<RavelryProject>)(ArrayList<?>) collect.getCollection();
				app.getProjectManager().addProjects(projs);
				RavelryPaginator projPage = collect.getPaginator();
				// TODO: handle paginator

				break;
			case FORUMSETS:
				RavelryCollection forumCollection = RavelryCollection.class.cast(response.result);
				ArrayList<RavelryForum> forums = (ArrayList<RavelryForum>)(ArrayList<?>)forumCollection.getCollection();
				app.getForumManager().addForums(forums);
				break;
			case FORUMTOPICS:
			case FORUMTOPICSCONT:
				RavelryCollection topicCollection = (RavelryCollection) response.result;				
				ArrayList<RavelryTopic> topics = (ArrayList<RavelryTopic>)(ArrayList<?>)topicCollection.getCollection();
				int page = 0;
				int tmaxPage = 0;
				if(topicCollection.getPaginator() != null) {
					page = topicCollection.getPaginator().getPage();
				}
				if(topicCollection.getPaginator() != null) {
					tmaxPage = topicCollection.getPaginator().getPageCount();
				}
				app.getForumManager().addTopics(topics, page, tmaxPage);
				break;
			case TOPICPOSTS:
			case TOPICPOSTSCONT:
			case TOPICPOSTSBATCH:
			case TOPICPOSTSBACKWARDS:
				RavelryCollection postCollection = (RavelryCollection) response.result;
				ArrayList<RavelryForumPost>posts = (ArrayList<RavelryForumPost>)(ArrayList<?>)postCollection.getCollection();
				int ppage = 0;
				int pmaxpage = 0;
				if(postCollection.getPaginator() != null) {
					ppage = postCollection.getPaginator().getPage();
					pmaxpage = postCollection.getPaginator().getLastPage();
				}
				int pForumId = response.forumId;
				if(response.task == ERavelryTask.TOPICPOSTSCONT) {
					boolean newPagePresent = app.getForumManager().addMorePosts(posts, ppage, pForumId, pmaxpage);
					if(newPagePresent) {
						retrieveTopicPosts(response.topicId, ppage+1, true, true, pForumId);
					}
				}
				else if(response.task == ERavelryTask.TOPICPOSTSBATCH) {
					int upperbound = response.upperbound;
					app.getForumManager().addPosts(posts, ppage, pForumId, pmaxpage);
					if(upperbound > ppage*25) {
						retrieveTopicPostsBatch(response.topicId, ppage+1, true, true, response.forumId, upperbound);
					}
					else {
						response.task = ERavelryTask.TOPICPOSTSBATCHCOMPLETE;
					}
				}
				else {
					app.getForumManager().addPosts(posts, ppage, pForumId, pmaxpage);
				}
				break;
			case FORUMSUBMITPOST:
				if(response.result == null) {
					// Null means tanslate never fired
				}
				break;
			case FORUMSUBMITPOSTEDIT:
				if(response.result == null) {
					// translate never fired....
				}
				else {
					app.getForumManager().replaceForumPost(response.forumId, response.topicId, (RavelryForumPost)response.result);
				}
				break;
			case MESSAGES:
				RavelryCollection messagesCollection = RavelryCollection.class.cast(response.result);
				RavelryPaginator messagePaginator = messagesCollection.getPaginator();
				int messagePage = messagePaginator.getPage();
				int messageLastPage = messagePaginator.getLastPage();

				ArrayList<RavelryMessage> messageArray = (ArrayList<RavelryMessage>)(ArrayList<?>)messagesCollection.getCollection();
				app.getMessagingManager().addMessagesToDumbList(messageArray);
				if(messagePage < messageLastPage) {
					retrieveMessages(response.messageBox, messagePage+1, "");
				}
				else if(response.messageBox.equals("inbox")) {
					retrieveMessages("sent", 1, "");
				}
				else if(response.messageBox.equals("sent")) {
					retrieveMessages("archived", 1, "");
				}
				else {
					app.getMessagingManager().finishedAddingMessagesToDumbList();
				}
				break;
			case MESSAGE:
				RavelryMessage message = RavelryMessage.class.cast(response.result);
				app.getMessagingManager().addCompletedMessage(message);
				app.getMessagingManager().addedACompletedMessage();
				break;
			case PATTERNS:
				RavelryCollection collection = RavelryCollection.class.cast(response.result);
				RavelryPaginator paginator = collection.getPaginator();
				ArrayList<RavelryPattern> patterns = (ArrayList<RavelryPattern>)(ArrayList<?>)collection.getCollection();
				if(paginator == null || paginator.getPageCount() == 1) {
					app.getPatternManager().setDumbPatterns(patterns, 1);
				}
				else if(paginator.getPage() > 1) {
					app.getPatternManager().addDumbPatterns(patterns, paginator.getPage(), paginator.getPageCount());
				}
				else if(paginator.getPageCount() > 1) {
					app.getPatternManager().setDumbPatterns(patterns, paginator.getPageCount());
				}
				break;
			case PATTERNSEXT:
				break;
			case QUEUECREATE:
				String t = "f";
				break;
			case USERPROFILE:
				app.getPersonManager().addUser(RavelryUser.class.cast(response.result));
				break;
			case NEEDLESIZES:
				RavelryCollection needleSizeCollection = RavelryCollection.class.cast(response.result);
				ArrayList<NeedleSize> needleSizes = (ArrayList<NeedleSize>)(ArrayList<?>)needleSizeCollection.getCollection();
				app.getToolsManager().addSizesToDictionary(needleSizes);
				retrieveNeedleTypes();
				break;
			case NEEDLETYPES:
				RavelryCollection needleTypeCollection = (RavelryCollection) response.result;
				ArrayList<Needle> needleTypes = (ArrayList<Needle>)(ArrayList<?>)needleTypeCollection.getCollection();
				app.getToolsManager().addNeedlesToDictionary(needleTypes);
				break;
			case NEEDLERECORDS:
				RavelryCollection needleRecCollection = (RavelryCollection) response.result;
				ArrayList<RavelryNeedleRecord> records = (ArrayList<RavelryNeedleRecord>)(ArrayList<?>)needleRecCollection.getCollection();
				app.getToolsManager().addNeedleRecords(records);
				break;
			case STASH:
			case UNIFIEDSTASH:
				RavelryCollection stashCollection = (RavelryCollection) response.result;
				ArrayList<RavelryStashGeneric> stashes = (ArrayList<RavelryStashGeneric>)(ArrayList<?>)stashCollection.getCollection();

				int stashpage = 0;
				int stashmaxPage = 0;
				if(stashCollection.getPaginator() != null) {
					stashpage = stashCollection.getPaginator().getPage();
					stashmaxPage = stashCollection.getPaginator().getPageCount();
				}
				app.getStashManager().addStashes(response.username, stashes, stashpage, stashmaxPage);
				break;
			case FAVORITECREATE:
				RavelryBookmark newBook = (RavelryBookmark) response.result;
				if(newBook.getType().equals("pattern")) {
					if(app.getPatternManager().getCachedPattern(newBook.getItemId()) != null) {
						app.getPatternManager().getCachedPattern(newBook.getItemId()).setBookmarkId(newBook.getId());
					}
				}
				break;
			case REMOVEFAVORITE:
				RavelryBookmark removeBook = (RavelryBookmark) response.result;
				if(removeBook.getType().equals("pattern")) {
					if(app.getPatternManager().getCachedPattern(removeBook.getItemId()) != null) {
						app.getPatternManager().getCachedPattern(removeBook.getItemId()).setBookmarkId(0);
						app.getPatternManager().getCachedPattern(removeBook.getItemId()).setFavorited(false);
					}
				}
				break;
			default: 
				break;
			}
			listener.processFinish(response);
		}
	}

	/**
	 * Adds a task to a local array to keep track of.
	 * @param task {@link RavelryAsyncTask} Task to add. 
	 */
	private void addAsyncTask(RavelryAsyncTask task) {
		synchronized (threads) {
			threads.add(task);
		}
	}

	/**
	 * Returns the current {@link RavelryUser}. Can be null. 
	 * @return {@link RavelryUser} Could be null if not logged in. 
	 */
	public RavelryUser getRavelryUser() {
		return ravelryUser;
	}

	/**
	 * Sets the current {@link RavelryUser}. 
	 * @param user {@link RavelryUser} The user, accepts null. 
	 */
	public void setRavelryUser(RavelryUser user) {
		this.ravelryUser = user;
	}

	/**
	 * Logs out of Knitci by destroying the current token and nulling out the current user. 
	 * @param context {@link Context} Context to use for accessing the preferences. 
	 */
	public void logOut(Context context) {
		KnitciPreferences.deleteToken(context);
		token = null;
		ravelryUser = null;
	}

	/**
	 * Checks to see if the user is logged in. A user is considered logged in if {@link RavelryUser} is not null. 
	 * @return boolean True or false if logged in. 
	 */
	public boolean isLoggedIn() {
		if(this.ravelryUser != null) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param type String
	 * 		The type of item that was favorited. One of: project, pattern, yarn, stash, forumpost, 
	 * 		designer, yarnbrand, yarnshop.
	 * @param id
	 * @param comment
	 * @param tags
	 * @return
	 */
	public static JsonObject jsonEncodeFavorite(String type, int id, String comment, ArrayList<String> tags) {
		HashMap<String, Object> pairs = new HashMap<String, Object>();
		pairs.put("type", type);
		pairs.put("favorited_id", id);
		if(!comment.isEmpty()) {
			pairs.put("comment", comment);
		}
		if(tags != null && !tags.isEmpty()) {
			String tagString = "";
			for(int i = 0; i < tags.size(); i++) {
				tagString += tags.get(i) + " ";
			}
			pairs.put("tag_list", tagString);
		}
		return JsonUtil.createSimpleJsonObject(pairs);
	}

	public static JsonObject jsonEncodeQueue(String finishByDt, String makeFor, String notes, int patternId,
			String patternAuthor, String patternName, String patternUrl, String personalYarnName,
			int skeins, int sortPosition, String startOnDt, int yarnId, ArrayList<String> tags) {
		HashMap<String, Object> pairs = new LinkedHashMap<String, Object>();

		if(finishByDt != null && !finishByDt.isEmpty()) {
			pairs.put("finish_by", finishByDt);
		}

		if(makeFor != null && !makeFor.isEmpty()) {
			pairs.put("make_for", makeFor);
		}

		if(notes != null && !notes.isEmpty()) {
			pairs.put("notes", notes);
		}

		if(patternId > 0) {
			pairs.put("pattern_id", patternId);
		}

		if(patternAuthor != null && !patternAuthor.isEmpty()) {
			pairs.put("personal_pattern_author", patternAuthor);
		}

		if(patternName != null && !patternName.isEmpty()) {
			pairs.put("personal_pattern_name", patternName);
		}

		if(patternUrl != null && !patternUrl.isEmpty()) {
			pairs.put("personal_pattern_url", patternUrl);
		}

		if(personalYarnName != null && !personalYarnName.isEmpty()) {
			pairs.put("personal_yarn_name", personalYarnName);
		}

		if(skeins > 0) {
			pairs.put("skeins", skeins);
		}

		if(sortPosition > 0) {
			pairs.put("sort_order", sortPosition);
		}

		if(startOnDt != null && !startOnDt.isEmpty()) {
			pairs.put("start_on", startOnDt);
		}

		if(yarnId > 0) {
			pairs.put("yarn_id", yarnId);
		}
		JsonObject obj = JsonUtil.createSimpleJsonObject(pairs);

		if(tags != null && !tags.isEmpty()) {
			obj.add("tag_names", JsonUtil.createArray(tags));
		}

		return obj;
	}
}