package com.knitci.pocket.ravelry;

import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.R.layout;
import com.knitci.pocket.R.menu;

import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.os.Build;

/**
 * An activity for letting a user login to ravelry. This activity utilizes a {@link WebView} to load the 
 * auth page. When the user clicks authorize on the Ravelry webpage, a {@link Token} object is created
 * and set in the {@link RavelryManager}. 
 * @author DJ021930
 *
 */
public class RavelryOAuthActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ravelry_oauth);
		// Show the Up button in the action bar.
		setupActionBar();
		
		final WebView webview = (WebView) findViewById(R.id.webview);
		
		final OAuthService s = new ServiceBuilder()
	    .provider(RavelryAPI.class)
	    .apiKey(RavelryAPI.CONSUMER_KEY)
	    .apiSecret(RavelryAPI.CONSUMER_SECRET)
	    .callback("oauthapp://ravelry.com")
	    .debug()
	    .build();
		
		final Token requestToken = s.getRequestToken();
	    Log.d("REQUEST_TOKEN" , s.getRequestToken().toString());
	    final String authURL = s.getAuthorizationUrl(requestToken);
	    Log.d("AUTHORIZE_TOKEN" , s.getAuthorizationUrl(requestToken).toString());
	    
	    //attach WebViewClient to intercept the callback url
	    webview.setWebViewClient(new WebViewClient()
	    {

	        @Override
	        public boolean shouldOverrideUrlLoading(WebView view, String url)
	        {

	            //check for our custom callback protocol
	            //otherwise use default behavior
	            if(url.startsWith("oauth"))
	            {
	                //authorization complete hide webview for now.
	                webview.setVisibility(View.GONE);
	                Uri uri = Uri.parse(url);
	                String verifier = uri.getQueryParameter("oauth_verifier");
	                Verifier v = new Verifier(verifier);

	                //save this token for practical use.
	                Token accessToken = s.getAccessToken(requestToken, v);

	                /*
	                OAuthRequest request = new OAuthRequest(Verb.GET, "https://api.ravelry.com/current_user.json");
	                s.signRequest(accessToken, request);
	                Response response = request.send();
	                Log.d("USER", "JSON:"+response.getCode());
					JSONObject json = null;
					try {
						json = new JSONObject(response.getBody());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						Log.d("FASF", "USER IS " + json.get("user"));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					Log.d("Access Token:", accessToken.getToken());
					Log.d("Secret Token:", accessToken.getSecret());*/
					
					Token toke = new Token(accessToken.getToken(), accessToken.getSecret());
					
					KnitciApplication app = (KnitciApplication) getApplication();
					app.getRavelryManager().setAndSaveToken(toke, getApplicationContext());
					
					finish();
	                return true;
	            }
	            return super.shouldOverrideUrlLoading(view, url);
	        }
	   });

	   //send user to authorization page
	   webview.loadUrl(authURL); 
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ravelry_oauth, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
