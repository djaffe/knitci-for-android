package com.knitci.pocket.ravelry;

import com.knitci.pocket.ravelry.data.access.RavelryAsyncTask;
import com.knitci.pocket.ravelry.data.access.RavelryAsyncTask.ERavelryTask;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;

/**
 * An interface for receiving a {@link RavelryAsyncTask} finished call. 
 * @author DJ021930
 */
public interface IRavelryAsync {
	
	/**
	 * Handles a {@link RavelryResponse} that came from a {@link RavelryAsyncTask}. All classes
	 * implementing this interface need to choose which {@link ERavelryTask} types to handle. 
	 * @param response {@link RavelryResponse} A response object. It's contents vary based on the
	 * API call that was used. 
	 */
	void processFinish(RavelryResponse response);
	
}
