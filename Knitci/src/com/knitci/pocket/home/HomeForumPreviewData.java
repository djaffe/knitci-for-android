package com.knitci.pocket.home;

import java.util.ArrayList;

import android.text.Html;

public class HomeForumPreviewData {

	public String forumDisplay;
	public ArrayList<String> topicDisplays;
	
	public HomeForumPreviewData() {
		topicDisplays = new ArrayList<String>();
	}
	
}
