package com.knitci.pocket.home;
import java.util.ArrayList;

import com.android.volley.toolbox.ImageLoader;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.R.layout;
import com.knitci.pocket.ravelry.data.object.RavelryObject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPattern;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject.ProjectPhotoUrlSize;

import android.content.Context;
import android.graphics.Point;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PatternPagerAdapter extends PagerAdapter {

	Context context;
	RavelryProject[] projects;
	ArrayList<RavelryObject> patterns;
	LayoutInflater inflater;

	public PatternPagerAdapter(Context context, ArrayList<RavelryObject> arrayList) {
		this.context = context;
		this.patterns = arrayList;
	}

	@Override
	public int getCount() {
		return patterns.size();
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == (RelativeLayout) arg1;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {

		// Declare Variables
		TextView txtcountry;
		RavelryPattern pattern = RavelryPattern.class.cast(patterns.get(position));
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View itemView = inflater.inflate(R.layout.home_pattern_cell, container, false);

		// Locate the TextViews in viewpager_item.xml
		txtcountry = (TextView) itemView.findViewById(R.id.home_pattern_cell_pattern_name);

		// Capture position and set to the TextViews
		txtcountry.setText(pattern.getPatternName());

		
		ImageView img = (ImageView) itemView.findViewById(R.id.home_pattern_cell_pattern_image);
		KnitciApplication app = (KnitciApplication)context.getApplicationContext();
		if(pattern.getFirstPhotos() != null && pattern.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.MEDIUM) != null) {
			app.getImageLoader().get(pattern.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.MEDIUM).getUrl(), ImageLoader.getImageListener(img, R.drawable.ic_launcher, R.drawable.ic_launcher));
		//	app.getImageLoader().get("http://www.fabafterfifty.co.uk/wp-content/uploads/2011/02/70s-hat.jpg", ImageLoader.getImageListener(img, R.drawable.ic_launcher, R.drawable.ic_launcher));
		}
		
		// Add viewpager_item.xml to ViewPager
		((ViewPager) container).addView(itemView);

		return itemView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// Remove viewpager_item.xml from ViewPager
		((ViewPager) container).removeView((RelativeLayout) object);

	}

//	@Override
//	public float getPageWidth(int position) {
//		return 1.0f;
//	}
	
}
