package com.knitci.pocket.home;

import object.KnitciActivity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdView;
import com.google.ads.InterstitialAd;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.home.fluffer.HomeActivityFluffer;
import com.knitci.pocket.ravelry.IRavelryAsync;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.ravelry.data.object.impl.RavelryForum;

/**
 * The home screen activity. Extends SlidingActivity for sliding menus.
 * Implements the IRavelryAsync interface for receiving rav async task 
 * posts. 
 * @author DJ021930
 *
 */
public class HomeActivity extends KnitciActivity implements IRavelryAsync, AdListener {
	
	 private InterstitialAd interstitial;
	 AdView adView;
	 
	/* (non-Javadoc)
	 * @see com.jeremyfeinstein.slidingmenu.lib.app.SlidingActivity#onCreate(android.os.Bundle)
	 */
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		stealAsyncListening();
		
		//request TEST ads to avoid being disabled for clicking your own ads
        AdRequest adRequest = new AdRequest();
 
        //test mode on EMULATOR
        adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
        
        //test mode on DEVICE (this example code must be replaced with your device uniquq ID)
        adRequest.addTestDevice("BB194F96A454E8AE8E021E77F7B58787");
 
        adView = (AdView)findViewById(R.id.adMob);      
 
        // Initiate a request to load an ad in test mode.
        // You can keep this even when you release your app on the market, because
        // only emulators and your test device will get test ads. The user will receive real ads.
        adView.loadAd(adRequest);
		
        
        // Create the interstitial.
        interstitial = new InterstitialAd(this, "ca-app-pub-4875747004163894/1786195490");

        // Create ad request.
        AdRequest adRequest2 = new AdRequest();
        adRequest2.addTestDevice("BB194F96A454E8AE8E021E77F7B58787");
        

        // Begin loading your interstitial.
        interstitial.loadAd(adRequest2);
        interstitial.setAdListener(this);
        
        
        
		setupSlidingMenu();
        
        // Check if the screen was turned. 
        HomeActivityFluffer fluffer = (HomeActivityFluffer) getLastNonConfigurationInstance();
        if(null != fluffer) {
        	// Restore stuff. 
        }
        else {
        	//beginLoadingRecentPatterns();
        }
		
        ViewPager pager = (ViewPager) findViewById(R.id.home_project_viewpager);
        
        int margin = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20*2,     getResources().getDisplayMetrics());
        pager.setPageMargin(-margin);
        pager.setOffscreenPageLimit(4);
        
        KnitciApplication.class.cast(getApplication()).unloadSplashImage();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		stealAsyncListening();
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.home, menu);
		return true;
	}
	
	@Override
	@Deprecated
	public Object onRetainNonConfigurationInstance() {
		HomeActivityFluffer fluffer = new HomeActivityFluffer();
		
		
		return fluffer;
	}

	/* (non-Javadoc)
	 * @see com.knitci.pocket.ravelry.IRavelryAsync#processFinish(com.knitci.pocket.ravelry.data.object.RavelryResponse)
	 */
	@Override
	public void processFinish(RavelryResponse response) {
		if(null == response) {
			return;
		}
		
		super.processFinish(response);
		
		switch(response.task) {
		case SPECIFICUSERPROJECTS:
			TextView tv = (TextView) getSlidingMenu().findViewById(R.id.slider_projects_count);
			RavelryCollection arr = (RavelryCollection) response.result;
			tv.setText(String.valueOf(arr.getCollection().size()));
			break;
		case PATTERNS:
			Toast toast = Toast.makeText(getApplicationContext(), "Patterns received", Toast.LENGTH_SHORT);
			toast.show();
			Log.d("Latest Patterns", "The latest patterns have been receied");
			patternsReceived(RavelryCollection.class.cast(response.result));
			break;
		case FORUMSETS:
			forumSetsReceived(RavelryCollection.class.cast(response.result));
			break;
		case HTTPERROR:
			break;
		default:
			break;
		}
	}

	/**
	 * Logs out of Knitci. The token is deleted from prefs, and 2001 is the result code
	 * sent back to the splash activity. 
	 * @param view View A view if this is called by a button. Can be null. 
	 */
	public void logout(View view) {
		KnitciApplication app = (KnitciApplication) getApplication();
		app.getRavelryManager().logOut(getApplicationContext());
		setResult(2001);
		finish();
	}
	
	private void beginLoadingRecentPatterns() {
		KnitciApplication app = KnitciApplication.class.cast(getApplicationContext());
		app.getRavelryManager().retrievePatterns("", 1, 5, "hat", "created");
		
		app.getRavelryManager().retrieveForumSets();
	}
	
	private void patternsReceived(RavelryCollection patternCollection) {
		ViewPager pager = (ViewPager) findViewById(R.id.home_project_viewpager);
		pager.setAdapter(new PatternPagerAdapter(getApplicationContext(), patternCollection.getCollection()));
	}
	
	private void forumSetsReceived(RavelryCollection forumSetCollection) {
		LinearLayout holder = LinearLayout.class.cast(findViewById(R.id.home_forum_set_holder));
		
		for(int i = 0; i < forumSetCollection.getCollection().size(); i++) {
			RavelryForum forum = RavelryForum.class.cast(forumSetCollection.getCollection().get(i));
			TextView tv = new TextView(getApplicationContext());
			tv.setText(forum.getName());
			tv.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			holder.addView(tv);
		}
	}
	
	private void forumTopicsReceived(RavelryCollection forumTopicCollection) {
		
	}

	@Override
	public void onDismissScreen(Ad arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFailedToReceiveAd(Ad arg0, ErrorCode arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLeaveApplication(Ad arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPresentScreen(Ad arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReceiveAd(Ad arg0) {
		// interstitial.show();
		// TODO Auto-generated method stub
	}
	
	@Override
	public void onForumsSlidingMenuPressed(View view) {
		// TODO Auto-generated method stub
		super.onForumsSlidingMenuPressed(view);
	}

}
