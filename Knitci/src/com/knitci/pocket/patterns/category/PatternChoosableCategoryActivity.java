package com.knitci.pocket.patterns.category;

import java.util.ArrayList;

import object.CheckBoxColorfulPostback;
import object.CheckBoxSupreme;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.R.anim;
import com.knitci.pocket.R.id;
import com.knitci.pocket.R.layout;
import com.knitci.pocket.R.menu;
import com.knitci.pocket.patterns.manager.PatternManager;

public class PatternChoosableCategoryActivity extends Activity implements OnClickListener, CheckBoxColorfulPostback {

	PatternManager patternManager;
	RavelryReferenceCategory highestCat;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pattern_category);
		patternManager = KnitciApplication.class.cast(getApplication()).getPatternManager();

		highestCat = RavelryReferenceCategory.class.cast(getIntent().getExtras().get("CATEGORY"));
	}

	@Override
	protected void onResume() {
		super.onResume();

		ArrayList<RavelryReferenceCategory> categories = KnitciApplication.class.cast(this.getApplication()).getPatternManager().getReferenceCategories();

		LinearLayout sv = LinearLayout.class.cast(findViewById(R.id.categories_holder));


		String topLevel = highestCat.name;

		if(sv.getChildCount() == 0) {
			CheckBox check = new CheckBox(this);
			check.setText("All " + topLevel);
			sv.addView(check);

			if(highestCat.subcategories != null && highestCat.subcategories.size() > 0) {
				for(int i = 0; i < highestCat.subcategories.size(); i++) {
					CheckBoxSupreme tvv = new CheckBoxSupreme(getApplicationContext());
					tvv.setChecked(patternManager.getCategoryManager().isCategoryChecked(highestCat.subcategories.get(i).value));
					tvv.setCheckBoxColorfulListener(this);
					tvv.setText(highestCat.subcategories.get(i).name);
					tvv.setSubText(patternManager.getCategoryManager().getPartialString(highestCat.subcategories.get(i).value, highestCat.subcategories.get(i).value));
					tvv.setTag(highestCat.subcategories.get(i).value);
					if(highestCat.subcategories.get(i).subcategories != null && highestCat.subcategories.get(i).subcategories.size() > 0) {
						tvv.showMoreIcon(true);
					}
					tvv.setOnClickListener(this);
					sv.addView(tvv);
				}
			}
		}
		else {
			for(int i = 1; i < sv.getChildCount(); i++) {
				CheckBoxSupreme tv = CheckBoxSupreme.class.cast(sv.getChildAt(i));
				tv.setSubText(patternManager.getCategoryManager().getPartialString(highestCat.subcategories.get(i-1).value, highestCat.subcategories.get(i-1).value));
			}
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pattern_choosable_category, menu);
		return true;
	}

	@Override
	public void onClick(View v) {

		TextView tv = TextView.class.cast(v);
		CheckBoxSupreme supreme = CheckBoxSupreme.class.cast(tv.getTag());
		String value = (String) supreme.getTag();
		RavelryReferenceCategory cat = RavelryReferenceCategory.class.cast(patternManager.getCategoryManager().getReferenceCategory(value));


		if(cat.subcategories != null && cat.subcategories.size() > 0) {
			Intent intent = new Intent(this, PatternChoosableCategoryActivity.class);
			intent.putExtra("CATEGORY", cat);
			startActivity(intent);
			overridePendingTransition(R.anim.animation_right_to_left, R.anim.animation_left_to_right);
		}
	}

	@Override
	public boolean checkBoxClicked(String value, boolean checked) {
		patternManager.getCategoryManager().setCategoryChecked(value, checked);
		Toast toast = Toast.makeText(getApplicationContext(), patternManager.getCategoryManager().getEnabledCategoryValues().toString(), Toast.LENGTH_SHORT);
		toast.show();
		return true;
	}

}
