package com.knitci.pocket.patterns.category;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import android.util.Log;

public class CategoryManager {

	private ArrayList<RavelryReferenceCategory> topLevelCategories;
	private HashMap<String, Boolean> checkedMap;
	private HashMap<String, RavelryReferenceCategory> categoryMap;

	public CategoryManager() {
		topLevelCategories = new ArrayList<RavelryReferenceCategory>();
		checkedMap = new LinkedHashMap<String, Boolean>();
		categoryMap = new LinkedHashMap<String, RavelryReferenceCategory>();
		setupReferenceCategories();
	}

	public ArrayList<RavelryReferenceCategory> getReferenceCategories() {
		return topLevelCategories;
	}

	public void setCategoryChecked(String cat, boolean checked) {
		checkedMap.put(cat, checked);
	}

	public boolean isCategoryChecked(String cat) {
		return checkedMap.get(cat);
	}

	public ArrayList<String> getEnabledCategoryValues() {
		ArrayList<String> vals = new ArrayList<String>();

		for (Entry<String, Boolean> entry : checkedMap.entrySet()) {
			String value = entry.getKey();
			boolean enabled = entry.getValue();
			if(enabled) {
				vals.add(value);
			}
		}

		return vals;
	}

	public RavelryReferenceCategory getReferenceCategory(String value) {
		return categoryMap.get(value);
	}
	
	public void clearChecks() {
		for (Entry<String, Boolean> entry : checkedMap.entrySet()) {
			checkedMap.put(entry.getKey(), false);
		}
	}

	private void setupReferenceCategories() {
		checkedMap = new LinkedHashMap<String, Boolean>();
		topLevelCategories = new ArrayList<RavelryReferenceCategory>();

		// Setup clothing
		RavelryReferenceCategory clothing = new RavelryReferenceCategory("Clothing", "clothing");
		ArrayList<RavelryReferenceCategory> clothingSub = new ArrayList<RavelryReferenceCategory>();

		clothingSub.add(new RavelryReferenceCategory("Coat / Jacket", "coat"));
		categoryMap.put("coat", clothingSub.get(clothingSub.size()-1));
		clothingSub.add(new RavelryReferenceCategory("Dress", "dress"));
		categoryMap.put("dress", clothingSub.get(clothingSub.size()-1));

		RavelryReferenceCategory intimate = new RavelryReferenceCategory("Intimate Apparel", "intimateapparel");
		categoryMap.put("intimateapparel", intimate);
		ArrayList<RavelryReferenceCategory> intimateSub = new ArrayList<RavelryReferenceCategory>();
		intimateSub.add(new RavelryReferenceCategory("Bra", "bra"));
		categoryMap.put("bra", intimateSub.get(intimateSub.size()-1));
		intimateSub.add(new RavelryReferenceCategory("Pasties", "pasties"));
		categoryMap.put("pasties", intimateSub.get(intimateSub.size()-1));
		intimateSub.add(new RavelryReferenceCategory("Underwear", "underwear"));
		categoryMap.put("underwear", intimateSub.get(intimateSub.size()-1));
		intimateSub.add(new RavelryReferenceCategory("Other", "other-intimateapparel"));
		categoryMap.put("other-intimateapparel", intimateSub.get(intimateSub.size()-1));
		intimate.subcategories = intimateSub;

		clothingSub.add(intimate);
		clothingSub.add(new RavelryReferenceCategory("Leggings", "leggings"));
		categoryMap.put("leggings", clothingSub.get(clothingSub.size()-1));
		clothingSub.add(new RavelryReferenceCategory("Onesies", "onesies"));
		categoryMap.put("onesies", clothingSub.get(clothingSub.size()-1));
		clothingSub.add(new RavelryReferenceCategory("Pants", "pants"));
		categoryMap.put("pants", clothingSub.get(clothingSub.size()-1));
		clothingSub.add(new RavelryReferenceCategory("Robe", "robe"));
		categoryMap.put("robe", clothingSub.get(clothingSub.size()-1));
		clothingSub.add(new RavelryReferenceCategory("Shorts", "shorts"));
		categoryMap.put("shorts", clothingSub.get(clothingSub.size()-1));
		clothingSub.add(new RavelryReferenceCategory("Shrug / Bolero", "shrug"));
		categoryMap.put("shrug", clothingSub.get(clothingSub.size()-1));
		clothingSub.add(new RavelryReferenceCategory("Skirt", "skirt"));
		categoryMap.put("skirt", clothingSub.get(clothingSub.size()-1));
		clothingSub.add(new RavelryReferenceCategory("Sleepwear", "sleepwear"));
		categoryMap.put("sleepwear", clothingSub.get(clothingSub.size()-1));
		clothingSub.add(new RavelryReferenceCategory("Soakers", "soakers"));
		categoryMap.put("soakers", clothingSub.get(clothingSub.size()-1));

		RavelryReferenceCategory sweater = new RavelryReferenceCategory("Sweater", "sweater");
		categoryMap.put("sweater", sweater);
		ArrayList<RavelryReferenceCategory> sweaterSub = new ArrayList<RavelryReferenceCategory>();
		sweaterSub.add(new RavelryReferenceCategory("Cardigan", "cardigan"));
		categoryMap.put("cardigan", sweaterSub.get(sweaterSub.size()-1));
		sweaterSub.add(new RavelryReferenceCategory("Pullover", "pullover"));
		categoryMap.put("pullover", sweaterSub.get(sweaterSub.size()-1));
		sweaterSub.add(new RavelryReferenceCategory("Other", "other-sweater"));
		categoryMap.put("other-sweater", sweaterSub.get(sweaterSub.size()-1));
		sweater.subcategories = sweaterSub;
		clothingSub.add(sweater);

		clothingSub.add(new RavelryReferenceCategory("Swimwear", "swimwear"));
		categoryMap.put("swimwear", clothingSub.get(clothingSub.size()-1));

		RavelryReferenceCategory tops = new RavelryReferenceCategory("Tops", "tops");
		categoryMap.put("tops", tops);
		ArrayList<RavelryReferenceCategory> topSub = new ArrayList<RavelryReferenceCategory>();
		topSub.add(new RavelryReferenceCategory("Sleeveless Top", "sleeveless-top"));
		categoryMap.put("sleeveless-top", topSub.get(topSub.size()-1));
		topSub.add(new RavelryReferenceCategory("Strapless Top", "strapless-top"));
		categoryMap.put("strapless-top", topSub.get(topSub.size()-1));
		topSub.add(new RavelryReferenceCategory("Tee", "tee"));
		categoryMap.put("tee", topSub.get(topSub.size()-1));
		topSub.add(new RavelryReferenceCategory("Other", "other-top"));
		categoryMap.put("other-top", topSub.get(topSub.size()-1));
		tops.subcategories = topSub;
		clothingSub.add(tops);

		clothingSub.add(new RavelryReferenceCategory("Vest", "vest"));
		categoryMap.put("vest", clothingSub.get(clothingSub.size()-1));
		clothingSub.add(new RavelryReferenceCategory("Other", "other-clothing"));
		categoryMap.put("other-clothing", clothingSub.get(clothingSub.size()-1));

		clothing.subcategories = clothingSub;
		categoryMap.put("clothing", clothing);
		topLevelCategories.add(clothing);


		// Setup accessories
		RavelryReferenceCategory access = new RavelryReferenceCategory("Accessories", "accessories");
		categoryMap.put("accessories", access);
		ArrayList<RavelryReferenceCategory> accessSub = new ArrayList<RavelryReferenceCategory>();

		RavelryReferenceCategory bag = new RavelryReferenceCategory("Bag", "bag");
		categoryMap.put("bag", bag);
		ArrayList<RavelryReferenceCategory> bagSub = new ArrayList<RavelryReferenceCategory>();
		bagSub.add(new RavelryReferenceCategory("Backpack", "backpack"));
		categoryMap.put("backpack", bagSub.get(bagSub.size()-1));
		bagSub.add(new RavelryReferenceCategory("Clutch", "clutch"));
		categoryMap.put("clutch", bagSub.get(bagSub.size()-1));
		bagSub.add(new RavelryReferenceCategory("Drawstring", "drawstring"));
		categoryMap.put("drawstring", bagSub.get(bagSub.size()-1));
		bagSub.add(new RavelryReferenceCategory("Duffle", "duffle"));
		categoryMap.put("duffle", bagSub.get(bagSub.size()-1));
		bagSub.add(new RavelryReferenceCategory("Laptop", "laptop"));
		categoryMap.put("laptop", bagSub.get(bagSub.size()-1));
		bagSub.add(new RavelryReferenceCategory("Market Bag", "market"));
		categoryMap.put("market", bagSub.get(bagSub.size()-1));
		bagSub.add(new RavelryReferenceCategory("Messenger", "messenger"));
		categoryMap.put("messenger", bagSub.get(bagSub.size()-1));
		bagSub.add(new RavelryReferenceCategory("Money", "money"));
		categoryMap.put("money", bagSub.get(bagSub.size()-1));
		bagSub.add(new RavelryReferenceCategory("Purse", "purse"));
		categoryMap.put("purse", bagSub.get(bagSub.size()-1));
		bagSub.add(new RavelryReferenceCategory("Tote", "tote"));
		categoryMap.put("tote", bagSub.get(bagSub.size()-1));
		bagSub.add(new RavelryReferenceCategory("Wristlet", "wristlet"));
		categoryMap.put("wristlet", bagSub.get(bagSub.size()-1));
		bagSub.add(new RavelryReferenceCategory("Other", "other-bag"));
		categoryMap.put("other-bag", bagSub.get(bagSub.size()-1));
		bag.subcategories = bagSub;
		accessSub.add(bag);

		accessSub.add(new RavelryReferenceCategory("Belt", "belt"));
		categoryMap.put("belt", accessSub.get(accessSub.size()-1));

		RavelryReferenceCategory feet = new RavelryReferenceCategory("Feet / Legs", "feet-legs");
		categoryMap.put("feet-legs", feet);
		ArrayList<RavelryReferenceCategory> feetSub = new ArrayList<RavelryReferenceCategory>();
		feetSub.add(new RavelryReferenceCategory("Booties", "booties"));
		categoryMap.put("booties", feetSub.get(feetSub.size()-1));
		feetSub.add(new RavelryReferenceCategory("Legwarmers", "legwarmers"));
		categoryMap.put("legwarmers", feetSub.get(feetSub.size()-1));
		feetSub.add(new RavelryReferenceCategory("Slippers", "slippers"));
		categoryMap.put("slippers", feetSub.get(feetSub.size()-1));
		RavelryReferenceCategory socks = new RavelryReferenceCategory("Socks", "socks");
		categoryMap.put("socks", socks);
		ArrayList<RavelryReferenceCategory> socksSub = new ArrayList<RavelryReferenceCategory>();
		socksSub.add(new RavelryReferenceCategory("Ankle", "ankle-sock"));
		categoryMap.put("ankle-sock", socksSub.get(socksSub.size()-1));
		socksSub.add(new RavelryReferenceCategory("Knee-highs", "knee-highs"));
		categoryMap.put("knee-highs", socksSub.get(socksSub.size()-1));
		socksSub.add(new RavelryReferenceCategory("Mid-calf", "mid-calf"));
		categoryMap.put("mid-calf", socksSub.get(socksSub.size()-1));
		socksSub.add(new RavelryReferenceCategory("Thigh-high", "thigh-high"));
		categoryMap.put("thigh-high", socksSub.get(socksSub.size()-1));
		socksSub.add(new RavelryReferenceCategory("Toeless", "toeless"));
		categoryMap.put("toeless", socksSub.get(socksSub.size()-1));
		socksSub.add(new RavelryReferenceCategory("Tube", "tube"));
		categoryMap.put("tube", socksSub.get(socksSub.size()-1));
		socksSub.add(new RavelryReferenceCategory("Other", "other-socks"));
		categoryMap.put("other-socks", socksSub.get(socksSub.size()-1));
		socks.subcategories = socksSub;
		feetSub.add(socks);
		feetSub.add(new RavelryReferenceCategory("Spats", "spats"));
		categoryMap.put("spats", feetSub.get(feetSub.size()-1));

		feetSub.add(new RavelryReferenceCategory("Other", "other-feet-legs"));
		categoryMap.put("other-feet-legs", feetSub.get(feetSub.size()-1));
		accessSub.add(feet);

		RavelryReferenceCategory hands = new RavelryReferenceCategory("Hands", "hands");
		categoryMap.put("hands", hands);
		ArrayList<RavelryReferenceCategory> handsSub = new ArrayList<RavelryReferenceCategory>();
		handsSub.add(new RavelryReferenceCategory("Convertible", "convertible"));
		categoryMap.put("convertible", handsSub.get(handsSub.size()-1));
		handsSub.add(new RavelryReferenceCategory("Cuffs", "cuffs"));
		categoryMap.put("cuffs", handsSub.get(handsSub.size()-1));
		handsSub.add(new RavelryReferenceCategory("Fingerless Gloves/Mitts", "fingerless"));
		categoryMap.put("fingerless", handsSub.get(handsSub.size()-1));
		handsSub.add(new RavelryReferenceCategory("Gloves", "gloves"));
		categoryMap.put("gloves", handsSub.get(handsSub.size()-1));
		handsSub.add(new RavelryReferenceCategory("Mittens", "mittens"));
		categoryMap.put("mittens", handsSub.get(handsSub.size()-1));
		handsSub.add(new RavelryReferenceCategory("Muff", "muff"));
		categoryMap.put("muff", handsSub.get(handsSub.size()-1));
		handsSub.add(new RavelryReferenceCategory("Other", "other-hands"));
		categoryMap.put("other-hands", handsSub.get(handsSub.size()-1));
		hands.subcategories = handsSub;
		accessSub.add(hands);

		RavelryReferenceCategory hat = new RavelryReferenceCategory("Hat", "hat");
		categoryMap.put("hat", hat);
		ArrayList<RavelryReferenceCategory> hatSub = new ArrayList<RavelryReferenceCategory>();
		hatSub.add(new RavelryReferenceCategory("Balaclava", "balaclava"));
		categoryMap.put("balaclava", hatSub.get(hatSub.size()-1));
		hatSub.add(new RavelryReferenceCategory("Beanie, Toque", "beanie-toque"));
		categoryMap.put("beanie-toque", hatSub.get(hatSub.size()-1));
		hatSub.add(new RavelryReferenceCategory("Beret, Tam", "beret-tam"));
		categoryMap.put("beret-tam", hatSub.get(hatSub.size()-1));
		hatSub.add(new RavelryReferenceCategory("Billed", "billed"));
		categoryMap.put("billed", hatSub.get(hatSub.size()-1));
		hatSub.add(new RavelryReferenceCategory("Bonnet", "bonnet"));
		categoryMap.put("bonnet", hatSub.get(hatSub.size()-1));
		hatSub.add(new RavelryReferenceCategory("Brimmed", "brimmed"));
		categoryMap.put("brimmed", hatSub.get(hatSub.size()-1));
		hatSub.add(new RavelryReferenceCategory("Cloche", "cloche"));
		categoryMap.put("cloche", hatSub.get(hatSub.size()-1));
		hatSub.add(new RavelryReferenceCategory("Earflap", "earflap"));
		categoryMap.put("earflap", hatSub.get(hatSub.size()-1));
		hatSub.add(new RavelryReferenceCategory("Pixie", "pixie"));
		categoryMap.put("pixie", hatSub.get(hatSub.size()-1));
		hatSub.add(new RavelryReferenceCategory("Stocking", "stocking"));
		categoryMap.put("stocking", hatSub.get(hatSub.size()-1));
		hatSub.add(new RavelryReferenceCategory("Yarmulke", "yarmulke"));
		categoryMap.put("yarmulke", hatSub.get(hatSub.size()-1));
		hatSub.add(new RavelryReferenceCategory("Other", "other-hat"));
		categoryMap.put("other-hat", hatSub.get(hatSub.size()-1));
		hat.subcategories = hatSub;
		accessSub.add(hat);

		RavelryReferenceCategory jewelry = new RavelryReferenceCategory("Jewelry", "jewelry");
		categoryMap.put("jewelry", jewelry);
		ArrayList<RavelryReferenceCategory> jewelrySub = new ArrayList<RavelryReferenceCategory>();
		jewelrySub.add(new RavelryReferenceCategory("Ankle", "ankle"));
		categoryMap.put("ankle", jewelrySub.get(jewelrySub.size()-1));
		jewelrySub.add(new RavelryReferenceCategory("Bracelet", "bracelet"));
		categoryMap.put("bracelet", jewelrySub.get(jewelrySub.size()-1));
		jewelrySub.add(new RavelryReferenceCategory("Brooch", "brooch"));
		categoryMap.put("brooch", jewelrySub.get(jewelrySub.size()-1));
		jewelrySub.add(new RavelryReferenceCategory("Earrings", "earrings"));
		categoryMap.put("earrings", jewelrySub.get(jewelrySub.size()-1));
		jewelrySub.add(new RavelryReferenceCategory("Necklace", "necklace"));
		categoryMap.put("necklace", jewelrySub.get(jewelrySub.size()-1));
		jewelrySub.add(new RavelryReferenceCategory("Ring", "ring"));
		categoryMap.put("ring", jewelrySub.get(jewelrySub.size()-1));
		jewelrySub.add(new RavelryReferenceCategory("Other", "other-jewelry"));
		categoryMap.put("other-jewelry", jewelrySub.get(jewelrySub.size()-1));
		jewelry.subcategories = jewelrySub;
		accessSub.add(jewelry);

		RavelryReferenceCategory neck = new RavelryReferenceCategory("Neck / Torso", "neck-torso");
		categoryMap.put("neck-torso", neck);
		ArrayList<RavelryReferenceCategory> neckSub = new ArrayList<RavelryReferenceCategory>();
		neckSub.add(new RavelryReferenceCategory("Bib", "bib"));
		categoryMap.put("bib", neckSub.get(neckSub.size()-1));
		neckSub.add(new RavelryReferenceCategory("Cape", "cape"));
		categoryMap.put("cape", neckSub.get(neckSub.size()-1));
		neckSub.add(new RavelryReferenceCategory("Collar","collar"));
		categoryMap.put("collar", neckSub.get(neckSub.size()-1));
		neckSub.add(new RavelryReferenceCategory("Cowl", "cowl"));
		categoryMap.put("cowl", neckSub.get(neckSub.size()-1));
		neckSub.add(new RavelryReferenceCategory("Necktie", "necktie"));
		categoryMap.put("necktie", neckSub.get(neckSub.size()-1));
		neckSub.add(new RavelryReferenceCategory("Poncho", "poncho"));
		categoryMap.put("poncho", neckSub.get(neckSub.size()-1));
		neckSub.add(new RavelryReferenceCategory("Scarf", "scarf"));
		categoryMap.put("scarf", neckSub.get(neckSub.size()-1));
		neckSub.add(new RavelryReferenceCategory("Shawl / Wrap", "shawl-wrap"));
		categoryMap.put("shawl-wrap", neckSub.get(neckSub.size()-1));
		neckSub.add(new RavelryReferenceCategory("Other", "other-neck-torso"));
		categoryMap.put("other-neck-torso", neckSub.get(neckSub.size()-1));
		neck.subcategories = neckSub;
		accessSub.add(neck);

		RavelryReferenceCategory headware = new RavelryReferenceCategory("Other Headwear", "headwear");
		categoryMap.put("headwear", headware);
		ArrayList<RavelryReferenceCategory> headwareSub = new ArrayList<RavelryReferenceCategory>();
		headwareSub.add(new RavelryReferenceCategory("Earwarmers", "earwarmers"));
		categoryMap.put("earwarmers", headwareSub.get(headwareSub.size()-1));
		headwareSub.add(new RavelryReferenceCategory("Eye mask", "eyemask"));
		categoryMap.put("eyemask", headwareSub.get(headwareSub.size()-1));
		headwareSub.add(new RavelryReferenceCategory("Hair accessories", "hairaccessories"));
		categoryMap.put("hairaccessories", headwareSub.get(headwareSub.size()-1));
		headwareSub.add(new RavelryReferenceCategory("Headband", "headband"));
		categoryMap.put("headband", headwareSub.get(headwareSub.size()-1));
		headwareSub.add(new RavelryReferenceCategory("Headwrap", "headwrap"));
		categoryMap.put("headwrap", headwareSub.get(headwareSub.size()-1));
		headwareSub.add(new RavelryReferenceCategory("Kerchief", "kerchief"));
		categoryMap.put("kerchief", headwareSub.get(headwareSub.size()-1));
		headwareSub.add(new RavelryReferenceCategory("Snood", "snood"));
		categoryMap.put("snood", headwareSub.get(headwareSub.size()-1));
		headwareSub.add(new RavelryReferenceCategory("Other", "other-headwear"));
		categoryMap.put("other-headwear", headwareSub.get(headwareSub.size()-1));
		headware.subcategories = headwareSub;
		accessSub.add(headware);

		accessSub.add(new RavelryReferenceCategory("Other", "other-accessories"));
		access.subcategories = accessSub;
		categoryMap.put("other-accessories", accessSub.get(accessSub.size()-1));
		topLevelCategories.add(access);

		// Home
		RavelryReferenceCategory home = new RavelryReferenceCategory("Home", "home");
		categoryMap.put("home", home);
		ArrayList<RavelryReferenceCategory> homeSub = new ArrayList<RavelryReferenceCategory>();

		RavelryReferenceCategory blanket = new RavelryReferenceCategory("Blanket", "blanket");
		categoryMap.put("blanket", blanket);
		ArrayList<RavelryReferenceCategory> blanketSub = new ArrayList<RavelryReferenceCategory>();
		blanketSub.add(new RavelryReferenceCategory("Baby Blanket", "babyblanket"));
		categoryMap.put("babyblanket", blanketSub.get(blanketSub.size()-1));
		blanketSub.add(new RavelryReferenceCategory("Bedspread", "bedspread"));
		categoryMap.put("bedspread", blanketSub.get(blanketSub.size()-1));
		blanketSub.add(new RavelryReferenceCategory("Throw", "throw"));
		categoryMap.put("throw", blanketSub.get(blanketSub.size()-1));
		blanketSub.add(new RavelryReferenceCategory("Other", "other-blanket"));
		categoryMap.put("other-blanket", blanketSub.get(blanketSub.size()-1));
		blanket.subcategories = blanketSub;
		homeSub.add(blanket);

		homeSub.add(new RavelryReferenceCategory("Bookmark", "bookmark"));
		categoryMap.put("bookmark", homeSub.get(homeSub.size()-1));

		RavelryReferenceCategory cleaning = new RavelryReferenceCategory("Cleaning", "cleaning");
		categoryMap.put("cleaning", cleaning);
		ArrayList<RavelryReferenceCategory> cleanSub = new ArrayList<RavelryReferenceCategory>();
		cleanSub.add(new RavelryReferenceCategory("Bath Mitt", "bathmitt"));
		categoryMap.put("bathmitt", cleanSub.get(cleanSub.size()-1));
		cleanSub.add(new RavelryReferenceCategory("Scrubber", "scrubber"));
		categoryMap.put("scrubber", cleanSub.get(cleanSub.size()-1));
		cleanSub.add(new RavelryReferenceCategory("Towel", "towel"));
		categoryMap.put("towel", cleanSub.get(cleanSub.size()-1));
		cleanSub.add(new RavelryReferenceCategory("Washcloth / Dishcloth", "washcloth"));
		categoryMap.put("washcloth", cleanSub.get(cleanSub.size()-1));
		cleanSub.add(new RavelryReferenceCategory("Other", "other-cleaning"));
		categoryMap.put("other-cleaning", cleanSub.get(cleanSub.size()-1));
		cleaning.subcategories = cleanSub;
		homeSub.add(cleaning);

		homeSub.add(new RavelryReferenceCategory("Coaster", "coaster"));
		categoryMap.put("coaster", homeSub.get(homeSub.size()-1));
		homeSub.add(new RavelryReferenceCategory("Containers", "containers"));
		categoryMap.put("containers", homeSub.get(homeSub.size()-1));

		RavelryReferenceCategory cozy = new RavelryReferenceCategory("Cozy", "cozy");
		categoryMap.put("cozy", cozy);
		ArrayList<RavelryReferenceCategory> cozySub = new ArrayList<RavelryReferenceCategory>();
		cozySub.add(new RavelryReferenceCategory("Automobile", "automobile"));
		categoryMap.put("automobile", cozySub.get(cozySub.size()-1));
		cozySub.add(new RavelryReferenceCategory("Bathroom", "bathroom"));
		categoryMap.put("bathroom", cozySub.get(cozySub.size()-1));
		cozySub.add(new RavelryReferenceCategory("Book Cover", "bookcover"));
		categoryMap.put("bookcover", cozySub.get(cozySub.size()-1));
		cozySub.add(new RavelryReferenceCategory("Coffee / Tea Pot", "coffee-teapot"));
		categoryMap.put("coffee-teapot", cozySub.get(cozySub.size()-1));
		cozySub.add(new RavelryReferenceCategory("Cup / Mug", "cup-mug"));
		categoryMap.put("cup-mug", cozySub.get(cozySub.size()-1));
		cozySub.add(new RavelryReferenceCategory("Electronics", "electronics"));
		categoryMap.put("electronics", cozySub.get(cozySub.size()-1));
		cozySub.add(new RavelryReferenceCategory("Food Cozy", "foodcozy"));
		categoryMap.put("foodcozy", cozySub.get(cozySub.size()-1));
		cozySub.add(new RavelryReferenceCategory("Glasses Case", "glassescase"));
		categoryMap.put("glassescase", cozySub.get(cozySub.size()-1));
		cozySub.add(new RavelryReferenceCategory("Hanger Cover", "hangercover"));
		categoryMap.put("hangercover", cozySub.get(cozySub.size()-1));
		cozySub.add(new RavelryReferenceCategory("Hot Water Bottle", "hotwaterbottle"));
		categoryMap.put("hotwaterbottle", cozySub.get(cozySub.size()-1));
		cozySub.add(new RavelryReferenceCategory("Lip Balm", "lipbalm"));
		categoryMap.put("lipbalm", cozySub.get(cozySub.size()-1));
		cozySub.add(new RavelryReferenceCategory("Mature Content Toys", "maturecontenttoys"));
		categoryMap.put("maturecontenttoys", cozySub.get(cozySub.size()-1));
		cozySub.add(new RavelryReferenceCategory("Sports Equipment", "maturecontenttoys"));
		categoryMap.put("maturecontenttoys", cozySub.get(cozySub.size()-1));
		cozySub.add(new RavelryReferenceCategory("Tissue Box Cover", "tissueboxcover"));
		categoryMap.put("tissueboxcover", cozySub.get(cozySub.size()-1));
		cozySub.add(new RavelryReferenceCategory("Other", "other-cozy"));
		categoryMap.put("other-cozy", cozySub.get(cozySub.size()-1));
		cozy.subcategories = cozySub;
		homeSub.add(cozy);

		homeSub.add(new RavelryReferenceCategory("Curtain", "curtain"));
		categoryMap.put("curtain", homeSub.get(homeSub.size()-1));

		RavelryReferenceCategory decor = new RavelryReferenceCategory("Decorative", "decorative");
		categoryMap.put("decorative", decor);
		ArrayList<RavelryReferenceCategory> decorSub = new ArrayList<RavelryReferenceCategory>();
		decorSub.add(new RavelryReferenceCategory("Christmas Stocking", "christmasstocking"));
		categoryMap.put("christmasstocking", decorSub.get(decorSub.size()-1));
		decorSub.add(new RavelryReferenceCategory("Doily", "doily"));
		categoryMap.put("doily", decorSub.get(decorSub.size()-1));
		decorSub.add(new RavelryReferenceCategory("Hanging Ornament", "hangingornament"));
		categoryMap.put("hangingornament", decorSub.get(decorSub.size()-1));
		decorSub.add(new RavelryReferenceCategory("Ornamental Flower", "ornamentalflower"));
		categoryMap.put("ornamentalflower", decorSub.get(decorSub.size()-1));
		decorSub.add(new RavelryReferenceCategory("Picture Frame", "pictureframe"));
		categoryMap.put("pictureframe", decorSub.get(decorSub.size()-1));
		decorSub.add(new RavelryReferenceCategory("Wall Hanging", "wallhanging"));
		categoryMap.put("wallhanging", decorSub.get(decorSub.size()-1));
		decorSub.add(new RavelryReferenceCategory("Wreath", "wreath"));
		categoryMap.put("wreath", decorSub.get(decorSub.size()-1));
		decorSub.add(new RavelryReferenceCategory("Other", "other-decorative"));
		categoryMap.put("other-decorative", decorSub.get(decorSub.size()-1));
		decor.subcategories = decorSub;
		homeSub.add(decor);

		homeSub.add(new RavelryReferenceCategory("Lampshade", "lampshade"));
		categoryMap.put("lampshade", homeSub.get(homeSub.size()-1));
		homeSub.add(new RavelryReferenceCategory("Medical", "medical"));
		categoryMap.put("medical", homeSub.get(homeSub.size()-1));
		homeSub.add(new RavelryReferenceCategory("Pillow", "pillow"));
		categoryMap.put("pillow", homeSub.get(homeSub.size()-1));
		homeSub.add(new RavelryReferenceCategory("Potholder", "potholder"));
		categoryMap.put("potholder", homeSub.get(homeSub.size()-1));
		homeSub.add(new RavelryReferenceCategory("Rug", "rug"));
		categoryMap.put("rug", homeSub.get(homeSub.size()-1));
		homeSub.add(new RavelryReferenceCategory("Sachet", "sachet"));
		categoryMap.put("sachet", homeSub.get(homeSub.size()-1));

		RavelryReferenceCategory table = new RavelryReferenceCategory("Table Setting", "tablesetting");
		categoryMap.put("tablesetting", table);
		ArrayList<RavelryReferenceCategory> tableSub = new ArrayList<RavelryReferenceCategory>();
		tableSub.add(new RavelryReferenceCategory("Napkin", "napkin"));
		categoryMap.put("napkin", tableSub.get(tableSub.size()-1));
		tableSub.add(new RavelryReferenceCategory("Placemat", "placemat"));
		categoryMap.put("placemat", tableSub.get(tableSub.size()-1));
		tableSub.add(new RavelryReferenceCategory("Table Runner", "tablerunner"));
		categoryMap.put("tablerunner", tableSub.get(tableSub.size()-1));
		tableSub.add(new RavelryReferenceCategory("Tablecloth", "tablecloth"));
		categoryMap.put("tablecloth", tableSub.get(tableSub.size()-1));
		tableSub.add(new RavelryReferenceCategory("Other", "other-tablesetting"));
		categoryMap.put("other-tablesetting", tableSub.get(tableSub.size()-1));
		table.subcategories = tableSub;
		homeSub.add(table);

		homeSub.add(new RavelryReferenceCategory("Other", "other-home"));
		categoryMap.put("other-home", homeSub.get(homeSub.size()-1));


		home.subcategories = homeSub;
		topLevelCategories.add(home);

		// Toys
		RavelryReferenceCategory toys = new RavelryReferenceCategory("Toys and Hobbies", "toysandhobbies");
		categoryMap.put("toysandhobbies", toys);
		ArrayList<RavelryReferenceCategory> toysSub = new ArrayList<RavelryReferenceCategory>();
		toysSub.add(new RavelryReferenceCategory("Ball", "ball"));
		categoryMap.put("ball", toysSub.get(toysSub.size()-1));
		toysSub.add(new RavelryReferenceCategory("Blocks", "blocks"));
		categoryMap.put("blocks", toysSub.get(toysSub.size()-1));
		toysSub.add(new RavelryReferenceCategory("Costume", "costume"));
		categoryMap.put("costume", toysSub.get(toysSub.size()-1));

		RavelryReferenceCategory craft = new RavelryReferenceCategory("Craft", "craft");
		categoryMap.put("craft", craft);
		ArrayList<RavelryReferenceCategory> craftSub = new ArrayList<RavelryReferenceCategory>();
		craftSub.add(new RavelryReferenceCategory("Crochet Hook Holder", "crochethookholder"));
		categoryMap.put("crochethookholder", craftSub.get(craftSub.size()-1));
		craftSub.add(new RavelryReferenceCategory("Needle Holder", "needleholder"));
		categoryMap.put("needleholder", craftSub.get(craftSub.size()-1));
		craftSub.add(new RavelryReferenceCategory("Pin Cushion", "pincushion"));
		categoryMap.put("pincushion", craftSub.get(craftSub.size()-1));
		craftSub.add(new RavelryReferenceCategory("Tape Measure Cover", "tapemeasurecover"));
		categoryMap.put("tapemeasurecover", craftSub.get(craftSub.size()-1));
		craftSub.add(new RavelryReferenceCategory("Other", "other-craft"));
		categoryMap.put("other-craft", craftSub.get(craftSub.size()-1));
		craft.subcategories = craftSub;
		toysSub.add(craft);

		toysSub.add(new RavelryReferenceCategory("Doll Clothes", "dollclothes"));
		categoryMap.put("dollclothes", toysSub.get(toysSub.size()-1));
		toysSub.add(new RavelryReferenceCategory("Food", "food"));
		categoryMap.put("food", toysSub.get(toysSub.size()-1));
		toysSub.add(new RavelryReferenceCategory("Game", "game"));
		categoryMap.put("game", toysSub.get(toysSub.size()-1));
		toysSub.add(new RavelryReferenceCategory("Mature Content", "maturecontent"));
		categoryMap.put("maturecontent", toysSub.get(toysSub.size()-1));
		toysSub.add(new RavelryReferenceCategory("Mobile", "mobile"));
		categoryMap.put("mobile", toysSub.get(toysSub.size()-1));
		toysSub.add(new RavelryReferenceCategory("Puppet", "puppet"));
		categoryMap.put("puppet", toysSub.get(toysSub.size()-1));

		RavelryReferenceCategory softies = new RavelryReferenceCategory("Softies", "softies");
		categoryMap.put("softies", softies);
		ArrayList<RavelryReferenceCategory> softiesSub = new ArrayList<RavelryReferenceCategory>();
		softiesSub.add(new RavelryReferenceCategory("Animal", "animal"));
		categoryMap.put("animal", softiesSub.get(softiesSub.size()-1));
		softiesSub.add(new RavelryReferenceCategory("Doll", "doll"));
		categoryMap.put("doll", softiesSub.get(softiesSub.size()-1));
		softiesSub.add(new RavelryReferenceCategory("Plant", "plant"));
		categoryMap.put("plant", softiesSub.get(softiesSub.size()-1));
		softiesSub.add(new RavelryReferenceCategory("Vehicle", "vehicle"));
		categoryMap.put("vehicle", softiesSub.get(softiesSub.size()-1));
		softiesSub.add(new RavelryReferenceCategory("Other","other-softies"));
		categoryMap.put("other-softies", softiesSub.get(softiesSub.size()-1));
		softies.subcategories = softiesSub;
		toysSub.add(softies);

		toysSub.add(new RavelryReferenceCategory("Other", "other-toysandhobbies"));
		categoryMap.put("other-toysandhobbies", toysSub.get(toysSub.size()-1));

		toys.subcategories = toysSub;
		topLevelCategories.add(toys);

		// Pets
		RavelryReferenceCategory pet = new RavelryReferenceCategory("Pet", "pet");
		categoryMap.put("pet", pet);
		ArrayList<RavelryReferenceCategory> petSub = new ArrayList<RavelryReferenceCategory>();
		petSub.add(new RavelryReferenceCategory("Accessory", "accessory"));
		categoryMap.put("accessory", petSub.get(petSub.size()-1));
		petSub.add(new RavelryReferenceCategory("Bedding", "bedding"));
		categoryMap.put("bedding", petSub.get(petSub.size()-1));
		petSub.add(new RavelryReferenceCategory("Clothing", "pet-clothing"));
		categoryMap.put("pet-clothing", petSub.get(petSub.size()-1));
		petSub.add(new RavelryReferenceCategory("Toys", "toys"));
		categoryMap.put("toys", petSub.get(petSub.size()-1));
		petSub.add(new RavelryReferenceCategory("Other", "other-pet"));
		categoryMap.put("other-pet", petSub.get(petSub.size()-1));
		pet.subcategories = petSub;
		topLevelCategories.add(pet);

		//Components
		RavelryReferenceCategory components = new RavelryReferenceCategory("Components", "pattern-component");
		categoryMap.put("pattern-component", components);
		ArrayList<RavelryReferenceCategory> componentsSub = new ArrayList<RavelryReferenceCategory>();
		componentsSub.add(new RavelryReferenceCategory("Afghan block", "afghanblock"));
		categoryMap.put("afghanblock", componentsSub.get(componentsSub.size()-1));
		componentsSub.add(new RavelryReferenceCategory("Applique / Embellishmer", "applique"));
		categoryMap.put("applique", componentsSub.get(componentsSub.size()-1));
		componentsSub.add(new RavelryReferenceCategory("Button", "button"));
		categoryMap.put("button", componentsSub.get(componentsSub.size()-1));
		componentsSub.add(new RavelryReferenceCategory("Chart", "chart"));
		categoryMap.put("chart", componentsSub.get(componentsSub.size()-1));
		componentsSub.add(new RavelryReferenceCategory("Edging", "edging"));
		categoryMap.put("edging", componentsSub.get(componentsSub.size()-1));
		componentsSub.add(new RavelryReferenceCategory("Floral block", "floralblock"));
		categoryMap.put("floralblock", componentsSub.get(componentsSub.size()-1));
		componentsSub.add(new RavelryReferenceCategory("Frog", "frog"));
		categoryMap.put("frog", componentsSub.get(componentsSub.size()-1));
		componentsSub.add(new RavelryReferenceCategory("Insertion", "insertion"));
		categoryMap.put("insertion", componentsSub.get(componentsSub.size()-1));
		componentsSub.add(new RavelryReferenceCategory("Stitch pattern", "stitchpattern"));
		categoryMap.put("stitchpattern", componentsSub.get(componentsSub.size()-1));
		componentsSub.add(new RavelryReferenceCategory("Tutorial", "tutorial"));
		categoryMap.put("tutorial", componentsSub.get(componentsSub.size()-1));
		componentsSub.add(new RavelryReferenceCategory("Other", "other-x-notapattern"));
		categoryMap.put("other-x-notapattern", componentsSub.get(componentsSub.size()-1));
		components.subcategories = componentsSub;
		topLevelCategories.add(components);


		checkedMap = new LinkedHashMap<String, Boolean>();
		checkedMap.put("clothing", false);
		checkedMap.put("coat", false);
		checkedMap.put("dress", false);
		checkedMap.put("intimateapparel", false);
		checkedMap.put("bra", false);
		checkedMap.put("pasties", false);
		checkedMap.put("underwear", false);
		checkedMap.put("other-intimateapparel", false);
		checkedMap.put("leggings", false);
		checkedMap.put("onesies", false);
		checkedMap.put("pants", false);
		checkedMap.put("robe", false);
		checkedMap.put("shorts", false);
		checkedMap.put("shrug", false);
		checkedMap.put("skirt", false);
		checkedMap.put("sleepwear", false);
		checkedMap.put("soakers", false);
		checkedMap.put("sweater", false);
		checkedMap.put("cardigan", false);
		checkedMap.put("pullover", false);
		checkedMap.put("other-sweater", false);
		checkedMap.put("swimwear", false);
		checkedMap.put("tops", false);
		checkedMap.put("sleeveless-top", false);
		checkedMap.put("strapless-top", false);
		checkedMap.put("tee", false);
		checkedMap.put("other-top", false);
		checkedMap.put("vest", false);
		checkedMap.put("other-clothing", false);
		checkedMap.put("accessories", false);
		checkedMap.put("bag", false);
		checkedMap.put("backpack", false);
		checkedMap.put("clutch", false);
		checkedMap.put("drawstring", false);
		checkedMap.put("duffle", false);
		checkedMap.put("laptop", false);
		checkedMap.put("market", false);
		checkedMap.put("messenger", false);
		checkedMap.put("money", false);
		checkedMap.put("purse", false);
		checkedMap.put("tote", false);
		checkedMap.put("wristlet", false);
		checkedMap.put("other-bag", false);
		checkedMap.put("belt", false);
		checkedMap.put("feet-legs", false);
		checkedMap.put("booties", false);
		checkedMap.put("legwarmers", false);
		checkedMap.put("slippers", false);
		checkedMap.put("socks", false);
		checkedMap.put("ankle-sock", false);
		checkedMap.put("knee-highs", false);
		checkedMap.put("mid-calf", false);
		checkedMap.put("thigh-high", false);
		checkedMap.put("toeless", false);
		checkedMap.put("tube", false);
		checkedMap.put("other-socks", false);
		checkedMap.put("spats", false);
		checkedMap.put("other-feet-legs", false);
		checkedMap.put("hands", false);
		checkedMap.put("convertible", false);
		checkedMap.put("cuffs", false);
		checkedMap.put("fingerless", false);
		checkedMap.put("gloves", false);
		checkedMap.put("mittens", false);
		checkedMap.put("muff", false);
		checkedMap.put("other-hands", false);
		checkedMap.put("hat", false);
		checkedMap.put("balaclava", false);
		checkedMap.put("beanie-toque", false);
		checkedMap.put("beret-tam", false);
		checkedMap.put("billed", false);
		checkedMap.put("bonnet", false);
		checkedMap.put("brimmed", false);
		checkedMap.put("cloche", false);
		checkedMap.put("earflap", false);
		checkedMap.put("pixie", false);
		checkedMap.put("stocking", false);
		checkedMap.put("yarmulke", false);
		checkedMap.put("other-hat", false);
		checkedMap.put("jewelry", false);
		checkedMap.put("ankle", false);
		checkedMap.put("bracelet", false);
		checkedMap.put("brooch", false);
		checkedMap.put("earrings", false);
		checkedMap.put("necklace", false);
		checkedMap.put("ring", false);
		checkedMap.put("other-jewelry", false);
		checkedMap.put("neck-torso", false);
		checkedMap.put("bib", false);
		checkedMap.put("cape", false);
		checkedMap.put("collar", false);
		checkedMap.put("cowl", false);
		checkedMap.put("necktie", false);
		checkedMap.put("poncho", false);
		checkedMap.put("scarf", false);
		checkedMap.put("shawl-wrap", false);
		checkedMap.put("other-neck-torso", false);
		checkedMap.put("headwear", false);
		checkedMap.put("earwarmers", false);
		checkedMap.put("eyemask", false);
		checkedMap.put("hairaccessories", false);
		checkedMap.put("headband", false);
		checkedMap.put("headwrap", false);
		checkedMap.put("kerchief", false);
		checkedMap.put("snood", false);
		checkedMap.put("other-headwear", false);
		checkedMap.put("other-accessories", false);
		checkedMap.put("home", false);
		checkedMap.put("blanket", false);
		checkedMap.put("babyblanket", false);
		checkedMap.put("bedspread", false);
		checkedMap.put("throw", false);
		checkedMap.put("other-blanket", false);
		checkedMap.put("bookmark", false);
		checkedMap.put("cleaning", false);
		checkedMap.put("bathmitt", false);
		checkedMap.put("scrubber", false);
		checkedMap.put("towel", false);
		checkedMap.put("washcloth", false);
		checkedMap.put("other-cleaning", false);
		checkedMap.put("coaster", false);
		checkedMap.put("containers", false);
		checkedMap.put("cozy", false);
		checkedMap.put("automobile", false);
		checkedMap.put("bathroom", false);
		checkedMap.put("bookcover", false);
		checkedMap.put("coffee-teapot", false);
		checkedMap.put("cup-mug", false);
		checkedMap.put("electronics", false);
		checkedMap.put("foodcozy", false);
		checkedMap.put("glassescase", false);
		checkedMap.put("hangercover", false);
		checkedMap.put("hotwaterbottle", false);
		checkedMap.put("lipbalm", false);
		checkedMap.put("maturecontenttoys", false);
		checkedMap.put("sportsequipment", false);
		checkedMap.put("tissueboxcover", false);
		checkedMap.put("other-cozy", false);
		checkedMap.put("curtain", false);
		checkedMap.put("decorative", false);
		checkedMap.put("christmasstocking", false);
		checkedMap.put("doily", false);
		checkedMap.put("hangingornament", false);
		checkedMap.put("ornamentalflower", false);
		checkedMap.put("pictureframe", false);
		checkedMap.put("wallhanging", false);
		checkedMap.put("wreath", false);
		checkedMap.put("other-decorative", false);
		checkedMap.put("lampshade", false);
		checkedMap.put("medical", false);
		checkedMap.put("pillow", false);
		checkedMap.put("potholder", false);
		checkedMap.put("rug", false);
		checkedMap.put("sachet", false);
		checkedMap.put("tablesetting", false);
		checkedMap.put("napkin", false);
		checkedMap.put("placemat", false);
		checkedMap.put("tablerunner", false);
		checkedMap.put("tablecloth", false);
		checkedMap.put("other-tablesetting", false);
		checkedMap.put("other-home", false);
		checkedMap.put("toysandhobbies", false);
		checkedMap.put("ball", false);
		checkedMap.put("blocks", false);
		checkedMap.put("costume", false);
		checkedMap.put("craft", false);
		checkedMap.put("crochethookholder", false);
		checkedMap.put("needleholder", false);
		checkedMap.put("pincushion", false);
		checkedMap.put("tapemeasurecover", false);
		checkedMap.put("other-craft", false);
		checkedMap.put("dollclothes", false);
		checkedMap.put("food", false);
		checkedMap.put("game", false);
		checkedMap.put("maturecontent", false);
		checkedMap.put("mobile", false);
		checkedMap.put("puppet", false);
		checkedMap.put("softies", false);
		checkedMap.put("animal", false);
		checkedMap.put("doll", false);
		checkedMap.put("plant", false);
		checkedMap.put("vehicle", false);
		checkedMap.put("other-softies", false);
		checkedMap.put("other-toysandhobbies", false);
		checkedMap.put("pet", false);
		checkedMap.put("accessory", false);
		checkedMap.put("bedding", false);
		checkedMap.put("pet-clothing", false);
		checkedMap.put("toys", false);
		checkedMap.put("other-pet", false);
		checkedMap.put("pattern-component", false);
		checkedMap.put("afghanblock", false);
		checkedMap.put("applique", false);
		checkedMap.put("button", false);
		checkedMap.put("chart", false);
		checkedMap.put("edging", false);
		checkedMap.put("floralblock", false);
		checkedMap.put("frog", false);
		checkedMap.put("insertion", false);
		checkedMap.put("stitchpattern", false);
		checkedMap.put("tutorial", false);
		checkedMap.put("other-x-notapattern", false);
	}

	public String getEntirePartialString() {
		String result = "";
		for(int i = 0; i < topLevelCategories.size(); i++) {
			String temp = getPartialString(topLevelCategories.get(i).value, "0");
			if(!result.isEmpty() && !temp.isEmpty()) {
				result += ", ";
			}
			result += temp;
		}
		if(result.isEmpty()) {
			return "All Categories";
		}
		return result;
	}
	
	public String getPartialString(String val, String beginning) {

		RavelryReferenceCategory topCat = categoryMap.get(val);
		// Oh shit case
		if(topCat == null) {
			return "";
		}
		String display = "";

		if(!val.equals(beginning)) {
			if(checkedMap.get(val)) {
				display += topCat.name;
			}
		}

		if(topCat.subcategories != null && topCat.subcategories.size() > 0) {

			for(int i = 0; i < topCat.subcategories.size(); i++) {
				String temp = getPartialString(topCat.subcategories.get(i).value, beginning);
				if(!temp.isEmpty()) {
					if(!display.isEmpty()) {
						display += ", ";
					}
					display += temp + ", ";
				}
			}
			display = display.trim();
			if(display.length() > 2 && display.substring(display.length()-1).equals(",")) {
				display = display.substring(0, display.length()-1);						
			}
		}
		String temp = display.replace(", , ", ", ");
		if(!temp.isEmpty() && temp.substring(temp.length()-1).equals(", ")) {
			return temp.substring(0, temp.length()-2);
		}
		return temp;
	}

	public boolean isCategoryPartial(String val, String beginning) {
		RavelryReferenceCategory cat = categoryMap.get(val);
		// Oh shit return
		if(cat == null) {
			return false;
		}
		if(cat.subcategories == null || cat.subcategories.isEmpty()) {
			if(!val.equals(beginning)) {
				if(isCategoryChecked(val)) {
					return true;
				}
			}
			return false;
		}
		for(int i = 0; i < cat.subcategories.size(); i++) {
			if(isCategoryPartial(cat.subcategories.get(i).value, beginning)) {
				return true;
			}
			if(isCategoryChecked(cat.subcategories.get(i).value)) {
				return true;
			}
		}
		return false;
	}

}
