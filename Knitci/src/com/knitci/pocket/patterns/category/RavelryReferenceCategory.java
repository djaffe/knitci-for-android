package com.knitci.pocket.patterns.category;

import java.io.Serializable;
import java.util.ArrayList;

public class RavelryReferenceCategory implements Serializable {


	private static final long serialVersionUID = 7768465357131298303L;
	public final String name;
	public ArrayList<RavelryReferenceCategory> subcategories;
	public boolean checked;
	public final String value;
	
	public RavelryReferenceCategory(String name, String value) {
		this.name = name;
		subcategories = null;
		checked = false;
		this.value = value;
	}
	
}
