package com.knitci.pocket.patterns.category;

import java.util.ArrayList;

import object.CheckBoxColorfulListener;
import object.CheckBoxColorfulPostback;
import object.CheckBoxSupreme;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.patterns.manager.PatternManager;

public class PatternTopLevelCategoryActivity extends Activity implements OnClickListener, CheckBoxColorfulPostback {

	PatternManager patternManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pattern_category);
		patternManager = KnitciApplication.class.cast(getApplication()).getPatternManager();
	}

	@Override
	protected void onResume() {
		super.onResume();
		ArrayList<RavelryReferenceCategory> categories = KnitciApplication.class.cast(this.getApplication()).getPatternManager().getReferenceCategories();

		LinearLayout sv = LinearLayout.class.cast(findViewById(R.id.categories_holder));
		if(sv.getChildCount() == 0) {
			for(int i = 0; i < categories.size(); i++) {
				CheckBoxSupreme tv = new CheckBoxSupreme(getApplicationContext());
				tv.setChecked(patternManager.getCategoryManager().isCategoryChecked(categories.get(i).value));
				tv.setCheckBoxColorfulListener(this);
				tv.setText(categories.get(i).name);
				tv.setSubText(patternManager.getCategoryManager().getPartialString(categories.get(i).value, categories.get(i).value));
				tv.setTag(categories.get(i).value);
				tv.setOnClickListener(this);
				sv.addView(tv);
			}
		}
		else {
			for(int i = 0; i < sv.getChildCount(); i++) {
				CheckBoxSupreme tv = CheckBoxSupreme.class.cast(sv.getChildAt(i));
				tv.setSubText(patternManager.getCategoryManager().getPartialString(categories.get(i).value, categories.get(i).value));
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pattern_category, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		TextView tv = TextView.class.cast(v);
		CheckBoxSupreme supreme = CheckBoxSupreme.class.cast(tv.getTag());
		String value = (String) supreme.getTag();
		RavelryReferenceCategory cat = patternManager.getCategoryManager().getReferenceCategory(value);

		Intent intent = new Intent(this, PatternChoosableCategoryActivity.class);
		intent.putExtra("CATEGORY", cat);
		startActivity(intent);
		overridePendingTransition(R.anim.animation_right_to_left, R.anim.animation_left_to_right);
	}

	@Override
	public boolean checkBoxClicked(String value, boolean checked) {
		patternManager.getCategoryManager().setCategoryChecked(value, checked);
		Toast toast = Toast.makeText(getApplicationContext(), patternManager.getCategoryManager().getEnabledCategoryValues().toString(), Toast.LENGTH_SHORT);
		toast.show();
		return true;
	}

}
