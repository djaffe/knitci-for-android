package com.knitci.pocket.patterns;

import java.util.ArrayList;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ImageView.ScaleType;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.knitci.pocket.KnitciApplication;

public class PatternImageAdapter extends PagerAdapter {

	public ArrayList<String> urls;
	OnClickListener listener;
	Context context;
	
	public PatternImageAdapter(Context context) {
		this.context = context;
		
	}

	@Override
	public int getCount() {
		if(urls != null) {
			return urls.size();
		}
		return 0;
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == (ImageView) arg1;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		NetworkImageView imageView = new NetworkImageView(context);
		imageView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		String url = urls.get(position);
		ImageLoader loader = KnitciApplication.class.cast(context).getImageLoader();
		imageView.setImageUrl(url, loader);
		imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		((ViewPager) container).addView(imageView, 0);
		imageView.setOnClickListener(listener);
		imageView.setTag(url);
		
		return imageView;
	}

	public void setItemClickedListener(OnClickListener listener) {
		this.listener = listener;
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((ImageView) object);
	}
}
