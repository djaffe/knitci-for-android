package com.knitci.pocket.patterns;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPattern;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject.ProjectPhotoUrlSize;
import com.knitci.pocket.util.StandardConvert;

public class PatternSearchAdapter extends BaseAdapter {

	ArrayList<RavelryPattern> patterns;
	Context context;
	LayoutInflater inflater;

	public PatternSearchAdapter(Context context, ArrayList<RavelryPattern> patterns) {
		this.context = context;
		this.patterns = patterns;
	}

	public void empty() {
		if(patterns != null) {
			patterns.clear();
			notifyDataSetChanged();
		}
	}

	@Override
	public int getCount() {
		return patterns.size();
	}

	@Override
	public Object getItem(int arg0) {
		return patterns.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		RavelryPattern pattern = patterns.get(arg0);
		View v = arg1;
		if(v == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.patternsearch_pattern_item, arg2, false);
			ViewHolder holder = new ViewHolder();
			//holder.img = ImageView.class.cast(v.findViewById(R.id.patternsearch_item_image));
			holder.img = NetworkImageView.class.cast(v.findViewById(R.id.patternsearch_item_image));
			v.setTag(holder);
		}

		if(pattern != null) {
			KnitciApplication app = (KnitciApplication)context.getApplicationContext();
			ViewHolder holder = (ViewHolder) v.getTag();
			String url = null;
			if(pattern.getFirstPhotos() == null) {

			}
			else if(pattern.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.THUMBNAIL) != null
					&& pattern.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.THUMBNAIL).getUrl() != null) {
				url = pattern.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.THUMBNAIL).getUrl();
			}
			else if(pattern.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.SQUARE) != null
					&& pattern.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.SQUARE).getUrl() != null) {
				url = pattern.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.SQUARE).getUrl();
			}
			else if(pattern.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.SMALL) != null
					&& pattern.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.SMALL).getUrl() != null) {
				url = pattern.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.SMALL).getUrl();
			}
			else if(pattern.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.MEDIUM) != null
					&& pattern.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.MEDIUM).getUrl() != null) {
				url = pattern.getFirstPhotos().getPhoto(ProjectPhotoUrlSize.MEDIUM).getUrl();
			}
			if(url != null) {
				holder.img.setImageUrl(url, app.getPatternLoader());
				//app.getPatternLoader().get(url, ImageLoader.getImageListener(holder.img, R.drawable.ic_launcher, R.drawable.ic_launcher));
			}
		}
		return v;

	}

	static class ViewHolder {
		//ImageView img;
		NetworkImageView img;
	}

}
