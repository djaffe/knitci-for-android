package com.knitci.pocket.patterns;

import java.util.ArrayList;

import object.DumbSpinner;
import object.IDumbSpinner;
import object.KnitciActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.edmodo.rangebar.RangeBar;
import com.edmodo.rangebar.RangeBar.OnRangeBarChangeListener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.patterns.attribute.PatternTopLevelAttributeActivity;
import com.knitci.pocket.patterns.category.PatternTopLevelCategoryActivity;
import com.knitci.pocket.patterns.demographic.PatternTopLevelDemographicActivity;
import com.knitci.pocket.patterns.manager.PatternManager;
import com.knitci.pocket.ravelry.RavelryManager;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPattern;

import cz.destil.settleup.gui.MultiSpinner;
import cz.destil.settleup.gui.MultiSpinner.MultiSpinnerListener;

public class PatternSearchActivity extends KnitciActivity implements OnItemSelectedListener, OnItemClickListener, 
OnScrollListener, MultiSpinnerListener, IDumbSpinner, OnRangeBarChangeListener {

	RavelryManager ravelryManager;
	PatternManager patternManager;
	PatternSearchAdapter adapter;
	boolean loadingMore;

	PatternSearchCriteria criteria;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pattern_search);

		ravelryManager = KnitciApplication.class.cast(getApplicationContext()).getRavelryManager();
		patternManager = KnitciApplication.class.cast(getApplication()).getPatternManager();
		GridView gv = GridView.class.cast(findViewById(R.id.patternsearch_gridview));
		gv.setOnScrollListener(this);
		setupSlidingMenu();
		setupSecondaryMenu();
		stealAsyncListening();
		criteria = new PatternSearchCriteria();
		if(patternManager.getDumbPatterns() != null && !patternManager.getDumbPatterns().isEmpty()) {
			loadingMore = true;
			populatePatterns(patternManager.getDumbPatterns());
		}
		else {
			loadingMore = false;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.pattern_search, menu);
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		stealAsyncListening();
		DumbSpinner dumbSpinner = DumbSpinner.class.cast(findViewById(R.id.slidingpattern_category_dumbspinner));
		dumbSpinner.setText(patternManager.getCategoryManager().getEntirePartialString());
		criteria.setCategories(patternManager.getCategoryManager().getEnabledCategoryValues());

		DumbSpinner dumbSpinnerAttributes = DumbSpinner.class.cast(findViewById(R.id.slidingpattern_attribute_dumbspinner));
		dumbSpinnerAttributes.setText(patternManager.getAttributeManager().getEntirePartialString());
		criteria.setAttributes(patternManager.getAttributeManager().getEnabledAttributeValues());

		DumbSpinner dumbSpinnerFit = DumbSpinner.class.cast(findViewById(R.id.slidingpattern_fit_dumbspinner));
		dumbSpinnerFit.setText(patternManager.getDemographicManager().getEntirePartialString());
		criteria.setFits(patternManager.getDemographicManager().getEnabledDemographicValues());
	}


	protected void setupSecondaryMenu() {
		//setBehindContentView(R.layout.slidingmenu_pattern_layout);
		SlidingMenu menu = getSlidingMenu();
		menu.setMode(SlidingMenu.LEFT_RIGHT);
		menu.setSecondaryMenu(R.layout.slidingmenu_pattern_layout);
		
		//menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		//menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		menu.setFadeDegree(0.35f);
		setSlidingActionBarEnabled(true);
		//menu.setMenu(R.layout.slidingmenu_pattern_layout);

		// Query
		EditText query = (EditText) findViewById(R.id.slidingpattern_search_string);
		query.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) { }

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

			@Override
			public void afterTextChanged(Editable s) { criteria.setQuery(s.toString()); } 
		});

		// Sorting filter
		Spinner sortSpinner = Spinner.class.cast(findViewById(R.id.slidingpattern_sort_spinner));
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
				R.array.patternsearch_sort_options, R.layout.simple_spinner_item_white);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sortSpinner.setAdapter(adapter);
		sortSpinner.setTag(EFilter.SORTING);
		sortSpinner.setOnItemSelectedListener(this);

		// Only with photos
		ToggleButton photoToggle = (ToggleButton) findViewById(R.id.slidingpattern_pictureonly_toggle);
		photoToggle.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				criteria.setOnlyPhotos(isChecked);
			}
		});

		// Submit button
		Button submitBtn = Button.class.cast(findViewById(R.id.slidingpattern_submit));
		submitBtn.setOnClickListener(this);
		Button clearBtn = (Button) findViewById(R.id.slidingpattern_clear);
		clearBtn.setOnClickListener(this);
		
		GridView gv = GridView.class.cast(findViewById(R.id.patternsearch_gridview));
		gv.setOnItemClickListener(this);

		// Craft filter
		ArrayList<String> strings = new ArrayList<String>();
		strings.add("Knitting");
		strings.add("Crocheting");
		strings.add("Machine Knitting");
		strings.add("Loom Knitting");
		MultiSpinner multiSpinner = (MultiSpinner) findViewById(R.id.slidingpattern_craft_multi_spinner);
		multiSpinner.setTag(EFilter.CRAFTS);
		multiSpinner.setItems(strings, "Any Craft", this);

		// Category filter
		DumbSpinner dumbSpinner = DumbSpinner.class.cast(findViewById(R.id.slidingpattern_category_dumbspinner));
		dumbSpinner.setHandler(this);
		dumbSpinner.setTag("CATEGORY");

		// Attribute filter
		DumbSpinner dumbSpinnerAttributes = DumbSpinner.class.cast(findViewById(R.id.slidingpattern_attribute_dumbspinner));
		dumbSpinnerAttributes.setHandler(this);
		dumbSpinnerAttributes.setTag("ATTRIBUTE");

		// Availability filter
		ArrayList<String> availFilters = new ArrayList<String>();
		availFilters.add("Free");
		availFilters.add("Purchase online");
		availFilters.add("Purchase in print");
		availFilters.add("Ravelry download");
		availFilters.add("In my library");
		MultiSpinner availSpin = (MultiSpinner) findViewById(R.id.slidingpattern_availability_multi_spinner);
		availSpin.setItems(availFilters, "Any Availability", this);
		availSpin.setTag(EFilter.AVAILABILITY);

		// Fit filter
		DumbSpinner dumbSpinnerFit = DumbSpinner.class.cast(findViewById(R.id.slidingpattern_fit_dumbspinner));
		dumbSpinnerFit.setHandler(this);
		dumbSpinnerFit.setTag("FIT");

		// Yarn Weight Filter
		MultiSpinner yarnWeightSpin = (MultiSpinner) findViewById(R.id.slidingpattern_yarnweight_multi_spinner);
		ArrayList<String> yarnWeights = new ArrayList<String>();
		yarnWeights.add("Thread");
		yarnWeights.add("Cobweb / 1 ply");
		yarnWeights.add("Lace / 2 ply");
		yarnWeights.add("Light Fingering / 3 ply");
		yarnWeights.add("Fingering / 4 ply");
		yarnWeights.add("Sport / 5 ply");
		yarnWeights.add("DK / 8 ply");
		yarnWeights.add("Worsted / 10 ply");
		yarnWeights.add("Aran / 10 ply");
		yarnWeights.add("Bulky / 12 ply");
		yarnWeights.add("Super Bulky");
		yarnWeights.add("Not Specified");
		yarnWeightSpin.setItems(yarnWeights, "Any Yarn Weight", this);
		yarnWeightSpin.setTag(EFilter.YARNWEIGHT);

		// yardage and meterage
		final EditText minLen = (EditText) findViewById(R.id.slidingpattern_minlength_et);
		final EditText maxLen = (EditText) findViewById(R.id.slidingpattern_maxlength_et);
		final Spinner unitLen = (Spinner) findViewById(R.id.slidingpattern_lengthunit_spinner);

		TextWatcher lengthWatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

			@Override
			public void afterTextChanged(Editable s) {
				int min;
				int max;
				if(minLen.getText().toString().isEmpty()) {
					min = 0;
				}
				else {
					min = Integer.parseInt(minLen.getText().toString());
				}

				if(maxLen.getText().toString().isEmpty()) {
					max = 0;
				}
				else {
					max = Integer.parseInt(maxLen.getText().toString());
				}
				if(unitLen.getSelectedItem().toString().equals("Yards")) {
					criteria.setYardageIn(min, max);
				}
				else {
					criteria.setMeterageIn(min, max);
				}
			}
		};

		unitLen.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				int min;
				int max;
				if(minLen.getText().toString().isEmpty()) {
					min = 0;
				}
				else {
					min = Integer.parseInt(minLen.getText().toString());
				}

				if(maxLen.getText().toString().isEmpty()) {
					max = 0;
				}
				else {
					max = Integer.parseInt(maxLen.getText().toString());
				}
				if(unitLen.getSelectedItem().toString().equals("Yards")) {
					criteria.setYardageIn(min, max);
				}
				else {
					criteria.setMeterageIn(min, max);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) { }
		});

		minLen.addTextChangedListener(lengthWatcher);
		maxLen.addTextChangedListener(lengthWatcher);

		// Difficulty
		RangeBar range = (RangeBar) findViewById(R.id.slidingpattern_difficulty_rangebar);
		range.setOnRangeBarChangeListener(this);

		// Yardage filter
		Spinner yarnLengthSpinner = (Spinner) findViewById(R.id.slidingpattern_lengthunit_spinner);
		ArrayList<String> yarnLengthUnits = new ArrayList<String>();
		yarnLengthUnits.add("Yards");
		yarnLengthUnits.add("Meters");
		ArrayAdapter<String> yarnLengthUnitAdapter = new ArrayAdapter<String>(this,R.layout.simple_spinner_item_white, yarnLengthUnits);
		yarnLengthSpinner.setAdapter(yarnLengthUnitAdapter);


		// My notebook
		MultiSpinner mynotebookSpin = (MultiSpinner) findViewById(R.id.slidingpattern_mynotebook_multi_spinner);
		ArrayList<String> mynotebook = new ArrayList<String>();
		mynotebook.add("In my queue");
		mynotebook.add("In my projects");
		mynotebook.add("In my favorites");
		mynotebook.add("Yarn in my stash");
		mynotebook.add("Favorite designers");
		mynotebookSpin.setItems(mynotebook, "Entire Notebook", this);
		mynotebookSpin.setTag(EFilter.NOTEBOOK);

		// Pattern Source Type
		MultiSpinner sourceSpin = (MultiSpinner) findViewById(R.id.slidingpattern_source_multi_spinner);
		ArrayList<String> source = new ArrayList<String>();
		source.add("Book");
		source.add("Magazine");
		source.add("Pamphlet/Booklet");
		source.add("Website");
		source.add("Webzine");
		source.add("eBook");
		sourceSpin.setItems(source, "Any Source", this);
		sourceSpin.setTag(EFilter.SOURCES);
		// Fiber
		MultiSpinner fiberSpin = (MultiSpinner) findViewById(R.id.slidingpattern_fiber_multi_spinner);
		fiberSpin.setTag(EFilter.FIBERS);
		ArrayList<String> fibers = new ArrayList<String>();
		fibers.add("Acrylic");
		fibers.add("Alpaca");
		fibers.add("Angora");
		fibers.add("Bamboo");
		fibers.add("Bison");
		fibers.add("Camel");
		fibers.add("Cashmere");
		fibers.add("Cotton");
		fibers.add("Hemp");
		fibers.add("Linen");
		fibers.add("Llama");
		fibers.add("Merino");
		fibers.add("Metallic");
		fibers.add("Microfiber");
		fibers.add("Mohair");
		fibers.add("Nylon");
		fibers.add("Other");
		fibers.add("Plant Fiber");
		fibers.add("Polyester");
		fibers.add("Qiviut");
		fibers.add("Rayon");
		fibers.add("Silk");
		fibers.add("Soy");
		fibers.add("Tencel");
		fibers.add("Wool");
		fibers.add("Yak");
		fiberSpin.setItems(fibers, "Any Fiber", this);

		// hooksize
		MultiSpinner hookSpin = (MultiSpinner) findViewById(R.id.slidingpattern_hooksize_multi_spinner);
		hookSpin.setTag(EFilter.HOOKS);
		ArrayList<String> hooks = new ArrayList<String>();
		hooks.add("Small hooks");
		hooks.add("2.25 mm (B)");
		hooks.add("2.5 mm");
		hooks.add("2.75 mm (C)");
		hooks.add("3.0 mm");
		hooks.add("3.25 mm (D)");
		hooks.add("3.5 mm (E)");
		hooks.add("3.75 mm (F)");
		hooks.add("4.0 mm (G)");
		hooks.add("4.5 mm");
		hooks.add("5.0 mm (H)");
		hooks.add("5.5 mm (I)");
		hooks.add("6.0 mm (J)");
		hooks.add("6.5 mm (K)");
		hooks.add("7.0 mm");
		hooks.add("7.5 mm");
		hooks.add("8.0 mm (L)");
		hooks.add("9.0 mm (M/N)");
		hooks.add("Large hooks");
		hookSpin.setItems(hooks, "Any Hook Size", this);

		// needlesize
		MultiSpinner needleSpin = (MultiSpinner) findViewById(R.id.slidingpattern_needlesize_multi_spinner);
		needleSpin.setTag(EFilter.NEEDLES);
		ArrayList<String> needles = new ArrayList<String>();
		needles.add("US 8/0 - 0.5 mm");
		needles.add("US 6/0 - 0.75 mm");
		needles.add("US 5/0 - 1.0 mm");
		needles.add("US 4/0 - 1.25 mm");
		needles.add("US 000 - 1.5 mm");
		needles.add("US 00 - 1.75 mm");
		needles.add("US 0 - 2.0 mm");
		needles.add("US 1 - 2.25 mm");
		needles.add("US 1 1/2 - 2.5 mm");
		needles.add("US 2 - 2.75 mm");
		needles.add("US 2 1/2 - 3.0 mm");
		needles.add("US 3 - 3.25 mm");
		needles.add("US 4 - 3.5 mm");
		needles.add("US 5 - 3.75 mm");
		needles.add("US 6 - 4.0 mm");
		needles.add("US 7 - 4.5 mm");
		needles.add("US 8 - 5.0 mm");
		needles.add("US 9 - 5.5 mm");
		needles.add("US 10 - 6.0 mm");
		needles.add("US 10 1/2 - 6.5 mm");
		needles.add("7.0 mm");
		needles.add("7.5 mm");
		needles.add("US 11 - 8.0 mm");
		needles.add("US 13 - 9.0 mm");
		needles.add("US 15 - 10.0 mm");
		needles.add("US 17 - 12.75 mm");
		needles.add("US 19 - 15.0mm");
		needles.add("US 35 - 19.0 mm");
		needles.add("US 50 - 25.0 mm");
		needleSpin.setItems(needles, "Any Needle Size", this);

		// Rating
		RatingBar ratingBar = (RatingBar) findViewById(R.id.slidingpattern_rating_ratingbar);
		ratingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {

			@Override
			public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
				criteria.setRating((int)rating);
			}
		});

		// Languages
		MultiSpinner languageSpin = (MultiSpinner) findViewById(R.id.slidingpattern_language_multi_spinner);
		languageSpin.setTag(EFilter.LANGUAGES);
		ArrayList<String> languages = new ArrayList<String>();
		languages.add("English");
		languages.add("Universal");
		languages.add("Chinese");
		languages.add("Czech");
		languages.add("Danish");
		languages.add("Dutch");
		languages.add("Estonian");
		languages.add("Finnish");
		languages.add("French");
		languages.add("German");
		languages.add("Hungarian");
		languages.add("Icelandic");
		languages.add("Italian");
		languages.add("Japanese");
		languages.add("Norwegian");
		languages.add("Polish");
		languages.add("Portuguese");
		languages.add("Russian");
		languages.add("Slovak");
		languages.add("Spanish");
		languages.add("Swedish");
		languageSpin.setItems(languages, "Any Language", this);

		// Designer
		EditText designer = (EditText) findViewById(R.id.slidingpattern_designer_et);
		designer.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) { }

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

			@Override
			public void afterTextChanged(Editable s) {
				criteria.setDesignerName(s.toString());
			}
		});

		// Keywords

		EditText keywords = (EditText) findViewById(R.id.slidingpattern_keywords_et);
		keywords.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) { }

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

			@Override
			public void afterTextChanged(Editable s) {
				criteria.setKeywords(s.toString());				
			}
		});
	}

	private void submitSearch() {

		if(adapter != null) {
			adapter.empty();
		}
		patternManager.lastLoadedPage = 0;
		loadingMore = true;
		//ravelryManager.retrievePatterns("", patternManager.lastLoadedPage+1, 100, "", "popularity");
		ravelryManager.retrievePatterns(criteria, patternManager.lastLoadedPage, 100);
	}

	public void populatePatterns(ArrayList<RavelryPattern> patterns) {
		if(patterns == null) {

		}
		else if(patterns.isEmpty()) {
			Toast toast = Toast.makeText(getApplicationContext(), "No Patterns Found", Toast.LENGTH_LONG);
			toast.show();
		}
		else if(adapter == null) {
			adapter = new PatternSearchAdapter(getApplicationContext(), patterns);
			GridView gv = GridView.class.cast(findViewById(R.id.patternsearch_gridview));
			gv.setOnItemSelectedListener(this);
			gv.setAdapter(adapter);
		}
		else {
			adapter.patterns = patterns;
			adapter.notifyDataSetChanged();
		}
		loadingMore = false;
	}
	
	private void clearSearchCriteria() {
		criteria = new PatternSearchCriteria();
		
		EditText query = (EditText) findViewById(R.id.slidingpattern_search_string);
		query.setText("");
		
		Spinner sort = (Spinner) findViewById(R.id.slidingpattern_sort_spinner);
		sort.setSelection(0);
		
		ToggleButton photo = (ToggleButton) findViewById(R.id.slidingpattern_pictureonly_toggle);
		photo.setChecked(false);
		
		MultiSpinner craft = (MultiSpinner) findViewById(R.id.slidingpattern_craft_multi_spinner);
		craft.reset();
		
		patternManager.getCategoryManager().clearChecks();
		patternManager.getAttributeManager().clearChecks();
		patternManager.getDemographicManager().clearChecks();
		
		MultiSpinner avail = (MultiSpinner) findViewById(R.id.slidingpattern_availability_multi_spinner);
		avail.reset();
		
		MultiSpinner weight = (MultiSpinner) findViewById(R.id.slidingpattern_yarnweight_multi_spinner);
		weight.reset();
		
		EditText min = (EditText) findViewById(R.id.slidingpattern_minlength_et);
		min.setText("");
		
		EditText max = (EditText) findViewById(R.id.slidingpattern_maxlength_et);
		max.setText("");
		
		Spinner unit = (Spinner) findViewById(R.id.slidingpattern_lengthunit_spinner);
		unit.setSelection(0);
		
		MultiSpinner note = (MultiSpinner) findViewById(R.id.slidingpattern_mynotebook_multi_spinner);
		note.reset();
		MultiSpinner source = (MultiSpinner) findViewById(R.id.slidingpattern_source_multi_spinner);;
		source.reset();
		MultiSpinner fiber = (MultiSpinner) findViewById(R.id.slidingpattern_fiber_multi_spinner);
		fiber.reset();
		MultiSpinner hook = (MultiSpinner) findViewById(R.id.slidingpattern_hooksize_multi_spinner);
		hook.reset();
		MultiSpinner needle = (MultiSpinner) findViewById(R.id.slidingpattern_needlesize_multi_spinner);
		needle.reset();
		
		RatingBar rating = (RatingBar) findViewById(R.id.slidingpattern_rating_ratingbar);
		rating.setRating(0);
		
		RangeBar diff = (RangeBar) findViewById(R.id.slidingpattern_difficulty_rangebar);
		diff.setThumbIndices(0, 10);
		
		MultiSpinner lang = (MultiSpinner) findViewById(R.id.slidingpattern_language_multi_spinner);
		lang.reset();
		
		EditText design = (EditText) findViewById(R.id.slidingpattern_designer_et);
		design.setText("");
		EditText key = (EditText) findViewById(R.id.slidingpattern_keywords_et);
		key.setText("");
		
		
		
	}

	@Override
	public void processFinish(RavelryResponse response) {
		switch(response.task) {
		case PATTERNS:
			populatePatterns(patternManager.getDumbPatterns());
			break;
		}

	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.slidingpattern_submit:
			InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); 
			inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
			
			submitSearch();
			break;
		case R.id.slidingpattern_clear:
			clearSearchCriteria();
			break;
		default:
			super.onClick(v);
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long arg3) {
		Spinner spinner = (Spinner) parent;
		if(spinner == null) {
			return;
		}
		EFilter filter = (EFilter) spinner.getTag();
		if(filter == null) {
			return;
		}

		switch(filter) {
		case SORTING:
			criteria.setSorting(spinner.getSelectedItem().toString());
			break;
		default:
			break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> adapterView) {
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		GridView gv = GridView.class.cast(findViewById(R.id.patternsearch_gridview));
		Intent intent = new Intent(this, PatternDetailActivity.class);
		intent.putExtra("PATTERNID", adapter.patterns.get(arg2).getPatternId());
		startActivity(intent);
	}

	@Override
	public void onScroll(AbsListView arg0, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {

		if (adapter == null) {
			return ;
		}

		if (adapter.getCount() == 0) {
			return ;
		}

		boolean loadMore = firstVisibleItem + visibleItemCount >= totalItemCount;
		boolean morePages = (patternManager.maxPage > 1 
				&& patternManager.lastLoadedPage != patternManager.maxPage);

		if(loadMore && !loadingMore && morePages) {
			loadingMore = true;

			KnitciApplication.class.cast(getApplicationContext()).getRavelryManager().retrievePatterns(criteria, patternManager.lastLoadedPage+1, 100);
		}

	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onItemsSelected(boolean[] selected) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean dumbSpinnerClicked(String tag) {

		if(tag.equals("CATEGORY")) {
			Intent intent = new Intent(this, PatternTopLevelCategoryActivity.class);
			startActivity(intent);
		}
		else if(tag.equals("FIT")) {
			Intent intent = new Intent(this, PatternTopLevelDemographicActivity.class);
			startActivity(intent);
		}
		else {
			Intent intent = new Intent(this, PatternTopLevelAttributeActivity.class);
			startActivity(intent);
		}
		return true;
	}

	private PatternSearchCriteria marshallSearchOptions() {
		PatternSearchCriteria criteria = new PatternSearchCriteria();

		return criteria;
	}

	private void unmarshallSearchOptions(PatternSearchCriteria criteria) {

	}

	@Override
	public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex,
			int rightThumbIndex) {
		TextView tv = (TextView) findViewById(R.id.slidingpattern_difficulty_disp);
		tv.setText(String.valueOf(leftThumbIndex) + "-" + String.valueOf(rightThumbIndex));		
		criteria.setDifficulty(leftThumbIndex, rightThumbIndex);
	}

	@Override
	public void onItemsSelected(ArrayList<String> selected, EFilter filter) {
		switch(filter) {
		case CRAFTS:
			criteria.setCrafts(selected);
			break;
		case AVAILABILITY:
			criteria.setAvailability(selected);
			break;
		case YARNWEIGHT:
			criteria.setWeight(selected);
			break;
		case NOTEBOOK:
			criteria.setMyNotebook(selected);
			break;
		case SOURCES:
			criteria.setSources(selected);
			break;
		case FIBERS:
			criteria.setFibers(selected);
			break;
		case HOOKS:
			criteria.setHookSizes(selected);
			break;
		case NEEDLES:
			criteria.setNeedleSizes(selected);
			break;
		case LANGUAGES:
			criteria.setLanguages(selected);
			break;
		}
	}

	public void filterChanged(EFilter filter, ArrayList<String> values) {

	}

	public void filterChanged(EFilter filter, String value) {

	}


	public static enum EFilter {
		SORTING, QUERY, PHOTOREQ, YARNWEIGHT, AVAILABILITY, ATTRIBUTE, CATEGORY, 
		FIT, CRAFTS, NOTEBOOK, SOURCES, HOOKS, NEEDLES, FIBERS, LANGUAGES,
		RATINGS, DIFFICULTY, YARDAGE, METERAGE, DESIGNERNAME, KEYWORDS
	}


	@Override
	public void onItemsSelected(ArrayList<String> selected) {
		// TODO Auto-generated method stub

	}

}
