package com.knitci.pocket.patterns;

import object.KnitciActivity;
import object.KnitciPlainActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPattern;

public class PatternPreviewActivity extends KnitciPlainActivity {

	RavelryPattern pattern;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pattern_preview);		
		stealAsyncListening();

		if(getIntent().getExtras() != null && getIntent().getExtras().getString("PATTERNPERMA") != null) {
			KnitciApplication.class.cast(getApplicationContext()).getRavelryManager().retrievePattern(getIntent().getExtras().getString("PATTERNPERMA"));
		}
	}

	@Override
	public void processFinish(RavelryResponse response) {
		switch(response.task) {
		case PATTERN:
			if(response.result != null) {
				this.pattern = (RavelryPattern) response.result;
				LinearLayout.class.cast(findViewById(R.id.pattern_preview_layout)).setVisibility(View.VISIBLE);
				ProgressBar.class.cast(findViewById(R.id.pattern_preview_progressBar)).setVisibility(View.GONE);
				populatePatternDetails();
			}
			else {

			}
			break;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		stealAsyncListening();
	}
	
	private void populatePatternDetails() {
		TextView.class.cast(findViewById(R.id.pattern_preview_title)).setText(pattern.getPatternName());
		TextView.class.cast(findViewById(R.id.pattern_preview_author)).setText(pattern.getAuthor().getName());
		String disp = "";
		for(int i = 0; i < pattern.getPatternCategories().size(); i++) {
			disp += pattern.getPatternCategories().get(i).getCategoryDisplay();
		}
		TextView.class.cast(findViewById(R.id.pattern_preview_category)).setText(disp);
		TextView.class.cast(findViewById(R.id.pattern_preview_desc)).setText(Html.fromHtml(pattern.getNotesHtml()));
	}
}
