package com.knitci.pocket.patterns;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternSearchCriteria implements Serializable {

	private static final long serialVersionUID = -7441447461837127854L;

	private String query;
	private String sort;
	private String photo;
	private ArrayList<String> weight;
	private ArrayList<String> availability;
	private ArrayList<String> pa;
	private ArrayList<String> pc;
	private ArrayList<String> fit;
	private ArrayList<String> crafts;
	private ArrayList<String> myNotebook;
	private ArrayList<String> sources;
	private ArrayList<String> hooks;
	private ArrayList<String> fibers;
	private ArrayList<String> needles;
	private ArrayList<String> languages;
	private String ravDesigner;
	private String ratings;
	private String difficulty;
	private String yardageIn;
	private String meterageIn;
	private String keywords;
	private String designerName;

	/**
	 * @return the keywords
	 */
	public String getKeywords() {
		return keywords;
	}

	/**
	 * @param keywords the keywords to set
	 */
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	/**
	 * @return the designerName
	 */
	public String getDesignerName() {
		return designerName;
	}

	/**
	 * @param designerName the designerName to set
	 */
	public void setDesignerName(String designerName) {
		this.designerName = designerName;
	}

	public PatternSearchCriteria() {
		languages = new ArrayList<String>();
		pa = new ArrayList<String>();
		pc = new ArrayList<String>();
		fit = new ArrayList<String>();
		crafts = new ArrayList<String>();
		availability = new ArrayList<String>();
		weight = new ArrayList<String>();
		myNotebook = new ArrayList<String>();
		sources = new ArrayList<String>();
		hooks = new ArrayList<String>();
		needles = new ArrayList<String>();
		fibers = new ArrayList<String>();
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getQuery() {
		return query;
	}

	public void setSorting(String sortingDisplayString) {
		if(sortingDisplayString.equalsIgnoreCase("name")) {
			sort = "name";
		}
		else if(sortingDisplayString.equalsIgnoreCase("best match")) {
			sort = "best";
		}
		else if(sortingDisplayString.equalsIgnoreCase("hot right now")) {
			sort = "recently-popular";
		}
		else if(sortingDisplayString.equalsIgnoreCase("most popular")) {
			sort = "popularity";
		}
		else if(sortingDisplayString.equalsIgnoreCase("most projects")) {
			sort = "projects";
		}
		else if(sortingDisplayString.equalsIgnoreCase("most favorites")) {
			sort = "favorites";
		}
		else if(sortingDisplayString.equalsIgnoreCase("most queued")) {
			sort = "queues";
		}
		else if(sortingDisplayString.equalsIgnoreCase("recently published")) {
			sort = "date";
		}
		else if(sortingDisplayString.equalsIgnoreCase("recently added")) {
			sort = "created";
		}
		else if(sortingDisplayString.equalsIgnoreCase("rating (highest first)")) {
			sort = "rating";
		}
		else if(sortingDisplayString.equalsIgnoreCase("difficulty (easiest first)")) {
			sort = "difficulty";
		}
		else if(sortingDisplayString.equalsIgnoreCase("yarn required (least first)")) {
			sort = "yarn";
		}
		else {
			sort = "";
		}
	}

	public String getSorting() {
		return sort;
	}

	public void setOnlyPhotos(boolean onlyPhotos) {
		if(onlyPhotos) {
			photo = "yes";
		}
		else {
			photo = "";
		}
	}

	public String getOnlyPhotos() {
		return photo;
	}

	public void setCrafts(ArrayList<String> craftarr) {
		crafts.clear();
		for(int i = 0; i < craftarr.size(); i++) {
			if(craftarr.get(i).equalsIgnoreCase("Crochet")) {
				crafts.add("crochet");
			}
			else if(craftarr.get(i).equalsIgnoreCase("Knitting")) {
				crafts.add("knitting");
			}
			else if(craftarr.get(i).equalsIgnoreCase("Machine Knitting")) {
				crafts.add("machine-knitting");
			}
			else if(craftarr.get(i).equalsIgnoreCase("Loom Knitting")) {
				crafts.add("loom-knitting");
			}
		}
	}

	public ArrayList<String> getCrafts() {
		return crafts;
	}

	public void setCategories(ArrayList<String> categories) {
		pc = categories;
	}

	public ArrayList<String> getCategories() {
		return pc;
	}

	public void setAttributes(ArrayList<String> attributes) {
		pa = attributes;
	}

	public ArrayList<String> getAttributes() {
		return pa;
	}

	public void setAvailability(ArrayList<String> availArr) {
		availability.clear();
		for(int i = 0; i < availArr.size(); i++) {
			String t = availArr.get(i);
			if(t.equalsIgnoreCase("Free")) {
				availability.add("free");
			}
			else if(t.equalsIgnoreCase("Purchase online")) {
				availability.add("online");
			}
			else if(t.equalsIgnoreCase("Purchase in print")) {
				availability.add("inprint");
			}
			else if(t.equalsIgnoreCase("Ravelry download")) {
				availability.add("ravelry");
			}
			else if(t.equalsIgnoreCase("In my library")) {
				availability.add("library");
			}
		}
	}

	public ArrayList<String> getAvailability() {
		return availability;
	}

	public void setFits(ArrayList<String> demos) {
		fit = demos;
	}

	public ArrayList<String> getFits() {
		return fit;
	}

	public void setWeight(ArrayList<String> weightDisplays) {
		weight.clear();
		for(int i = 0; i < weightDisplays.size(); i++) {
			String disp = weightDisplays.get(i);
			if(disp.equalsIgnoreCase("Thread")) {
				weight.add("thread");
			}
			else if(disp.equalsIgnoreCase("Cobweb / 1 ply")) {
				weight.add("cobweb");
			}
			else if(disp.equalsIgnoreCase("Lace / 2 ply")) {
				weight.add("lace");
			}
			else if(disp.equalsIgnoreCase("Light Fingering / 3 ply")) {
				weight.add("light-fingering");
			}
			else if(disp.equalsIgnoreCase("Fingering / 4 ply")) {
				weight.add("fingering");
			}
			else if(disp.equalsIgnoreCase("Sport / 5 ply")) {
				weight.add("sport");
			}
			else if(disp.equalsIgnoreCase("DK / 8 ply")) {
				weight.add("dk");
			}
			else if(disp.equalsIgnoreCase("Worsted / 10 ply")) {
				weight.add("worsted");
			}
			else if(disp.equalsIgnoreCase("Aran / 10 ply")) {
				weight.add("aran");
			}
			else if(disp.equalsIgnoreCase("Bulky / 12 ply")) {
				weight.add("bulky");
			}
			else if(disp.equalsIgnoreCase("Super Bulky")) {
				weight.add("super-bulky");
			}
			else if(disp.equalsIgnoreCase("No weight specified")) {
				weight.add("unknown");
			}
		}
	}

	public ArrayList<String> getWeight() {
		return weight;
	}

	public void setYardageIn(int min, int max) {
		meterageIn = "";
		if(max == 0 && min > 0) {
			yardageIn = String.valueOf(min) + "%7C";
		}
		else if(min == 0 && max > 0) {
			yardageIn = "%7C" + String.valueOf(max);
		}
		else if(min > 0 && max > 0) {
			yardageIn = String.valueOf(min) + "%7C" + String.valueOf(max);
		}
	}

	public String getYardageInt() {
		return yardageIn;
	}

	public void setMeterageIn(int min, int max) {
		yardageIn = "";
		if(max == 0 && min > 0) {
			meterageIn = String.valueOf(min) + "%7C";
		}
		else if(min == 0 && max > 0) {
			meterageIn = "%7C" + String.valueOf(max);
		}
		else if(min > 0 && max > 0) {
			meterageIn = String.valueOf(min) + "%7C" + String.valueOf(max);
		}
	}

	public String getMeterageIn() {
		return meterageIn;
	}

	public void setMyNotebook(ArrayList<String> noteArr) {
		myNotebook.clear();
		for(int i = 0; i < noteArr.size(); i++) {
			String t = noteArr.get(i);
			if(t.equalsIgnoreCase("In my queue")) {
				myNotebook.add("queued");
			}
			if(t.equalsIgnoreCase("In my projects")) {
				myNotebook.add("projects");
			}
			if(t.equalsIgnoreCase("In my favorites")) {
				myNotebook.add("faved");
			}
			if(t.equalsIgnoreCase("Yarn in my stash")) {
				myNotebook.add("stashed");
			}
			if(t.equalsIgnoreCase("Favorite designers")) {
				myNotebook.add("favedesigners");
			}
		}

	}

	public ArrayList<String> getmyNotebook() {
		return myNotebook;
	}

	public void setSources(ArrayList<String> sourceArr) {
		sources.clear();
		for(int i = 0; i < sourceArr.size(); i++) {
			String t = sourceArr.get(i);
			if(t.equalsIgnoreCase("Book")) {
				sources.add("book");
			}
			else if(t.equalsIgnoreCase("Magazine")) {
				sources.add("magazine");
			}
			else if(t.equalsIgnoreCase("Pamphlet/Booklet")) {
				sources.add("booklet");
			}
			else if(t.equalsIgnoreCase("Website")) {
				sources.add("website");
			}
			else if(t.equalsIgnoreCase("Webzine")) {
				sources.add("webzine");
			}
			else if(t.equalsIgnoreCase("eBook")) {
				sources.add("eBook");
			}
		}
	}

	public ArrayList<String> getSources() {
		return sources;
	}

	public void setHookSizes(ArrayList<String> hookarr) {
		hooks.clear();
		for(int i = 0; i < hookarr.size(); i++) {
			String t = hookarr.get(i);
			Pattern p = Pattern.compile("(\\d+\\.\\d+ mm");
			Matcher m = p.matcher(t);
			if(m.matches()) {
				hooks.add(m.group().replaceAll("\\s+", ""));
			}
		}
	}

	public ArrayList<String> getHookSizes() {
		return hooks;
	}

	public void setFibers(ArrayList<String> fiberArr) {
		fibers.clear();
		for(int i = 0; i < fiberArr.size(); i++) {
			String t = fiberArr.get(i);
			fibers.add(t.toLowerCase());
		}
	}
	
	public ArrayList<String> getFibers() {
		return fibers;
	}
	
	public void setNeedleSizes(ArrayList<String> needleArr) {
		needles.clear();
		for(int i = 0; i < needleArr.size(); i++) {
			Pattern p = Pattern.compile("(\\d+\\.\\d+ mm)");
			Matcher m = p.matcher(needleArr.get(i));
			if(m.matches()) {
				needles.add(m.group().replaceAll("\\s+", ""));
			}
		}
	}
	
	public ArrayList<String> getNeedleSizes() {
		return needles;
	}
	
	public void setRating(int rate) {
		ratings = String.valueOf(rate);
	}
	
	public String getRating() {
		return ratings;
	}
	
	public void setDifficulty(int min, int max) {
		difficulty = String.valueOf(min) + "%7C" + String.valueOf(max);
	}
	
	public String getDifficulty() {
		return difficulty;
	}
	
	public void setRavelryDesigner(boolean rav) {
		if(rav) {
			ravDesigner = "yes";
		}
		else {
			ravDesigner = "no";
		}
	}
	
	public String getRavelryDesigner() {
		return ravDesigner;
	}
	
	public void setLanguages(ArrayList<String> langs) {
		languages.clear();
		for(int i = 0; i < langs.size(); i++) {
			String t = langs.get(i);
			if(t.equalsIgnoreCase("Universal")) {
				languages.add("u");
			}
			else if(t.equalsIgnoreCase("Chinese")) {
				languages.add("cz");
			}
			else if(t.equalsIgnoreCase("Czech")) {
				languages.add("cs");
			}
			else if(t.equalsIgnoreCase("Danish")) {
				languages.add("da");
			}
			else if(t.equalsIgnoreCase("Dutch")) {
				languages.add("nl");
			}
			else if(t.equalsIgnoreCase("English")) {
				languages.add("en");
			}
			else if(t.equalsIgnoreCase("Estonian")) {
				languages.add("et");
			}
			else if(t.equalsIgnoreCase("Finnish")) {
				languages.add("fi");
			}
			else if(t.equalsIgnoreCase("French")) {
				languages.add("fr");
			}
			else if(t.equalsIgnoreCase("German")) {
				languages.add("de");
			}
			else if(t.equalsIgnoreCase("Hungarian")) {
				languages.add("hu");
			}
			else if(t.equalsIgnoreCase("Icelandic")) {
				languages.add("is");
			}
			else if(t.equalsIgnoreCase("Italian")) {
				languages.add("it");
			}
			else if(t.equalsIgnoreCase("Japanese")) {
				languages.add("ja");
			}
			else if(t.equalsIgnoreCase("Norwegian")) {
				languages.add("no");
			}
			else if(t.equalsIgnoreCase("Polish")) {
				languages.add("pl");
			}
			else if(t.equalsIgnoreCase("Portuguese")) {
				languages.add("pt");
			}
			else if(t.equalsIgnoreCase("Russian")) {
				languages.add("ru");
			}
			else if(t.equalsIgnoreCase("Slovak")) {
				languages.add("sk");
			}
			else if(t.equalsIgnoreCase("Spanish")) {
				languages.add("es");
			}
			else if(t.equalsIgnoreCase("Swedish")) {
				languages.add("sv");
			}
		}
	}
	
	public ArrayList<String> getLanguages() {
		return languages;
	}
	
	
}
