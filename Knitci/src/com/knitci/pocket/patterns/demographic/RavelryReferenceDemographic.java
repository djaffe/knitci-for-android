package com.knitci.pocket.patterns.demographic;

import java.io.Serializable;
import java.util.ArrayList;

import com.knitci.pocket.patterns.category.RavelryReferenceCategory;

public class RavelryReferenceDemographic implements Serializable {

	private static final long serialVersionUID = 707030448072397568L;
	public final String name;
	public ArrayList<RavelryReferenceDemographic> subcategories;
	public boolean checked;
	public final String value;
	public boolean checkable;
	
	public RavelryReferenceDemographic(String name, String value) {
		this.name = name;
		subcategories = null;
		checked = false;
		this.value = value;
		checkable = true;
	}
	
}
