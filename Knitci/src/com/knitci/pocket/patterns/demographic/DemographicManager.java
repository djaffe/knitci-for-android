package com.knitci.pocket.patterns.demographic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;



public class DemographicManager {

	private ArrayList<RavelryReferenceDemographic> topLevelDemographics;
	private HashMap<String, Boolean> checkedMap;
	private HashMap<String, RavelryReferenceDemographic> demographicMap;
	
	public DemographicManager() {
		topLevelDemographics = new ArrayList<RavelryReferenceDemographic>();
		checkedMap = new LinkedHashMap<String, Boolean>();
		demographicMap = new LinkedHashMap<String, RavelryReferenceDemographic>();
		setupReferenceDemographics();
	}

	public ArrayList<RavelryReferenceDemographic> getReferenceDemographics() {
		return topLevelDemographics;
	}
	
	public void setDemographicChecked(String demo, boolean checked) {
		checkedMap.put(demo, checked);
	}

	public boolean isDemographicChecked(String demo) {
		return checkedMap.get(demo);
	}
	
	public ArrayList<String> getEnabledDemographicValues() {
		ArrayList<String> vals = new ArrayList<String>();

		for (Entry<String, Boolean> entry : checkedMap.entrySet()) {
			String value = entry.getKey();
			boolean enabled = entry.getValue();
			if(enabled) {
				vals.add(value);
			}
		}
		return vals;
	}
	
	public RavelryReferenceDemographic getReferenceDemographic(String value) {
		return demographicMap.get(value);
	}
	
	public void clearChecks() {
		for (Entry<String, Boolean> entry : checkedMap.entrySet()) {
			checkedMap.put(entry.getKey(), false);
		}
	}
	

	public String getEntirePartialString() {
		String result = "";
		for(int i = 0; i < topLevelDemographics.size(); i++) {
			String temp = getPartialString(topLevelDemographics.get(i).value, "0");
			if(!result.isEmpty() && !temp.isEmpty()) {
				result += ", ";
			}
			result += temp;
		}
		if(result.isEmpty()) {
			return "All Types";
		}
		return result;
	}
	
	public String getPartialString(String val, String beginning) {

		RavelryReferenceDemographic topCat = demographicMap.get(val);
		// Oh shit case
		if(topCat == null) {
			return "";
		}
		String display = "";

		if(!val.equals(beginning)) {
			if(checkedMap.get(val)) {
				display += topCat.name;
			}
		}

		if(topCat.subcategories != null && topCat.subcategories.size() > 0) {

			for(int i = 0; i < topCat.subcategories.size(); i++) {
				String temp = getPartialString(topCat.subcategories.get(i).value, beginning);
				if(!temp.isEmpty()) {
					if(!display.isEmpty()) {
						display += ", ";
					}
					display += temp + ", ";
				}
			}
			display = display.trim();
			if(display.length() > 2 && display.substring(display.length()-1).equals(",")) {
				display = display.substring(0, display.length()-1);						
			}
		}
		String temp = display.replace(", , ", ", ");
		if(!temp.isEmpty() && temp.substring(temp.length()-1).equals(", ")) {
			return temp.substring(0, temp.length()-2);
		}
		return temp;
	}

	public boolean isCategoryPartial(String val, String beginning) {
		RavelryReferenceDemographic cat = demographicMap.get(val);
		// Oh shit return
		if(cat == null) {
			return false;
		}
		if(cat.subcategories == null || cat.subcategories.isEmpty()) {
			if(!val.equals(beginning)) {
				if(isDemographicChecked(val)) {
					return true;
				}
			}
			return false;
		}
		for(int i = 0; i < cat.subcategories.size(); i++) {
			if(isCategoryPartial(cat.subcategories.get(i).value, beginning)) {
				return true;
			}
			if(isDemographicChecked(cat.subcategories.get(i).value)) {
				return true;
			}
		}
		return false;
	}

	
	public void setupReferenceDemographics() {
		// Age or Size
		RavelryReferenceDemographic ageSize = new RavelryReferenceDemographic("Age or Size", "agesize");
		demographicMap.put("agesize", ageSize);
		checkedMap.put("agesize", false);
		ageSize.checkable = false;
		ArrayList<RavelryReferenceDemographic> ageSizeSub = new ArrayList<RavelryReferenceDemographic>();
		
		ageSizeSub.add(new RavelryReferenceDemographic("Adult", "adult"));
		checkedMap.put("adult", false);
		demographicMap.put("adult", ageSizeSub.get(ageSizeSub.size()-1));
		
		ageSizeSub.add(new RavelryReferenceDemographic("Baby", "baby"));
		checkedMap.put("baby", false);
		demographicMap.put("baby", ageSizeSub.get(ageSizeSub.size()-1));
		
		ageSizeSub.add(new RavelryReferenceDemographic("Child (4-12)", "child"));
		checkedMap.put("child", false);
		demographicMap.put("child", ageSizeSub.get(ageSizeSub.size()-1));
		
		ageSizeSub.add(new RavelryReferenceDemographic("Doll", "doll-size"));
		checkedMap.put("doll-size", false);
		demographicMap.put("doll-size", ageSizeSub.get(ageSizeSub.size()-1));
		
		ageSizeSub.add(new RavelryReferenceDemographic("Newborn", "newborn-size"));
		checkedMap.put("newborn-size", false);
		demographicMap.put("newborn-size", ageSizeSub.get(ageSizeSub.size()-1));
		
		ageSizeSub.add(new RavelryReferenceDemographic("Preemie", "preemie"));
		checkedMap.put("preemie", false);
		demographicMap.put("preemie", ageSizeSub.get(ageSizeSub.size()-1));
		
		ageSizeSub.add(new RavelryReferenceDemographic("Teen (13-17)", "teen"));
		checkedMap.put("teen", false);
		demographicMap.put("teen", ageSizeSub.get(ageSizeSub.size()-1));
		
		ageSizeSub.add(new RavelryReferenceDemographic("Toddler (1-3)", "toddler"));
		checkedMap.put("toddler", false);
		demographicMap.put("toddler", ageSizeSub.get(ageSizeSub.size()-1));
		
		ageSize.subcategories = ageSizeSub;
		topLevelDemographics.add(ageSize);
		
		// Ease
		
		RavelryReferenceDemographic ease = new RavelryReferenceDemographic("Ease", "ease");
		demographicMap.put("ease", ease);
		checkedMap.put("ease", false);
		ease.checkable = false;
		ArrayList<RavelryReferenceDemographic> easeSub = new ArrayList<RavelryReferenceDemographic>();

		easeSub.add(new RavelryReferenceDemographic("Negative Ease", "negative-ease"));
		checkedMap.put("negative-ease", false);
		demographicMap.put("negative-ease", easeSub.get(easeSub.size()-1));
		
		easeSub.add(new RavelryReferenceDemographic("No Ease", "no-ease"));
		checkedMap.put("no-ease", false);
		demographicMap.put("no-ease", easeSub.get(easeSub.size()-1));
		
		easeSub.add(new RavelryReferenceDemographic("Positive Ease", "positive-ease"));
		checkedMap.put("positive-ease", false);
		demographicMap.put("positive-ease", easeSub.get(easeSub.size()-1));
		
		ease.subcategories = easeSub;
		topLevelDemographics.add(ease);
		
		// Fit
		
		RavelryReferenceDemographic fit = new RavelryReferenceDemographic("Fit", "fit");
		demographicMap.put("fit", fit);
		checkedMap.put("fit",  false);
		fit.checkable = false;
		ArrayList<RavelryReferenceDemographic> fitSub = new ArrayList<RavelryReferenceDemographic>();
		
		fitSub.add(new RavelryReferenceDemographic("Fitted", "fitted"));
		checkedMap.put("fitted", false);
		demographicMap.put("fitted", fitSub.get(fitSub.size()-1));

		fitSub.add(new RavelryReferenceDemographic("Maternity Fit", "maternity"));
		checkedMap.put("maternity", false);
		demographicMap.put("maternity", fitSub.get(fitSub.size()-1));
		
		fitSub.add(new RavelryReferenceDemographic("Miniature", "miniature"));
		checkedMap.put("miniature", false);
		demographicMap.put("miniature", fitSub.get(fitSub.size()-1));
		
		fitSub.add(new RavelryReferenceDemographic("Oversized Fit", "oversized"));
		checkedMap.put("oversized", false);
		demographicMap.put("oversized", fitSub.get(fitSub.size()-1));
		
		fitSub.add(new RavelryReferenceDemographic("Petite Fit", "petite"));
		checkedMap.put("petite", false);
		demographicMap.put("petite", fitSub.get(fitSub.size()-1));
		
		fitSub.add(new RavelryReferenceDemographic("Plus Fit", "plus"));
		checkedMap.put("plus", false);
		demographicMap.put("plus", fitSub.get(fitSub.size()-1));
		
		fitSub.add(new RavelryReferenceDemographic("Tall Fit", "tall"));
		checkedMap.put("tall", false);
		demographicMap.put("tall", fitSub.get(fitSub.size()-1));
		
		fit.subcategories = fitSub;
		topLevelDemographics.add(fit);
		// Gender
		RavelryReferenceDemographic gender = new RavelryReferenceDemographic("Gender", "gender");
		demographicMap.put("gender", gender);
		checkedMap.put("gender",  false);
		gender.checkable = false;
		ArrayList<RavelryReferenceDemographic> genderSub = new ArrayList<RavelryReferenceDemographic>();
		
		genderSub.add(new RavelryReferenceDemographic("Female", "female"));
		checkedMap.put("female", false);
		demographicMap.put("female", genderSub.get(genderSub.size()-1));
		
		genderSub.add(new RavelryReferenceDemographic("Male", "male"));
		checkedMap.put("male", false);
		demographicMap.put("male", genderSub.get(genderSub.size()-1));
		
		genderSub.add(new RavelryReferenceDemographic("Unisex", "unisex"));
		checkedMap.put("unisex", false);
		demographicMap.put("unisex", genderSub.get(genderSub.size()-1));
		
		gender.subcategories = genderSub;
		topLevelDemographics.add(gender);
		
		
	}
	
}
