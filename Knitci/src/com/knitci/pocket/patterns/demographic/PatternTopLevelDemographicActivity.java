package com.knitci.pocket.patterns.demographic;

import java.util.ArrayList;

import object.CheckBoxColorfulPostback;
import object.CheckBoxSupreme;

import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.R.anim;
import com.knitci.pocket.R.id;
import com.knitci.pocket.R.layout;
import com.knitci.pocket.R.menu;
import com.knitci.pocket.patterns.attribute.PatternChoosableAttributeActivity;
import com.knitci.pocket.patterns.attribute.RavelryReferenceAttribute;
import com.knitci.pocket.patterns.manager.PatternManager;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PatternTopLevelDemographicActivity extends Activity implements CheckBoxColorfulPostback, OnClickListener {

	PatternManager patternManager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pattern_category);
		patternManager = KnitciApplication.class.cast(getApplication()).getPatternManager();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pattern_top_level_demographic, menu);
		return true;
	}
	

	@Override
	protected void onResume() {
		super.onResume();
		ArrayList<RavelryReferenceDemographic> demos = KnitciApplication.class.cast(this.getApplication()).getPatternManager().getReferenceDemographics();

		LinearLayout sv = LinearLayout.class.cast(findViewById(R.id.categories_holder));
		if(sv.getChildCount() == 0) {
			for(int i = 0; i < demos.size(); i++) {
				CheckBoxSupreme tv = new CheckBoxSupreme(getApplicationContext());
				tv.setChecked(patternManager.getDemographicManager().isDemographicChecked(demos.get(i).value));
				tv.setCheckBoxColorfulListener(this);
				tv.setText(demos.get(i).name);
				tv.setSubText(patternManager.getDemographicManager().getPartialString(demos.get(i).value, demos.get(i).value));
				String vv = demos.get(i).value;
				tv.setTag(vv);
				tv.setOnClickListener(this);
				sv.addView(tv);
				if(!demos.get(i).checkable) {
					tv.hideCheckBox();
				}
			}
		}
		else {
			for(int i = 0; i < sv.getChildCount(); i++) {
				CheckBoxSupreme tv = CheckBoxSupreme.class.cast(sv.getChildAt(i));
				tv.setSubText(patternManager.getDemographicManager().getPartialString(demos.get(i).value, demos.get(i).value));
			}
		}
	}


	@Override
	public void onClick(View v) {
		TextView tv = TextView.class.cast(v);
		CheckBoxSupreme supreme = CheckBoxSupreme.class.cast(tv.getTag());
		String value = (String) supreme.getTag();
		RavelryReferenceDemographic att = patternManager.getDemographicManager().getReferenceDemographic(value);

		Intent intent = new Intent(this, PatternChoosableDemographicActivity.class);
		intent.putExtra("FIT", att);
		startActivity(intent);
		overridePendingTransition(R.anim.animation_right_to_left, R.anim.animation_left_to_right);
	}

	@Override
	public boolean checkBoxClicked(String value, boolean checked) {
		patternManager.getDemographicManager().setDemographicChecked(value, checked);
		return true;
	}

	

}
