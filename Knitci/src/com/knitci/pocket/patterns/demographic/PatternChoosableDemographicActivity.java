package com.knitci.pocket.patterns.demographic;

import java.util.ArrayList;

import object.CheckBoxColorfulPostback;
import object.CheckBoxSupreme;

import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.R.anim;
import com.knitci.pocket.R.id;
import com.knitci.pocket.R.layout;
import com.knitci.pocket.R.menu;
import com.knitci.pocket.patterns.attribute.PatternChoosableAttributeActivity;
import com.knitci.pocket.patterns.attribute.RavelryReferenceAttribute;
import com.knitci.pocket.patterns.manager.PatternManager;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PatternChoosableDemographicActivity extends Activity implements CheckBoxColorfulPostback, OnClickListener {

	PatternManager patternManager;
	RavelryReferenceDemographic highestAtt;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pattern_category);
		patternManager = KnitciApplication.class.cast(getApplication()).getPatternManager();

		highestAtt = RavelryReferenceDemographic.class.cast(getIntent().getExtras().get("FIT"));
	}


	@Override
	protected void onResume() {
		super.onResume();

		ArrayList<RavelryReferenceDemographic> demos = KnitciApplication.class.cast(this.getApplication()).getPatternManager().getReferenceDemographics();

		LinearLayout sv = LinearLayout.class.cast(findViewById(R.id.categories_holder));


		String topLevel = highestAtt.name;

		if(sv.getChildCount() == 0) {
			CheckBox check = new CheckBox(this);
			check.setText("All " + topLevel);
			sv.addView(check);

			if(highestAtt.subcategories != null && highestAtt.subcategories.size() > 0) {
				for(int i = 0; i < highestAtt.subcategories.size(); i++) {
					CheckBoxSupreme tvv = new CheckBoxSupreme(getApplicationContext());
					tvv.setChecked(patternManager.getDemographicManager().isDemographicChecked(highestAtt.subcategories.get(i).value));
					tvv.setCheckBoxColorfulListener(this);
					tvv.setText(highestAtt.subcategories.get(i).name);
					tvv.setSubText(patternManager.getDemographicManager().getPartialString(highestAtt.subcategories.get(i).value, highestAtt.subcategories.get(i).value));
					tvv.setTag(highestAtt.subcategories.get(i).value);
					if(highestAtt.subcategories.get(i).subcategories != null && highestAtt.subcategories.get(i).subcategories.size() > 0) {
						tvv.showMoreIcon(true);
					}
					tvv.setOnClickListener(this);
					sv.addView(tvv);
					if(!highestAtt.subcategories.get(i).checkable) {
						tvv.hideCheckBox();
					}
				}
			}
		}
		else {
			for(int i = 1; i < sv.getChildCount(); i++) {
				CheckBoxSupreme tv = CheckBoxSupreme.class.cast(sv.getChildAt(i));
				tv.setSubText(patternManager.getDemographicManager().getPartialString(highestAtt.subcategories.get(i-1).value, highestAtt.subcategories.get(i-1).value));
			}
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pattern_choosable_demographic, menu);
		return true;
	}
	

	@Override
	public void onClick(View v) {

		TextView tv = TextView.class.cast(v);
		CheckBoxSupreme supreme = CheckBoxSupreme.class.cast(tv.getTag());
		//RavelryReferenceAttribute cat = RavelryReferenceAttribute.class.cast(supreme.getTag());
		String value = (String) supreme.getTag();
		RavelryReferenceDemographic cat = patternManager.getDemographicManager().getReferenceDemographic(value);


		if(cat.subcategories != null && cat.subcategories.size() > 0) {
			Intent intent = new Intent(this, PatternChoosableDemographicActivity.class);
			intent.putExtra("FIT", cat);
			startActivity(intent);
			overridePendingTransition(R.anim.animation_right_to_left, R.anim.animation_left_to_right);
		}
	}

	@Override
	public boolean checkBoxClicked(String value, boolean checked) {
		patternManager.getDemographicManager().setDemographicChecked(value, checked);
		return true;
	}


}
