package com.knitci.pocket.patterns;

import java.util.ArrayList;

import object.KnitciActivityDlg;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.actionbarsherlock.view.Menu;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.RavelryManager;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryCollection;
import com.knitci.pocket.ravelry.data.object.impl.RavelryYarn;
import com.knitci.yarnpicker.YarnPicker;
import com.knitci.yarnpicker.internal.IYarnPicker;

public class PatternQueueCreateActivity extends KnitciActivityDlg implements IYarnPicker, OnClickListener {
	
	private RavelryManager ravelryManager;
	private ProgressDialog progressDialog;
	private int patternId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pattern_queue_create);
		YarnPicker picker = (YarnPicker) findViewById(R.id.patternqueuecreate_yarnpicker);
		ravelryManager = KnitciApplication.class.cast(getApplicationContext()).getRavelryManager();
		stealAsyncListening();
		picker.setHandler(this);
		
		Button ok = (Button) findViewById(R.id.patternqueuecreate_ok);
		Button cancel = (Button) findViewById(R.id.patternqueuecreate_cancel);
		ok.setOnClickListener(this);
		cancel.setOnClickListener(this);
		
		if(getIntent() == null || getIntent().getExtras() == null || getIntent().getExtras().getInt("PATTERNID") < 0) {
			finish();
		}
		patternId = getIntent().getExtras().getInt("PATTERNID");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.pattern_queue_create, menu);
		return true;
	}

	@Override
	public void yarnPickerLinkClicked(String searchTerm) {
		ravelryManager.retrieveYarns(searchTerm, 1, 15, "");
	}

	@Override
	public void processFinish(RavelryResponse response) {
		switch(response.task) {
		case YARNS:
			YarnPicker picker = (YarnPicker) findViewById(R.id.patternqueuecreate_yarnpicker);
			RavelryCollection collection = (RavelryCollection) response.result;
			ArrayList<RavelryYarn> yarns = (ArrayList<RavelryYarn>)(ArrayList<?>)collection.getCollection();
			picker.populateResults(yarns);
			break;
		case QUEUECREATE:
			progressDialog.dismiss();
			if(1==2) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);

				builder.setTitle("Post Failed");

				builder.setMessage("Failed to submit forum post. Error code " + response.result.getHttpCode() + ".");
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
					}
				});
				AlertDialog dialog = builder.create();
				dialog.show();
			}
			break;
		}
	}
	
	private void submitQueue() {
		progressDialog = ProgressDialog.show(this, "Hello", "message");
		progressDialog.setCancelable(true);
		progressDialog.setOnCancelListener(new OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		
		YarnPicker picker = (YarnPicker) findViewById(R.id.patternqueuecreate_yarnpicker);
		EditText skeinsEt = (EditText) findViewById(R.id.patternqueuecreate_skeins);
		EditText makeForEt = (EditText) findViewById(R.id.patternqueuecreate_makefor);
		EditText notesEt = (EditText) findViewById(R.id.patternqueuecreate_notes);
		
		ravelryManager.submitCreateQueueFromPattern(patternId,
				(picker.isYarnFreetext() ? 0 : picker.getLastSelectedYarn().getId()),
				(picker.isYarnFreetext() ? picker.getFreetextYarn() : null),
				(isInteger(skeinsEt.getText().toString()) ? Integer.parseInt(skeinsEt.getText().toString()) : 1),
				null,
				makeForEt.getText().toString(),
				null,
				notesEt.getText().toString(),
				-1);
	}

	@Override
	public void onClick(View arg0) {
		switch(arg0.getId()) {
		case R.id.patternqueuecreate_ok:
			submitQueue();
			break;
		case R.id.patternqueuecreate_cancel:
			finish();
			break;
		}		
	}
	
	public static boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    }
	    // only got here if we didn't return false
	    return true;
	}
}
