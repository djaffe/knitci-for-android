package com.knitci.pocket.patterns.attribute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.knitci.pocket.R.color;
import com.knitci.pocket.patterns.category.RavelryReferenceCategory;


public class AttributeManager {

	private ArrayList<RavelryReferenceAttribute> topLevelAttributes;
	private HashMap<String, Boolean> checkedMap;
	private HashMap<String, RavelryReferenceAttribute> attributeMap;

	public AttributeManager() {
		topLevelAttributes = new ArrayList<RavelryReferenceAttribute>();
		checkedMap = new LinkedHashMap<String, Boolean>();
		attributeMap = new LinkedHashMap<String, RavelryReferenceAttribute>();
		setupReferenceAttributes();
	}
	
	public ArrayList<RavelryReferenceAttribute> getReferenceAttributes() {
		return topLevelAttributes;
	}
	
	public RavelryReferenceAttribute getReferenceAttribute(String value) {
		return attributeMap.get(value);
	}
	
	public void setAttributeChecked(String attribute, boolean checked) {
		checkedMap.put(attribute, checked);
	}
	
	public boolean isAttributeChecked(String attribute) {
		if(attribute == null) {
			return false;
		}
		if(null == checkedMap.get(attribute)) {
			return false;
		}
		return checkedMap.get(attribute);
	}
	
	public ArrayList<String> getEnabledAttributeValues() {
		ArrayList<String> vals = new ArrayList<String>();

		for (Entry<String, Boolean> entry : checkedMap.entrySet()) {
			String value = entry.getKey();
			boolean enabled = entry.getValue();
			if(enabled) {
				vals.add(value);
			}
		}

		return vals;
	}
	
	public void clearChecks() {
		for (Entry<String, Boolean> entry : checkedMap.entrySet())
		{
			checkedMap.put(entry.getKey(), false);
		}
	}

	public String getEntirePartialString() {
		String result = "";
		for(int i = 0; i < topLevelAttributes.size(); i++) {
			String temp = getPartialString(topLevelAttributes.get(i).value, "0");
			if(!result.isEmpty() && !temp.isEmpty()) {
				result += ", ";
			}
			result += temp;
		}
		if(result.isEmpty()) {
			return "All Attributes";
		}
		return result;
	}
	
	public String getPartialString(String val, String beginning) {

		RavelryReferenceAttribute topCat = attributeMap.get(val);
		// Oh shit case
		if(topCat == null) {
			return "";
		}
		String display = "";

		if(!val.equals(beginning)) {
			if(checkedMap.get(val)) {
				display += topCat.name;
			}
		}

		if(topCat.subcategories != null && topCat.subcategories.size() > 0) {

			for(int i = 0; i < topCat.subcategories.size(); i++) {
				String temp = getPartialString(topCat.subcategories.get(i).value, beginning);
				if(!temp.isEmpty()) {
					if(!display.isEmpty()) {
						display += ", ";
					}
					display += temp + ", ";
				}
			}
			display = display.trim();
			if(display.length() > 2 && display.substring(display.length()-1).equals(",")) {
				display = display.substring(0, display.length()-1);						
			}
		}
		String temp = display.replace(", , ", ", ");
		if(!temp.isEmpty() && temp.substring(temp.length()-1).equals(", ")) {
			return temp.substring(0, temp.length()-2);
		}
		return temp;
	}
	
	public boolean isAttributePartial(String val, String beginning) {
		RavelryReferenceAttribute cat = attributeMap.get(val);
		// Oh shit return
		if(cat == null) {
			return false;
		}
		if(cat.subcategories == null || cat.subcategories.isEmpty()) {
			if(!val.equals(beginning)) {
				if(isAttributeChecked(val)) {
					return true;
				}
			}
			return false;
		}
		for(int i = 0; i < cat.subcategories.size(); i++) {
			if(isAttributePartial(cat.subcategories.get(i).value, beginning)) {
				return true;
			}
			if(isAttributeChecked(cat.subcategories.get(i).value)) {
				return true;
			}
		}
		return false;
	}
	
	private void setupReferenceAttributes() {
		// Setup Colorwork
		RavelryReferenceAttribute colorWork = new RavelryReferenceAttribute("Colorwork", "colorwork");
		attributeMap.put("colorwork", colorWork);
		checkedMap.put("colorwork", false);
		colorWork.checkable = false;
		ArrayList<RavelryReferenceAttribute> colorworkSub = new ArrayList<RavelryReferenceAttribute>();
		
		colorworkSub.add(new RavelryReferenceAttribute("Intarsia", "Intarsia"));
		attributeMap.put("Intarsia", colorworkSub.get(colorworkSub.size()-1));
		checkedMap.put("Intarsia", false);
		
		colorworkSub.add(new RavelryReferenceAttribute("Corrugated Ribbing", "corrugated-ribbing"));
		attributeMap.put("corrugated-ribbing", colorworkSub.get(colorworkSub.size()-1));
		checkedMap.put("corrugated-ribbing", false);

		colorworkSub.add(new RavelryReferenceAttribute("Illusion/Shadow", "illusion"));
		attributeMap.put("illusion", colorworkSub.get(colorworkSub.size()-1));
		checkedMap.put("illusion", false);
		
		colorworkSub.add(new RavelryReferenceAttribute("Mosaic", "mosaic"));
		attributeMap.put("mosaic", colorworkSub.get(colorworkSub.size()-1));
		checkedMap.put("mosaic", false);
		
		colorworkSub.add(new RavelryReferenceAttribute("Stranded", "stranded"));
		attributeMap.put("stranded", colorworkSub.get(colorworkSub.size()-1));
		checkedMap.put("stranded", false);
		
		colorworkSub.add(new RavelryReferenceAttribute("Stripes/Colorwork", "stripes-colorwork"));
		attributeMap.put("stripes-colorwork", colorworkSub.get(colorworkSub.size()-1));
		checkedMap.put("stripes-colorwork", false);
		
		colorworkSub.add(new RavelryReferenceAttribute("Other", "other-colorwork"));
		attributeMap.put("other-colorwork", colorworkSub.get(colorworkSub.size()-1));
		checkedMap.put("other-colorwork", false);
		
		colorWork.subcategories = colorworkSub;
		
		topLevelAttributes.add(colorWork);
		
		// Construction
		RavelryReferenceAttribute construction = new RavelryReferenceAttribute("Construction", "construction");
		attributeMap.put("construction", construction);
		checkedMap.put("construction", false);
		construction.checkable = false;
		ArrayList<RavelryReferenceAttribute> constructionSub = new ArrayList<RavelryReferenceAttribute>();
		
		constructionSub.add(new RavelryReferenceAttribute("Bias", "bias"));
		attributeMap.put("bias", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("bias", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Bottom Up", "bottom-up"));
		attributeMap.put("bottom-up", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("bottom-up", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Buttonholes", "buttonholes"));
		attributeMap.put("buttonholes", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("buttonholes", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Double Knitting", "doubleknit"));
		attributeMap.put("doubleknit", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("doubleknit", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Entrelac", "entrelac"));
		attributeMap.put("entrelac", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("entrelac", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Felted/Fulled", "felted"));
		attributeMap.put("felted", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("felted", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Freeform", "freeform"));
		attributeMap.put("freeform", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("freeform", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Gusset", "gusset"));
		attributeMap.put("gusset", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("gusset", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Icord", "icord"));
		attributeMap.put("icord", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("icord", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Knitchener/Grafting", "kitchener"));
		attributeMap.put("kitchener", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("kitchener", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Modular/Join as you go", "modular"));
		attributeMap.put("modular", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("modular", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Moebius", "moebius"));
		attributeMap.put("moebius", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("moebius", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Motifs", "motifs"));
		attributeMap.put("motifs", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("motifs", false);
		
		constructionSub.add(new RavelryReferenceAttribute("One-Piece", "one-piece"));
		attributeMap.put("one-piece", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("one-piece", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Provisional Cast-on", "provisional"));
		attributeMap.put("provisional", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("provisional", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Seamed", "seamed"));
		attributeMap.put("seamed", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("seamed", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Seamless", "seamless"));
		attributeMap.put("seamless", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("seamless", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Selvedge", "selvedge"));
		attributeMap.put("selvedge", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("selvedge", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Short Rows", "short-rows"));
		attributeMap.put("short-rows", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("short-rows", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Sideways", "sideways"));
		attributeMap.put("sideways", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("sideways", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Steeks", "steeks"));
		attributeMap.put("steeks", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("steeks", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Three Needle Bind Off", "three-needle-bind"));
		attributeMap.put("hree-needle-bind", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("hree-needle-bind", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Thrums", "thrums"));
		attributeMap.put("thrums", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("thrums", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Top Down", "top-down"));
		attributeMap.put("top-down", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("top-down", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Twined Knitting", "twined"));
		attributeMap.put("twined", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("twined", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Worked Flat", "worked-flat"));
		attributeMap.put("worked-flat", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("worked-flat", false);
		
		constructionSub.add(new RavelryReferenceAttribute("Worked In the Round", "in-the-round"));
		attributeMap.put("in-the-round", constructionSub.get(constructionSub.size()-1));
		checkedMap.put("in-the-round", false);
		
		construction.subcategories = constructionSub;
		
		topLevelAttributes.add(construction);
		
		//Crochet
		RavelryReferenceAttribute crochet = new RavelryReferenceAttribute("Crochet Techniques", "crochettechniques");
		checkedMap.put("crochettechniques", false);
		attributeMap.put("crochettechniques", crochet);
		crochet.checkable = false;
		ArrayList<RavelryReferenceAttribute> crochetSub = new ArrayList<RavelryReferenceAttribute>(); 
		
		crochetSub.add(new RavelryReferenceAttribute("Irish Crochet", "irish-crochet"));
		attributeMap.put("irish-crochet", crochetSub.get(crochetSub.size()-1));
		checkedMap.put("irish-crochet", false);
		
		crochetSub.add(new RavelryReferenceAttribute("Tunisian/Afghan Crochet", "tunisian"));
		attributeMap.put("tunisian", crochetSub.get(crochetSub.size()-1));
		checkedMap.put("tunisian", false);
		
		crochetSub.add(new RavelryReferenceAttribute("Broomstick Crochet", "broomstick"));
		attributeMap.put("broomstick", crochetSub.get(crochetSub.size()-1));
		checkedMap.put("broomstick", false);
		
		crochetSub.add(new RavelryReferenceAttribute("Bruges Crochet", "bruges"));
		attributeMap.put("bruges", crochetSub.get(crochetSub.size()-1));
		checkedMap.put("bruges", false);
		
		crochetSub.add(new RavelryReferenceAttribute("Bullion/Roll Stitch", "bullion"));
		attributeMap.put("bullion", crochetSub.get(crochetSub.size()-1));
		checkedMap.put("bullion", false);
		
		crochetSub.add(new RavelryReferenceAttribute("Clones Knot", "clones"));
		attributeMap.put("clones", crochetSub.get(crochetSub.size()-1));
		checkedMap.put("clones", false);
		
		crochetSub.add(new RavelryReferenceAttribute("Cro-hook/Croknit", "crohook"));
		attributeMap.put("crohook", crochetSub.get(crochetSub.size()-1));
		checkedMap.put("crohook", false);
		
		crochetSub.add(new RavelryReferenceAttribute("Cro-tatting/Crochet", "crotat"));
		attributeMap.put("crotat", crochetSub.get(crochetSub.size()-1));
		checkedMap.put("crotat", false);
		
		crochetSub.add(new RavelryReferenceAttribute("Filet Crochet", "filet-crochet"));
		attributeMap.put("filet-crochet", crochetSub.get(crochetSub.size()-1));
		checkedMap.put("filet-crochet", false);
		
		crochetSub.add(new RavelryReferenceAttribute("Front/Back Post Stitch", "post-stitch"));
		attributeMap.put("post-stitch", crochetSub.get(crochetSub.size()-1));
		checkedMap.put("post-stitch", false);
		
		crochetSub.add(new RavelryReferenceAttribute("Granny Square", "granny-square"));
		attributeMap.put("granny-square", crochetSub.get(crochetSub.size()-1));
		checkedMap.put("granny-square", false);
		
		crochetSub.add(new RavelryReferenceAttribute("Hairpin Crochet", "hairpin-crochet"));
		attributeMap.put("hairpin-crochet", crochetSub.get(crochetSub.size()-1));
		checkedMap.put("hairpin-crochet", false);
		
		crochetSub.add(new RavelryReferenceAttribute("Lover's Knot/Soloman's", "lovers"));
		attributeMap.put("lovers", crochetSub.get(crochetSub.size()-1));
		checkedMap.put("lovers", false);
		
		crochetSub.add(new RavelryReferenceAttribute("Pineapple Crochet", "pineapple"));
		attributeMap.put("pineapple", crochetSub.get(crochetSub.size()-1));
		checkedMap.put("pineapple", false);
		
		crochetSub.add(new RavelryReferenceAttribute("Slip Stitch Crochet", "slip-stitch"));
		attributeMap.put("slip-stitch", crochetSub.get(crochetSub.size()-1));
		checkedMap.put("slip-stitch", false);
		
		crochetSub.add(new RavelryReferenceAttribute("Surface Crochet", "surface-crochet"));
		attributeMap.put("surface-crochet", crochetSub.get(crochetSub.size()-1));
		checkedMap.put("surface-crochet", false);
		
		crochetSub.add(new RavelryReferenceAttribute("Tapestry Crochet", "tapestry-crochet"));
		attributeMap.put("tapestry-crochet", crochetSub.get(crochetSub.size()-1));
		checkedMap.put("tapestry-crochet", false);
		
		crochet.subcategories = crochetSub;
		
		topLevelAttributes.add(crochet);
		
		//Design Elements
		RavelryReferenceAttribute design = new RavelryReferenceAttribute("Design Elements", "designelements");
		checkedMap.put("designelements", false);
		attributeMap.put("designelements", design);
		design.checkable = false;
		ArrayList<RavelryReferenceAttribute> designSub = new ArrayList<RavelryReferenceAttribute>(); 

		designSub.add(new RavelryReferenceAttribute("A line", "aline"));
		attributeMap.put("aline", designSub.get(designSub.size()-1));
		checkedMap.put("aline", false);
		
		RavelryReferenceAttribute collar = new RavelryReferenceAttribute("Collar", "collars");
		attributeMap.put("collars", collar);
		checkedMap.put("collars", false);
		collar.checkable = false;
		ArrayList<RavelryReferenceAttribute> collarSub = new ArrayList<RavelryReferenceAttribute>();
		
		collarSub.add(new RavelryReferenceAttribute("Collared", "collar"));
		attributeMap.put("collar", collarSub.get(collarSub.size()-1));
		checkedMap.put("collar", false);
		
		collarSub.add(new RavelryReferenceAttribute("Mandarin", "mandarin"));
		attributeMap.put("mandarin", collarSub.get(collarSub.size()-1));
		checkedMap.put("mandarin", false);
		
		collarSub.add(new RavelryReferenceAttribute("Peter Pan", "peterpan"));
		attributeMap.put("peterpan", collarSub.get(collarSub.size()-1));
		checkedMap.put("peterpan", false);
		
		collarSub.add(new RavelryReferenceAttribute("Rolled", "rolled"));
		attributeMap.put("rolled", collarSub.get(collarSub.size()-1));
		checkedMap.put("rolled", false);
		
		collarSub.add(new RavelryReferenceAttribute("Shawl Collar", "shawl"));
		attributeMap.put("shawl", collarSub.get(collarSub.size()-1));
		checkedMap.put("shawl", false);
		
		collarSub.add(new RavelryReferenceAttribute("Shirt", "shirt-collar"));
		attributeMap.put("shirt-collar", collarSub.get(collarSub.size()-1));
		checkedMap.put("shirt-collar", false);
		
		collar.subcategories = collarSub;
		
		RavelryReferenceAttribute edging = new RavelryReferenceAttribute("Edging", "edging");
		attributeMap.put("edging", edging);
		checkedMap.put("edging", false);
		edging.checkable = false;
		ArrayList<RavelryReferenceAttribute> edgingSub = new ArrayList<RavelryReferenceAttribute>();
		
		edgingSub.add(new RavelryReferenceAttribute("Crocheted Edging", "crocheted-edging"));
		attributeMap.put("crocheted-edging", edgingSub.get(edgingSub.size()-1));
		checkedMap.put("crocheted-edging", false);
		
		edgingSub.add(new RavelryReferenceAttribute("Icord Edging", "icord-edging"));
		attributeMap.put("icord-edging", edgingSub.get(edgingSub.size()-1));
		checkedMap.put("icord-edging", false);
		
		edgingSub.add(new RavelryReferenceAttribute("Lace Edging", "lace-edging"));
		attributeMap.put("lace-edging", edgingSub.get(edgingSub.size()-1));
		checkedMap.put("lace-edging", false);
		
		edgingSub.add(new RavelryReferenceAttribute("Other Edging", "other-edging"));
		attributeMap.put("other-edging", edgingSub.get(edgingSub.size()-1));
		checkedMap.put("other-edging", false);
		
		edging.subcategories = edgingSub;
		
		RavelryReferenceAttribute neck = new RavelryReferenceAttribute("Neck", "neck");
		attributeMap.put("neck", neck);
		checkedMap.put("neck", false);
		neck.checkable = false;
		ArrayList<RavelryReferenceAttribute> neckSub = new ArrayList<RavelryReferenceAttribute>();
		
		neckSub.add(new RavelryReferenceAttribute("Ballet Neck", "ballet-neck"));
		attributeMap.put("ballet-neck", neckSub.get(neckSub.size()-1));
		checkedMap.put("ballet-neck", false);
		
		neckSub.add(new RavelryReferenceAttribute("Boat Neck", "boat-neck"));
		attributeMap.put("boat-neck", neckSub.get(neckSub.size()-1));
		checkedMap.put("boat-neck", false);
		
		neckSub.add(new RavelryReferenceAttribute("Cowl", "cowl-neck"));
		attributeMap.put("cowl-neck", neckSub.get(neckSub.size()-1));
		checkedMap.put("cowl-neck", false);
		
		neckSub.add(new RavelryReferenceAttribute("Crew Neck", "crew-neck"));
		attributeMap.put("crew-neck", neckSub.get(neckSub.size()-1));
		checkedMap.put("crew-neck", false);
		
		neckSub.add(new RavelryReferenceAttribute("Funnel Neck", "funnel-neck"));
		attributeMap.put("funnel-neck", neckSub.get(neckSub.size()-1));
		checkedMap.put("funnel-neck", false);
		
		neckSub.add(new RavelryReferenceAttribute("Halter", "halter-neck"));
		attributeMap.put("halter-neck", neckSub.get(neckSub.size()-1));
		checkedMap.put("halter-neck", false);
		
		neckSub.add(new RavelryReferenceAttribute("Henley", "henley-neck"));
		attributeMap.put("henley-neck", neckSub.get(neckSub.size()-1));
		checkedMap.put("henley-neck", false);
		
		neckSub.add(new RavelryReferenceAttribute("Keyhole Neck", "keyhole-neck"));
		attributeMap.put("keyhole-neck", neckSub.get(neckSub.size()-1));
		checkedMap.put("keyhole-neck", false);
		
		neckSub.add(new RavelryReferenceAttribute("Mock Turtle", "mock-turtleneck"));
		attributeMap.put("mock-turtleneck", neckSub.get(neckSub.size()-1));
		checkedMap.put("mock-turtleneck", false);
		
		neckSub.add(new RavelryReferenceAttribute("Scoop Neck", "scoop-neck"));
		attributeMap.put("scoop-neck", neckSub.get(neckSub.size()-1));
		checkedMap.put("scoop-neck", false);
		
		neckSub.add(new RavelryReferenceAttribute("Square Neck", "square-neck"));
		attributeMap.put("square-neck", neckSub.get(neckSub.size()-1));
		checkedMap.put("square-neck", false);
		
		neckSub.add(new RavelryReferenceAttribute("Surplice Neck", "surplice-neck"));
		attributeMap.put("surplice-neck", neckSub.get(neckSub.size()-1));
		checkedMap.put("surplice-neck", false);
		
		neckSub.add(new RavelryReferenceAttribute("Sweetheart Neck", "sweetheart-neck"));
		attributeMap.put("sweetheart-neck", neckSub.get(neckSub.size()-1));
		checkedMap.put("sweetheart-neck", false);
		
		neckSub.add(new RavelryReferenceAttribute("Turtle Neck", "turtleneck"));
		attributeMap.put("turtleneck", neckSub.get(neckSub.size()-1));
		checkedMap.put("turtleneck", false);
		
		neckSub.add(new RavelryReferenceAttribute("V Neck", "v-neck"));
		attributeMap.put("v-neck", neckSub.get(neckSub.size()-1));
		checkedMap.put("v-neck", false);
		
		neck.subcategories = neckSub;
		
		RavelryReferenceAttribute pocket = new RavelryReferenceAttribute("Pocket", "pock");
		attributeMap.put("pock", pocket);
		checkedMap.put("pock", false);
		pocket.checkable = false;
		ArrayList<RavelryReferenceAttribute> pocketSub = new ArrayList<RavelryReferenceAttribute>();
		
		pocketSub.add(new RavelryReferenceAttribute("Afterthought Pocket", "afterthought-pocket"));
		attributeMap.put("afterthought-pocket", pocketSub.get(pocketSub.size()-1));
		checkedMap.put("afterthought-pocket", false);
		
		pocketSub.add(new RavelryReferenceAttribute("Any Pockets", "pockets"));
		attributeMap.put("pockets", pocketSub.get(pocketSub.size()-1));
		checkedMap.put("pockets", false);
		
		pocketSub.add(new RavelryReferenceAttribute("Hidden/Inseam", "hidden-pocket"));
		attributeMap.put("hidden-pocket", pocketSub.get(pocketSub.size()-1));
		checkedMap.put("hidden-pocket", false);
		
		pocketSub.add(new RavelryReferenceAttribute("Patch", "patch-pocket"));
		attributeMap.put("patch-pocket", pocketSub.get(pocketSub.size()-1));
		checkedMap.put("patch-pocket", false);
		
		pocketSub.add(new RavelryReferenceAttribute("Set-In", "set-in-pocket"));
		attributeMap.put("set-in-pocket", pocketSub.get(pocketSub.size()-1));
		checkedMap.put("set-in-pocket", false);
		
		pocketSub.add(new RavelryReferenceAttribute("Tubular", "tubular-pocket"));
		attributeMap.put("tubular-pocket", pocketSub.get(pocketSub.size()-1));
		checkedMap.put("tubular-pocket", false);
		
		pocket.subcategories = pocketSub;
		
		RavelryReferenceAttribute sleeve = new RavelryReferenceAttribute("Sleeve", "sleeve");
		attributeMap.put("sleeve", sleeve);
		checkedMap.put("sleeve", false);
		sleeve.checkable = false;
		ArrayList<RavelryReferenceAttribute> sleeveSub = new ArrayList<RavelryReferenceAttribute>();

		sleeveSub.add(new RavelryReferenceAttribute("3/4 Length", "3-4-sleeve"));
		attributeMap.put("3-4-sleeve", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("3-4-sleeve", false);

		sleeveSub.add(new RavelryReferenceAttribute("Nalgar (EZ notation)", "nalgar-sleeve"));
		attributeMap.put("nalgar-sleeve", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("nalgar-sleeve", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Any Sleeves", "sleeves"));
		attributeMap.put("sleeves", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("sleeves", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Bracelet Length", "bracelet-sleeve"));
		attributeMap.put("bracelet-sleeve", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("bracelet-sleeve", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Cap", "cap-sleeve"));
		attributeMap.put("cap-sleeve", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("cap-sleeve", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Contiguous", "contiguous"));
		attributeMap.put("contiguous", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("contiguous", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Cuffed", "cuffed-sleeve"));
		attributeMap.put("cuffed-sleeve", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("cuffed-sleeve", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Dolman", "dolman-sleeve"));
		attributeMap.put("dolman-sleeve", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("dolman-sleeve", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Drop", "drop-sleeve"));
		attributeMap.put("drop-sleeve", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("drop-sleeve", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Elbow", "elbow-sleeve"));
		attributeMap.put("elbow-sleeve", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("elbow-sleeve", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Flutter", "flutter-sleeve"));
		attributeMap.put("flutter-sleeve", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("flutter-sleeve", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Kimono", "kimono-sleeve"));
		attributeMap.put("kimono-sleeve", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("kimono-sleeve", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Long", "long-sleeve"));
		attributeMap.put("long-sleeve", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("long-sleeve", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Modified Drop", "modified-drop-sleeve"));
		attributeMap.put("modified-drop-sleeve", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("modified-drop-sleeve", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Puffed", "puffed-sleeve"));
		attributeMap.put("puffed-sleeve", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("puffed-sleeve", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Raglan", "raglan-sleeve"));
		attributeMap.put("raglan-sleeve", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("raglan-sleeve", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Saddle Shoulder", "saddle-shoulder"));
		attributeMap.put("saddle-shoulder", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("saddle-shoulder", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Set In", "set-in-sleeve"));
		attributeMap.put("set-in-sleeve", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("set-in-sleeve", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Short", "short-sleeve"));
		attributeMap.put("short-sleeve", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("short-sleeve", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Sleeveless Sleeve", "sleeveless"));
		attributeMap.put("sleeveless", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("sleeveless", false);
		
		sleeveSub.add(new RavelryReferenceAttribute("Tulip", "tulip-sleeve"));
		attributeMap.put("tulip-sleeve", sleeveSub.get(sleeveSub.size()-1));
		checkedMap.put("tulip-sleeve", false);

		sleeve.subcategories = sleeveSub;
		
		designSub.add(new RavelryReferenceAttribute("Appliqued/Embellished", "appliqued"));
		attributeMap.put("appliqued", designSub.get(designSub.size()-1));
		checkedMap.put("appliqued", false);
		
		designSub.add(new RavelryReferenceAttribute("Asymmetric", "asymmetric"));
		attributeMap.put("asymmetric", designSub.get(designSub.size()-1));
		checkedMap.put("asymmetric", false);
		
		designSub.add(new RavelryReferenceAttribute("Back Fastening", "backfastening"));
		attributeMap.put("backfastening", designSub.get(designSub.size()-1));
		checkedMap.put("backfastening", false);
		
		designSub.add(new RavelryReferenceAttribute("Beads / Pailettes", "beads"));
		attributeMap.put("beads", designSub.get(designSub.size()-1));
		checkedMap.put("beads", false);
		
		designSub.add(new RavelryReferenceAttribute("Box Pleats", "box-pleats"));
		attributeMap.put("box-pleats", designSub.get(designSub.size()-1));
		checkedMap.put("box-pleats", false);
		
		designSub.add(new RavelryReferenceAttribute("Braids/Plaiting", "braids-plaiting"));
		attributeMap.put("braids-plaiting", designSub.get(designSub.size()-1));
		checkedMap.put("braids-plaiting", false);
		
		designSub.add(new RavelryReferenceAttribute("Buttoned", "buttoned"));
		attributeMap.put("buttoned", designSub.get(designSub.size()-1));
		checkedMap.put("buttoned", false);
		
		designSub.add(new RavelryReferenceAttribute("Circular Yoke", "circular-yoke"));
		attributeMap.put("circular-yoke", designSub.get(designSub.size()-1));
		checkedMap.put("circular-yoke", false);
		
		designSub.add(new RavelryReferenceAttribute("Convertible", "convertible"));
		attributeMap.put("convertible", designSub.get(designSub.size()-1));
		checkedMap.put("convertible", false);
		
		designSub.add(new RavelryReferenceAttribute("Cropped", "cropped"));
		attributeMap.put("cropped", designSub.get(designSub.size()-1));
		checkedMap.put("cropped", false);
		
		designSub.add(new RavelryReferenceAttribute("Darts", "darts"));
		attributeMap.put("darts", designSub.get(designSub.size()-1));
		checkedMap.put("darts", false);
		
		designSub.add(new RavelryReferenceAttribute("Double Breasted", "doublebreasted"));
		attributeMap.put("doublebreasted", designSub.get(designSub.size()-1));
		checkedMap.put("doublebreasted", false);
		
		designSub.add(new RavelryReferenceAttribute("Duplicate Stitch", "duplicate-stitch"));
		attributeMap.put("duplicate-stitch", designSub.get(designSub.size()-1));
		checkedMap.put("duplicate-stitch", false);
		
		designSub.add(new RavelryReferenceAttribute("Embroidery", "embroidery"));
		attributeMap.put("embroidery", designSub.get(designSub.size()-1));
		checkedMap.put("embroidery", false);
		
		designSub.add(new RavelryReferenceAttribute("Empire Waist", "empire"));
		attributeMap.put("empire", designSub.get(designSub.size()-1));
		checkedMap.put("empire", false);
		
		designSub.add(new RavelryReferenceAttribute("Facings", "facings"));
		attributeMap.put("facings", designSub.get(designSub.size()-1));
		checkedMap.put("facings", false);
		
		designSub.add(new RavelryReferenceAttribute("Fringe", "fringe"));
		attributeMap.put("fringe", designSub.get(designSub.size()-1));
		checkedMap.put("fringe", false);
		
		designSub.add(new RavelryReferenceAttribute("Front Fastening", "front-fastening"));
		attributeMap.put("front-fastening", designSub.get(designSub.size()-1));
		checkedMap.put("front-fastening", false);
		
		designSub.add(new RavelryReferenceAttribute("Gathers", "gathers"));
		attributeMap.put("gathers", designSub.get(designSub.size()-1));
		checkedMap.put("gathers", false);
		
		designSub.add(new RavelryReferenceAttribute("Hems", "hems"));
		attributeMap.put("hems", designSub.get(designSub.size()-1));
		checkedMap.put("hems", false);
		
		designSub.add(new RavelryReferenceAttribute("Hood", "hood"));
		attributeMap.put("hood", designSub.get(designSub.size()-1));
		checkedMap.put("hood", false);
		
		designSub.add(new RavelryReferenceAttribute("Lined", "lined"));
		attributeMap.put("lined", designSub.get(designSub.size()-1));
		checkedMap.put("lined", false);
		
		designSub.add(new RavelryReferenceAttribute("Notched", "notched"));
		attributeMap.put("notched", designSub.get(designSub.size()-1));
		checkedMap.put("notched", false);
		
		designSub.add(new RavelryReferenceAttribute("Peplum", "peplum"));
		attributeMap.put("peplum", designSub.get(designSub.size()-1));
		checkedMap.put("peplum", false);
		
		designSub.add(new RavelryReferenceAttribute("Picots", "picots"));
		attributeMap.put("picots", designSub.get(designSub.size()-1));
		checkedMap.put("picots", false);
		
		designSub.add(new RavelryReferenceAttribute("Pleated", "pleated"));
		attributeMap.put("pleated", designSub.get(designSub.size()-1));
		checkedMap.put("pleated", false);
		
		designSub.add(new RavelryReferenceAttribute("Racer Back", "racerback"));
		attributeMap.put("racerback", designSub.get(designSub.size()-1));
		checkedMap.put("racerback", false);
		
		designSub.add(new RavelryReferenceAttribute("Ruffles", "ruffles"));
		attributeMap.put("ruffles", designSub.get(designSub.size()-1));
		checkedMap.put("ruffles", false);
		
		designSub.add(new RavelryReferenceAttribute("Shoulder Fastening", "shoulder-fastening"));
		attributeMap.put("shoulder-fastening", designSub.get(designSub.size()-1));
		checkedMap.put("shoulder-fastening", false);
		
		designSub.add(new RavelryReferenceAttribute("Single Breasted", "single-breasted"));
		attributeMap.put("single-breasted", designSub.get(designSub.size()-1));
		checkedMap.put("single-breasted", false);
		
		designSub.add(new RavelryReferenceAttribute("Snaps", "snaps"));
		attributeMap.put("snaps", designSub.get(designSub.size()-1));
		checkedMap.put("snaps", false);
		
		designSub.add(new RavelryReferenceAttribute("Straight", "straight"));
		attributeMap.put("straight", designSub.get(designSub.size()-1));
		checkedMap.put("straight", false);
		
		designSub.add(new RavelryReferenceAttribute("Stripes", "stripes"));
		attributeMap.put("stripes", designSub.get(designSub.size()-1));
		checkedMap.put("stripes", false);
		
		designSub.add(new RavelryReferenceAttribute("Swing", "swing"));
		attributeMap.put("swing", designSub.get(designSub.size()-1));
		checkedMap.put("swing", false);
		
		designSub.add(new RavelryReferenceAttribute("Tassel / Pompom", "tassel"));
		attributeMap.put("tassel", designSub.get(designSub.size()-1));
		checkedMap.put("tassel", false);
		
		designSub.add(new RavelryReferenceAttribute("Tied", "tied"));
		attributeMap.put("tied", designSub.get(designSub.size()-1));
		checkedMap.put("tied", false);
		
		designSub.add(new RavelryReferenceAttribute("Waist Shaping", "waist"));
		attributeMap.put("waist", designSub.get(designSub.size()-1));
		checkedMap.put("waist", false);
		
		designSub.add(new RavelryReferenceAttribute("Wrap", "wrap"));
		attributeMap.put("wrap", designSub.get(designSub.size()-1));
		checkedMap.put("wrap", false);
		
		designSub.add(new RavelryReferenceAttribute("Zipper", "zipper"));
		attributeMap.put("zipper", designSub.get(designSub.size()-1));
		checkedMap.put("zipper", false);
		
		design.subcategories = designSub;
		
		topLevelAttributes.add(design);
		
		//Fabric Characterists
		RavelryReferenceAttribute fabric = new RavelryReferenceAttribute("Fabric Characteristics", "fabriccharacteristics");
		attributeMap.put("fabriccharacteristics", fabric);
		checkedMap.put("fabriccharacteristics", false);
		fabric.checkable = false;
		ArrayList<RavelryReferenceAttribute> fabricSub = new ArrayList<RavelryReferenceAttribute>(); 

		fabricSub.add(new RavelryReferenceAttribute("Bubble/Popcurn/Nupp", "bubble-or-popcorn"));
		attributeMap.put("bubble-or-popcorn", fabricSub.get(fabricSub.size()-1));
		checkedMap.put("bubble-or-popcorn", false);
		
		fabricSub.add(new RavelryReferenceAttribute("Cables", "cables"));
		attributeMap.put("cables", fabricSub.get(fabricSub.size()-1));
		checkedMap.put("cables", false);
		
		fabricSub.add(new RavelryReferenceAttribute("Chevron/Flame Stitch", "chevron"));
		attributeMap.put("chevron", fabricSub.get(fabricSub.size()-1));
		checkedMap.put("chevron", false);

		fabricSub.add(new RavelryReferenceAttribute("Dropped Stitches", "dropped-stitches"));
		attributeMap.put("dropped-stitches", fabricSub.get(fabricSub.size()-1));
		checkedMap.put("dropped-stitches", false);
		
		fabricSub.add(new RavelryReferenceAttribute("Elastic", "elastic"));
		attributeMap.put("elastic", fabricSub.get(fabricSub.size()-1));
		checkedMap.put("elastic", false);
		
		fabricSub.add(new RavelryReferenceAttribute("Eyelets", "eyelets"));
		attributeMap.put("eyelets", fabricSub.get(fabricSub.size()-1));
		checkedMap.put("eyelets", false);
		
		fabricSub.add(new RavelryReferenceAttribute("Lace", "lace"));
		attributeMap.put("lace", fabricSub.get(fabricSub.size()-1));
		checkedMap.put("lace", false);
		
		fabricSub.add(new RavelryReferenceAttribute("Mesh", "mesh"));
		attributeMap.put("mesh", fabricSub.get(fabricSub.size()-1));
		checkedMap.put("mesh", false);
		
		fabricSub.add(new RavelryReferenceAttribute("Reversible", "reversible"));
		attributeMap.put("reversible", fabricSub.get(fabricSub.size()-1));
		checkedMap.put("reversible", false);
		
		fabricSub.add(new RavelryReferenceAttribute("Ribbed / Ribbing", "ribbed"));
		attributeMap.put("ribbed", fabricSub.get(fabricSub.size()-1));
		checkedMap.put("ribbed", false);
		
		fabricSub.add(new RavelryReferenceAttribute("Ripple", "ripple"));
		attributeMap.put("ripple", fabricSub.get(fabricSub.size()-1));
		checkedMap.put("ripple", false);
		
		fabricSub.add(new RavelryReferenceAttribute("Slipped Stitches", "slipped-stitches"));
		attributeMap.put("slipped-stitches", fabricSub.get(fabricSub.size()-1));
		checkedMap.put("slipped-stitches", false);
		
		fabricSub.add(new RavelryReferenceAttribute("Smocked", "smocked"));
		attributeMap.put("smocked", fabricSub.get(fabricSub.size()-1));
		checkedMap.put("smocked", false);
		
		fabricSub.add(new RavelryReferenceAttribute("Textured", "textured"));
		attributeMap.put("textured", fabricSub.get(fabricSub.size()-1));
		checkedMap.put("textured", false);
		
		fabricSub.add(new RavelryReferenceAttribute("Twisted Stitches", "twisted-stitches"));
		attributeMap.put("twisted-stitches", fabricSub.get(fabricSub.size()-1));
		checkedMap.put("twisted-stitches", false);
		
		fabric.subcategories = fabricSub;
		
		topLevelAttributes.add(fabric);
		
		//Mature Content
		RavelryReferenceAttribute mature = new RavelryReferenceAttribute("Mature Content", "maturecontent");
		attributeMap.put("maturecontent", mature);
		checkedMap.put("maturecontent", false);
		mature.checkable = false;
		ArrayList<RavelryReferenceAttribute> matureSub = new ArrayList<RavelryReferenceAttribute>(); 

		matureSub.add(new RavelryReferenceAttribute("Mature Content", "mature"));
		attributeMap.put("mature", matureSub.get(matureSub.size()-1));
		checkedMap.put("mature", false);
		
		mature.subcategories = matureSub;
		
		topLevelAttributes.add(mature);
		
		//Patern Instructions
		RavelryReferenceAttribute pattern = new RavelryReferenceAttribute("Pattern Instructions", "patterninstructions");
		attributeMap.put("patterninstructions", pattern);
		checkedMap.put("patterninstructions", false);
		pattern.checkable = false;
		ArrayList<RavelryReferenceAttribute> patternSub = new ArrayList<RavelryReferenceAttribute>(); 

		patternSub.add(new RavelryReferenceAttribute("Chart", "chart"));
		attributeMap.put("chart", patternSub.get(patternSub.size()-1));
		checkedMap.put("chart", false);
		
		patternSub.add(new RavelryReferenceAttribute("Has Machine Instruction", "machine-instructions"));
		attributeMap.put("machine-instructions", patternSub.get(patternSub.size()-1));
		checkedMap.put("machine-instructions", false);
		
		patternSub.add(new RavelryReferenceAttribute("Has Schematic", "schematic"));
		attributeMap.put("schematic", patternSub.get(patternSub.size()-1));
		checkedMap.put("schematic", false);
		
		patternSub.add(new RavelryReferenceAttribute("Photo Tutorial", "phototutorial"));
		attributeMap.put("phototutorial", patternSub.get(patternSub.size()-1));
		checkedMap.put("phototutorial", false);
		
		patternSub.add(new RavelryReferenceAttribute("Pattern Recipe", "pattern-recipe"));
		attributeMap.put("pattern-recipe", patternSub.get(patternSub.size()-1));
		checkedMap.put("pattern-recipe", false);
		
		patternSub.add(new RavelryReferenceAttribute("Video Tutorial", "video-tutorial"));
		attributeMap.put("video-tutorial", patternSub.get(patternSub.size()-1));
		checkedMap.put("video-tutorial", false);
		
		patternSub.add(new RavelryReferenceAttribute("Written Pattern", "written-pattern"));
		attributeMap.put("written-pattern", patternSub.get(patternSub.size()-1));
		checkedMap.put("written-pattern", false);
		
		pattern.subcategories = patternSub;
		
		topLevelAttributes.add(pattern);
		
		//Regional / Ethnic Styles
		RavelryReferenceAttribute region = new RavelryReferenceAttribute("Regional/Ethnic Styles", "regional");
		attributeMap.put("regional", region);
		checkedMap.put("regional", false);
		region.checkable = false;
		ArrayList<RavelryReferenceAttribute> regionSub = new ArrayList<RavelryReferenceAttribute>(); 
		
		regionSub.add(new RavelryReferenceAttribute("Amigurumi", "amigurumi"));
		attributeMap.put("amigurumi", regionSub.get(regionSub.size()-1));
		checkedMap.put("amigurumi", false);
		
		regionSub.add(new RavelryReferenceAttribute("Andean / Peruvian", "andean"));
		attributeMap.put("andrean", regionSub.get(regionSub.size()-1));
		checkedMap.put("andean", false);
		
		regionSub.add(new RavelryReferenceAttribute("Aran", "aran"));
		attributeMap.put("aran", regionSub.get(regionSub.size()-1));
		checkedMap.put("aran", false);

		regionSub.add(new RavelryReferenceAttribute("Bavarian", "bavarian"));
		attributeMap.put("bavarian", regionSub.get(regionSub.size()-1));
		checkedMap.put("bavarian", false);
		
		regionSub.add(new RavelryReferenceAttribute("Cowichan (Salish)", "cowichan"));
		attributeMap.put("cowichan", regionSub.get(regionSub.size()-1));
		checkedMap.put("cowichan", false);
		
		regionSub.add(new RavelryReferenceAttribute("Danish", "danish"));
		attributeMap.put("danish", regionSub.get(regionSub.size()-1));
		checkedMap.put("danish", false);
		
		regionSub.add(new RavelryReferenceAttribute("Estonian", "estonian"));
		attributeMap.put("estonian", regionSub.get(regionSub.size()-1));
		checkedMap.put("estonian", false);
		
		regionSub.add(new RavelryReferenceAttribute("Fair Isle", "fairisle"));
		attributeMap.put("fairisle", regionSub.get(regionSub.size()-1));
		checkedMap.put("fairisle", false);
		
		regionSub.add(new RavelryReferenceAttribute("Faroese", "faroese"));
		attributeMap.put("faroese", regionSub.get(regionSub.size()-1));
		checkedMap.put("faroese", false);
		
		regionSub.add(new RavelryReferenceAttribute("Finnish / Suomi", "finnish"));
		attributeMap.put("finnish", regionSub.get(regionSub.size()-1));
		checkedMap.put("finnish", false);
		
		regionSub.add(new RavelryReferenceAttribute("Guernsey/Gansey", "guernsey"));
		attributeMap.put("guernsey", regionSub.get(regionSub.size()-1));
		checkedMap.put("guernsey", false);
		
		regionSub.add(new RavelryReferenceAttribute("Icelandic", "icelandic"));
		attributeMap.put("icelandic", regionSub.get(regionSub.size()-1));
		checkedMap.put("icelandic", false);
		
		regionSub.add(new RavelryReferenceAttribute("Irish", "irish"));
		attributeMap.put("irish", regionSub.get(regionSub.size()-1));
		checkedMap.put("irish", false);
		
		regionSub.add(new RavelryReferenceAttribute("Latvian", "latvian"));
		attributeMap.put("latvian", regionSub.get(regionSub.size()-1));
		checkedMap.put("latvian", false);
		
		regionSub.add(new RavelryReferenceAttribute("Norwegian", "norwegian"));
		attributeMap.put("norwegian", regionSub.get(regionSub.size()-1));
		checkedMap.put("norwegian", false);
		
		regionSub.add(new RavelryReferenceAttribute("Orenburg", "orenburg"));
		attributeMap.put("orenburg", regionSub.get(regionSub.size()-1));
		checkedMap.put("orenburg", false);
		
		regionSub.add(new RavelryReferenceAttribute("Sami / Lapp", "sami-lapp"));
		attributeMap.put("sami-lapp", regionSub.get(regionSub.size()-1));
		checkedMap.put("sami-lapp", false);
		
		regionSub.add(new RavelryReferenceAttribute("Shetland", "Shetland"));
		attributeMap.put("Shetland", regionSub.get(regionSub.size()-1));
		checkedMap.put("Shetland", false);
		
		regionSub.add(new RavelryReferenceAttribute("Swedish", "swedish"));
		attributeMap.put("swedish", regionSub.get(regionSub.size()-1));
		checkedMap.put("swedish", false);
		
		regionSub.add(new RavelryReferenceAttribute("Turkish", "turkish"));
		attributeMap.put("turkish", regionSub.get(regionSub.size()-1));
		checkedMap.put("turkish", false);
		
		region.subcategories = regionSub;
		
		topLevelAttributes.add(region);
		
		//Shapes
		RavelryReferenceAttribute shapes = new RavelryReferenceAttribute("Shapes", "shapes");
		attributeMap.put("shapes", shapes);
		checkedMap.put("shapes", false);
		shapes.checkable = false;
		ArrayList<RavelryReferenceAttribute> shapesSub = new ArrayList<RavelryReferenceAttribute>(); 

		shapesSub.add(new RavelryReferenceAttribute("3 Dimensional", "3-dimensional"));
		attributeMap.put("3-dimensional", shapesSub.get(shapesSub.size()-1));
		checkedMap.put("3-dimensional", false);

		shapesSub.add(new RavelryReferenceAttribute("Circle", "circle-shaped"));
		attributeMap.put("circle-shaped", shapesSub.get(shapesSub.size()-1));
		checkedMap.put("circle-shaped", false);
		
		shapesSub.add(new RavelryReferenceAttribute("Crescent", "crescent-shape"));
		attributeMap.put("crescent-shape", shapesSub.get(shapesSub.size()-1));
		checkedMap.put("crescent-shape", false);
		
		shapesSub.add(new RavelryReferenceAttribute("Cube", "cube-shaped"));
		attributeMap.put("cube-shaped", shapesSub.get(shapesSub.size()-1));
		checkedMap.put("cube-shaped", false);
		
		shapesSub.add(new RavelryReferenceAttribute("Half-Circle", "halfcircle-shape"));
		attributeMap.put("halfcircle-shape", shapesSub.get(shapesSub.size()-1));
		checkedMap.put("halfcircle-shape", false);
		
		shapesSub.add(new RavelryReferenceAttribute("Hexagon", "hexagon"));
		attributeMap.put("hexagon", shapesSub.get(shapesSub.size()-1));
		checkedMap.put("hexagon", false);
		
		shapesSub.add(new RavelryReferenceAttribute("Octagon", "octagon"));
		attributeMap.put("octagon", shapesSub.get(shapesSub.size()-1));
		checkedMap.put("octagon", false);
		
		shapesSub.add(new RavelryReferenceAttribute("Oval", "oval"));
		attributeMap.put("oval", shapesSub.get(shapesSub.size()-1));
		checkedMap.put("oval", false);
		
		shapesSub.add(new RavelryReferenceAttribute("Pentagon", "pentagon-shape"));
		attributeMap.put("pentagon-shape", shapesSub.get(shapesSub.size()-1));
		checkedMap.put("pentagon-shape", false);
		
		shapesSub.add(new RavelryReferenceAttribute("Pyramid", "pyramid"));
		attributeMap.put("pyramid", shapesSub.get(shapesSub.size()-1));
		checkedMap.put("pyramid", false);
		
		shapesSub.add(new RavelryReferenceAttribute("Rectangle", "rectangle"));
		attributeMap.put("rectangle", shapesSub.get(shapesSub.size()-1));
		checkedMap.put("rectangle", false);
		
		shapesSub.add(new RavelryReferenceAttribute("Sphere", "sphere-shaped"));
		attributeMap.put("sphere-shaped", shapesSub.get(shapesSub.size()-1));
		checkedMap.put("sphere-shaped", false);
		
		shapesSub.add(new RavelryReferenceAttribute("Square", "square"));
		attributeMap.put("square", shapesSub.get(shapesSub.size()-1));
		checkedMap.put("square", false);
		
		shapesSub.add(new RavelryReferenceAttribute("Star", "star-shaped"));
		attributeMap.put("star-shaped", shapesSub.get(shapesSub.size()-1));
		checkedMap.put("star-shaped", false);

		shapesSub.add(new RavelryReferenceAttribute("Triangle", "triangle-shaped"));
		attributeMap.put("triangle-shaped", shapesSub.get(shapesSub.size()-1));
		checkedMap.put("triangle-shaped", false);
		
		shapes.subcategories = shapesSub;
		
		topLevelAttributes.add(shapes);
		
		//Sock Techniques 
		RavelryReferenceAttribute sock = new RavelryReferenceAttribute("Sock Techniques", "socks");
		attributeMap.put("socks", sock);
		checkedMap.put("socks", false);
		sock.checkable = false;
		ArrayList<RavelryReferenceAttribute> sockSub = new ArrayList<RavelryReferenceAttribute>(); 
		
		RavelryReferenceAttribute heel = new RavelryReferenceAttribute("Heel", "heel");
		attributeMap.put("heel", heel);
		checkedMap.put("heel", false);
		heel.checkable = false;
		ArrayList<RavelryReferenceAttribute> heelSub = new ArrayList<RavelryReferenceAttribute>();
		
		heelSub.add(new RavelryReferenceAttribute("Dutch Heel", "dutch-heel"));
		attributeMap.put("dutch-heel", heelSub.get(heelSub.size()-1));
		checkedMap.put("dutch-heel", false);
		
		heelSub.add(new RavelryReferenceAttribute("Afterthought Heel", "afterthought-heel"));
		attributeMap.put("afterthought-heel", heelSub.get(heelSub.size()-1));
		checkedMap.put("afterthought-heel", false);
		
		heelSub.add(new RavelryReferenceAttribute("Heel Flap", "heel-flap"));
		attributeMap.put("heel-flap", heelSub.get(heelSub.size()-1));
		checkedMap.put("heel-flap", false);
		
		heelSub.add(new RavelryReferenceAttribute("Other Heel", "other-heel"));
		attributeMap.put("other-heel", heelSub.get(heelSub.size()-1));
		checkedMap.put("other-heel", false);

		heelSub.add(new RavelryReferenceAttribute("Short Row Heel", "short-row-heel"));
		attributeMap.put("short-row-heel", heelSub.get(heelSub.size()-1));
		checkedMap.put("short-row-heel", false);
		
		heel.subcategories = heelSub;
		
		sockSub.add(heel);
		
		RavelryReferenceAttribute toe = new RavelryReferenceAttribute("Toe", "toe");
		attributeMap.put("toe", toe);
		checkedMap.put("toe", false);
		
		toe.checkable = false;
		ArrayList<RavelryReferenceAttribute> toeSub = new ArrayList<RavelryReferenceAttribute>();
		
		toeSub.add(new RavelryReferenceAttribute("Short Row Toe", "short-row-toe"));
		attributeMap.put("short-row-toe", toeSub.get(toeSub.size()-1));
		checkedMap.put("short-row-toe", false);
		
		toeSub.add(new RavelryReferenceAttribute("Star", "star-toe"));
		attributeMap.put("star-toe", toeSub.get(toeSub.size()-1));
		checkedMap.put("star-toe", false);
		
		toeSub.add(new RavelryReferenceAttribute("Wide", "wide-toe"));
		attributeMap.put("wide-toe", toeSub.get(toeSub.size()-1));
		checkedMap.put("wide-toe", false);
		
		toeSub.add(new RavelryReferenceAttribute("Other", "other-toe"));
		attributeMap.put("other-toe", toeSub.get(toeSub.size()-1));
		checkedMap.put("other-toe", false);
		
		toe.subcategories = toeSub;
		
		sockSub.add(toe);
		
		sockSub.add(new RavelryReferenceAttribute("No Shaping", "noshaping"));
		attributeMap.put("noshaping", sockSub.get(sockSub.size()-1));
		checkedMap.put("noshaping", false);
		
		sockSub.add(new RavelryReferenceAttribute("Seamed Sock", "seamed-sock"));
		attributeMap.put("seamed-sock", sockSub.get(sockSub.size()-1));
		checkedMap.put("seamed-sock", false);
		
		sockSub.add(new RavelryReferenceAttribute("Shaped Arches", "shaped-arches"));
		attributeMap.put("shaped-arches", sockSub.get(sockSub.size()-1));
		checkedMap.put("shaped-arches", false);
		
		sockSub.add(new RavelryReferenceAttribute("Starting In Middle", "start-in-middle"));
		attributeMap.put("start-in-middle", sockSub.get(sockSub.size()-1));
		checkedMap.put("start-in-middle", false);
		
		sockSub.add(new RavelryReferenceAttribute("Toe Up", "toe-up"));
		attributeMap.put("toe-up", sockSub.get(sockSub.size()-1));
		checkedMap.put("toe-up", false);
		
		sockSub.add(new RavelryReferenceAttribute("Top/Cuff Down", "top-cuff-down"));
		attributeMap.put("top-cuff-down", sockSub.get(sockSub.size()-1));
		checkedMap.put("top-cuff-down", false);
		
		sockSub.add(new RavelryReferenceAttribute("Two at a Time", "2-at-a-time"));
		attributeMap.put("2-at-a-time", sockSub.get(sockSub.size()-1));
		checkedMap.put("2-at-a-time", false);
		sock.subcategories = sockSub;
		
		topLevelAttributes.add(sock);
		
	}
}
