package com.knitci.pocket.patterns.attribute;

import java.io.Serializable;
import java.util.ArrayList;

public class RavelryReferenceAttribute implements Serializable {

	private static final long serialVersionUID = -5387818918826060691L;
	public final String name;
	public ArrayList<RavelryReferenceAttribute> subcategories;
	public boolean checked;
	public final String value;
	public boolean checkable;
	
	public RavelryReferenceAttribute(String name, String value) {
		this.name = name;
		subcategories = null;
		checked = false;
		this.value = value;
		this.checkable = true;
	}
	
}
