package com.knitci.pocket.patterns.attribute;

import java.util.ArrayList;

import object.CheckBoxColorfulPostback;
import object.CheckBoxSupreme;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.patterns.category.PatternChoosableCategoryActivity;
import com.knitci.pocket.patterns.category.RavelryReferenceCategory;
import com.knitci.pocket.patterns.manager.PatternManager;

public class PatternTopLevelAttributeActivity extends Activity implements CheckBoxColorfulPostback, OnClickListener {

	PatternManager patternManager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pattern_category);
		patternManager = KnitciApplication.class.cast(getApplication()).getPatternManager();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pattern_top_level_attribute, menu);
		return true;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		ArrayList<RavelryReferenceAttribute> attributes = KnitciApplication.class.cast(this.getApplication()).getPatternManager().getReferenceAttributes();

		LinearLayout sv = LinearLayout.class.cast(findViewById(R.id.categories_holder));
		if(sv.getChildCount() == 0) {
			for(int i = 0; i < attributes.size(); i++) {
				CheckBoxSupreme tv = new CheckBoxSupreme(getApplicationContext());
				tv.setChecked(patternManager.getAttributeManager().isAttributeChecked(attributes.get(i).value));
				tv.setCheckBoxColorfulListener(this);
				tv.setText(attributes.get(i).name);
				tv.setSubText(patternManager.getAttributeManager().getPartialString(attributes.get(i).value, attributes.get(i).value));
				String vv = attributes.get(i).value;
				tv.setTag(vv);
				tv.setOnClickListener(this);
				sv.addView(tv);
				if(!attributes.get(i).checkable) {
					tv.hideCheckBox();
				}
			}
		}
		else {
			for(int i = 0; i < sv.getChildCount(); i++) {
				CheckBoxSupreme tv = CheckBoxSupreme.class.cast(sv.getChildAt(i));
				tv.setSubText(patternManager.getAttributeManager().getPartialString(attributes.get(i).value, attributes.get(i).value));
			}
		}
	}


	@Override
	public void onClick(View v) {
		TextView tv = TextView.class.cast(v);
		CheckBoxSupreme supreme = CheckBoxSupreme.class.cast(tv.getTag());
		String value = (String) supreme.getTag();
		RavelryReferenceAttribute att = patternManager.getAttributeManager().getReferenceAttribute(value);

		Intent intent = new Intent(this, PatternChoosableAttributeActivity.class);
		intent.putExtra("ATTRIBUTE", att);
		startActivity(intent);
		overridePendingTransition(R.anim.animation_right_to_left, R.anim.animation_left_to_right);
	}

	@Override
	public boolean checkBoxClicked(String value, boolean checked) {
		patternManager.getAttributeManager().setAttributeChecked(value, checked);
		return true;
	}

}
