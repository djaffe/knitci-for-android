package com.knitci.pocket.patterns;

import java.util.ArrayList;

import object.KnitciActivity;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.patterns.manager.PatternManager;
import com.knitci.pocket.projects.ProjectListActivity;
import com.knitci.pocket.ravelry.RavelryManager;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPattern;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPhotoSet;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject.ProjectPhotoUrlSize;
import com.knitci.pocket.util.FullscreenImage;
import com.knitci.pocket.util.KnitciPreferences;
import com.knitci.yarnpicker.YarnPicker;
import com.knitci.yarnpicker.internal.IYarnPicker;


public class PatternDetailActivity extends KnitciActivity implements OnClickListener {

	int patternId;
	RavelryPattern pattern;
	PatternManager patternManager;
	RavelryManager ravelryManager;
	boolean addingToFavoriteInProgress;
	boolean removingFavoriteInProgress;
	

	// Action bar
	private ActionBar mActionBar;
	private LayoutInflater mInflater;
	private View mCustomView;
	private TextView mTitleTextView;

	private PatternImageAdapter imageAdapter;
	AlertDialog.Builder addQueueDialogBuilder;	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pattern_detail);
		initActionBar();
		stealAsyncListening();
		setupSlidingMenu();
		setupSecondaryMenu();
		patternId = getIntent().getExtras().getInt("PATTERNID");
		String permalink = getIntent().getExtras().getString("PATTERNPERMA");
		KnitciApplication app = KnitciApplication.class.cast(getApplicationContext());
		this.patternManager = app.getPatternManager();
		this.ravelryManager = app.getRavelryManager();
		addingToFavoriteInProgress = false;
		removingFavoriteInProgress = false;
		
		addQueueDialogBuilder = new AlertDialog.Builder(this);

		if(permalink != null && !permalink.isEmpty()) {
			RavelryPattern pattern = patternManager.getCachedPattern(permalink);
			if(pattern == null) {
				ravelryManager.retrievePattern(permalink);
			}
			else {
				this.pattern = pattern;
				populatePattern(pattern);
			}
		}

		else {
			RavelryPattern pattern = patternManager.getCachedPattern(patternId);
			if(pattern == null) {
				ravelryManager.retrievePattern(patternId);
			}
			else {
				this.pattern = pattern;
				populatePattern(pattern);
			}
		}

		ViewPager vp = (ViewPager) findViewById(R.id.patterndetail_imageviewpager);
		vp.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				ScrollView.class.cast(findViewById(R.id.patterndetail_scrollview)).requestDisallowInterceptTouchEvent(true);
				return false;
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.pattern_detail, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if(pattern == null) {
			return super.onPrepareOptionsMenu(menu);
		}

		// Handle favorite menu
		if(addingToFavoriteInProgress) {
			menu.findItem(R.id.patterndetail_menu_addtofavorites).setTitle("Adding to favorites...");
			menu.findItem(R.id.patterndetail_menu_addtofavorites).setEnabled(false);
		}
		else if(removingFavoriteInProgress) {
			menu.findItem(R.id.patterndetail_menu_addtofavorites).setTitle("Removing favorite...");
			menu.findItem(R.id.patterndetail_menu_addtofavorites).setEnabled(false);
		}
		else if(pattern.isFavorited()) {
			menu.findItem(R.id.patterndetail_menu_addtofavorites).setTitle("Remove from favorites");
			menu.findItem(R.id.patterndetail_menu_addtofavorites).setEnabled(true);
		}
		else {
			menu.findItem(R.id.patterndetail_menu_addtofavorites).setTitle("Add to favorites");
			menu.findItem(R.id.patterndetail_menu_addtofavorites).setEnabled(true);
		}

		// Handle Queue menu
		if(pattern.isQueued()) {
			menu.findItem(R.id.patterndetail_menu_addtoqueue).setTitle("Remove from queue");
		}
		else {
			menu.findItem(R.id.patterndetail_menu_addtoqueue).setTitle("Add to queue");
		}

		// Handle pattern menu
		if(!pattern.isFree()) {
			if(pattern.getPrice() > 0) {
				menu.findItem(R.id.patterndetail_menu_download).setTitle("Must purchase on Ravelry - $" + String.format("%.2f",pattern.getPrice()));
				menu.findItem(R.id.patterndetail_menu_download).setEnabled(false);
			}
			else {
				menu.findItem(R.id.patterndetail_menu_download).setTitle("Must purchase on Ravelry.");
				menu.findItem(R.id.patterndetail_menu_download).setEnabled(false);
			}
		}
		else if(pattern.isFree() && pattern.getDownloadLocation().getType().equals("ravelry")
				&& pattern.getDownloadLocation().isFree()
				&& pattern.getDownloadLocation().getUrl() != null
				&& !pattern.getDownloadLocation().getUrl().isEmpty()) {
			//Leave it as download pattern
		}
		else if(pattern.isFree() && pattern.isDownloadable()
				&& !pattern.isRavelryDownload()
				&& !pattern.getDownloadLocation().getType().equals("ravelry")
				&& pattern.getDownloadLocation().getUrl() != null
				&& !pattern.getDownloadLocation().getUrl().isEmpty()) {
			menu.findItem(R.id.patterndetail_menu_download).setTitle("Visit website");
		}
		else {
			menu.findItem(R.id.patterndetail_menu_download).setTitle("See details for Pattern");
			menu.findItem(R.id.patterndetail_menu_download).setEnabled(false);
		}

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		//return super.onMenuItemSelected(featureId, item);

		if(item != null && item.getTitle() != null) {
			String title = item.getTitle().toString();
			if(title.equals("View Projects")) {
				Intent intent = new Intent(this, ProjectListActivity.class);
				intent.putExtra("PATTERNID", patternId);
				startActivity(intent);
			}

			else if(title.equals("Add to favorites")) {
				addToFavorite();
			}
			else if(title.equals("Remove from favorites")) {
				removeFavorite();
			}

			else if(title.equals("Add to queue")) {
				addToQueue();
			}
			else if(title.equals("Remove from queue")) {
				removeFromQueue();
			}

			else if(title.equals("Add to library")) {

			}
			else if(title.equals("Remove from library")) {

			}

			else if(title.equals("Cast on")) {

			}
			else if(title.equals("Share this pattern")) {
				Intent shareIntent = new Intent(Intent.ACTION_SEND);
				shareIntent.setType("text/plain");
				shareIntent.putExtra(Intent.EXTRA_TEXT, "http://www.ravelry.com/patterns/library/"+pattern.getPermalink());
				startActivity(Intent.createChooser(shareIntent, "Share..."));
			}
			else if(title.equals("Download pattern")) {
				if(pattern.getDownloadLocation().isFree()) {
					String downloadUrl = pattern.getDownloadLocation().getUrl();
					downloadPattern(downloadUrl);
				}
			}

		}
		return true;
	}

	private void addToFavorite() {
		addingToFavoriteInProgress = true;
		ravelryManager.submitCreateFavorite("pattern", patternId, "My comment.");
	}

	private void removeFavorite() {
		removingFavoriteInProgress = true;
		ravelryManager.removeFavorite(this.pattern.getBookmarkId());
	}

	private void addToQueue() {
		Intent intent = new Intent(this, PatternQueueCreateActivity.class);
		intent.putExtra("PATTERNID", patternId);
		startActivity(intent);
	}

	private void removeFromQueue() {

	}

	@Override
	public void processFinish(RavelryResponse response) {
		super.processFinish(response);
		switch(response.task) {
		case PATTERN:
			RavelryPattern pattern = RavelryPattern.class.cast(response.result);
			patternManager.addPatternToCache(pattern);
			populatePattern(pattern);
			break;
		case FAVORITECREATE:
			addingToFavoriteInProgress = false;
			if(response.result != null) {
				this.pattern.setFavorited(true);
				supportInvalidateOptionsMenu();
			}
			break;
		case REMOVEFAVORITE:
			removingFavoriteInProgress = false;
			this.pattern.setFavorited(false);
			supportInvalidateOptionsMenu();
			break;
		default:
			break;
		}
	}

	public void populatePattern(RavelryPattern pattern) {
		this.pattern = pattern;

		populateFavorite(pattern.getFavoritesCount());
		populateProject(pattern.getProjectsCount());
		populateQueue(pattern.getQueuedProjectsCount());
		populateRating(pattern.getAverageRating());
		populateName(pattern.getPatternName());

		if(pattern.getAuthor() != null) {
			populateAuthor(pattern.getAuthor().getName());
		}

		populateDetails();
		populatePhotos();
		supportInvalidateOptionsMenu();
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		String url = (String) v.getTag();
		Intent intent = new Intent(this, FullscreenImage.class);
		intent.putExtra("url", url);
		startActivity(intent);
	}

	private void initActionBar() {
		mActionBar = getSupportActionBar();
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		mInflater = LayoutInflater.from(this);
		mCustomView = mInflater.inflate(R.layout.custom_action_layout, null);
		mTitleTextView = (TextView) mCustomView.findViewById(R.id.title_text);
		mTitleTextView.setText("My Own Title");
		mActionBar.setCustomView(mCustomView);
		mActionBar.setDisplayShowCustomEnabled(true);
		// mActionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.at_header_bg));
	}

	private void populateFavorite(int count) {
		TextView tv = (TextView) findViewById(R.id.patterndetail_favorites_count);
		ImageView img = (ImageView) findViewById(R.id.patterndetail_drawable_heart);
		if(count > 0) {
			tv.setText(String.valueOf(count));
			tv.setVisibility(View.VISIBLE);
			img.setVisibility(View.VISIBLE);
		}
		else {
			tv.setVisibility(View.GONE);
			img.setVisibility(View.GONE);
		}
	}

	private void populateQueue(int count) {
		TextView tv = (TextView) findViewById(R.id.patterndetail_queue_count);
		ImageView img = (ImageView) findViewById(R.id.patterndetail_drawable_queue);
		if(count > 0) {
			tv.setText(String.valueOf(count));
			tv.setVisibility(View.VISIBLE);
			img.setVisibility(View.VISIBLE);
		}
		else {
			tv.setVisibility(View.GONE);
			img.setVisibility(View.GONE);
		}
	}

	private void populateProject(int count) {
		TextView tv = (TextView) findViewById(R.id.patterndetail_project_count);
		ImageView img = (ImageView) findViewById(R.id.patterndetail_drawable_projects);
		if(count > 0) {
			tv.setText(String.valueOf(count));
			tv.setVisibility(View.VISIBLE);
			img.setVisibility(View.VISIBLE);
		}
		else {
			tv.setVisibility(View.GONE);
			img.setVisibility(View.GONE);
		}
	}

	private void populateRating(float rating) {
		RatingBar bar = (RatingBar) findViewById(R.id.patterndetail_ratingbar);
		bar.setRating(rating);
		bar.setIsIndicator(true);
	}

	private void populateName(String name) {
		TextView tv = (TextView) findViewById(R.id.patterndetail_name);
		tv.setText(name);
	}

	private void populateAuthor(String author) {
		TextView tv = (TextView) findViewById(R.id.patterndetail_author);
		tv.setText("by " + author);
	}

	private void populateDetails() {

		// Published In
		if(pattern.getPrintings() != null && pattern.getPrintings().getPrimarySource() != null) {
			TextView tv = (TextView) findViewById(R.id.patterndetail_publishedin);
			tv.setText(pattern.getPrintings().getPrimarySource().getName());
		}
		else if(pattern.getPrintings() != null && pattern.getPrintings().getSources() != null
				&& pattern.getPrintings().getSources().get(0) != null) {
			TextView tv = (TextView) findViewById(R.id.patterndetail_publishedin);
			tv.setText(pattern.getPrintings().getSources().get(0).getName());
		}
		else {
			LinearLayout.class.cast(findViewById(R.id.patterndetail_publishedin_layout)).setVisibility(View.GONE);
		}

		// Craft
		if(pattern.getCraft() != null && pattern.getCraft().getCraft() != null
				&& !pattern.getCraft().getCraft().isEmpty()) {
			TextView tv = (TextView) findViewById(R.id.patterndetail_craft);
			tv.setText(pattern.getCraft().getCraft());
		}
		else {
			LinearLayout.class.cast(findViewById(R.id.patterndetail_craft_layout)).setVisibility(View.GONE);
		}

		// Category
		if(pattern.getPatternCategories() != null && pattern.getPatternCategories().size() > 0) {
			String disp = "";
			for(int i = 0; i < pattern.getPatternCategories().size(); i++) {
				disp += pattern.getPatternCategories().get(i).getCategoryDisplay();
			}
			TextView.class.cast(findViewById(R.id.patterndetail_category)).setText(disp);
		}
		else {
			LinearLayout.class.cast(findViewById(R.id.patterndetail_category_layout)).setVisibility(View.GONE);
		}

		// Published Date
		if(pattern.getPublishedDtTm() != null) {
			TextView tv = (TextView) findViewById(R.id.patterndetail_published);
			tv.setText(pattern.getPublishedDtTm().toLocaleString());
		}
		else {
			LinearLayout.class.cast(findViewById(R.id.patterndetail_published_layout)).setVisibility(View.GONE);
		}

		// Yarns Suggested
		if(pattern.getPacks() != null && pattern.getPacks().size() > 0) {
			TextView.class.cast(findViewById(R.id.patterndetail_yarnssuggested)).setText(pattern.getPacks().get(0).getYarnName());
		}
		else {
			LinearLayout.class.cast(findViewById(R.id.patterndetail_yarnssuggested_layout)).setVisibility(View.GONE);
		}

		// Yarn Weight
		if(pattern.getYarnWeight() != null) {
			TextView.class.cast(findViewById(R.id.patterndetail_yarnweight)).setText(pattern.getYarnWeight().getName());
		}
		else {
			LinearLayout.class.cast(findViewById(R.id.patterndetail_yarnweight_layout)).setVisibility(View.GONE);
		}

		// Gauge
		if(pattern.getGaugeDescription() != null && !pattern.getGaugeDescription().isEmpty()) {
			TextView.class.cast(findViewById(R.id.patterndetail_gauge)).setText(pattern.getGaugeDescription());
		}
		else {
			LinearLayout.class.cast(findViewById(R.id.patterndetail_gauge_layout)).setVisibility(View.GONE);
		}

		// Needle Size
		if(pattern.getNeedles() != null && !pattern.getNeedles().isEmpty()) {
			String disp = "";
			for(int i = 0; i < pattern.getNeedles().size(); i++) {
				if(i > 0) {
					disp += "\n";
				}
				disp += pattern.getNeedles().get(i).getName();
			}
			if(!disp.isEmpty()) {
				TextView.class.cast(findViewById(R.id.patterndetail_needlesizes)).setText(disp);
			}
		}
		else {
			LinearLayout.class.cast(findViewById(R.id.patterndetail_needlesizes_layout)).setVisibility(View.GONE);
		}

		// Yardage
		if(!pattern.getYardageDescription().isEmpty()) {
			TextView.class.cast(findViewById(R.id.patterndetail_yardage)).setText(pattern.getYardageDescription());
		}
		else {
			LinearLayout.class.cast(findViewById(R.id.patterndetail_yardage_layout)).setVisibility(View.GONE);
		}

		// Sizes available
		if(!pattern.getSizesAvailable().isEmpty()) {
			TextView.class.cast(findViewById(R.id.patterndetail_sizes)).setText(pattern.getSizesAvailable());
		}
		else {
			LinearLayout.class.cast(findViewById(R.id.patterndetail_sizes_layout)).setVisibility(View.GONE);
		}

		// Difficulty
		if(pattern.getDifficultyCount() > 0) {
			TextView.class.cast(findViewById(R.id.patterndetail_difficulty)).setText(String.valueOf(pattern.getDifficultyAverage()));
		}
		else {
			LinearLayout.class.cast(findViewById(R.id.patterndetail_difficulty_layout)).setVisibility(View.GONE);
		}

		// Notes
		if(pattern.getNotesHtml() != null && !pattern.getNotesHtml().isEmpty()) {
			TextView.class.cast(findViewById(R.id.patterndetail_notes)).setText(Html.fromHtml(pattern.getNotesHtml()));
			TextView.class.cast(findViewById(R.id.patterndetail_notes)).setMovementMethod(LinkMovementMethod.getInstance());
		}
		else if(!pattern.getNotes().isEmpty()) {
			TextView.class.cast(findViewById(R.id.patterndetail_notes)).setText(pattern.getNotes());
		}
	}

	private void populatePhotos() {
		ArrayList<RavelryPhotoSet> photos = pattern.getPhotos();
		if(photos == null || photos.isEmpty()) {
			return;
		}
		ArrayList<String> urls = new ArrayList<String>();
		ViewPager vp = (ViewPager) findViewById(R.id.patterndetail_imageviewpager);
		for(int i = 0; i < photos.size(); i++) {
			RavelryPhotoSet set = photos.get(i);
			if(set.getPhoto(ProjectPhotoUrlSize.MEDIUM) != null) {
				urls.add(set.getPhoto(ProjectPhotoUrlSize.MEDIUM).getUrl());
			}
		}

		if(imageAdapter == null) {
			imageAdapter = new PatternImageAdapter(getApplicationContext());
		}
		vp.setAdapter(imageAdapter);
		imageAdapter.urls = urls;
		imageAdapter.setItemClickedListener(this);
		imageAdapter.notifyDataSetChanged();
	}

	public void setupSecondaryMenu() {

	}

	@SuppressLint("NewApi")
	protected void downloadPattern(String url) {
		Log.i("Dl Type", pattern.getDownloadLocation().getType());
		if(KnitciPreferences.isDownloadManagerAvailable(getApplicationContext())) {
			DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
			request.setDescription("Ravelry Pattern Download");
			request.setTitle(pattern.getPermalink()+".pdf");
			// in order for this if to run, you must use the android 3.2 to compile your app
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				request.allowScanningByMediaScanner();
				request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
			}
			request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, pattern.getPermalink()+".pdf");

			// get download service and enqueue file
			DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
			manager.enqueue(request);
		}
		else {
			// Need to write my own download manager i guess? 
		}
	}

}
