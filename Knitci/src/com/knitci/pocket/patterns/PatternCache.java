package com.knitci.pocket.patterns;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.knitci.pocket.ravelry.data.object.impl.RavelryPattern;

public class PatternCache {

	private HashMap<Integer, RavelryPattern> patternHolder;
	private HashMap<String, Integer> permalinkIdMap;
	
	public PatternCache() {
		patternHolder = new LinkedHashMap<Integer, RavelryPattern>();
		permalinkIdMap = new HashMap<String, Integer>();
	}
	
	public synchronized void putPattern(RavelryPattern pattern) {
		patternHolder.put(pattern.getPatternId(), pattern);
		permalinkIdMap.put(pattern.getPermalink(), pattern.getPatternId());
	}
	
	public synchronized void putPatterns(ArrayList<RavelryPattern> patterns) {
		Iterator<RavelryPattern> it = patterns.iterator();
		while(it.hasNext()) {
			RavelryPattern pattern = it.next();
			patternHolder.put(pattern.getPatternId(), pattern);
		}
	}
	
	public synchronized RavelryPattern getPattern(int patternId) {
		return patternHolder.get(patternId);
	}
	
	public synchronized RavelryPattern getPattern(String permalink) {
		if(permalinkIdMap.get(permalink) != null && permalinkIdMap.get(permalink) > 0) {
			return patternHolder.get(permalinkIdMap.get(permalink));
		}
		return null;
	}
	
}
