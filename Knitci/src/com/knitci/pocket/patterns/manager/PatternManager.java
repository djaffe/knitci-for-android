package com.knitci.pocket.patterns.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import android.content.Context;

import com.knitci.pocket.patterns.PatternCache;
import com.knitci.pocket.patterns.attribute.AttributeManager;
import com.knitci.pocket.patterns.attribute.RavelryReferenceAttribute;
import com.knitci.pocket.patterns.category.CategoryManager;
import com.knitci.pocket.patterns.category.RavelryReferenceCategory;
import com.knitci.pocket.patterns.demographic.DemographicManager;
import com.knitci.pocket.patterns.demographic.RavelryReferenceDemographic;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPattern;

public class PatternManager {

	private Context context;
	private PatternCache patternCache;
	private ArrayList<RavelryPattern> dumbPatterns;
	public int lastLoadedPage = 0;
	public int maxPage = 1;
	
	private CategoryManager categoryManager;
	ArrayList<RavelryReferenceCategory> topLevelCategories;
	HashMap<String, Boolean> checkedMap;
	
	private AttributeManager attributeManager;
	private DemographicManager demographicManager;
	
	public PatternManager(Context context) {
		this.context = context;
		this.patternCache = new PatternCache();
		this.dumbPatterns = new ArrayList<RavelryPattern>();
		categoryManager = new CategoryManager();
		demographicManager = new DemographicManager();
		attributeManager = new AttributeManager();
	}
	
	public RavelryPattern getCachedPattern(int patternId) {
		return patternCache.getPattern(patternId);
	}
	
	public RavelryPattern getCachedPattern(String permalink) {
		return patternCache.getPattern(permalink);
	}
	
	public void addPatternToCache(RavelryPattern pattern) {
		patternCache.putPattern(pattern);
	}
	
	public void setDumbPatterns(ArrayList<RavelryPattern> patterns, int maxPage) {
		lastLoadedPage = 1;
		this.maxPage = maxPage;
		this.dumbPatterns.clear();
		this.dumbPatterns = patterns;
	}
	
	public void addDumbPatterns(ArrayList<RavelryPattern> patterns, int page, int maxPage) {
		this.maxPage = maxPage;
		lastLoadedPage = page;
		dumbPatterns.addAll(patterns);
	}
	
	public ArrayList<RavelryPattern> getDumbPatterns() {
		return this.dumbPatterns;
	}
	
	public ArrayList<RavelryReferenceCategory> getReferenceCategories() {
		return categoryManager.getReferenceCategories();
	}
	
	public ArrayList<RavelryReferenceAttribute> getReferenceAttributes() {
		return attributeManager.getReferenceAttributes();
	}
	
	public ArrayList<RavelryReferenceDemographic> getReferenceDemographics() {
		return demographicManager.getReferenceDemographics();
	}
	public CategoryManager getCategoryManager() {
		return categoryManager;
	}
	
	public AttributeManager getAttributeManager() {
		return attributeManager;
	}
	
	public DemographicManager getDemographicManager() {
		return demographicManager;
	}
}
