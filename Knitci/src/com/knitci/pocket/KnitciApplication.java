package com.knitci.pocket;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.ViewGroup;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.knitci.pocket.patterns.manager.PatternManager;
import com.knitci.pocket.projects.manager.ProjectManager;
import com.knitci.pocket.queue.QueueManager;
import com.knitci.pocket.ravelry.RavelryManager;
import com.knitci.pocket.social.forum.manager.ForumManager;
import com.knitci.pocket.social.messaging.manager.MessagingManager;
import com.knitci.pocket.social.people.manager.PersonManager;
import com.knitci.pocket.stash.manager.StashManager;
import com.knitci.pocket.tools.ToolsManager;
import com.knitci.pocket.util.BitmapCache;

/**
 * A subclass of the android {@link Application} object to hold global variables. 
 * @author DJ021930
 */
public class KnitciApplication extends Application {

	private final RavelryManager ravelryManager;
	private final ProjectManager projectManager;
	private final ForumManager forumManager;
	private final MessagingManager messagingManager;
	private final PatternManager patternManager;
	private final PersonManager personManager;
	private final QueueManager queueManager;
	private final ToolsManager toolsManager;
	private final StashManager stashManager;
	private RequestQueue requestQueue;
	private ImageLoader imageLoader;
	private ImageLoader avatarLoader;
	private ImageLoader patternLoader;
	private ImageLoader projectLoader;
	
	private ViewGroup backgroundLayout;
	private Bitmap splashBitmap;
	private BitmapDrawable splashBitmapDrawable;
	
	private Bitmap darkfabricBitmap;
	private BitmapDrawable darkfabricBitmapDrawable;
	private Bitmap lightfabricBitmap;
	private BitmapDrawable lightfabricBitmapDrawble;
	
	@Override
	public void onCreate() {
		super.onCreate();
		//TestFlight.takeOff(this, "78eaf52d-d8aa-47f7-94d3-53cd433628ec");
	}
	
	/**
	 * Constructor that initializes global variables. 
	 */
	public KnitciApplication() {
		ravelryManager = new RavelryManager(this);
		projectManager = new ProjectManager();
		forumManager = new ForumManager(this);
		messagingManager = new MessagingManager(this);
		patternManager = new PatternManager(this);
		personManager = new PersonManager(this);
		queueManager = new QueueManager(this);
		toolsManager = new ToolsManager(this);
		stashManager = new StashManager(this);
		Log.e("Max Heap:", ""+ Long.toString(Runtime.getRuntime().maxMemory()));
	}
	
	public void loadSplashImage(ViewGroup layout, int id) {
		backgroundLayout = layout;
		splashBitmap = BitmapFactory.decodeStream(getResources().openRawResource(id));
		splashBitmapDrawable = new BitmapDrawable(splashBitmap);
		backgroundLayout.setBackgroundDrawable(splashBitmapDrawable);
		
		darkfabricBitmap = BitmapFactory.decodeStream(getResources().openRawResource(R.drawable.vertical_cloth_2x));
		darkfabricBitmapDrawable = new BitmapDrawable(darkfabricBitmap);
		darkfabricBitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
		lightfabricBitmap = BitmapFactory.decodeStream(getResources().openRawResource(R.drawable.light_vertical_cloth_2x));
		lightfabricBitmapDrawble = new BitmapDrawable(lightfabricBitmap);
		lightfabricBitmapDrawble.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
		
	}
	
	public void unloadSplashImage() {
		if(backgroundLayout != null) {
			backgroundLayout.setBackgroundDrawable(null);
		}
		if(splashBitmapDrawable != null) {
			splashBitmap.recycle();
		}
		splashBitmapDrawable = null;
		splashBitmap = null;
	}
	
	public BitmapDrawable getLightFabricDrawable() {
		return lightfabricBitmapDrawble;
	}
	
	public BitmapDrawable getDarkFabricDrawable() {
		return darkfabricBitmapDrawable;
	}
	
	/**
	 * Retrieves the {@link RavelryManager}. 
	 * @return {@link RavelryManager} The ravelry manager. Should never be null. 
	 */
	public RavelryManager getRavelryManager() {
		return ravelryManager;
	}
	
	public void setupVolley() {
		requestQueue = Volley.newRequestQueue(this);
		imageLoader = new ImageLoader(requestQueue, new BitmapCache(10));
		avatarLoader = new ImageLoader(requestQueue, new BitmapCache(40));
		patternLoader = new ImageLoader(requestQueue, new BitmapCache(100));
		projectLoader = new ImageLoader(requestQueue, new BitmapCache(100));
	}
	
	public RequestQueue getRequestQueue() {
		return requestQueue;
	}
	
	public ImageLoader getImageLoader() {
		return imageLoader;
	}
	
	public ImageLoader getPatternLoader() {
		return patternLoader;
	}
	
	public ImageLoader getAvatarLoader() {
		return avatarLoader;
	}
	
	public ImageLoader getProjectLoader() {
		return projectLoader;
	}
	
	public MessagingManager getMessagingManager() {
		return messagingManager;
	}

	/**
	 * @return the projectManager
	 */
	public ProjectManager getProjectManager() {
		return projectManager;
	}

	/**
	 * @return the forumManager
	 */
	public ForumManager getForumManager() {
		return forumManager;
	}
	
	public PatternManager getPatternManager() {
		return patternManager;
	}
	
	public PersonManager getPersonManager() {
		return personManager;
	}
	
	public QueueManager getQueueManager() {
		return queueManager;
	}

	/**
	 * @return the toolsManager
	 */
	public ToolsManager getToolsManager() {
		return toolsManager;
	}

	/**
	 * @return the stashManager
	 */
	public StashManager getStashManager() {
		return stashManager;
	}
	
	

	
	
}
