package object;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;

public class CheckBoxColorful extends CheckBox {

	CheckBoxColorfulListener listener;
	
	public CheckBoxColorful(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public void setListener(CheckBoxColorfulListener listener) {
		this.listener = listener;
	}
	
	@Override
	public boolean performClick() {
		super.performClick();
		return listener.performedClick();
	}

}
