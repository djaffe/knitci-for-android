package object;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import com.knitci.pocket.R;

public class DumbSpinner extends Spinner {
	
	IDumbSpinner spinnerInterface;

	public DumbSpinner(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void setHandler(IDumbSpinner handler) {
		this.spinnerInterface = handler;
	}
	
	public void setText(String display) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                R.layout.simple_spinner_item_white,
                new String[] { display });
        setAdapter(adapter);
	}
	
	@Override
	public boolean performClick() {
		return spinnerInterface.dumbSpinnerClicked(String.class.cast(this.getTag()));
	}
	
	
}
