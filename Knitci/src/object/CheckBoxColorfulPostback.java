package object;

public interface CheckBoxColorfulPostback {

	boolean checkBoxClicked(String value, boolean checked);
	
}
