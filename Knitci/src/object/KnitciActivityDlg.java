package object;

import com.actionbarsherlock.app.SherlockActivity;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.ravelry.IRavelryAsync;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;

public class KnitciActivityDlg extends SherlockActivity implements IRavelryAsync {

	protected void stealAsyncListening() {
		KnitciApplication app = (KnitciApplication) getApplication();
		app.getRavelryManager().setListener(this);
	}
	
	@Override
	public void processFinish(RavelryResponse response) {
		// TODO Auto-generated method stub
		
	}

}
