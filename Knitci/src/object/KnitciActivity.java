package object;

import imagecircle.ImageCircle;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingActivity;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.favorites.FavoriteListActivity;
import com.knitci.pocket.patterns.PatternSearchActivity;
import com.knitci.pocket.projects.ProjectListActivity;
import com.knitci.pocket.queue.QueueListActivity;
import com.knitci.pocket.ravelry.IRavelryAsync;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;
import com.knitci.pocket.social.forum.ForumListActivity;
import com.knitci.pocket.social.messaging.MessagingActivity;
import com.knitci.pocket.stash.StashListActivity;
import com.knitci.pocket.tools.ToolsActivity;

public class KnitciActivity extends SlidingActivity implements IRavelryAsync, OnClickListener{

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.knitci.pocket.ravelry.IRavelryAsync#processFinish(com.knitci.pocket.ravelry.data.object.RavelryResponse)
	 */
	@Override
	public void processFinish(RavelryResponse response) {

	}

	protected void stealAsyncListening() {
		KnitciApplication app = (KnitciApplication) getApplication();
		app.getRavelryManager().setListener(this);
	}

	protected void setupSlidingMenu() {
		setBehindContentView(R.layout.slidingmenu_layout);
		SlidingMenu menu = getSlidingMenu();
		menu.setMode(SlidingMenu.LEFT);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		menu.setFadeDegree(0.35f);
		setSlidingActionBarEnabled(true);
		menu.setMenu(R.layout.slidingmenu_layout);
		ScrollView.class.cast(findViewById(R.id.slidingmanu_scrollview)).setBackgroundDrawable(KnitciApplication.class.cast(getApplication()).getDarkFabricDrawable());
		
		LinearLayout forumsSlidingMenuItem = LinearLayout.class.cast(findViewById(R.id.slider_forums_layout));
		forumsSlidingMenuItem.setOnClickListener(this);
		
		LinearLayout messagingSlodingMenuItem = LinearLayout.class.cast(findViewById(R.id.slider_messaging_layout));
		messagingSlodingMenuItem.setOnClickListener(this);
		
		LinearLayout patternSearchSlidingMenuItem = LinearLayout.class.cast(findViewById(R.id.slider_pattern_search));
		patternSearchSlidingMenuItem.setOnClickListener(this);
		
		LinearLayout projectSlidingMenuItem = LinearLayout.class.cast(findViewById(R.id.slider_projects_layout));
		projectSlidingMenuItem.setOnClickListener(this);
		
		LinearLayout queueSlidingMenuitem = LinearLayout.class.cast(findViewById(R.id.slider_queue_layout));
		queueSlidingMenuitem.setOnClickListener(this);
		
		LinearLayout toolsSlidingMenuItem = LinearLayout.class.cast(findViewById(R.id.slider_tools_layout));
		toolsSlidingMenuItem.setOnClickListener(this);
		
		LinearLayout stashSlidingMenuItem = LinearLayout.class.cast(findViewById(R.id.slider_stash_layout));
		stashSlidingMenuItem.setOnClickListener(this);
		
		LinearLayout favoriteSlidingMenuItem = LinearLayout.class.cast(findViewById(R.id.sliding_favorites_layout));
		favoriteSlidingMenuItem.setOnClickListener(this);
		
		ImageCircle circ = (ImageCircle) findViewById(R.id.slidingmenu_avatar);
		circ.setImageResource(R.drawable.deadmau5_large);
		
	}
	
	public void onForumsSlidingMenuPressed(View view) {
		closeSlidingMenu();
		Intent intent = new Intent(this, ForumListActivity.class);
		startActivity(intent);
	}
	
	public void onMessagingSlidingMenuPressed(View view) {
		closeSlidingMenu();
		Intent intent = new Intent(this, MessagingActivity.class);
		startActivity(intent);
	}
	
	public void onPatternSlidingMenuPressed(View view) {
		closeSlidingMenu();
		Intent intent = new Intent(this, PatternSearchActivity.class);
		startActivity(intent);
	}
	
	public void onProjectSlidingMenuPressed(View view) {
		closeSlidingMenu();
		Intent intent = new Intent(this, ProjectListActivity.class);
		intent.putExtra("USERID", KnitciApplication.class.cast(getApplication()).getRavelryManager().getRavelryUser().getId());
		startActivity(intent);
	}

	public void onQueueSlidingMenuPressed(View view) {
		closeSlidingMenu();
		Intent intent = new Intent(this, QueueListActivity.class);
		startActivity(intent);
	}
	
	public void onStashSlidingMenuPressed(View view) {
		closeSlidingMenu();
		Intent intent = new Intent(this, StashListActivity.class);
		startActivity(intent);
	}
	
	public void onToolsSlidingMenuPressed(View view) {
		closeSlidingMenu();
		Intent intent = new Intent(this, ToolsActivity.class);
		startActivity(intent);
	}
	
	public void onFavoritesSlidingMenuPressed(View view) {
		closeSlidingMenu();
		Intent intent = new Intent(this, FavoriteListActivity.class);
		startActivity(intent);
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.slider_forums_layout:
			onForumsSlidingMenuPressed(v);
			break;
		case R.id.slider_messaging_layout:
			onMessagingSlidingMenuPressed(v);
			break;
		case R.id.slider_pattern_search:
			onPatternSlidingMenuPressed(v);
			break;
		case R.id.slider_projects_layout:
			onProjectSlidingMenuPressed(v);
			break;
		case R.id.slider_queue_layout:
			onQueueSlidingMenuPressed(v);
			break;
		case R.id.slider_tools_layout:
			onToolsSlidingMenuPressed(v);
			break;
		case R.id.slider_stash_layout:
			onStashSlidingMenuPressed(v);
			break;
		case R.id.sliding_favorites_layout:
			onFavoritesSlidingMenuPressed(v);
			break;
		}
		
	}
	
	protected void closeSlidingMenu() {
		if(null != getSlidingMenu() && getSlidingMenu().isMenuShowing()) {
			getSlidingMenu().toggle(false);
		}
	}

}
