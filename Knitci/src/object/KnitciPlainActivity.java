package object;

import android.view.View;
import android.view.View.OnClickListener;

import com.actionbarsherlock.app.SherlockActivity;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.ravelry.IRavelryAsync;
import com.knitci.pocket.ravelry.data.object.RavelryResponse;

public class KnitciPlainActivity extends SherlockActivity implements IRavelryAsync, OnClickListener{

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void processFinish(RavelryResponse response) {
		// TODO Auto-generated method stub
		
	}
	

	protected void stealAsyncListening() {
		KnitciApplication app = (KnitciApplication) getApplication();
		app.getRavelryManager().setListener(this);
	}

}
