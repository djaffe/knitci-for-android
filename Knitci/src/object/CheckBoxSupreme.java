package object;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.knitci.pocket.R;
import com.knitci.pocket.patterns.category.RavelryReferenceCategory;

public class CheckBoxSupreme extends LinearLayout implements CheckBoxColorfulListener {

	CheckBoxColorfulPostback checkBoxListener;
	
	public CheckBoxSupreme(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		LayoutInflater inflater = (LayoutInflater) context
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		    inflater.inflate(R.layout.checkboxsupreme, this, true);
		    
		CheckBoxColorful colorfulCheck = CheckBoxColorful.class.cast(findViewById(R.id._checkboxsupremebox));
		colorfulCheck.setListener(this);
		TextView tv = TextView.class.cast(findViewById(R.id._checkboxsupremetext));
		tv.setClickable(true);
		tv.setTag(this);
	}
	
	@Override
	public void setOnClickListener(OnClickListener l) {
		TextView tv = TextView.class.cast(findViewById(R.id._checkboxsupremetext));
		tv.setOnClickListener(l);
	}
	
	public void setCheckBoxColorfulListener(CheckBoxColorfulPostback l) {
		this.checkBoxListener = l;
	}
	
	public void hideCheckBox() {
		CheckBoxColorful colorfulCheck = CheckBoxColorful.class.cast(findViewById(R.id._checkboxsupremebox));
		colorfulCheck.setVisibility(GONE);
	}
	
	public CheckBoxSupreme(Context context) {
		this(context, null);
	}

	public void setText(String text) {
		TextView tv = TextView.class.cast(findViewById(R.id._checkboxsupremetext));
		tv.setText(text);
	}
	
	public void setSubText(String text) {
		TextView tv = TextView.class.cast(findViewById(R.id._checkboxsupremesubtext));
		tv.setText(text);
	}
	
	public void showMoreIcon(boolean show) {
		if(show) {
			TextView tv = TextView.class.cast(findViewById(R.id._checkboxsupremetext));
			tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, android.R.drawable.ic_delete, 0);
		}
		else {
			TextView tv = TextView.class.cast(findViewById(R.id._checkboxsupremetext));
			tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		}
	}

	public void setChecked(boolean checked) {
		CheckBoxColorful colorfulCheck = CheckBoxColorful.class.cast(findViewById(R.id._checkboxsupremebox));
		colorfulCheck.setChecked(checked);
	}
	
	@Override
	public boolean performedClick() {
		String value = (String) this.getTag();
		// RavelryReferenceCategory cat = RavelryReferenceCategory.class.cast(getTag());
		CheckBoxColorful colorfulCheck = CheckBoxColorful.class.cast(findViewById(R.id._checkboxsupremebox));
		// return checkBoxListener.checkBoxClicked(cat.value, colorfulCheck.isChecked());
		return checkBoxListener.checkBoxClicked(value, colorfulCheck.isChecked());
	}
	
	
}
