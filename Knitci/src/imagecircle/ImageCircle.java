package imagecircle;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.graphics.drawable.BitmapDrawable;

public class ImageCircle extends ImageView {

	public ImageCircle(Context context) {
		this(context, null);
	}
	
	public ImageCircle(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		if(w == 0 || h == 0) {
			return;
		}
		else {
			this.getLayoutParams().height = w;
		}
	}
	
	@Override
	protected void onDraw(Canvas canvas) {

	    Drawable drawable = getDrawable();

	    if (drawable == null) {
	        return;
	    }

	    if (getWidth() == 0 || getHeight() == 0) {
	        return; 
	    }
	    Bitmap b =  ((BitmapDrawable) drawable).getBitmap() ;
	    Bitmap bitmap = b.copy(Bitmap.Config.ARGB_8888, true);

	    int w = getWidth(), h = getHeight();

	    Bitmap sbmp;
	    if(bitmap.getWidth() != w || bitmap.getHeight() != w)
	        sbmp = Bitmap.createScaledBitmap(bitmap, w, w, false);
	    else
	        sbmp = bitmap;
	    Paint paint = new Paint();
	    paint.setColor(Color.parseColor("#FFFFFF"));
	    
	    canvas.drawCircle(sbmp.getWidth() / 2, sbmp.getHeight() / 2,
	            sbmp.getWidth() / 2, paint);
	    

	    Bitmap roundBitmap =  getCroppedBitmap(bitmap, w, sbmp.getWidth()/2);
	    canvas.drawBitmap(roundBitmap, 0,0, null);
	}
	
	public static Bitmap getCroppedBitmap(Bitmap bmp, int radius, int innerR) {
	    Bitmap sbmp;
	    if(bmp.getWidth() != radius || bmp.getHeight() != radius)
	        sbmp = Bitmap.createScaledBitmap(bmp, radius, radius, false);
	    else
	        sbmp = bmp;
	    Bitmap output = Bitmap.createBitmap(sbmp.getWidth(),
	            sbmp.getHeight(), Config.ARGB_8888);
	    Canvas canvas = new Canvas(output);

	    final int color = 0xffa19774;
	    final Paint paint = new Paint();
	    final Rect rect = new Rect(0, 0, (int)(sbmp.getWidth()/.95), (int)(sbmp.getHeight()/.95));

	    paint.setAntiAlias(true);
	    paint.setFilterBitmap(true);
	    paint.setDither(true);
	    canvas.drawARGB(0, 0, 0, 0);
	    paint.setColor(Color.parseColor("#FFFFFF"));
	    canvas.drawCircle(innerR, innerR, (int)(innerR*.95), paint);
	    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	    canvas.drawBitmap(sbmp, rect, rect, paint);


	    return output;
	}

}
