package edittextloader;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.knitci.pocket.KnitciApplication;
import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPattern;
import com.knitci.pocket.ravelry.data.object.impl.RavelryProject.ProjectPhotoUrlSize;

public class EditTextLoaderAdapter extends BaseAdapter {

	ArrayList<RavelryPattern> patterns;
	Context context;
	LayoutInflater inflater;
	
	public EditTextLoaderAdapter(Context context) {
		this.context = context;
	}
	
	@Override
	public int getCount() {
		return patterns.size();
	}

	@Override
	public Object getItem(int arg0) {
		return patterns.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return patterns.get(arg0).getPatternId();
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		RavelryPattern pattern = patterns.get(arg0);
		View v = arg1;
		if(v == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.edittextloaderadapteritem, arg2, false);
			ViewHolder holder = new ViewHolder();
			holder.name = TextView.class.cast(v.findViewById(R.id.edittextloaderadapter_name));
			holder.author = TextView.class.cast(v.findViewById(R.id.edittextloaderadapter_author));
			v.setTag(holder);
		}

		if(pattern != null) {
			KnitciApplication app = (KnitciApplication)context.getApplicationContext();
			ViewHolder holder = (ViewHolder) v.getTag();
			holder.name.setText(pattern.getPatternName());
			if(pattern.getPatternAuthor() != null) {
				holder.author.setText(pattern.getPatternAuthor().getName());
			}
			else if(pattern.getPatternDesigner() != null) {
				holder.author.setText(pattern.getPatternDesigner().getName());
			}
			
		}
		return v;

	}

	static class ViewHolder {
		TextView name;
		TextView author;
	}

}
