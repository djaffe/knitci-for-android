package edittextloader;

import java.util.ArrayList;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.knitci.pocket.R;
import com.knitci.pocket.ravelry.data.object.impl.RavelryPattern;

public class EditTextLoader extends LinearLayout implements OnClickListener, OnItemSelectedListener {

	EditTextLoaderListener listener;
	EditTextLoaderAdapter adapter;
	Context context;

	public EditTextLoader(Context context) {
		this(context, null);
	}

	public EditTextLoader(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;

		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.edittextloader, this, true);

		ImageView iv = (ImageView) findViewById(R.id.edittextloader_iv);
		iv.setClickable(true);
		iv.setImageResource(R.drawable.chevron_right);
		iv.setOnClickListener(this);

		EditText tv = (EditText) findViewById(R.id.edittextloader_et);

		ListView lv = (ListView) findViewById(R.id.edittextloader_listview);
		lv.setVisibility(View.GONE);
		iv.setOnClickListener(this);

	}

	public void setListener(EditTextLoaderListener listener) {
		this.listener = listener;
	}
	
	public void hideList() {
		ListView lv = (ListView) findViewById(R.id.edittextloader_listview);
		lv.setVisibility(View.GONE);
	}
	
	public void searchFinished() {
		
	}
	
	public void prepopulateEditText(String string) {
		EditText tv = (EditText) findViewById(R.id.edittextloader_et);
		tv.setText(string);
	}
	
	public void populateList(ArrayList<RavelryPattern> patterns) {
		if(adapter == null) {
			adapter = new EditTextLoaderAdapter(context);
		}
		adapter.patterns = patterns;
		adapter.notifyDataSetChanged();
		ListView lv = (ListView) findViewById(R.id.edittextloader_listview);
		lv.setAdapter(adapter);
		lv.setVisibility(View.VISIBLE);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.edittextloader_iv:
			if(listener != null) {
				EditText tv = (EditText) findViewById(R.id.edittextloader_et);
				listener.searchTriggered(tv.getText().toString());
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}


}
