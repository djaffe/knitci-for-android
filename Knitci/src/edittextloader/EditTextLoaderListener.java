package edittextloader;

public interface EditTextLoaderListener {
	void searchTriggered(String searchString);
}
